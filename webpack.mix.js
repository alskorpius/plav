let webpack = require('webpack')
let mix = require('laravel-mix')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
  .webpackConfig({
    output: {
      publicPath: '/',
      chunkFilename: `js/chunks/[name].js`
    },
    plugins: [
      new webpack.DefinePlugin({
        ENV_IS_PRODUCTION: JSON.stringify(mix.inProduction())
      })
    ]
  })
  .copyDirectory('resources/assets/fonts', 'public/fonts')
  .copyDirectory('resources/assets/images', 'public/images')
  .js('resources/assets/js/app.js', 'public/js')
  .extract(['jquery', 'vue', 'noty'])
  .sass('resources/assets/sass/app.scss', 'public/css')
  .sass('resources/assets/sass/vendor.scss', 'public/css')
  .browserSync('plav.dev')

if (!mix.inProduction()) {
  mix.sourceMaps()
}

if (mix.inProduction()) {
  mix.version()
}
