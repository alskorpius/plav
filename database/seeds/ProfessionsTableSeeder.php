<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ProfessionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $professions = [
            [
                'name' => '1-системный механик',
            ],
            [
                'name' => '1 помощник капитана',
            ],
            [
                'name' => '2 помощник капитана',
            ],
            [
                'name' => '3 помощник капитана',
            ],
            [
                'name' => 'инженер-инспектор',
            ],
            [
                'name' => 'инженер-гидролог',
            ],
        ];
        foreach ($professions AS $profession) {
            $model = new \App\Models\Profession();
            $model->name = $profession['name'];
            $model->slug = str_slug($profession['name']);
            $model->save();
        }
    }
}
