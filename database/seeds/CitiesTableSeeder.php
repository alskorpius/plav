<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            [
                'name' => 'Алешки',
            ],
            [
                'name' => 'Херсон',
            ],
        ];
        foreach ($cities AS $city) {
            $model = new \App\Models\City();
            $model->name = $city['name'];
            $model->save();
        }
    }
}
