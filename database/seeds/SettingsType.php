<?php

use Illuminate\Database\Seeder;

class SettingsType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $model = new \App\Models\SettingsType();
        $model->name = 'Однострочное текстовое поле';
        $model->alias = 'input';
        $model->save();

        $model2 = new \App\Models\SettingsType();
        $model2->name = 'Текстовое поле';
        $model2->alias = 'text';
        $model2->save();

        $model3 = new \App\Models\SettingsType();
        $model3->name = 'Список';
        $model3->alias = 'select';
        $model3->save();

        $model4 = new \App\Models\SettingsType();
        $model4->name = 'Текстовый редактор';
        $model4->alias = 'tiny';
        $model4->save();

        $model5 = new \App\Models\SettingsType();
        $model5->name = 'Дата';
        $model5->alias = 'date';
        $model5->save();

        $model6 = new \App\Models\SettingsType();
        $model6->name = 'Пароль';
        $model6->alias = 'password';
        $model6->save();

        $model7 = new \App\Models\SettingsType();
        $model7->name = 'Радио кнопка';
        $model7->alias = 'radio';
        $model7->save();

        $model8 = new \App\Models\SettingsType();
        $model8->name = 'Файл';
        $model8->alias = 'file';
        $model8->save();
    }
}
