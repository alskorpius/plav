<?php

use Illuminate\Database\Seeder;

class SettingsGroup extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $model = new \App\Models\SettingsGroup();
        $model->name = 'Базовые';
        $model->alias = 'basic';
        $model->position = 1;
        $model->save();

        $model2 = new \App\Models\SettingsGroup();
        $model2->name = 'Статика';
        $model2->alias = 'static';
        $model2->position = 2;
        $model2->save();

        $model6 = new \App\Models\SettingsGroup();
        $model6->name = 'Контактные данные';
        $model6->alias = 'contacts';
        $model6->position = 3;
        $model6->save();
    }
}
