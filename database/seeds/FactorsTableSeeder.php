<?php

use Illuminate\Database\Seeder;

class FactorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $factors = [
            [
                'name' => '3-347 Комиссия Плавсостава',
            ],
            [
                'name' => '3-348',
            ],
            [
                'name' => '3-349',
            ],
        ];
        foreach ($factors AS $factor) {
            $model = new \App\Models\Card\Factor();
            $model->name = $factor['name'];
            $model->save();
        }
    }
}
