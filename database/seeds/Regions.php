<?php

use Illuminate\Database\Seeder;

class Regions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = [
            [
                'name' => 'Кахетия',
                'country_id' => '1',
            ],
            [
                'name' => 'Картли',
                'country_id' => '1',
            ],
            [
                'name' => 'Имерети',
                'country_id' => '1',
            ],
            [
                'name' => 'Рача-Лечхуми',
                'country_id' => '1',
            ],
        ];
        foreach ($regions AS $region) {
            $model = new \App\Models\Providers\ShopRegion();
            $model->name = $region['name'];
            $model->country_id = $region['country_id'];
            $model->save();
        }
    }
}
