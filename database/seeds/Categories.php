<?php

use Illuminate\Database\Seeder;

class Categories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 5; $i++) {
            $categories = new \App\Models\ArticlesCategory();
            $categories->name = $faker->sentence(rand(2, 10));
            $categories->position = $i;
            $categories->slug = $faker->slug(rand(2, 10));
            $categories->status = !rand(0, 1);
            $categories->keywords = $faker->sentence(10);
            $categories->description = $faker->sentence(25);
            $categories->h1 = $faker->sentence();
            $categories->title = $faker->sentence();
            $categories->save();
        }
    }

}
