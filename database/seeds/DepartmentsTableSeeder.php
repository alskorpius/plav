<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = [
            [
                'code' => '1',
                'name' => 'Приватна фирма',
            ],
            [
                'code' => '11',
                'name' => 'Марлоу',
            ],
        ];
        foreach ($departments AS $department) {
            $model = new \App\Models\Department();
            $model->name = $department['name'];
            $model->code = $department['code'];
            $model->save();
        }
    }
}
