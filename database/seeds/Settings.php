<?php

use Illuminate\Database\Seeder;

class Settings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            [
                'name' => 'Кол-во выводимых строк на всех страницах со списками',
                'value' => '10',
                'key' => 'rows-count',
                'status' => '1',
                'group' => 'basic',
                'data' => '[]',
                'type' => 'input',
                'position' => 0,
                'validation' => [
                    'value' => ['required', 'numeric', 'min:1', 'max:100'],
                ],
            ], // value
            [
                'name' => 'Кол-во выводимых строк на страницах списков Блога',
                'value' => '10',
                'key' => 'articles-per-page',
                'status' => '1',
                'group' => 'basic',
                'data' => '[]',
                'type' => 'input',
                'position' => 3,
                'validation' => [
                    'value' => ['required', 'numeric', 'min:1', 'max:100'],
                ],
            ], // value
            [
                'name' => 'Copyright',
                'value' => 'Qwerty qwerty',
                'key' => 'copyright',
                'status' => '1',
                'group' => 'static',
                'data' => '[]',
                'type' => 'input',
                'position' => 9,
                'validation' => [
                    'value' => ['required', 'max:191'],
                ],
            ], // value
            [
                'name' => 'Email',
                'value' => 'admin@test.com',
                'key' => 'email',
                'status' => '1',
                'group' => 'contacts',
                'data' => '',
                'type' => 'input',
                'position' => 2,
                'validation' => [
                    'value' => ['required'],
                ],
            ], // value
            [
                'name' => 'Часы работы в будние',
                'value' => 'С 9:00 До 20:00',
                'key' => 'workDays',
                'status' => '1',
                'group' => 'contacts',
                'data' => '',
                'type' => 'input',
                'position' => 4,
                'validation' => [
                    'value' => ['required'],
                ],
            ], // value
            [
                'name' => 'Часы работы в субботу',
                'value' => 'С 10:00 До 17:00',
                'key' => 'saturday',
                'status' => '1',
                'group' => 'contacts',
                'data' => '',
                'type' => 'input',
                'position' => 5,
                'validation' => [
                    'value' => ['required'],
                ],
            ], // value
            [
                'name' => 'Часы работы в воскресенье',
                'value' => 'ОБСУДИТЬ',
                'key' => 'sunday',
                'status' => '1',
                'group' => 'contacts',
                'data' => '',
                'type' => 'input',
                'position' => 6,
                'validation' => [
                    'value' => ['required'],
                ],
            ], // value
        ];
        $i = 0;
        foreach ($settings AS $setting) {
            $model = new \App\Models\Settings();
            $model->name = $setting['name'];
            $model->value = $setting['value'];
            $model->key = str_replace('.', '_', $setting['key']);
            $model->status = $setting['status'];
            $model->group = $setting['group'];
            $model->data = $setting['data'];
            $model->type = $setting['type'];
            $model->position = $i;
            $model->validation = $setting['validation'];
            $model->save();
            $i++;
        }
    }
}
