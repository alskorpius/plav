<?php

use Illuminate\Database\Seeder;

class Lines extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lines = [
            [
                'name' => 'День Рождения',
                'slug' => 'den-rozhdeniya',
                'status' => '1',
                'h1' => 'День Рождения',
                'title' => 'День Рождения',
                'keywords' => 'День Рождения',
                'description' => 'День Рождения',
                'content' => 'День Рождения',
            ],
			[
				'name' => 'VIP',
				'slug' => 'vip',
				'status' => '1',
				'h1' => 'VIP',
				'title' => 'VIP',
				'keywords' => 'VIP',
				'description' => 'VIP',
				'content' => 'VIP',
			],
			[
				'name' => 'Новый Год',
				'slug' => 'novui-god',
				'status' => '1',
				'h1' => 'Новый Год',
				'title' => 'Новый Год',
				'keywords' => 'Новый Год',
				'description' => 'Новый Год',
				'content' => 'Новый Год',
			],
			[
				'name' => 'Рождество',
				'slug' => 'rozhdestvo',
				'status' => '1',
				'h1' => 'Рождество',
				'title' => 'Рождество',
				'keywords' => 'Рождество',
				'description' => 'Рождество',
				'content' => 'Рождество',
			],
			[
				'name' => 'Вечеринка',
				'slug' => 'vecherinka',
				'status' => '1',
				'h1' => 'Вечеринка',
				'title' => 'Вечеринка',
				'keywords' => 'Вечеринка',
				'description' => 'Вечеринка',
				'content' => 'Вечеринка',
			],
			[
				'name' => 'Свадьба',
				'slug' => 'svadba',
				'status' => '1',
				'h1' => 'Свадьба',
				'title' => 'Свадьба',
				'keywords' => 'Свадьба',
				'description' => 'Свадьба',
				'content' => 'Свадьба',
			],
			[
				'name' => 'Свадьба',
				'slug' => 'svadba',
				'status' => '1',
				'h1' => 'Свадьба',
				'title' => 'Свадьба',
				'keywords' => 'Свадьба',
				'description' => 'Свадьба',
				'content' => 'Свадьба',
			],
			[
				'name' => '14 февраля (День влюбленных)',
				'slug' => '14-fevralia-den-vlyblenuh',
				'status' => '1',
				'h1' => '14 февраля (День влюбленных)',
				'title' => '14 февраля (День влюбленных)',
				'keywords' => '14 февраля (День влюбленных)',
				'description' => '14 февраля (День влюбленных)',
				'content' => '14 февраля (День влюбленных)',
			],
			[
				'name' => 'Для мужчин',
				'slug' => 'dlya-muzhchin',
				'status' => '1',
				'h1' => 'Для мужчин',
				'title' => 'Для мужчин',
				'keywords' => 'Для мужчин',
				'description' => 'Для мужчин',
				'content' => 'Для мужчин',
			],
			[
				'name' => 'Пикник',
				'slug' => 'piknik',
				'status' => '1',
				'h1' => 'Пикник',
				'title' => 'Пикник',
				'keywords' => 'Пикник',
				'description' => 'Пикник',
				'content' => 'Пикник',
			],
			[
				'name' => 'Пасха',
				'slug' => 'pasha',
				'status' => '1',
				'h1' => 'Пасха',
				'title' => 'Пасха',
				'keywords' => 'Пасха',
				'description' => 'Пасха',
				'content' => 'Пасха',
			],
			[
				'name' => 'Выпускной',
				'slug' => 'vupysknoi',
				'status' => '1',
				'h1' => 'Выпускной',
				'title' => 'Выпускной',
				'keywords' => 'Выпускной',
				'description' => 'Выпускной',
				'content' => 'Выпускной',
			],
        ];
        foreach ($lines AS $line) {
            $model = new \App\Models\Catalog\ProductsLine();
            $model->name = $line['name'];
            $model->slug = $line['slug'];
            $model->status = $line['status'];
            $model->h1 = $line['h1'];
            $model->title = $line['title'];
            $model->keywords = $line['keywords'];
            $model->description = $line['description'];
            $model->content = $line['content'];
            $model->save();
        }
    }
}
