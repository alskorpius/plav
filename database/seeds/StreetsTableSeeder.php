<?php

use Illuminate\Database\Seeder;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class StreetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $streets = [];

        $reader = new Xlsx();
        $file = 'public_admin/streets.xlsx';

        // Читаем файл и записываем информацию в переменную
        $spreadsheet = $reader->load($file);

        // Так можно достать объект Cells, имеющий доступ к содержимому ячеек
        $cells = $spreadsheet->getActiveSheet()->getCellCollection();

        // Далее перебираем все заполненные строки
        for ($row = 1; $row <= $cells->getHighestRow(); $row++){
            for ($col = 'A'; $col <= 'A'; $col++) {
                // Так можно получить значение конкретной ячейки
                $streets[] = ['name' => $cells->get($col.$row)->getValue()];
            }
        }

        foreach ($streets AS $street) {
            $model = new \App\Models\Street();
            $model->name = $street['name'];
            $model->save();
        }
    }
}
