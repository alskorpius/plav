<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersRolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        $this->call(CitiesTableSeeder::class);
        $this->call(StreetsTableSeeder::class);
        $this->call(DepartmentsTableSeeder::class);
        $this->call(ProfessionsTableSeeder::class);
        $this->call(FactorsTableSeeder::class);

        $this->call(CardsTableSeeder::class);
        //$this->call(CardsAddressesTableSeeder::class);


        $this->call(SettingsType::class);
        $this->call(SettingsGroup::class);
        $this->call(Settings::class);

        $this->call(AdminPages::class);
        $this->call(PagesRoles::class);
    }
}
