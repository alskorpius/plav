<?php

use Illuminate\Database\Seeder;

class AdminPages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = [
            [
                'name' => 'Контентные страницы',
            ],
            [
                'name' => 'Меню',
            ],
            [
                'name' => 'Новости',
            ],
            [
                'name' => 'Статьи',
            ],
            [
                'name' => 'Мероприятия',
            ],
            [
                'name' => 'Опросы',
            ],
            [
                'name' => 'Магазин',
            ],
            [
                'name' => 'Заказы',
            ],
            [
                'name' => 'Список пользователей',
            ],
            [
                'name' => 'Черный список',
            ],
            [
                'name' => 'Обратная связь',
            ],
            [
                'name' => 'Шаблоны писем',
            ],
            [
                'name' => 'Настройки сайта',
            ],
            [
                'name' => 'SEO',
            ],
            [
                'name' => 'Статистика',
            ],
        ];
        foreach ($pages AS $page) {
            $model = new \App\Models\AdminPage();
            $model->name = $page['name'];
            $model->save();
        }
    }
}
