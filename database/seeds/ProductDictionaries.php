<?php

use Illuminate\Database\Seeder;

class ProductDictionaries extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dictionaries = [
            [
                'name' => 'Цвет',
                'slug' => 'tsvet',
            ],
            [
                'name' => 'Сладость',
                'slug' => 'sladost',
            ],
            [
                'name' => 'Тип',
                'slug' => 'tip',
            ],
            [
                'name' => 'Продукт',
                'slug' => 'vid',
            ],
            [
                'name' => 'Процент алкоголя',
                'slug' => 'krepost',
            ],
            [
                'name' => 'Сорт винограда',
                'slug' => 'sort-vinograda',
            ],
            [
                'name' => 'Количество сахара',
                'slug' => 'kolichestvo-sahara',
            ],
        ];
        foreach ($dictionaries AS $item) {
            $model = new \App\Models\Catalog\ShopProductsDictionary();
            $model->name = $item['name'];
            $model->slug = $item['slug'];
            $model->status = 1;
            $model->save();
        }
    }
}
