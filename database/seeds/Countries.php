<?php

use Illuminate\Database\Seeder;

class Countries extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
            [
                'name' => 'Грузия',
                'slug' => 'gruzia',
            ],
            [
                'name' => 'Словения',
				'slug' => 'slovenia',
			],
            [
                'name' => 'Италия',
				'slug' => 'italia',
			],
            [
                'name' => 'Франция',
				'slug' => 'francia',
			],
            [
                'name' => 'Германия',
				'slug' => 'germania',
			],
        ];
        foreach ($countries AS $country) {
            $model = new \App\Models\Providers\ShopCountry();
            $model->name = $country['name'];
            $model->save();
        }
    }
}
