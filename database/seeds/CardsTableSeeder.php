<?php

use Faker\Factory as Fake;
use Illuminate\Database\Seeder;

class CardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Fake::create('uk_UA');

        $card = new \App\Models\Card\Card();
        $card->gender = rand(0, 1);
        if(!$card->gender) {
            $card->first_name = $faker->firstNameFemale;
            $card->last_name = $faker->lastName;
            $card->middle_name = $faker->firstNameFemale;
        } else {
            $card->first_name = $faker->firstNameMale;
            $card->last_name = $faker->lastName;
            $card->middle_name = $faker->middleNameMale;
        }

        $card->birthday = $faker->date('Y-m-d');

        $card->married = rand(0, 1);
        $card->fisherman = rand(0, 1);
        $card->payment = rand(0, 2);

        $card->department_id = 1;
        $card->profession_id = 1;
        $card->factor_id = rand(1, 3);

        $card->is_registration_address = 1;
        $card->status = 1;
        $card->last_visit = $faker->date('Y-m-d');
        $card->save();
    }
}
