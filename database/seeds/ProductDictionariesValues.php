<?php

use Illuminate\Database\Seeder;

class ProductDictionariesValues extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = [
            [
                'name' => 'Вино',
                'slug' => 'vino',
                'dictionary_id' => '4',
            ],
            [
                'name' => 'Коньяк',
                'slug' => 'konyak',
                'dictionary_id' => '4',
            ],
            [
                'name' => '10',
                'slug' => '10',
                'dictionary_id' => '5',
            ],
            [
                'name' => '8',
                'slug' => '8',
                'dictionary_id' => '5',
            ],
            [
                'name' => '12',
                'slug' => '12',
                'dictionary_id' => '5',
            ],
			[
				'name' => '45',
				'slug' => '45',
				'dictionary_id' => '5',
			],
			[
				'name' => '30',
				'slug' => '30',
				'dictionary_id' => '5',
			],
            [
                'name' => 'Сладкое',
                'slug' => 'sladkoe',
                'dictionary_id' => '2',
            ],
            [
                'name' => 'Полусладкое',
                'slug' => 'polusladkoe',
                'dictionary_id' => '2',
            ],
			[
				'name' => 'Полусухое',
				'slug' => 'polusukhoe',
				'dictionary_id' => '2',
			],
            [
                'name' => 'Сухое',
                'slug' => 'sukhoe',
                'dictionary_id' => '2',
            ],
            [
                'name' => 'Ркацители',
                'slug' => 'rkatsiteli',
                'dictionary_id' => '6',
            ],
            [
                'name' => 'Саперави',
                'slug' => 'saperavi',
                'dictionary_id' => '6',
            ],
			[
				'name' => 'Киси',
				'slug' => 'kisi',
				'dictionary_id' => '6',
			],
			[
				'name' => 'Цинандали',
				'slug' => 'tsinandali',
				'dictionary_id' => '6',
			],
			[
				'name' => 'Мцване',
				'slug' => 'mtsvane',
				'dictionary_id' => '6',
			],
			[
				'name' => 'Каберне Совиньон',
				'slug' => 'kabarne-sovinion',
				'dictionary_id' => '6',
			],
			[
				'name' => 'Оцханури Сапере',
				'slug' => 'otshanuri-saperne',
				'dictionary_id' => '6',
			],
			[
				'name' => 'Хихви',
				'slug' => 'hihvi',
				'dictionary_id' => '6',
			],
			[
				'name' => 'Риболла Джалла',
				'slug' => 'ribola-dzhala',
				'dictionary_id' => '6',
			],
			[
				'name' => 'Фриулано',
				'slug' => 'friunalo',
				'dictionary_id' => '6',
			],
			[
				'name' => 'Совиньон Зеленый',
				'slug' => 'sovinion-zelenui',
				'dictionary_id' => '6',
			],
			[
				'name' => 'Пино Гри',
				'slug' => 'pino-gri',
				'dictionary_id' => '6',
			],
			[
				'name' => 'Пино Блан',
				'slug' => 'pino-blan',
				'dictionary_id' => '6',
			],
			[
				'name' => 'Совиньон Блан',
				'slug' => 'sovinion-blan',
				'dictionary_id' => '6',
			],
			[
				'name' => 'Мерло',
				'slug' => 'merlo',
				'dictionary_id' => '6',
			],
			[
				'name' => 'Каберне Фран',
				'slug' => 'kaberne-fran',
				'dictionary_id' => '6',
			],
			[
				'name' => 'Пти Вердо',
				'slug' => 'pti-verdo',
				'dictionary_id' => '6',
			],
			[
				'name' => 'Александроули',
				'slug' => 'alexandrouli',
				'dictionary_id' => '6',
			],
			[
				'name' => 'Муджуретули',
				'slug' => 'mudzhuretuli',
				'dictionary_id' => '6',
			],
			[
				'name' => 'Оджалеши',
				'slug' => 'odzhaleshi',
				'dictionary_id' => '6',
			],
            [
                'name' => 'Игристое',
                'slug' => 'igristoe',
                'dictionary_id' => '3',
            ],
            [
                'name' => 'Тихое',
                'slug' => 'tikhoe',
                'dictionary_id' => '3',
            ],
			[
				'name' => 'Крепленое',
				'slug' => 'kreplenoe',
				'dictionary_id' => '3',
			],
            [
                'name' => 'Белое',
                'slug' => 'beloe',
                'dictionary_id' => '1',
				'option' => '#efe5e5',
            ],
            [
                'name' => 'Красное',
                'slug' => 'krasnoe',
                'dictionary_id' => '1',
				'option' => '#ff0000',
            ],
            [
                'name' => 'Розовое',
                'slug' => 'rozovoe',
                'dictionary_id' => '1',
				'option' => '#f900e3',
            ],
			[
				'name' => 'Янтарное',
				'slug' => 'yantarnoe',
				'dictionary_id' => '1',
				'option' => '#f95700',
			],
			[
				'name' => '5гр',
				'slug' => '5gr',
				'dictionary_id' => '7',
			],
			[
				'name' => '15гр',
				'slug' => '15gr',
				'dictionary_id' => '7',
			],
			[
				'name' => '25гр',
				'slug' => '25gr',
				'dictionary_id' => '7',
			],
			[
				'name' => '35гр',
				'slug' => '35gr',
				'dictionary_id' => '7',
			],
        ];
        foreach ($values AS $item) {
            $model = new \App\Models\Catalog\ShopProductsDictionariesValue();
            $model->name = $item['name'];
            $model->slug = $item['slug'];
            $model->dictionary_id = $item['dictionary_id'];
            $model->option = isset($item['option']) ? $item['option'] : null;
            $model->save();
        }
    }
}
