<?php

use Illuminate\Database\Seeder;

class Providers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $providers = [
            [
                'name' => 'Dakishvilli Family Winery',
                'description' => 'Dakishvilli Family Winery',
                'country_id' => '1',
            ],
            [
                'name' => 'Maranuli',
                'description' => 'Maranuli',
                'country_id' => '1',
            ],
            [
                'name' => 'Vine Ponto',
                'description' => 'Vine Ponto',
                'country_id' => '1',
            ],
			[
				'name' => 'Mosmieri',
				'description' => 'Mosmieri',
				'country_id' => '1',
			],
			[
				'name' => 'Telavi Old Cellar',
				'description' => 'Telavi Old Cellar',
				'country_id' => '1',
			],
			[
				'name' => 'Danieli',
				'description' => 'Danieli',
				'country_id' => '1',
			],
			[
				'name' => 'Kabaj',
				'description' => 'Kabaj',
				'country_id' => '1',
			],
			[
				'name' => 'Kindzmarauli Marani',
				'description' => 'Kindzmarauli Marani',
				'country_id' => '1',
			],
        ];
        foreach ($providers AS $provider) {
            $model = new \App\Models\Providers\ShopProvider();
            $model->name = $provider['name'];
            $model->description = $provider['description'];
            $model->country_id = $provider['country_id'];
            $model->save();
        }
    }
}
