<?php

use Illuminate\Database\Seeder;

class ProductsPresentation extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker\Factory::create('ru_RU');

		for ($i = 0; $i < 200; $i++) {
			$model = new \App\Models\Catalog\ProductsPresentation();
			$model->content = $faker->sentence(100);
			$model->color = $faker->sentence(20);
			$model->aroma = $faker->sentence(20);
			$model->taste = $faker->sentence(20);
			$model->combination = $faker->sentence(20);
			$model->product_id = $i;
			$model->save();
		}
    }
}
