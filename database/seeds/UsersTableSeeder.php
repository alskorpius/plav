<?php

use Illuminate\Database\Seeder;
use Faker\Factory AS Fake;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Fake::create();
		/*$filepath = 'public/uploads/images/user/original';

		if (!File::exists($filepath)) {
			File::makeDirectory($filepath, null, true);
		}*/

        $user = new \App\Models\User();
        $user->first_name = $faker->firstName;
        $user->second_name = $faker->lastName;
        $user->middle_name = $faker->firstNameMale;
        $user->phone = $faker->phoneNumber;
        $user->address = $faker->address;
        $user->subscription = $faker->randomElement([0, 1]);
        $user->email = 'admin@test.com';
        $user->role_alias = 'administrator';
        $user->password = bcrypt('admin');
        $user->remember_token = str_random(10);
        $user->save();

        $user1 = new \App\Models\User();
        $user1->first_name = $faker->firstName;
        $user1->second_name = $faker->lastName;
        $user1->middle_name = $faker->firstNameMale;
        $user1->phone = $faker->phoneNumber;
        $user1->address = $faker->address;
        $user1->subscription = $faker->randomElement([0, 1]);
        $user1->email = 'user@test.com';
        $user1->role_alias = 'manager';
        $user1->password = bcrypt('123456');
        $user1->remember_token = str_random(10);
        $user1->save();

        for ($i = 0; $i < 5; $i++) {
            $user2 = new \App\Models\User();
            $user2->first_name = $faker->firstName;
            $user2->second_name = $faker->lastName;
            $user2->middle_name = $faker->firstNameMale;
            $user2->phone = $faker->phoneNumber;
            $user2->address = $faker->address;
            $user2->subscription = $faker->randomElement([0, 1]);
            $user2->email = $faker->email;
            $user2->role_alias = 'user';
            $user2->password = bcrypt('123456');
            $user2->remember_token = str_random(10);
            $user2->save();
        }

        for ($i = 0; $i < 5; $i++) {
            $user3 = new \App\Models\User();
            $user3->first_name = $faker->firstName;
            $user3->second_name = $faker->lastName;
            $user3->middle_name = $faker->firstNameMale;
            $user3->phone = $faker->phoneNumber;
            $user3->address = $faker->address;
			/*if (env('FAKER_LOCAL') == false) {
				$user3->avatar = $faker->image($filepath, 1920, 1080, 'food', false);
			} else {
				$user3->avatar = $faker->file('resources/faker_images', $filepath, false);
			}*/
            $user3->subscription = $faker->randomElement([0, 1]);
            $user3->email = $faker->email;
            $user3->role_alias = 'manager';
            $user3->password = bcrypt('123456');
            $user3->remember_token = str_random(10);
            $user3->save();
        }
    }
}
