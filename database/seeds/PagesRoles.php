<?php

use Illuminate\Database\Seeder;

class PagesRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'page_id' => 1,
                'role_id' => 3,
                'status' => 1,
            ],
            [
                'page_id' => 2,
                'role_id' => 3,
                'status' => 1,
            ],
            [
                'page_id' => 3,
                'role_id' => 3,
                'status' => 1,
            ],
            [
                'page_id' => 4,
                'role_id' => 3,
                'status' => 1,
            ],
            [
                'page_id' => 5,
                'role_id' => 3,
                'status' => 1,
            ],
            [
                'page_id' => 6,
                'role_id' => 3,
                'status' => 1,
            ],
            [
                'page_id' => 7,
                'role_id' => 3,
                'status' => 1,
            ],
            [
                'page_id' => 8,
                'role_id' => 3,
                'status' => 1,
            ],
            [
                'page_id' => 9,
                'role_id' => 3,
                'status' => 1,
            ],
            [
                'page_id' => 10,
                'role_id' => 3,
                'status' => 1,
            ],
            [
                'page_id' => 11,
                'role_id' => 3,
                'status' => 1,
            ],
            [
                'page_id' => 12,
                'role_id' => 3,
                'status' => 1,
            ],
            [
                'page_id' => 13,
                'role_id' => 3,
                'status' => 1,
            ],
            [
                'page_id' => 14,
                'role_id' => 3,
                'status' => 1,
            ],
            [
                'page_id' => 15,
                'role_id' => 3,
                'status' => 1,
            ],
        ];
        foreach ($roles AS $role) {
            $model = new \App\Models\PagesRole();
            $model->page_id = $role['page_id'];
            $model->role_id = $role['role_id'];
            $model->status = $role['status'];
            $model->save();
        }
    }
}
