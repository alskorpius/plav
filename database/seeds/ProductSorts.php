<?php

use Illuminate\Database\Seeder;

class ProductSorts extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
		for ($i = 0; $i < 400; $i++) {
			$model = new \App\Models\Catalog\ProductSort();
			$model->product_id = rand(1, 200);
			$model->sort_id = rand(12, 31);
			$model->save();

		}
	}
}
