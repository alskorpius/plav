<?php

use Illuminate\Database\Seeder;

class ProductsPrices extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');

        $volumes = [250, 500, 700, 750, 1000, 1500, 2000];
        for ($i = 0; $i < 800; $i++) {
            $model = new \App\Models\Catalog\ProductsPrice();
            $model->volume = $volumes[rand(0, 6)];
            $model->weight = $faker->randomFloat(2, 1, 3);
            $model->year = $faker->year();
            $model->price = rand(200, 4000);
            $model->product_id = rand(1, 200);
            $model->artikul = $faker->postcode;
            $model->sale = ($i % 5 == 0) ? rand(5, 80) : null;
            $model->save();
        }
    }
}
