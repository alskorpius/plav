<?php

use Illuminate\Database\Seeder;

class MailTemplates extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $templates = [
            [
                'alias' => 'registration',
                'name' => 'Регистрация нового пользователя',
                'subject' => 'registration subject',
                'content' => 'registration content',
                'data' => [
                    'password' => 'Пароль',
                    'email' => 'Email',
                ],
            ], // password, email
            [
                'alias' => 'callback_admin',
                'name' => 'Письмо администратору о просьбе перезвонить',
                'subject' => 'callback_admin subject',
                'content' => 'callback_admin content',
                'data' => [
                    'name' => 'Имя',
                    'phone' => 'Номер телефона',
                ],
            ], // name, phone
            [
                'alias' => 'contact_admin',
                'name' => 'Письмо администратору о новом сообщении',
                'subject' => 'contact_admin subject',
                'content' => 'contact_admin content',
                'data' => [
                    'name' => 'Имя',
                    'email' => 'Email',
                    'text' => 'Текст сообщения',
                    'phone' => 'Номер телефона',
                ],
            ], // name, email, text, phone
            [
                'alias' => 'review_admin',
                'name' => 'Письмо администратору о новом отзыве',
                'subject' => 'review_admin subject',
                'content' => 'review_admin content',
                'data' => [
                    'email' => 'Email',
                    'review' => 'Отзыв',
                ],
            ], // email, review
            [
                'alias' => 'new_order_admin',
                'name' => 'Письмо администратору о новом заказе',
                'subject' => 'new_order_admin subject',
                'content' => 'new_order_admin content',
                'data' => [
                    'id' => 'ID заказа',
                    'amount' => 'Стоимость заказа',
                ],
            ], // id, amount
            [
                'alias' => 'closed_order_admin',
                'name' => 'Письмо администратору об отменном пользователем заказе',
                'subject' => 'closed_order_admin subject',
                'content' => 'closed_order_admin content',
                'data' => [
                    'id' => 'ID заказа',
                    'amount' => 'Стоимость заказа',
                ],
            ], // id, amount
            [
                'alias' => 'order_to_provider',
                'name' => 'Письмо производителю с содержанием заказа',
                'subject' => 'order_to_provider subject',
                'content' => 'order_to_provider content',
                'data' => [
                    'count' => 'Количество товара',
                    'product_name' => 'Название продукта',
                    'product_link' => 'Ссылка на продукт',
                    'comment' => 'Комментарий к товару',
                    'breast' => 'Объем груди',
                    'waist' => 'Объем талии',
                    'thighs' => 'Объем бедер',
                    'provider_name' => 'Название производителя',
                ],
            ], // count, product_name, product_link, comment, breast, waist, thighs, provider_name
            [
                'alias' => 'forgot_password',
                'name' => 'Письмо пользователю с новым паролем',
                'subject' => 'forgot_password subject',
                'content' => 'forgot_password content',
                'data' => [
                    'email' => 'Email',
                    'password' => 'Пароль',
                ],
            ], // email, password
            [
                'alias' => 'contact',
                'name' => 'Письмо пользователю от администратора в раздел переписки',
                'subject' => 'contact subject',
                'content' => 'contact content',
                'data' => [
                    'subject' => 'Тема диалога',
                    'text' => 'Текст сообщения',
                ],
            ], // subject, text
            [
                'alias' => 'resend_order',
                'name' => 'Письмо пользователю от администратора с содержанием заказа',
                'subject' => 'resend_order subject',
                'content' => 'resend_order content',
                'data' => [
                    'id' => 'ID заказа',
                    'amount' => 'Стоимость заказа',
                ],
            ], // id, amount
        ];
        foreach ($templates AS $template) {
            $model = new \App\Models\MailTemplate();
            $model->alias = $template['alias'];
            $model->name = $template['name'];
            $model->subject = $template['subject'];
            $model->content = $template['content'];
            $model->data = $template['data'];
            $model->save();
        }
    }
}
