<?php

use Illuminate\Database\Seeder;

class Notifications extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = [
            [
                'type' => \App\Models\Notification::TYPE_REGISTRATION,
                'data' => [
                    'id' => 2,
                ],
            ],
            [
                'type' => \App\Models\Notification::TYPE_NEW_ORDER,
                'data' => [
                    'id' => 12,
                    'amount' => 2001,
                ],
            ],
            [
                'type' => \App\Models\Notification::TYPE_REGISTRATION,
                'data' => [
                    'id' => 1,
                ],
            ],
            [
                'type' => \App\Models\Notification::TYPE_NEW_ORDER,
                'data' => [
                    'id' => 13,
                    'amount' => 1200,
                ],
            ],
            [
                'type' => \App\Models\Notification::TYPE_REGISTRATION,
                'data' => [
                    'id' => 3,
                ],
            ],
            [
                'type' => \App\Models\Notification::TYPE_NEW_ORDER,
                'data' => [
                    'id' => 11,
                    'amount' => 340,
                ],
            ],
        ];
        foreach ($pages AS $page) {
            $model = new \App\Models\Notification();
            $model->type = $page['type'];
            $model->data = $page['data'];
            $model->save();
        }
    }
}
