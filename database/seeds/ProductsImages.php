<?php

use Illuminate\Database\Seeder;

class ProductsImages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker\Factory::create('ru_RU');
		$filepath = 'public/uploads/images/products_images/original';

		if (!File::exists($filepath)) {
			File::makeDirectory($filepath, null, true);
		}

		for ($i = 0; $i < 410; $i++)  {
			$model = new \App\Models\Catalog\ProductsImage();
			if (env('FAKER_LOCAL') == false) {
				$model->image = $faker->image($filepath, 1920, 1080, 'food', false);
			} else {
				$model->image = $faker->file('resources/faker_images', $filepath, false);
			}
			$model->position = $faker->randomDigit;
			$model->status = 1;
			$model->product_id = rand(1, 200);
			$model->save();
		}
    }
}
