<?php

use Illuminate\Database\Seeder;

class Products extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker\Factory::create('ru_RU');

		$filepath = 'public/uploads/images/products/original';

		if (!File::exists($filepath)) {
			File::makeDirectory($filepath, 777, true, true);
		}

		for ($i = 0; $i < 200; $i++) {
			$model = new \App\Models\Catalog\Product();
			$model->name = $faker->sentence(rand(2, 10));
			$model->slug = $faker->slug(rand(2, 10));
			$model->status = 1;
			$model->color_id = rand(35, 38);
			$model->sweetness_id = rand(8, 11);
			$model->type_id = rand(32, 34);
			$model->kind_id = rand(1, 2);
			$model->alcohol_id = rand(3, 7);
			$model->sugar_id = rand(39, 42);
			$model->brand_id = rand(1, 11);
			$model->country_id = 1;
			$model->novelty = rand(0,1);
			$model->line_id = rand(1, 12);
			$model->compilation_id = rand(1, 10);
			if (env('FAKER_LOCAL') == false) {
				$model->image = $faker->image($filepath, 1920, 1080, 'food', false);
			} else {
				$model->image = $faker->file('resources/faker_images', $filepath, false);
			}
			$model->keywords = $faker->sentence(10);
			$model->description = $faker->sentence(25);
			$model->h1 = $faker->sentence();
			$model->title = $faker->sentence();
			$model->save();
		}
    }
}
