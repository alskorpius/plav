<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = [
            ['О нас', '/about-us', 0],
            ['Доставка и оплата', '/payment-and-delivery', 1],
            ['Отзывы', '/reviews', 2],
            ['Новости', '/news', 3],
            ['Контакты', '/contacts', 4],
        ];
        foreach ($menu AS $element) {
            $model = new \App\Models\Menu();
            $model->name = $element[0];
            $model->url = $element[1];
            $model->position = $element[2];
            $model->status = true;
            $model->save();
        }
    }
}
