<?php

use Illuminate\Database\Seeder;

class Brands extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$brands = [
			[
				'name' => 'Dakishvili Family Selection',
				'slug' => 'dakishvili-family-selection',
				'shop_provider_id' => rand(1,7),
				'status' => '1',
				'position' => rand(1,10),
				'h1' => 'Dakishvili Family Selection',
				'title' => 'Dakishvili Family Selection',
				'keywords' => 'Dakishvili Family Selection',
				'description' => 'Dakishvili Family Selection',
				'content' => 'Dakishvili Family Selection',
			],
			[
				'name' => 'Orgo',
				'slug' => 'orgo',
				'shop_provider_id' => rand(1,7),
				'status' => '1',
				'position' => rand(1,10),
				'h1' => 'Orgo',
				'title' => 'Orgo',
				'keywords' => 'Orgo',
				'description' => 'Orgo',
				'content' => 'Orgo',
			],
			[
				'name' => 'Vita Vinea',
				'slug' => 'vita-vinea',
				'shop_provider_id' => rand(1,7),
				'status' => '1',
				'position' => rand(1,10),
				'h1' => 'Vita Vinea',
				'title' => 'Vita Vinea',
				'keywords' => 'Vita Vinea',
				'description' => 'Vita Vinea',
				'content' => 'Vita Vinea',
			],
			[
				'name' => 'Maranuli',
				'slug' => 'maranuli',
				'shop_provider_id' => rand(1,7),
				'status' => '1',
				'position' => rand(1,10),
				'h1' => 'Maranuli',
				'title' => 'Maranuli',
				'keywords' => 'Maranuli',
				'description' => 'Maranuli',
				'content' => 'Maranuli',
			],
			[
				'name' => 'Vine Ponto',
				'slug' => 'vine-ponto',
				'shop_provider_id' => rand(1,7),
				'status' => '1',
				'position' => rand(1,10),
				'h1' => 'Vine Ponto',
				'title' => 'Vine Ponto',
				'keywords' => 'Vine Ponto',
				'description' => 'Vine Ponto',
				'content' => 'Vine Ponto',
			],
			[
				'name' => 'Mosmieri',
				'slug' => 'mosmieri',
				'shop_provider_id' => rand(1,7),
				'status' => '1',
				'position' => rand(1,10),
				'h1' => 'Mosmieri',
				'title' => 'Mosmieri',
				'keywords' => 'Mosmieri',
				'description' => 'Mosmieri',
				'content' => 'Mosmieri',
			],
			[
				'name' => 'Maisuradze Wines',
				'slug' => 'maisuradze-wines',
				'shop_provider_id' => rand(1,7),
				'status' => '1',
				'position' => rand(1,10),
				'h1' => 'Maisuradze Wines',
				'title' => 'Maisuradze Wines',
				'keywords' => 'Maisuradze Wines',
				'description' => 'Maisuradze Wines',
				'content' => 'Maisuradze Wines',
			],
			[
				'name' => 'Telavi Old Cellar',
				'slug' => 'telavi-old-cellar',
				'shop_provider_id' => rand(1,7),
				'status' => '1',
				'position' => rand(1,10),
				'h1' => 'Telavi Old Cellar',
				'title' => 'Telavi Old Cellar',
				'keywords' => 'Telavi Old Cellar',
				'description' => 'Telavi Old Cellar',
				'content' => 'Telavi Old Cellar',
			],
			[
				'name' => 'Danieli',
				'slug' => 'danieli',
				'shop_provider_id' => rand(1,7),
				'status' => '1',
				'position' => rand(1,10),
				'h1' => 'Danieli',
				'title' => 'Danieli',
				'keywords' => 'Danieli',
				'description' => 'Danieli',
				'content' => 'Danieli',
			],
			[
				'name' => 'Terra Initia',
				'slug' => 'terra-initia',
				'shop_provider_id' => rand(1,7),
				'status' => '1',
				'position' => rand(1,10),
				'h1' => 'Terra Initia',
				'title' => 'Terra Initia',
				'keywords' => 'Terra Initia',
				'description' => 'Terra Initia',
				'content' => 'Terra Initia',
			],
			[
				'name' => 'Kabaj',
				'slug' => 'kabaj',
				'shop_provider_id' => rand(1,7),
				'status' => '1',
				'position' => rand(1,10),
				'h1' => 'Kabaj',
				'title' => 'Kabaj',
				'keywords' => 'Kabaj',
				'description' => 'Kabaj',
				'content' => 'Kabaj',
			],
		];
		foreach ($brands AS $brand) {
			$model = new \App\Models\Catalog\ShopBrand();
			$model->name = $brand['name'];
			$model->slug = $brand['slug'];
			$model->shop_provider_id = $brand['shop_provider_id'];
			$model->status = $brand['status'];
			$model->position = $brand['position'];
			$model->h1 = $brand['h1'];
			$model->title = $brand['title'];
			$model->keywords = $brand['keywords'];
			$model->description = $brand['description'];
			$model->content = $brand['content'];
			$model->save();
		}
    }
}
