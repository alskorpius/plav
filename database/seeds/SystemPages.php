<?php

use Illuminate\Database\Seeder;
use Faker\Factory AS Fake;

class SystemPages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Fake::create();

        $pages = [
            [
                'name' => 'Главная',
                'slug' => '/index',
                'h1' => 'Главная',
                'title' => 'Главная',
                'keywords' => 'Главная',
                'descriptions' => 'Главная',
                'settings' => '[]',
            ],
            [
                'name' => 'О нас',
                'slug' => '/about-us',
                'h1' => 'О нас',
                'title' => 'О нас',
                'keywords' => 'О нас',
                'descriptions' => 'О нас',
                'settings' => '[]',
            ],
            [
                'name' => 'Кто мы',
                'slug' => '/about-us/who-are-we',
                'h1' => 'Кто мы',
                'title' => 'Кто мы',
                'keywords' => 'Кто мы',
                'descriptions' => 'Кто мы',
                'settings' => '[]',
            ],
            [
                'name' => 'Наши эксперты',
                'slug' => '/about-us/our-experts',
                'h1' => 'Наши эксперты',
                'title' => 'Наши эксперты',
                'keywords' => 'Наши эксперты',
                'descriptions' => 'Наши эксперты',
                'settings' => '[]',
            ],
			[
				'name' => 'Знак качества WDS',
				'slug' => '/about-us/quality',
				'h1' => 'Знак качества WDS',
				'title' => 'Знак качества WDS',
				'keywords' => 'Знак качества WDS',
				'descriptions' => 'Знак качества WDS',
				'settings' => '[]',
			],
            [
                'name' => 'Блог',
                'slug' => '/blog',
                'h1' => 'Блог',
                'title' => 'Блог',
                'keywords' => 'Блог',
                'descriptions' => 'Блог',
                'settings' => '[]',
            ],
            [
                'name' => 'Новости',
                'slug' => '/blog/news',
                'h1' => 'Новости',
                'title' => 'Новости',
                'keywords' => 'Новости',
                'descriptions' => 'Новости',
                'settings' => '[]',
            ],
            [
                'name' => 'Статьи',
                'slug' => '/blog/articles',
                'h1' => 'Статьи',
                'title' => 'Статьи',
                'keywords' => 'Статьи',
                'descriptions' => 'Статьи',
                'settings' => '[]',
            ],
            [
                'name' => 'Мероприятия',
                'slug' => '/blog/events',
                'h1' => 'Статьи',
                'title' => 'Статьи',
                'keywords' => 'Статьи',
                'descriptions' => 'Статьи',
                'settings' => '[]',
            ],
            [
                'name' => 'Опросы',
                'slug' => '/voting',
                'h1' => 'Опросы',
                'title' => 'Опросы',
                'keywords' => 'Опросы',
                'descriptions' => 'Опросы',
                'settings' => '[]',
            ],
            [
                'name' => 'Наши бренды',
                'slug' => '/our-brands',
                'h1' => 'Наши бренды',
                'title' => 'Наши бренды',
                'keywords' => 'Наши бренды',
                'descriptions' => 'Наши бренды',
                'settings' => '[]',
            ],
            [
                'name' => 'Онлайн магазин',
                'slug' => '/shop',
                'h1' => 'Онлайн магазин',
                'title' => 'Онлайн магазин',
                'keywords' => 'Онлайн магазин',
                'descriptions' => 'Онлайн магазин',
                'settings' => '[]',
            ],
            [
                'name' => 'Где купить',
                'slug' => '/where-buy',
                'h1' => 'Где купить',
                'title' => 'Где купить',
                'keywords' => 'Где купить',
                'descriptions' => 'Где купить',
                'settings' => '[]',
            ],
            [
                'name' => 'Контакты',
                'slug' => '/contacts',
                'h1' => 'Контакты',
                'title' => 'Контакты',
                'keywords' => 'Контакты',
                'descriptions' => 'Контакты',
                'settings' => '{"latitude":"46.637326","longitude":"32.6125771","zoom":"17"}',
            ],
            [
                'name' => 'Отзывы',
                'slug' => '/reviews',
                'h1' => 'Отзывы',
                'title' => 'Отзывы',
                'keywords' => 'Отзывы',
                'descriptions' => 'Отзывы',
                'settings' => '[]',
            ],
            [
                'name' => 'Поиск',
                'slug' => '/search',
                'h1' => 'Поиск',
                'title' => 'Поиск',
                'keywords' => 'Поиск',
                'descriptions' => 'Поиск',
                'settings' => '[]',
            ],
            [
                'name' => 'Оформления заказа',
                'slug' => '/ordering',
                'h1' => 'Оформления заказа',
                'title' => 'Оформления заказа',
                'keywords' => 'Оформления заказа',
                'descriptions' => 'Оформления заказа',
                'settings' => '[]',
            ],
            [
                'name' => 'Доставка и оплата',
                'slug' => '/payment-and-delivery',
                'h1' => 'Доставка и оплата',
                'title' => 'Доставка и оплата',
                'keywords' => 'Доставка и оплата',
                'descriptions' => 'Доставка и оплата',
                'settings' => '[]',
            ],
			[
				'name' => 'Бренды',
				'slug' => '/brands',
				'h1' => 'Бренды',
				'title' => 'Бренды',
				'keywords' => 'Бренды',
				'descriptions' => 'Бренды',
				'settings' => '[]',
			],
        ];

        $i = 0;
        $faker = Faker\Factory::create();
        foreach ($pages AS $page) {
            $model = new \App\Models\SystemPage();
            $model->name = $page['name'];
            $model->slug = $page['slug'];
            $model->content = $faker->sentence(rand(2, 200));
            $model->h1 = $page['h1'];
            $model->title = $page['title'];
            $model->keywords = $page['keywords'];
            $model->description = $page['descriptions'];
            $model->settings = $page['settings'];
            $model->save();
            $i++;
        }
    }
}
