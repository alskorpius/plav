<?php

use Illuminate\Database\Seeder;

class Pages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = [
            [
                'name' => 'Доставка и оплата',
                'slug' => 'dostavka-i-oplata',
                'status' => '1',
                'h1' => 'Доставка и оплата',
                'title' => 'Доставка и оплата',
                'keywords' => 'Доставка и оплата',
                'descriptions' => 'Доставка и оплата',
            ],
        ];
        $i = 0;
        $faker = Faker\Factory::create();
        foreach ($pages AS $page) {
            $model = new \App\Models\Page();
            $model->name = $page['name'];
            $model->slug = $page['slug'];
            $model->content = $faker->sentence(rand(2, 200));
            $model->status = $page['status'];
            $model->position = $i;
            $model->h1 = $page['h1'];
            $model->title = $page['title'];
            $model->keywords = $page['keywords'];
            $model->description = $page['descriptions'];
            $model->save();
            $i++;
        }
    }
}
