<?php

use Illuminate\Database\Seeder;

class UsersRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $model = new \App\Models\UsersRole;
        $model->name = 'Администратор';
        $model->alias = 'administrator';
        $model->save();

        $model = new \App\Models\UsersRole;
        $model->name = 'Пользователь';
        $model->alias = 'user';
        $model->save();

        $model = new \App\Models\UsersRole;
        $model->name = 'Оператор';
        $model->alias = 'manager';
        $model->save();
    }
}
