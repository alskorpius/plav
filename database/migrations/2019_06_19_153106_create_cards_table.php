<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->timestamp('birthday')->nullable();
            $table->boolean('gender')->default(1);
            $table->boolean('married')->default(0);
            $table->boolean('fisherman')->default(0);
            $table->unsignedTinyInteger('payment')
                ->default(0)
                ->comment('0 - 100%, 1 - 50%, 2 - free');

            $table->integer('department_id')->unsigned()->nullable();
            $table->integer('profession_id')->unsigned()->nullable();
            $table->integer('factor_id')->unsigned()->nullable();

            $table->boolean('status')->default(true);
            $table->boolean('is_registration_address')->default(true);
            $table->timestamps();
            $table->timestamp('last_visit')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
