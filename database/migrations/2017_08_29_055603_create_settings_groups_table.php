<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('alias',32)->unique();
            $table->integer('position')->default(0);
        });

        Schema::table('settings', function (Blueprint $table) {
            $table->string('group', 32)->nullable();
            $table->foreign('group')->references('alias')->on('settings_groups')
                ->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
