<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('alias',32)->unique();
        });

        Schema::table('settings', function (Blueprint $table) {
            $table->string('data')->nullable();
            $table->string('type', 32)->nullable();
            $table->foreign('type')->references('alias')->on('settings_types')
                ->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
