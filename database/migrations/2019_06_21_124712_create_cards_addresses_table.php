<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('card_id');
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('street_id');
            $table->unsignedTinyInteger('housing_number')->nullable();
            $table->string('house_number')->nullable();
            $table->unsignedTinyInteger('apartment_number')->nullable();
            $table->boolean('is_registration_address')->default(1);

            $table->foreign('card_id')
                ->references('id')
                ->on('cards')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('city_id')
                ->references('id')
                ->on('cities')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('street_id')
                ->references('id')
                ->on('streets')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards_addresses');
    }
}
