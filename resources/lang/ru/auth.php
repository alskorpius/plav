<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Логин и/или пароль введены некорректно',
    'throttle' => 'Слишком много попыток входа. Следующая попытка возможна через :seconds секунд.',

    'logout' => 'Вы успешно вышли из ЛК',
    'refresh_failed' => 'Не удалось обновить токен',
    'registration_failed' => 'Регистрация успешна. Вы можете войти на сайт, используя свои email и пароль',

];
