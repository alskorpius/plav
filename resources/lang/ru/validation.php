<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Поле ":attribute" должно быть отмечено.',
    'active_url' => 'Значение поля ":attribute" не является корректным URL адресом.',
    'after' => 'Значение поля ":attribute" должно быть датой больше чем :date.',
    'after_or_equal' => 'Значение поля ":attribute" должно быть датой больше или равной :date.',
    'alpha' => 'Значение поля ":attribute" может содержать в себе только буквы.',
    'alpha_dash' => 'Значение поля ":attribute" может содержать в себе только буквы, цифры и нижние подчеркивания.',
    'alpha_num' => 'Значение поля ":attribute" может содержать в себе только буквы и цифры.',
    'array' => 'Значение поля ":attribute" должно быть массивом.',
    'before' => 'Значение поля ":attribute" должно быть датой меньше :date.',
    'before_or_equal' => 'Значение поля ":attribute" должно быть датой меньше или равной :date.',
    'between' => [
        'numeric' => 'Значение должно быть в пределах :min и :max.',
        'file' => 'Размер файла должен быть в пределах :min и :max киллобайт.',
        'string' => 'Строка должна быть не короче :min и не длиннее :max символов.',
        'array' => 'Количество элементов массива должно быть в пределах между :min и :max.',
    ],
    'boolean' => 'Значение поля ":attribute" должно быть булевым.',
    'confirmed' => 'Значение поля ":attribute" не подтверждено.',
    'date' => 'Значение поля ":attribute" не является датой.',
    'date_format' => 'Значение поля ":attribute" не совпадает с форматом :format.',
    'different' => 'Значение поля ":attribute" и :other должны отличаться.',
    'digits' => 'Значение поля ":attribute" должно содержать в себе :digits цифр.',
    'digits_between' => 'Значение поля ":attribute" должно содержать от :min до :max символов.',
    'dimensions' => 'Файл не является изображением.',
    'distinct' => 'Значение поля ":attribute" повторяющееся.',
    'email' => 'Значение поля ":attribute" должно быть email адресом.',
    'exists' => 'Выбранное значение некорректно.',
    'file' => 'Выберите файл.',
    'filled' => 'Поле ":attribute" не заполнено.',
    'image' => 'Выберите корректное изображение.',
    'in' => 'Выбранное поле ":attribute" некорректно.',
    'in_array' => 'Значение не существует в :other.',
    'integer' => 'Значение поля ":attribute" должно быть числом.',
    'ip' => 'Значение поля ":attribute" должно быть корректным IP адресом.',
    'ipv4' => 'Значение поля ":attribute" должно быть корректным IPv4 адресом.',
    'ipv6' => 'Значение поля ":attribute" должно быть корректным IPv6 адресом.',
    'json' => 'Значение поля ":attribute" должно быть корректной JSON строкой.',
    'max' => [
        'numeric' => 'Значение поля ":attribute" не может быть больше :max.',
        'file' => 'Файл не может весить больше :max киллобайт.',
        'string' => 'Значение поля ":attribute" не может быть длиннее :max символов.',
        'array' => 'Массив не может содержать больше :max элементов.',
    ],
    'mimes' => 'Значение поля ":attribute" должно быть файлом типа: :values.',
    'mimetypes' => 'Значение поля ":attribute" должно быть файлом типа: :values.',
    'min' => [
        'numeric' => 'Значение поля ":attribute" должно быть не меньше :min.',
        'file' => 'Размер файла должен быть не меньше :min киллобайт.',
        'string' => 'Значение поля ":attribute" должно быть не короче :min символов.',
        'array' => 'Массив должно содержать не меньше :min элементов.',
    ],
    'not_in' => 'Выбранное значение поля ":attribute" некорректно.',
    'numeric' => 'Значение поля ":attribute" должно быть числом.',
    'present' => 'Значение должно быть заполнено.',
    'phone_number' => 'Поле ":attribute" должно быть номером телефона.',
    'regex' => 'Формат телефона некорректен.',
    'required' => 'Поле ":attribute" обязательно для заполнения.',
    'required_if' => 'Поле ":other" обязательно, для выбраного значения',
    'required_unless' => 'Поле обязательно для заполнения, если :other является одним из значений: :values.',
    'required_with' => 'Поле обязательно для заполнения, если хотя бы одно из полей [:values] заполнено.',
    'required_with_all' => 'Поле обязательно для заполнения, если все поля [:values] заполнены.',
    'required_without' => 'Поле обязательно для заполнения, если поля [:values] не заполнены.',
    'required_without_all' => 'Поле обязательно для заполнения, если ниодно из полей [:values] не заполнено.',
    'same' => 'Значения этого поля и :other должны совпадать.',
    'size' => [
        'numeric' => 'Число должно быть длинной в :size цифр.',
        'file' => 'Размер файла должен быть равен :size киллобайт.',
        'string' => 'Строка должна быть длинной :size символов.',
        'array' => 'Массив должен содержать :size элементов.',
    ],
    'string' => 'Значение поля ":attribute" должно быть строкой.',
    'timezone' => 'Значение поля ":attribute" должно быть корректным часовым поясом.',
    'unique' => 'Введенное значение не уникально.',
    'uploaded' => 'Файл не был загружен.',
    'url' => 'Формат некорректен.',

    'current_password' => 'Пожалуйста укажите корректный действующий пароль.',
    'product_exists' => 'Такого продукта не существует!',
    'size_table' => 'Такой размерной сетки нет в БД!',
    'real_topic' => 'Такой темы не существует либо она была удалена!',
    'real_user' => 'Такого пользователя не существует либо он был удален!',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'region_id' => 'регионы',
        'compilation_id' => 'подборки',
        'brand_id' => 'бренд',
        'line_id' => 'линейки',
        'addressBD' => 'адрес',
        'city' => 'город',
        'review' => 'отзыв',
        'message' => 'сообщение',
        'delivery_warehouse' => 'склад новой почты',
        'slug' => 'Алиас',
        'date' => 'Дата',
        'name' => 'название',
        'phone' => 'номер телефона',
        'tel' => 'номер телефона',
        'answer_date' => 'Дата ответа',
        'answer' => 'ответ',
        'url' => 'URL',
        'parent_id' => 'Родитель',
        'content' => 'контент',
        'h1' => 'H1',
        'title' => 'Title',
        'keywords' => 'Keywords',
        'description' => 'Description',
        'status' => 'Статус',
        'infographics' => 'Виджет с инфографикой',
        'advantages' => 'Виджет с преимуществами',
        'email' => 'email',
        'password' => 'пароль',
        'second_name' => 'Фамилия',
        'first_name' => 'имя',
        'middle_name' => 'Отчество',
        'address' => 'Адрес',
        'summary' => 'Описание',
        'price' => 'Цена',
        'size_id' => 'Таблица размеров',
        'nominal' => 'Номинал',
        'value' => 'Значение',
        'image' => 'Изображение',
        'from' => 'От',
        'to' => 'До',
        'percent' => 'Скидка, %',
        'word' => 'Слово',
        'weight' => 'Вес',
        'delivery_type' => 'пункт доставки',
        'delivery_provider_id' => 'Транспортная компания',
        'payment_system_id' => 'Способ оплаты',
        'color' => 'Цвет',
        'user_id' => 'Пользователь',
        'subject' => 'Тема',
        'text' => 'Сообщение',
        'review' => 'Отзыв',
        'rating' => 'Оценка',
        'alt' => 'Alt',
        'script' => 'Код',
        'last_name' => 'Фамилия',
        'birthday' => 'День рождения',
        'house_number' => '№ дома',
        'housing_number' => 'Корпус',
        'apartment_number' => 'Квартира',

        'delivery-1' => 'Введите сумму, грн',
        'delivery-2' => 'Введите сумму, грн',
        'delivery-3' => 'Введите сумму, грн',
        'delivery-4' => 'Введите сумму, грн',
        'delivery-5' => 'Введите сумму, грн',
        'delivery-6' => 'Введите сумму, грн',
        'delivery-7' => 'Введите сумму, RUB',
        'delivery-8' => 'Введите сумму, RUB',
        'delivery-9' => 'Введите сумму, RUB',
        'delivery-10' => 'Введите сумму, RUB',
        'delivery-11' => 'Введите сумму, RUB',
        'delivery-12' => 'Введите сумму, RUB',
    ],

];
