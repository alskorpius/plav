<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен совпадать с подтверждением.',
    'reset' => 'Ваш пароль сброшен!',
    'sent' => 'На ваш email была отправлена ссылка сброса пароля!',
    'token' => 'Токен для сброса пароля некорректный.',
    'user' => "Мы не можем найти пользователя с таким email адресом.",

];
