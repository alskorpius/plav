export default class Counter {
  static counters = {}
  elements = []

  /**
   * Creates a new counter instance
   * @param {string} name counter name
   * @param {number} value initial value
   * @param {{}} options
   * @prop {string} prefix
   * @prop {array<HTMLElement>} elements
   * @protected {number} _value
   */
  constructor (name, value, options = {}) {
    let escapedValue = parseInt(value)
    escapedValue = isNaN(escapedValue) ? 0 : escapedValue
    let {
      prefix = '',
      suffix = '',
      step = 1
    } = options
    this.prefix = prefix
    this.suffix = suffix
    this.name = name
    this.step = step
    this._value = escapedValue
    Counter.counters[name] = this
  }

  /**
   * Creates a new counter instance and returns it
   * @param values
   * @returns {Counter}
   */
  static create (...values) {
    return new this(...values)
  }

  /**
   * Returns the current counter value
   * @returns {number}
   */
  get value () {
    return this._value
  }

  /**
   * Sets a new counter value
   * @param {number} value
   */
  set value (value) {
    this._value = +value
    this.updateValueInDOM()
  }

  get text () {
    return this.prefix + this.value + this.suffix
  }

  /**
   *
   * @param delta
   */
  increment (delta = this.step) {
    this.value += +delta
  }

  /**
   *
   * @param delta
   */
  decrement (delta = this.step) {
    this.value -= +delta
  }

  /**
   *
   * @param {HTMLElement} element
   */
  linkElement (element) {
    if (element instanceof window.HTMLElement) {
      this.elements.push(element)
    } else {
      console.warn('Only HTMLElement is acceptable')
    }
  }

  updateValueInDOM () {
    this.elements.forEach(element => {
      element.innerHTML = this.text
    })
  }

  /**
   * Return a counter instance by name
   * @param {string} name counter name
   * @returns {Counter}
   */
  static getCounter (name) {
    return Counter.counters[name]
  }
}
/**
 * List of created counters
 * @type {{}}
 */
window.CC = Counter
