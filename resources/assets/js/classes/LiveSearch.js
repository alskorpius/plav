import $ from 'jquery'
import Debounce from 'lodash.debounce'
import { setContent } from '../modules/helpers'

export default class LiveSearch {
  constructor (userOptions = {}) {
    let defaultOptions = {
      css: {
        isOpen: 'is-open',
        hasResult: 'has-result',
        siteSearchIsOpen: 'site-search-is-open'
      }
    }
    this.options = $.extend(true, {}, defaultOptions, userOptions)
    this.init()
  }

  init () {
    this.addListeners()
  }

  addListeners () {
    let _self = this

    this.options.$input.on('keyup', Debounce((e) => {
      if (e.keyCode === 13) {
        this.options.$form.submit()
        return
      }

      let query = e.target.value.trim()

      if (query && query.length >= 3) {
        $.ajax({
          type: 'POST',
          url: this.options.url,
          data: {
            query: query
          }
        })
          .done((res) => {
            res = (typeof data === 'string') ? JSON.parse(res) : res
            _self.render(res)
              .then(() => {
                this.options.$suggestionsPanel.addClass(this.options.css.hasResult)
              })
              .catch(err => {
                this.options.$suggestionsPanel.removeClass(this.options.css.hasResult)
                console.warn(err)
              })
          })
          .fail((err) => {
            console.warn('Ошибка загрузки данных', err)
          })
          .always(() => {})
      } else {
        this.options.$suggestionsPanel.removeClass(this.options.css.hasResult)
      }
    }, 400)).on('focus', (e) => {
      let query = e.target.value.trim()

      if (query && query.length >= 3) {
        this.options.$suggestionsPanel.addClass(this.options.css.hasResult)
      }
    }).on('blur', () => {
      let tm
      clearTimeout(tm)
      tm = setTimeout(() => {
        this.options.$suggestionsPanel.removeClass(this.options.css.hasResult)
      }, 300)
    })
  }

  search () {
    this.options.$input.trigger('keyup')
  }

  render (data) {
    return new Promise((resolve, reject) => {
      if (!data) {
        reject(new Error('Нет данных для отображения'))
      }

      let content = []
      let template = this.options.$suggestionsItem[0].outerHTML

      data.result.forEach((responseItem) => {
        content.push(setContent(template, responseItem))
      })

      this.options.$suggestionsList[0].innerHTML = content.join('')

      resolve()
    })
  }
}
