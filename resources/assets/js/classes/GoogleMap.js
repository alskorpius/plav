import $ from 'jquery'
import { setContent } from '../modules/helpers'

export default class GoogleMap {
  constructor (container, options = {}) {
    if (!container) {
      console.warn('The container for Google maps must be specified')
      return
    }

    let asset = window.projectConfig.assets
    let defaultOptions = {
      center: {
        // Координаты Лиссабона
        lat: 38.721990,
        lng: -9.137804
      },
      zoom: 3,
      infoWindowMarkupTpl: `
          <div class="iw is-hidden">
              <div class="iw__heading">
                  <div class="iw__lines">__iw-images__</div>
              </div>
              <div class="iw__body">
                  <div class="iw__block">
                      <div class="iw__city">__iw-region__</div>
                  </div>
                  <div class="iw__block">
                      <div class="iw__name">__iw-name__</div>
                  </div>
                  <div class="iw__block">
                      <div class="iw__address">__iw-address__</div>
                  </div>
              </div>
          </div>
      `,
      mapStyle: [
        {
          'elementType': 'geometry',
          'stylers': [
            {
              'color': '#f5f5f5'
            }
          ]
        },
        {
          'elementType': 'labels.icon',
          'stylers': [
            {
              'visibility': 'off'
            }
          ]
        },
        {
          'elementType': 'labels.text.fill',
          'stylers': [
            {
              'color': '#616161'
            }
          ]
        },
        {
          'elementType': 'labels.text.stroke',
          'stylers': [
            {
              'color': '#f5f5f5'
            }
          ]
        },
        {
          'featureType': 'administrative.land_parcel',
          'elementType': 'labels.text.fill',
          'stylers': [
            {
              'color': '#bdbdbd'
            }
          ]
        },
        {
          'featureType': 'poi',
          'elementType': 'geometry',
          'stylers': [
            {
              'color': '#eeeeee'
            }
          ]
        },
        {
          'featureType': 'poi',
          'elementType': 'labels.text.fill',
          'stylers': [
            {
              'color': '#757575'
            }
          ]
        },
        {
          'featureType': 'poi.park',
          'elementType': 'geometry',
          'stylers': [
            {
              'color': '#e5e5e5'
            }
          ]
        },
        {
          'featureType': 'poi.park',
          'elementType': 'labels.text.fill',
          'stylers': [
            {
              'color': '#9e9e9e'
            }
          ]
        },
        {
          'featureType': 'road',
          'elementType': 'geometry',
          'stylers': [
            {
              'color': '#ffffff'
            }
          ]
        },
        {
          'featureType': 'road.arterial',
          'elementType': 'labels.text.fill',
          'stylers': [
            {
              'color': '#757575'
            }
          ]
        },
        {
          'featureType': 'road.highway',
          'elementType': 'geometry',
          'stylers': [
            {
              'color': '#dadada'
            }
          ]
        },
        {
          'featureType': 'road.highway',
          'elementType': 'labels.text.fill',
          'stylers': [
            {
              'color': '#616161'
            }
          ]
        },
        {
          'featureType': 'road.local',
          'elementType': 'labels.text.fill',
          'stylers': [
            {
              'color': '#9e9e9e'
            }
          ]
        },
        {
          'featureType': 'transit.line',
          'elementType': 'geometry',
          'stylers': [
            {
              'color': '#e5e5e5'
            }
          ]
        },
        {
          'featureType': 'transit.station',
          'elementType': 'geometry',
          'stylers': [
            {
              'color': '#eeeeee'
            }
          ]
        },
        {
          'featureType': 'water',
          'elementType': 'geometry',
          'stylers': [
            {
              'color': '#c9c9c9'
            }
          ]
        },
        {
          'featureType': 'water',
          'elementType': 'labels.text.fill',
          'stylers': [
            {
              'color': '#9e9e9e'
            }
          ]
        },
        {
          'featureType': 'landscape.man_made',
          'elementType': 'geometry.stroke',
          'stylers': [
            {
              'color': '#6b6b6b'
            },
            {
              'visibility': 'on'
            }
          ]
        }
      ],
      markerIcon: {
        locations_0: `${asset}images/markers/shop-sm.png`,
        locations_1: `${asset}images/markers/restaurant-sm.png`,
        locations_2: `${asset}images/markers/supermarket-sm.png`,
        locations_3: `${asset}images/markers/gas.png`
      }
    }

    this.options = $.extend(true, {}, defaultOptions, options)
    this.container = container
    this.readyMarkers = []
    this.readyInfoWindows = []

    this.init()
  }

  static fetchData (url) {
    return $.ajax({
      type: 'GET',
      url: url,
      contentType: 'application/json',
      beforeSend: function (request) { // @TODO -> Удалить заголовки после подвязки к реальным данным
        request.setRequestHeader('JsonStub-User-Key', 'ebc59248-deda-4aa7-87e0-2424f9cd7a71');
        request.setRequestHeader('JsonStub-Project-Key', 'd01bd1bb-341d-42d5-8dc9-a3e69e69823c');
      }
    })
  }

  init () {
    this.map = this.createMap()
  }

  addListeners (marker, infoWindow) {
    let map = this.map

    marker.addListener('click', function () {
      google.maps.event.trigger(map, 'click')
      infoWindow.open(map, marker)
    })

    map.addListener('click', function () {
      infoWindow.close()
    })

    infoWindow.addListener('domready', function () {
      let $iwOuter = $('.gm-style-iw')
      let $iw = $iwOuter.find('.iw')

      $iw.removeClass('is-hidden')
    })
  }

  createMap () {
    return new google.maps.Map(this.container, {
      center: {
        lat: parseFloat(this.options.center.lat),
        lng: parseFloat(this.options.center.lng)
      },
      zoom: this.options.zoom,
      styles: this.options.mapStyle
    })
  }

  createMarker (markerOpts) {
    let oMarker = new google.maps.Marker(markerOpts)
    this.readyMarkers.push(oMarker)
    return oMarker
  }

  createInfoWindow (infoWindowOpts) {
    let content = setContent(this.options.infoWindowMarkupTpl, infoWindowOpts.content)
    let oInfoWindow = new google.maps.InfoWindow({
      content: content,
      position: infoWindowOpts.position,
      pixelOffset: new google.maps.Size(24, 25)
    })
    this.readyInfoWindows.push(oInfoWindow)

    return oInfoWindow
  }

  prepareMarkers (arrMarkers = []) {
    arrMarkers.forEach(marker => {
      let icon = this.options.markerIcon[marker.locations.toLocaleLowerCase()]

      let createdMarker = this.createMarker({
        // map: this.map, // Установка маркера на карту происходит в соответствующих модулях карт в зависимости от каких-либо условий
        position: {
          lat: parseFloat(marker.position.lat),
          lng: parseFloat(marker.position.lng)
        },
        icon: icon,
        regions: marker.regions.toLocaleLowerCase(),
        locations: marker.locations.toLocaleLowerCase(),
        lines: marker.lines,
        infoWindow: marker.infoWindow
      })

      // TODO -> Возможно стоит пользоваться одним инфо-виндоу, но каждый раз менять в нем контент (.setContent)
      // Меньше действия и меньше памяти откусит
      // https://stackoverflow.com/questions/15719951/google-maps-api-v3-auto-center-map-with-multiple-markers?noredirect=1&lq=1
      // https://stackoverflow.com/questions/45455062/google-maps-reuse-single-infowindow-for-multiple-markers
      // https://stackoverflow.com/questions/11984258/change-google-maps-to-re-use-single-infowindow-rather-than-creating-multiple
      marker.infoWindow.icon = icon
      let createdInfoWindow = this.createInfoWindow({
        content: marker.infoWindow,
        position: {
          lat: parseFloat(marker.position.lat),
          lng: parseFloat(marker.position.lng)
        }
      })

      this.addListeners(createdMarker, createdInfoWindow)
    })
  }

  setMarker (marker) {
    marker.setMap(this.map)
  }

  removeMarker (marker) {
    marker.setMap(null)
  }

  fit (markers, maxZoom = 16) {
    if (markers.length) {
      let bounds = new google.maps.LatLngBounds()

      markers.forEach((marker) => {
        bounds.extend(marker.getPosition())
      })

      this.map.fitBounds(bounds)

      setTimeout(() => {
        let currentZoom = this.map.getZoom()
        if (currentZoom > maxZoom) {
          this.map.setZoom(maxZoom)
        }
      }, 50)
    } else {
      this.map.setCenter({
        lat: parseFloat(this.options.center.lat),
        lng: parseFloat(this.options.center.lng)
      })
      this.map.setZoom(this.options.zoom)
    }
  }
}
