import Noty from 'noty'

Noty.overrideDefaults({
  theme: 'wds'
})

export default class Notifier extends Noty {
  static create (options) {
    return new Noty(options)
  }

  static prepareOptions (text = '', options = {}) {
    let opts
    if (typeof text === 'object') {
      opts = text
    } else if (typeof text === 'string' && typeof options === 'object') {
      opts = Object.assign(options, {text})
    } else {
      console.error('Incorrect types of arguments')
    }
    return Object.assign(opts, {
      timeout: 3000
    })
  }

  static info (text, options) {
    this.create(Object.assign(this.prepareOptions(text, options), {type: 'info'})).show()
  }

  static alert (text, options) {
    this.create(Object.assign(this.prepareOptions(text, options), {type: 'alert'})).show()
  }

  static error (text, options) {
    this.create(Object.assign(this.prepareOptions(text, options), {type: 'error'})).show()
  }

  static success (text, options) {
    this.create(Object.assign(this.prepareOptions(text, options), {type: 'success'})).show()
  }

  static warning (text, options) {
    this.create(Object.assign(this.prepareOptions(text, options), {type: 'warning'})).show()
  }

  static echo () {
    Notifier.alert('alert')
    Notifier.info('info')
    Notifier.success('success')
    Notifier.warning('warning')
    Notifier.error('error')
  }
}
