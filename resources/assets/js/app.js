import $ from 'jquery'
import './modules/ajax-global-config'
import Debounce from 'lodash.debounce'
import 'slick-carousel'
import 'magnific-popup'
import {Notifier, Counter} from './classes'
import './modules/CartComponent'

/**
 * Экспортируем переменные в глобальную область видимости
 */
window.jQuery = window.$ = $
window.Noty = Notifier
window.ModuleLoader = ModuleLoader

window.getLocale = function() {
  return document.documentElement.lang.split('-')[0].toLocaleLowerCase();
}

let $body
let $preloader = $('#preloader')

$(document).ready(function () {
  $body = $('body')
  $preloader.fadeOut(800, 'linear', function () {
    setTimeout(function () {
      $body.removeClass('is-loading')
    }, 200)
    setTimeout(function () {
      $body.addClass('ready')
    }, 600)
  })

  ModuleLoader.setImportPromise(moduleName =>import('./modules/' + moduleName))
  ModuleLoader.init({
    '[data-quiz]': 'Quiz',
    '[data-images-rotator]': 'ImagesRotator',
    '[data-hero-slider]': 'HeroSlider',
    '[data-accordion]': 'Accordion',
    '[data-slider]': 'Slider',
    '[data-parallax-container]': 'ParallaxStarter',
    '[data-equal-height]': 'EqualHeight',
    '[data-scroll-to]': 'ScrollsController',
    '[data-product]': 'ProductController',
    'form': 'FormsController',
    'select': 'CustomSelect',
    '[data-google-api]': 'GoogleApiController',
    '[data-tabs]': 'TabsController',
    '[data-filter]': 'FilterController',
    '[data-live-search]': 'LiveSearchController',
    '#vintage-switcher': 'VintageSwitcherController',
    '[data-reward-popper]': 'RewardPopperController'
  })

  /**
   * Инициализация счетчика избранных товаров
   * @type {*|jQuery}
   */
  let $favoritesCounter = $('#favorites-button')
    .children('span')
    .children('span')
  if ($favoritesCounter.length) {
    Counter.create('favorites', $favoritesCounter[0].innerHTML)
      .linkElement($favoritesCounter[0])
  }
  /// Конец счетчика избранных товаров

  $('[data-menu-trigger]').on('click', e => {
    e.preventDefault()
    $body.toggleClass('nav-menu-is-open')
    $('#navigation-menu').toggleClass('is-open')
    $(e.currentTarget).toggleClass('is-open')
  })

  $('#multi-level-menu').on('click', '.menu-plus', e => {
    let $link = $(e.currentTarget)
    let $listItem = $link.closest('li')
    let $container = $(e.delegateTarget)
    let $previousOpened = $container.find('.is-open')
    let open = function ($item) {
      $container.addClass('inner-level-opened')
      $item.addClass('is-open')
      $item.children('ul').slideDown(400, 'linear')
    }

    let close = function ($item) {
      $item.removeClass('is-open')
      $container.removeClass('inner-level-opened')
      $item.children('ul').slideUp(400, 'linear')
    }

    if ($previousOpened.length) {
      close($previousOpened)
    }
    if (!$previousOpened.is($listItem)) {
      open($listItem)
    }
  })

  $('.js-init[data-mfp-src]').magnificPopup({
    type: 'inline',
    mainClass: 'mfp-fade',
    removalDelay: 600,
    fixedContentPos: true,
    fixedBgPos: true,
    closeBtnInside: true
  })

  $('#account-enter-button').on('click', function (e) {
    if (!+(window.projectConfig.isAuth)) {

    } else {
      e.preventDefault()
      $.magnificPopup.open({
        items: {
          src: '#popup-login'
        },
        mainClass: 'mfp-fade',
        removalDelay: 600,
        fixedContentPos: true,
        fixedBgPos: true
      })
    }
  })

  let $classToggler = $('[data-toggle-class]')
  if ($classToggler.length) {
    $classToggler.each((i, toggler) => {
      let $toggler = $(toggler)
      let sClass = $toggler.data('toggle-class')
      $toggler.on('click', e => {
        e.preventDefault()
        e.stopPropagation()
        $toggler.toggleClass(sClass)
      })
      $toggler.find('[data-toggle-stop-propagation]').on('click', e => {
        e.stopPropagation()
      })
      $(document).on('click', e => {
        $toggler.removeClass(sClass)
      })
    })
  }

  $(window).on('scroll', Debounce(function () {
    let {scrollY} = window
    if ($body && $body.length) {
      if (scrollY <= 100) {
        $body.addClass('page-on-top')
      } else if (scrollY > 100) {
        $body.removeClass('page-on-top')
      }
    }
  }, 300)).trigger('scroll')

  setTimeout(function () {
    let checkAge = localStorage.getItem('check-age');
    if (!checkAge && checkAge !== 0) {
      localStorage.setItem('back-uri', location.pathname);
      window.location.href = '/age-check';
    } else if (checkAge == 0) {
      window.location.href = '/age-check';
    }
  }, 2000);

	let burgerEl = $('.navigation-menu-burger--header');
	console.log(burgerEl);
})
