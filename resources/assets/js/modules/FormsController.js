import $ from 'jquery'

export function loaderInit ($forms, ModuleLoader) {
  let callbacks = {
    quizForm: function (e, $form, response) {
      if (response.success && 'html' in response) {
        let $newCard = $(response.html)
        let $oldCard = $form.closest('[data-quiz]')
        $oldCard.after($newCard)
        $oldCard.remove()
        setTimeout(() => {
          ModuleLoader.loadAndInitModule('Quiz', $newCard)
        }, 500)
      }
    },
    eventForm: function (e, $form, response) {
      if (response.success) {
        $form.html(response.html)
      }
    }
  }

  $('.form-control').on('blur', function (e) {
    let $control = $(e.currentTarget)
    if ($control.is('select')) return
    let isEmpty = $control.val().length
    if (isEmpty) {
      $control.addClass('is-dirty')
    } else {
      $control.removeClass('is-dirty')
    }
  }).trigger('blur')

  $forms.on('submit', function (e) {
    e.preventDefault()
    let $form = $(e.currentTarget)
    let data = new window.FormData($form[0])

    $.ajax({
      url: $form.attr('action'),
      method: $form.attr('method'),
      processData: false,
      contentType: false,
      data: data
    })
      .done((response) => {
        if (response.success) {
          let callbackName = $form.data('on-success')
          if (typeof callbacks[callbackName] === 'function') {
            callbacks[callbackName](e, $form, response)
          }
        }
        if (response.redirect) {
          window.location = response.redirect
        }
      })
      .fail((jqXHR, textStatus, errorThrown) => {})
      .always(() => {})
  })
}
