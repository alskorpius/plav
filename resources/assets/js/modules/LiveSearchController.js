import $ from 'jquery'

export function loaderInit ($elements) {
  $elements.each((i, element) => {
    let $element = $(element)
    let moduleName = $element.data('module')

    import('./' + moduleName).then((module) => {
      module.default($element)
      if ($element.data('siteSearch') !== 'is-inited') {
        $element.data('siteSearch', 'is-inited')
      }
    }).catch((err) => {
      console.error('Модуль не загружен \n', err)
    })
  })
}
