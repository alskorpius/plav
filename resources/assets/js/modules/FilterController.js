import $ from 'jquery'
import Debounce from 'lodash.debounce'

class Filter {
  constructor ($container, url) {
    let $el = $($container)
    let $parent = $el.parent()

    this.url = url
    this.params = {}
    this.$watchInputs = $()
    this.$el = $el
    this.$parent = $parent
    this.$triggerOpen = $parent.find('[data-filter="trigger-open"]')
    this.$triggerClose = $parent.find('[data-filter="trigger-close"]')

    this.init()
  }

  init () {
    this.$el.on('change', 'input:checkbox', e => {
      let name = e.currentTarget.name
      this.sendData().then(response => {
        this.showResult(e.currentTarget, response.total_found, response.url)
        this.toggleActiveParam(name, this.params[name].label.html())
      })
    })
    this.$parent.on('click', e => {
      e.stopPropagation()
    })
    this.$triggerOpen.on('click', e => {
      this.$parent.addClass('filter-is-open')
      this.$el.addClass('is-open')
    })
    this.$triggerClose.on('click', e => {
      this.$parent.removeClass('filter-is-open')
      this.$el.removeClass('is-open')
    })
    $(document).on('click', e => {
      this.$parent.removeClass('filter-is-open')
      this.$el.removeClass('is-open')
    })
  }

  collectParams (dataContainerSelector = 'filter-param-container',
                 dataParamSelector = 'filter-param') {
    let $params = this.$el.find(`[data-${dataContainerSelector}]`)
    $params.each((i, el) => {
      let $param = $(el)
      let param = this.params[$param.data(dataContainerSelector)] = {}
      let $paramElements = $param.find(`[data-${dataParamSelector}]`)
      $paramElements.each((i, paramEl) => {
        let $paramEl = $(paramEl)
        param[$paramEl.data(dataParamSelector)] = $paramEl
      })
    })
    return this.params
  }

  getCheckedParams () {
    return Object.keys(this.params).filter(param => {
      if (this.params[param].input.prop('checked') === true) {
        return true
      }
    })
  }

  sendData () {
    let checkedParams = this.getCheckedParams()
    let data = {}
    if (this.$el.data('filterType')) {
      data.filterType = this.$el.data('filterType')
    }
    if (this.$el.data('filterLang')) {
      data.filterLang = this.$el.data('filterLang')
    }
    data.checked_items = checkedParams.map(
      param => this.params[param].input[0].name)
    this.$watchInputs.each((i, input) => {
      data[input.name] = input.value
    })
    return $.post(this.url, data).then(response => {
      if (response.success) {
        this.refreshCheckboxes(response.items)
      }
      return response
    })
  }

  refreshCheckboxes (items) {
    if (!items) return
    Object.keys(items).forEach(item => {
      if (item in this.params) {
        let {value, input} = this.params[item]
        if (input.is(':checked')) {
          value.html('')
        } else if (items[item] > 0) {
          value.html(items[item])
          input.attr('disabled', false)
        } else {
          value.html('')
          input.attr('disabled', true)
        }
      }
    })
  }

  watchInput (input) {
    let $input = $(input)
    this.$watchInputs = this.$watchInputs.add($input)
    if ($input.is('select')) {
      $input.on('change', e => {
        this.sendData().then((response) => {
          this.showResult(e.target, response.total_found, response.url)
          if (e.target.dataset.forceReload) {
            window.location.href = response.url
          }
        })
      })
    } else {
      $input.on('keyup', Debounce(function (e) {
        this.sendData().then((response) => {
          this.showResult(e.target, response.total_found, response.url)
          if (e.target.dataset.forceReload) {
            window.location.href = response.url
          }
        })
      }.bind(this), 400))
    }
  }

  addResult ($result) {
    this.$result = $($result)
  }

  addActiveParams ($container) {
    this.$activeParams = $container
    if (this.$activeParams && this.$activeParams.length) {
      this.$activeParams.on('click', '[data-delete]', e => {
        console.log(e)
        let name = e.currentTarget.dataset.delete
        this.params[name].input.trigger('click')
      })
    }
  }

  showResult (changedElement, totalFound, url) {
    let $element = $(changedElement)
    let $result = this.$result
    let offsetTop = $element.is(':hidden') ? 0 : $element.offset().top - this.$el.offset().top
    $result.children('span').html(totalFound)
    $result.children('a').attr('href', url)
    if ($result.hasClass('is-shown')) {
      $result.animate({top: offsetTop})
    } else {
      $result.css('top', offsetTop)
      $result.addClass('is-shown')
    }
  }

  toggleActiveParam (name, label) {
    let $container = this.$activeParams
    if (!$container.length) return false
    let $tag = $container.find(`[data-tag-name="${name}"]`)
    if ($tag.length) {
      $tag.remove()
    } else {
      $tag = $(`<div class="active-filters__item" data-tag-name="${name}">
                <div class="active-filters__text">${label}</div>
                <div class="active-filters__delete" data-delete="${name}"></div>
            </div>`)
      $tag.appendTo($container)
    }
  }

  exec (oParams) {
    for (let item in oParams) {
      if (!oParams.hasOwnProperty(item)) continue
      let filterItem = oParams[item]
      if (!filterItem.input[0].checked) continue
      this.toggleActiveParam(filterItem.input[0].name, filterItem.label[0].innerText)
    }
  }
}

export function loaderInit ($filters) {
  $filters.each((i, filterContainer) => {
    let filter = new Filter(filterContainer, filterContainer.dataset.filter)
    let goToDataHref = (e) => {
      window.location.href = e.currentTarget.selectedOptions[0].dataset.href
    }

    $('#sortBy').on('change', goToDataHref)
    $('#perPage').on('change', goToDataHref)

	$('#price-from, #price-to').on('change', function () {
		filter.watchInput($('#price-from'))
    	filter.watchInput($('#price-to'))
	});
    filter.addResult($(filterContainer).find('.filter__result'))
    filter.addActiveParams($('#active-filters'))
    filter.collectParams('filter-param-container')
    filter.exec(filter.params)
  })
}
