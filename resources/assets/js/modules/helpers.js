import $ from 'jquery'

/**
 * Скролим к элементу, заданному в атрибуте data-scroll-to.
 * @param {string|HTMLElement|jQuery} $targetOrSelector
 * @param options
 * @param callback
 */
export function scrollToElement ($targetOrSelector, options = {}, callback) {
  let $target = $($targetOrSelector)
  if (!$target.length) {
    console.warn(`Target ${$targetOrSelector} was not found`)
    return false
  }
  let {
    speed = 1000
  } = options
  let headerHeight = $('.layout__header').outerHeight()
  let scrollTop = $(window).scrollTop()
  let destination = $target.offset().top - headerHeight
  destination = destination < 0 ? 0 : destination
  let delta = Math.abs(destination - scrollTop)
  let duration = delta / speed * 1000
  $('html, body').stop().animate({
    scrollTop: destination
  }, {
    duration: duration,
    done: callback
  })
  return false
}

/**
 * Реплейсит ключи в шаблонное строке по заданому объекту с данными, где ключ в строке совпадают с ключами в объекте
 * @param tpl
 * @param oKeys
 * @returns {*}
 */
export function setContent (tpl, oKeys) {
  let buffer = tpl

  Object.keys(oKeys).forEach(key => {
    let regExp = new RegExp(`__${key}__`, 'gi')
    let temp = []

    if (!Array.isArray(oKeys[key])) oKeys[key] = [oKeys[key]]

    oKeys[key].forEach((item) => {
      /**
       * ключи нужно писать с namespace-ами, чтоб небыло пересечений
       */
      switch (key) {
        // Ключ 'iw-images' используется в темплейте infoWindow для гугл карты
        case 'iw-images':
          temp.push(`
            <div class="iw__img-wrap">
              <div class="iw__img">
                <img src="${item}" alt="Wine Line">
              </div>
            </div>
          `)
          break

        default:
          buffer = buffer.replace(regExp, oKeys[key])
          break
      }
    })

    buffer = buffer.replace(regExp, temp.join(''))
  })

  return buffer
}
