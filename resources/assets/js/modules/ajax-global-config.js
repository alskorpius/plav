import $ from 'jquery'
import {Notifier} from '../classes'

$.ajaxSetup({
  method: 'POST',
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
    'Content-Language': document.documentElement.lang
  },
  dateType: 'json'
})

$(document).ajaxComplete(function (event, jqxhr) {
  let response = jqxhr.responseJSON
  if (!response) return false

  if (typeof response.message === 'string') {
    response.message = {text: response.message}
  }
  let oMessage = Object.assign({
    type: response.success ? 'success' : response.error ? 'error' : 'info',
    timeout: 3000
  }, response.message)
  if (oMessage.text) {
    Notifier.create(oMessage).show()
  }
})
