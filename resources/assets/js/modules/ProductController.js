import $ from 'jquery'
import { Counter } from '../classes'

function addInCartClasses ($element) {
  $element.addClass('in-cart')
}

export function loaderInit ($elements) {
  let routes = window.projectConfig.routes.product
  let cart = window.Cart
  let $productsInCart = $()
  cart.cartPromise.then(cart => {
    let productsInCartIds = cart.items.map(item => item.product.id)
    productsInCartIds.forEach(productId => {
      $productsInCart = $productsInCart.add(
        $elements.filter(`[data-product=${productId}]`))
    })
    addInCartClasses($productsInCart)
  })

  $elements.on('click', '[data-action]', e => {
    let inFavoriteClass = 'in-favorites'
    let inWaitClass = 'in-wait'
    let $product = $(e.delegateTarget)
    let $button = $(e.currentTarget)
    let productId = $product.data('product')
    let priceId = $product.data('price-id')
    let action = $button.data('action')
    let isFavorite = $product.hasClass(inFavoriteClass)
    let isWait = $product.hasClass(inWaitClass)
    let pricesCount = $product.data('prices-count')

    if (action === 'addToCart') {
      if (pricesCount > 1) {
        $.post(routes.prices, {product_id: productId})
          .then(response => {
            if (response.success) {
              $.magnificPopup.open({
                mainClass: 'mfp-fade',
                items: {
                  src: response.html
                },
                fixedContentPos: true,
                fixedBgPos: true,
                callbacks: {
                  open () {
                    this.currItem.inlineElement.on('click', 'button', e => {
                      let $popup = $(e.delegateTarget)
                      let checkedPrice = +$popup.find('input:checked').val()
                      cart.addItem(productId, checkedPrice)
                      addInCartClasses($product)
                      $.magnificPopup.close()
                    })
                  }
                }
              })
            }
          })
      } else {
        cart.addItem(productId, priceId)
        addInCartClasses($product)
      }
    } else if (action === 'addToFavorites') {
      $.post(routes.favorites, {
        product_id: productId
      }).then((response) => {
        if (response.success) {
          let counter = Counter.getCounter('favorites')
          if (isFavorite) {
            counter.decrement()
          } else {
            counter.increment()
          }
          $product.toggleClass(inFavoriteClass, !isFavorite)
        }
      })
    } else if (action === 'addToWait') {
      $.post(routes.wait, {
        product_id: productId
      }).then((response) => {
        if (response.success) {
          $product.toggleClass(inWaitClass, !isWait)
        }
      })
    }
  })
}
