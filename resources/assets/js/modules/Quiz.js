import $ from 'jquery'
import Chart from '../../../../node_modules/chart.js/src/chart'

export default class Quiz {
  /**
   *
   * @param {HTMLElement} element
   * @param {Object} userOptions
   */
  constructor (element, userOptions) {
    /**
     * @type {jQuery|HTMLElement}
     */
    this.$el = $(element)
    /**
     * Параметры по умолчанию
     * @type {{resultSelector: string, canvasSelector: string, colorDataAttrName: string, labelDataAttrName: string}}
     */
    this.defaultOptions = {
      resultSelector: '[data-result]',
      canvasSelector: 'canvas',
      colorDataAttrName: 'color',
      labelDataAttrName: 'label'
    }
    this.options = Object.assign({}, this.defaultOptions, this.userOptions)
    this.$canvas = this.$el.find(this.options.canvasSelector)
    if (!this.$canvas.length) {
      console.error('No canvas element was found')
      return
    }
    this.drawChart()
  }

  /**
   * Собирает, необходимую для построения графика информацию
   * @returns {{data: Array, colors: Array, labels: Array}}
   */
  collectData () {
    let {options} = this
    let $results = this.$el.find(options.resultSelector)
    let collectedData = {
      data: [],
      colors: [],
      labels: []
    }
    $results.each((i, result) => {
      collectedData.labels.push(result.dataset[options.labelDataAttrName])
      collectedData.colors.push(result.dataset[options.colorDataAttrName])
      collectedData.data.push(result.dataset.result)
    })
    return collectedData
  }

  drawChart () {
    let {data, colors, labels} = this.collectData()
    this.chart = new Chart(this.$canvas, {
      type: 'doughnut',
      data: {
        datasets: [
          {
            data,
            backgroundColor: colors,
            borderColor: '#fff',
            borderWidth: 4
          }],
        labels

      },
      options: {
        legend: false
      }
    })
  }
}

export function loaderInit ($elements) {
  $elements.each((i, element) => {
    if ($(element).data('Quiz') instanceof Quiz === false) {
      $(element).data('Quiz', new Quiz(element))
    }
  })
}
