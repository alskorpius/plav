import $ from 'jquery'
import { Counter } from '../classes'

export function loaderInit ($el) {
  let $product = $('.js-init[data-product]')
  let $oldPrice = $('#product-old-price')
  let $price = $('#product-price')
  let $artikul = $('#product-artikul')
  let oldPriceCounter = new Counter('oldPrice', $oldPrice.data('initial'), {
    'prefix': '<span>₴</span>'
  })
  let priceCounter = new Counter('price', $price.data('initial'), {
    'prefix': '<span>₴</span>'
  })
  oldPriceCounter.linkElement($oldPrice[0])
  priceCounter.linkElement($price[0])
  $el.on('change', 'input', e => {
    let {oldPrice, price, artikul, priceId} = $(e.currentTarget).data()
    let hasSale = oldPrice !== price
    oldPriceCounter.value = oldPrice
    priceCounter.value = price

    $oldPrice.toggle(hasSale)
    $price.toggleClass('product__price--sale', hasSale)
    $artikul.text(artikul)
    $product.data('price-id', priceId)
  })
}
