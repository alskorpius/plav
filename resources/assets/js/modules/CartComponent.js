import $ from 'jquery'
import Vue from 'vue'

import { Counter, Notifier } from '../classes'
import CartItem from '../vue/cart/CartItem.vue'
import Debounce from 'lodash.debounce'

let routes = window.projectConfig.routes.cart
let $body = $('body')
let $cartButton = $('#cart-button')
let $cartCounterElement = $cartButton.children('span').children('span')
Vue.component('cart-item', CartItem)
window.Cart = new Vue({
  name: 'cart',
  components: {
    CartItem
  },
  data () {
    return {
      cart: {},
      routes,
      counter: new Counter('cart', 0, {prefix: '₴'}),
      cartPromise: null,
      deliveryCost: 0
    }
  },
  created () {
    this.fetchCart().then(() => {
      $cartButton.attr('disabled', false).removeClass('is-loading')
    })
    this.counter.linkElement($cartCounterElement[0])
  },
  computed: {
    deliveryCostSafe () {
      let dc = parseInt(this.deliveryCost)
      return isNaN(dc) ? 0 : dc
    },
    total () {
      return this.totalPrice + this.deliveryCostSafe
    },
    items () {
      return this.cart.items || []
    },
    totalPrice () {
      return this.cart.total_amount || 0
    },
    totalWeight () {
      return this.cart.total_weight ? this.cart.total_weight.toFixed(2) : 0
    }
  },
  methods: {

    fetchCart () {
      this.cartPromise = $.post(routes.get).then(this.processResponse)
      return this.cartPromise
    },
    addItem (productId, priceId, count = 1) {
      return $.post(routes.add, {
        product_id: productId,
        price_id: priceId,
        count: count
      }).then(this.processResponse)
    },
    modifyItem: Debounce(function (cartItemId, count) {
      $.post(routes.modify, {
        cartItemId: cartItemId,
        count
      }).then(this.processResponse)
    }, 300),
    removeItem (cartItemId) {
      $.post(routes.remove, {
        cartItemId: cartItemId
      }).then(this.processResponse)
    },
    processResponse (response) {
      if (response.cart) {
        this.cart = {...response.cart}
        this.counter.value = response.cart.total_amount
        return response.cart
      }
    }
  }
})

window.popupCart = new Vue({
  data () {
    return {
      trigger: $cartButton,
      isOpen: false,
      rootCart: window.Cart
    }
  },
  created () {
    this.trigger.on('click', () => {
      if (this.rootCart.items.length) {
        this.toggle()
      } else {
        Notifier.info(window.getLocale() === 'ru' ? 'В вашей корзине еще нет товаров' : 'There are no products in your shopping cart yet.')
      }
    })
  },
  methods: {
    toggle () {
      if (this.isOpen) {
        this.close()
      } else {
        this.open()
      }
    },
    open () {
      this.isOpen = true
      $body.addClass('cart-is-open')
    },
    close () {
      this.isOpen = false
      $body.removeClass('cart-is-open')
    }
  }
}).$mount($('#cart')[0])

let $checkoutCart = $('#checkout-cart')
if ($checkoutCart.length) {
  window.checkoutCart = new Vue({
    data () {
      return {
        rootCart: window.Cart
      }
    }
  }).$mount($checkoutCart[0])
}
$(document).ready(function () {
  window.Cart.deliveryCost = $('[data-cart-delivery-cost]:checked')
    .eq(0)
    .data('cartDeliveryCost')
})
$('#order-steps').on('click', '[data-cart-delivery-cost]', e => {
  window.Cart.deliveryCost = e.currentTarget.dataset.cartDeliveryCost
})
