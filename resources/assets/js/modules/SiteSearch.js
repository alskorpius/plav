import $ from 'jquery'
import LiveSearch from '../classes/LiveSearch'

export default function siteSearch ($root) {
  let $openTrigger = $root.find('[data-mark="site-search-open"]')
  let $closeTrigger = $root.find('[data-mark="site-search-close"]')
  let $searchPanel = $root.find('[data-mark="site-search-panel"]')
  let $form = $searchPanel.find('form')
  let $input = $root.find('[data-mark="site-search-input"]')
  let $suggestionsPanel = $root.find('[data-mark="site-search-suggestions-panel"]')
  let $suggestionsList = $root.find('[data-mark="site-search-suggestions-list"]')
  let $suggestionsItem = $root.find('[data-mark="site-search-suggestions-item"]').detach()
  let css = {
    isOpen: 'is-open',
    hasResult: 'has-result',
    siteSearchIsOpen: 'site-search-is-open'
  }

  let siteSearch = new LiveSearch({
    url: window.projectConfig.routes.siteSearch,
    $root: $root,
    $form: $form,
    $input: $input,
    $suggestionsPanel: $suggestionsPanel,
    $suggestionsList: $suggestionsList,
    $suggestionsItem: $suggestionsItem.removeAttr('data-mark')
  })

  $openTrigger.on('click', e => {
    e.preventDefault()
    $root.addClass(css.siteSearchIsOpen)
    $searchPanel.addClass(css.isOpen)
  })

  $closeTrigger.on('click', e => {
    e.preventDefault()
    $root.removeClass(css.siteSearchIsOpen)
    $searchPanel.removeClass(css.isOpen)
  })
}
