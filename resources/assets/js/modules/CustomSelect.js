import $ from 'jquery'
import 'chosen-js'

export function loaderInit ($selects) {
  $selects.each((i, select) => {
    let $select = $(select)
    $select.chosen({})
  })
}
