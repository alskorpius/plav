import $ from 'jquery'

export default class Accordion {
  constructor (root, params = {}) {
    let $root = $(root)
    let defaults = {
      slideDuration: 400,
      activeClass: 'is-open',
      mode: 'default'
    }
    let inlineParams = $root.data('accordion') || {}

    this.$root = $root
    this.$items = $root.find('[data-accordion="item"]')
    this.params = $.extend(true, {}, defaults, params, inlineParams)

    this.init()
  }

  init () {
    this.$items.each((i, item) => {
      let $item = $(item)
      let $trigger = $item.find('[data-accordion="trigger"]')
      let $content = $item.find('[data-accordion="content"]')

      $item.add($trigger).add($content).data({
        'item': $item,
        'trigger': $trigger,
        'content': $content
      })

      if (!$item.hasClass(this.params.activeClass)) {
        $item.data('content').hide()
      }

      this.listeners($item)
    })
  }

  listeners ($item) {
    $item.data('trigger').on('click', (e) => {
      if (/^label|input$/.test(e.target.tagName.toLowerCase())) {
        return
      }

      this.toggle($item)

      switch (this.params.mode) {
        case 'only_one':
          this.onOnlyOne($item)
          break

        default:
          break
      }
    })
  }

  open ($item) {
    $item.addClass(this.params.activeClass)
    $item.data('content').slideDown(this.params.slideDuration)
  }

  close ($item) {
    $item.removeClass(this.params.activeClass)
    $item.data('content').slideUp(this.params.slideDuration)
  }

  toggle ($item) {
    if ($item.hasClass(this.params.activeClass)) {
      this.close($item)
    } else {
      this.open($item)
    }
  }

  onOnlyOne ($currentItem) {
    this.$items
      .filter(function () {
        return this !== $currentItem[0]
      })
      .each((i, rest) => {
        let $rest = $(rest)
        this.close($rest)
      })
  }
}

export function loaderInit ($elements) {
  $elements.each((i, element) => {
    let $element = $(element)
    if ($element.data('Accordion') instanceof Accordion === false) {
      $element.data('Accordion', new Accordion(element))
    }
  })
}
