import $ from 'jquery'

export function init ($root) {
  _spread($root)

  function _spread ($root) {
    let selection = $root.data('equal-height')
    if (!Array.isArray(selection)) selection = [selection]
    selection.forEach(selector => {
      _equalAction($root.find(selector))
    })
  }
}

function _equalAction ($items) {
  _equalize()

  $(window).on('resize', function () {
    _equalize()
  })

  function _equalize () {
    $items.height('initial')

    let maxH = $items.eq(0).height()

    $items.each((i, node) => {
      let $node = $(node)
      maxH = ($node.height() > maxH) ? $node.height() : maxH
    })

    $items.height(maxH)
  }
}

export function loaderInit ($containers) {
  $containers.each((i, container) => {
    init($(container))
  })
}
