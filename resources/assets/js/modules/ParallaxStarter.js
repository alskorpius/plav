import $ from 'jquery'
import Parallax from 'parallax-js'

export function loaderInit ($parallaxContainers) {
  $parallaxContainers.each((i, container) => {
    let parallaxInstance = new Parallax(container, {
      selector: '[data-depth]',
      relativeInput: true,
      hoverOnly: true,
      clipRelativeInput: true,
      invertX: true,
      invertY: true
    })
    $(container).data('parallaxInstance', parallaxInstance)
  })
}
