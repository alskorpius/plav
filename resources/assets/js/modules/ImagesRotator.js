import $ from 'jquery'

export default class ImagesRotator {
  /**
   *
   * @param {HTMLElement} element
   * @param {Object} userOptions
   */
  constructor (element, userOptions) {
    /**
     * @type {jQuery|HTMLElement}
     */
    this.$el = $(element)

    /**
     * Параметры по умолчанию
     * @type {{imageContainerSelector: string, imagesArrayDataAttr: string}}
     */
    this.defaultOptions = {
      imageContainerSelector: '[data-image]',
      imagesArrayDataAttr: 'images-rotator',
      interval: 2000
    }
    this.options = Object.assign({}, this.defaultOptions, this.userOptions)

    this.$containers = this.$el.find(this.options.imageContainerSelector)
    this.images = this.getImages()

    this.lastImageIndex = 0
    this.lastConteinerIndex = 0
    this.drawInitialImages()
    setInterval(() => {
      this.changeImage()
    }, this.options.interval)
  }

  drawInitialImages () {
    this.$containers.each((i, container) => {
      this.setImage(container, this.images[this.lastImageIndex++])
    })
  }

  getImages () {
    return this.$el.data(this.options.imagesArrayDataAttr)
  }

  changeImage () {
    if (this.lastConteinerIndex >= this.$containers.length) {
      this.lastConteinerIndex = 0
    }
    if (this.lastImageIndex >= this.images.length) {
      this.lastImageIndex = 0
    }
    this.setImage(this.$containers.eq(this.lastConteinerIndex++),
      this.images[this.lastImageIndex++])
  }

  setImage ($container, imageUrl) {
    $($container).css('background-image', `url(${imageUrl})`)
  }
}

export function loaderInit ($elements) {
  $elements.each((i, element) => {
    if ($(element).data('ImagesRotator') instanceof ImagesRotator === false) {
      $(element).data('ImagesRotator', new ImagesRotator(element))
    }
  })
}
