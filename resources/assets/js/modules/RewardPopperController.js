import $ from 'jquery'
import Popper from 'popper.js'

export default class RewardPopper {
  constructor (root) {
    this.$root = $(root)
    this.$layout = $('.layout')
    this.$reference = this.$root.find('[data-reference]')
    this.$popper = this.$root.find('[data-popper]')

    this.init()
  }

  init () {
    this.popperInstance = new Popper(this.$reference.get(0), this.$popper.get(0), {
      placement: 'bottom',
      modifiers: {
        flip: {
          behavior: ['left', 'right']
        },
        preventOverflow: {
          boundariesElement: this.$layout.find('.product').get(0)
        }
      }
    })
  }
}

export function loaderInit ($elements) {
  $elements.each((i, element) => {
    let $element = $(element)
    if ($element.data('RewardPopper') instanceof RewardPopper === false) {
      $element.data('RewardPopper', new RewardPopper(element))
    }
  })
}
