import { scrollToElement } from './helpers'
import $ from 'jquery'

export function loaderInit ($elements) {
  $elements.on('click', function (e) {
    e.preventDefault()
    let $trigger = $(e.currentTarget)
    scrollToElement($trigger.data('scroll-to'), {
      speed: $trigger.data('speed')
    })
  })
}
