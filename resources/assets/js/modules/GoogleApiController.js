import $ from 'jquery'

let GoogleApiLoaded = import('load-google-maps-api-2').then((module) => {
  let lang = document.documentElement.lang.split('-').pop().toLowerCase()
  let googleApiKey = window.projectConfig.googleApiKey

  if (googleApiKey) {
    module.key = googleApiKey
    module.language = lang === 'ua' ? 'uk' : lang
  } else {
    console.warn('No api key in project config')
    return
  }

  return module()
})

export function loaderInit ($elements) {
  GoogleApiLoaded.then(() => {
    $elements.each((i, element) => {
      let $element = $(element)
      let moduleName = $element.data('module')

      import('./' + moduleName).then((module) => {
        module.default($element)
        if ($element.data('google-api') !== 'is-inited') {
          $element.data('google-api', 'is-inited')
        }
      }).catch((err) => {
        console.error('Модуль не загружен \n', err)
      })
    })
  })
}
