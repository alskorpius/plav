import $ from 'jquery'
import GoogleMap from '../classes/GoogleMap'

export default function ($root) {
  let map = null
  let currOpts = {}
  let $filter = $('.filter')
  let $parent = $filter.parent()
  let $triggerOpen = $parent.find('[data-filter="trigger-open"]')
  let $triggerClose = $parent.find('[data-filter="trigger-close"]')

  GoogleMap.fetchData(window.projectConfig.routes.map.whereBuy)
    .done((res) => {
      res = (typeof res === 'string') ? JSON.parse(res) : res

      currOpts.rawMarkers = res.markers
      currOpts.center = res.center
      currOpts.zoom = res.zoom

      map = new GoogleMap($root.get(0), {
        center: res.center,
        zoom: res.zoom
      })

      map.prepareMarkers(currOpts.rawMarkers)
      filterMarkers()
      setTimeout(function () {
          map.map.setZoom(res.zoom);
          //map.map.setCenter(res.center);
      }, 1000)
    })
    .fail((err) => {
      console.error('GoogleMap fetchData failed \n', err)
    })
    .always((res) => {})

  $filter.on('change', 'input[type="checkbox"]', function () {
    filterMarkers()
  })

  $parent.on('click', e => {
    e.stopPropagation()
  })

  $triggerOpen.on('click', e => {
    $parent.addClass('filter-is-open')
    $filter.addClass('is-open')
  })

  $triggerClose.on('click', e => {
    $parent.removeClass('filter-is-open')
    $filter.removeClass('is-open')
  })

  $(document).on('click', e => {
    $parent.removeClass('filter-is-open')
    $filter.removeClass('is-open')
  })

  function filterMarkers () {
    let oChecked = {
      regions: [],
      locations: [],
      lines: []
    }
    let shownMarkers = []

    /**
     * Наполняем объект `oChecked` чекнутыми значениями
     */
    Object.keys(oChecked).forEach((filterBy) => {
      $filter
        .find(`[data-filter-by="${filterBy}"]`)
        .find('input[type="checkbox"]')
        .each((i, input) => {
          let name = input.name

          if (input.checked && name) {
            oChecked[filterBy].push(name)
          }
        })
    })

    /**
     * Проверяем наполнение.
     * Например, если массив с локациями пустой (юзер не чекнул ни одной локации),
     * то удаляем этот ключ из объекта, тем самым исключая этот ключ из проверки
     */
    Object.keys(oChecked).forEach((filterBy) => {
      if (!oChecked[filterBy].length) {
        delete oChecked[filterBy]
      }
    })

    map.readyMarkers.forEach((marker) => {
      /**
       * Если объект с ключами пустой совсем (юзер не чекнул ни одного значения или при загрузке страницы ничего не выбрано по-умолчанию),
       * то удаляем маркеры с карты и ничего не показываем
       */
      if (!Object.keys(oChecked).length) {
        map.removeMarker(marker)
        return
      }

      /**
       * Фильтруем маркеры по выбранным условиям
       * @type {boolean}
       */
      let allConditionsIsOk = Object.keys(oChecked).every(condition => {
        let consilience = false

        if (!Array.isArray(oChecked[condition])) {
          oChecked[condition] = [oChecked[condition]]
        }

        if (!Array.isArray(marker[condition])) {
          marker[condition] = [marker[condition]]
        }

        oChecked[condition].forEach(oCheckedInnerCondition => {
          marker[condition].forEach((markerInnerCondition) => {
            if (markerInnerCondition === oCheckedInnerCondition) {
              consilience = true
            }
          })
        })

        return consilience
      })

      if (allConditionsIsOk) {
        map.setMarker(marker)
        shownMarkers.push(marker)
      } else {
        map.removeMarker(marker)
      }
    })

    map.fit(shownMarkers)
  }
}
