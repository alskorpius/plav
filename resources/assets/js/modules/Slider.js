import $ from 'jquery'
import 'slick-carousel'

export default class Slider {
  constructor (root) {
    let $root = $(root)
    this.presets = {
      cards: {
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        prevArrow: $root.find('[data-slider="control-prev"]').get(0),
        nextArrow: $root.find('[data-slider="control-next"]').get(0),
        responsive: this.getResponsiveParams(350 * 1.75, 4) // 1.85 - коэффициент от балды, чтоб фотки не налазили друг на друга при адаптиве
      },
      product: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true
      }
    }

    this.$root = $root
    this.preset = $root.data('preset')
    this.sliderContent = $root.find('[data-slider="content"]')

    this.init()
  }

  init () {
    this.sliderContent.slick(this.presets[this.preset])

    if (this.preset === 'product') {
      this.$root.find('.slick-dots').wrap('<div class="product__slider-controls"/>')
      this.sliderControls = this.$root.find('.product__slider-controls')
      this.sliderControls.prepend(`<div class="product__slides-counter" />`)
      this.slidesCounter = this.$root.find('.product__slides-counter')

      this.setSlidesCounter(this.sliderContent.slick('slickCurrentSlide'), this.sliderContent.slick('getSlick').$slides.length)

      this.sliderContent.on('afterChange', (e, slick, currentSlide) => {
        this.setSlidesCounter(currentSlide, slick.$slides.length)
      })
    }
  }

  getResponsiveParams (slideWidth, maxSlideCount) {
    let arrResponsiveParams = []
    for (let i = 1; i <= maxSlideCount; i++) {
      arrResponsiveParams.push({
        breakpoint: slideWidth * i,
        settings: {
          slidesToShow: i
        }
      })
    }
    return arrResponsiveParams
  }

  setSlidesCounter (currentSlide, totalSlides) {
    this.slidesCounter.html(`${currentSlide + 1}/${totalSlides}`)
  }
}

export function loaderInit ($elements) {
  $elements.each((i, element) => {
    let $element = $(element)
    if ($element.data('Accordion') instanceof Slider === false) {
      $element.data('Accordion', new Slider(element))
    }
  })
}
