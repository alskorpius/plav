@extends('layouts.app')
<?php

$initialSlideName = isset($initialSlideName) ? $initialSlideName : null;
?>
<style>
    .layout__header {
        height: 4rem !important;
        background-color: rgba(255, 255, 255, 0.7) !important;
    }

    .layout__header:hover {
        background-color: rgba(255, 255, 255, 0.9) !important;
    }

    .nav-menu-is-open .layout__header {
        background-color: transparent !important;
    }

    .cart-is-open .layout__header {
        background-color: #fff !important;
    }

    .cart {
        top: 4rem !important;
    }

    .site-search__main {
        padding: 6px 8px !important;
    }

    .layout__body {
        padding: 0 !important;
    }

    .side-panel ul li.line {
        background-color: #fff !important;
    }

    .side-panel ul a {
        color: #fff !important;
    }

    .side-panel ul li:hover a:not(.navigation-menu-trigger) {
        color: #71dbd4 !important;
    }

    .navigation-menu-burger__inner > span:before {
        background-color: #fff !important;
    }

    .layout__header .navigation-menu-burger__inner > span:before {
        background-color: #000 !important;
    }

    .nav-menu-is-open .side-panel ul li.line {
        background-color: #000 !important;
    }

    .nav-menu-is-open .side-panel ul a {
        color: #000 !important;
    }

    .nav-menu-is-open .side-panel ul li:hover a:not(.navigation-menu-trigger) {
        color: #71dbd4 !important;
    }

    .nav-menu-is-open .navigation-menu-burger__inner > span:before {
        background-color: #000 !important;
    }

    .hero-screen__square-holder {
        display: none;
    }

    .hero-decoration {
        z-index: 0;
        background-image: url("/images/hero-inner-bg1.png");
        background-repeat: no-repeat;
        background-size: cover;
        background-position: 50% 50%;
    }

    .hero-decoration:after {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        z-index: 0;
        display: block;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .3);
    }

    .hero-screen__links {
        width: 100%;
        font-weight: 700;
        position: absolute;
        left: 50%;
        bottom: 7vmin;
        transform: translateX(-50%);
        pointer-events: auto;
        display: flex;
        align-items: center;
        justify-content: center;
        color: #fff;
    }

    .hero-screen__links .button {
        color: #fff !important;
    }

    .hero-screen__links .button--decorated::after {
        border-bottom-color: #fff !important;
    }

    @media screen and (min-height: 640px) and (min-width: 640px) {
        .hero-screen__links {
            bottom: 10vmin;
        }
    }

    .hero-screen__links span {
        text-align: center;
        flex-shrink: 0;
        padding: 0 20px;
    }

    @media only screen and (max-width: 480px) {
        .hero-screen__links {
            flex-wrap: wrap;
        }

        .hero-screen__links span {
            flex-basis: 100%;
        }
    }

    .draw {
        /*background-color: #e1e3e4;*/
    }

    .hero-screen__description--404 {
        color: #fff;
        font-family: "Century Gothic", sans-serif;
        font-size: 1em;
        font-weight: 700;
        height: 2em;
        text-transform: uppercase;
        text-align: center;
        position: absolute;
        top: 0;
        bottom: 0;
        right: 0;
        left: 0;
        margin: auto;
        transform: translateY(8.5em);
    }
</style>
@section('content')
    <div id="hero-slider" class="hero-slider" data-initial-slide-name="{{$initialSlideName}}">
        <div class="hero-decoration"></div>
        <div class="hero-slider__slides" data-hero-slides>
            @component('site.ui.hero-screen.slide',[
                'circleBackground'=>'/images/hero-inner-bg1.png',
                'outerLogo'=>'404',
                'screenTheme'=>'aqua',
                'screenName'=>'about',
                'isInitial'=>true,
            ])
                @slot('decorItems')
                    {{--<div data-depth="0.1" class="hero-screen__parallax-item">
                        <div class="hero-screen__parallax-inner leaf leaf--1">
                            <img src="/images/leaf1.png" alt="leaf">
                        </div>
                    </div>
                    <div data-depth="0.1" class="hero-screen__parallax-item">
                        <div class="hero-screen__parallax-inner leaf leaf--2">
                            <img src="/images/leaf2.png" alt="leaf"></div>
                    </div>
                    <div data-depth="0.1" class="hero-screen__parallax-item">
                        <div class="hero-screen__parallax-inner leaf leaf--3">
                            <img src="/images/leaf3.png" alt="leaf">
                        </div>
                    </div>--}}
                    <div data-depth="0.1" class="hero-screen__parallax-item">
                        <div class="hero-screen__description hero-screen__description--404">
                            <span>К сожалению,<br> страница не найдена!</span>
                        </div>
                    </div>
                @endslot
                @slot('links')
                    <div class="hero-screen__links">
                        @include('site.ui.button', [
                            'tag'=>'a',
                           'text'=>'Вернитесь на главную',
                           'class'=>'button',
                           'href'=>route('site.index'),
                           'modifiers' => ['decorated', 'decorated-line-only', 'decorated-line-left']
                       ])
                        <span>или</span>
                        @include('site.ui.button', [
                            'tag'=>'a',
                           'text'=>'Перейдите в каталог',
                           'class'=>'button',
                           'href'=>route('products'),
                           'modifiers' => ['decorated', 'decorated-line-only', 'decorated-line']
                       ])
                    </div>
                @endslot
            @endcomponent
        </div>
    </div>
@endsection
