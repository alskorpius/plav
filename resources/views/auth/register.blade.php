@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="panel-heading">Register</div>

        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
            @include('admin.widgets.form.input', ['type' => 'email', 'name' => 'email', 'label' => 'Email', 'options' => ['required']])
            @include('admin.widgets.form.input', ['type' => 'password', 'name' => 'password', 'label' => 'Пароль', 'options' => ['required']])
            @include('admin.widgets.form.input', ['type' => 'password', 'name' => 'confirmPassword', 'label' => 'Повторите пароль', 'options' => ['required']])
            <button type="submit">Submit</button>
        </form>
    </div>
@endsection
