@extends('layouts.app')

@section('content')
<div class="container">
    @include('site.ui.heading',['size'=>1,'text'=>__('profile.reset-pass')])

    <form action="{{ route('password.email.custom') }}" id="form--pass-reset" class="form form--pass-reset" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <input required class="form-control" id="email" type="email" name="email">
            <label class="form-label" for="email">{{ __('profile.email') }}</label>
            <div class="line"></div>
        </div>
        <div class="_flex _justify-center _mt-def">
            @include('site.ui.button',[
                'type'=>'submit',
                'text'=>__('profile.reset-pass'),
                'modifiers'=>'default',
                'attrs' => [
                    "onclick" => "ga('send', 'event', 'button', 'reset-password'); return true;"
                ]
            ])
        </div>
    </form>
</div>
@endsection
