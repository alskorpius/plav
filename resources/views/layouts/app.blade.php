@php
$scripts = isset($scripts) ? $scripts : []
@endphp

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    @section('ogimage')
        <meta property="og:image" content="https://{{$_SERVER['HTTP_HOST']}}/images/main-logo.jpg"/>
        <meta property="og:image:url" content="http://{{$_SERVER['HTTP_HOST']}}/images/main-logo.jpg"/>
        <meta property="og:image:type" content="image/jpeg"/>
        <meta property="og:image:width" content="1200"/>
        <meta property="og:image:height" content="630"/>
    @show

    @if((isset($noIndex) && $noIndex == true) || (stristr(request()->fullUrl(), 'cena-ot_') || stristr(request()->fullUrl(), 'cena-do_')))
        <meta name="robots" content="noindex,nofollow">
    @endif

<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! \App\Helpers\Meta::generate() !!}
    <script>
        window.projectConfig = {
            assets: '{{ asset('') }}',
            routes: {
                cart: {
                    get: "{{route('cart.get-cart')}}",
                    add: "{{route('cart.add')}}",
                    modify: "{{route('cart.count-items')}}",
                    remove: "{{route('cart.remove-item')}}",
                    order: "{{route('ordering')}}"
                },
                product: {
                    prices: "{{route('product.get-prices')}}",
                    favorites: "{{route('product.favorites')}}",
                    wait: "{{route('add-wait')}}"
                },
                map: {
                    whereBuy: '{{route('where-buy.spots')}}'
                },
                siteSearch: '{{route('live-search')}}'
            },
            isAuth: "{{ Auth::check() ? 0 : 1 }}",
            googleApiKey: 'AIzaSyBbk9aLcghUiNtlHjlavPeVh7dOiHs4yuw'
        }
    </script>
    @include('site.widgets.favicon')
<!-- Styles -->
    @include('site.widgets.styles')
    @stack('styles')
    @foreach(array_get($scripts, 'head', []) as $scriptHead)
        {!! $scriptHead !!}
    @endforeach
</head>
<body class="{{implode(' ', $bodyClass)}}">
@foreach(array_get($scripts, 'body', []) as $scriptHead)
    {!! $scriptHead !!}
@endforeach
@include('site.widgets.preloader', ["id"=>"preloader"])
@if(isset($isAdult) === false || $isAdult === true)
    @include('site.widgets.navigation-menu')
    @include('site.widgets.left_panel')
    @include('site.widgets.right_panel', ['pages' => $pages])
@endif
<div class="layout">
    @if(isset($showHeader) === false || $showHeader === true)
        @include('site.widgets.header')
    @endif
    @yield('beforeContent')
    @if(isset($isAdult) === false || $isAdult === true)
        <div class="layout__body">
            @yield('content')
        </div>
    @endif
    @if(isset($showFooter) === false || $showFooter === true)
        @include('site.widgets.footer')
    @endif
</div>
@if(isset($isAdult) === false || $isAdult === true)
    @include('site.widgets.cart')
    <div style="display: none;">
        @include('site.ui.popups.login')
        @include('site.ui.popups.sign-up')
    </div>
    <a href="#" class="scroll-up js-init" data-scroll-to="head" data-speed="4000" title="{{ __('custom.scrollup') }}" rel="nofollow">{{ __('custom.scrollup') }}</a>
@endif
@foreach(array_get($scripts, 'footer', []) as $scriptHead)
    {!! $scriptHead !!}
@endforeach
<!-- Scripts -->
@include('site.widgets.scripts')
@stack('scripts')
</body>
</html>
