<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"
      style="background-color:#f8f8f8 !important;background-image:none !important;background-repeat:repeat !important;background-position:top left !important;background-attachment:scroll !important;">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title></title>
</head>
<body
    style="width:100% !important;min-width:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:1.3;background-color:#f8f8f8 !important;background-image:none !important;background-repeat:repeat !important;background-position:top left !important;background-attachment:scroll !important;">
<span class="preheader"
      style="display:none !important;visibility:hidden;mso-hide:all !important;font-size:1px;color:#f3f3f3;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;"></span>
<table class="body"
       style="border-spacing:0;border-collapse:collapse;vertical-align:top;background-color:#f8f8f8 !important;background-image:none !important;background-repeat:repeat !important;background-position:top left !important;background-attachment:scroll !important;height:100%;width:100%;color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:1.3;">
    <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
        <td class="center" align="center" valign="top"
            style="word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:1.3;">
            <center data-parsed="" style="width:100%;min-width:580px;">
                <table align="center" class="wrapper header float-center"
                       style="border-spacing:0;border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#f8f8f8;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;Margin:0 auto;float:none;text-align:center;">
                    <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                        <td class="wrapper-inner"
                            style="word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:1.3;padding-top:20px;padding-bottom:20px;padding-right:20px;padding-left:20px;">
                            <table align="center" class="container"
                                   style="border-spacing:0;border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;width:580px;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;Margin:0 auto;text-align:inherit;background-color:transparent;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;">
                                <tbody>
                                <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                                    <td style="word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:1.3;">
                                        <table class="row collapse"
                                               style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;width:100%;position:relative;display:table;">
                                            <tbody>
                                            <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                                                <th class="small-12 large-6 columns first"
                                                    style="vertical-align:middle;color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;text-align:left;font-size:16px;line-height:1.3;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;Margin:0 auto;padding-bottom:0;padding-right:0;padding-left:0;width:290px;">
                                                    <table
                                                        style="border-spacing:0;border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                                                        <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                                                            <th style="color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:1.3;">
                                                                <img src="{{$_SERVER['HTTP_HOST']}}/images/logo.png"
                                                                     style="outline-style:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:auto;max-width:50%;clear:both;display:inline-block;" alt="">
                                                            </th>
                                                        </tr>
                                                    </table>
                                                </th>
                                                <th class="small-12 large-6 columns last"
                                                    style="vertical-align:middle;color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;text-align:left;font-size:16px;line-height:1.3;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;Margin:0 auto;padding-bottom:0;padding-right:0;padding-left:0;width:290px;">
                                                    <table
                                                        style="border-spacing:0;border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                                                        <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                                                            <th style="color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:1.3;">
                                                                <p class="text-right"
                                                                   style="font-weight:normal;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-right:0;margin-left:0;Margin:0;margin-bottom:10px;Margin-bottom:10px;line-height:1.8;font-family:'Century Gothic' !important;font-size:14px;text-align:right;color:#fff;padding-top:15px;">
                                                                    <a href="{{$_SERVER['HTTP_HOST']}}"
                                                                       style="font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;line-height:1.3;color:#00dcd5;font-size:14px;text-decoration:underline;">Посетить
                                                                        сайт</a></p>
                                                            </th>
                                                        </tr>
                                                    </table>
                                                </th>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
                <table align="center" class="container float-center"
                       style="border-spacing:0;border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#fefefe;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;width:580px;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;Margin:0 auto;float:none;text-align:center;">
                    <tbody>
                    <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                        <td style="word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:1.3;">
                            <table class="wrapper secondary" align="center"
                                   style="border-spacing:0;border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                                <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                                    <td class="wrapper-inner"
                                        style="word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:1.3;">
                                        @yield('content')
                                        <hr>
                                        <table class="row"
                                               style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;width:100%;position:relative;display:table;">
                                            <tbody>
                                            <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                                                <th class="small-12 large-6 columns first"
                                                    style="color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;text-align:left;font-size:16px;line-height:1.3;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;Margin:0 auto;padding-bottom:16px;width:274px;padding-left:8px;padding-right:8px;">
                                                    <table
                                                        style="border-spacing:0;border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                                                        <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                                                            <th style="color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:1.3;">

                                                            </th>
                                                        </tr>
                                                    </table>
                                                </th>
                                                <th class="small-12 large-6 columns last"
                                                    style="color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:10px;text-align:left;font-size:16px;line-height:1.3;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;Margin:0 auto;padding-bottom:16px;width:274px;padding-left:8px;padding-right:8px;">
                                                    <table
                                                        style="border-spacing:0;border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                                                        <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                                                            <th style="color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:1.3;">
                                                                <table
                                                                    style="border-spacing:0;border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                                                                    <tr style="padding-top:10px;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;">
                                                                        <td class="text-right"
                                                                            style="word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;font-size:16px;line-height:1.3;text-align:right;">
                                                                            <a href="{{config('db.socials.fb')}}"
                                                                               style="font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;line-height:1.3;color:#00dcd5;font-size:14px;text-decoration:underline;"><img
                                                                                    src="{{$_SERVER['HTTP_HOST']}}/images/fb.png" alt="{{$_SERVER['HTTP_HOST']}}/images/fb.png"
                                                                                    style="outline-style:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:auto;max-width:100%;clear:both;display:inline-block;border-style:none;"></a>
                                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            <a href="{{config('db.socials.instagram')}}"
                                                                               style="font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;line-height:1.3;color:#00dcd5;font-size:14px;text-decoration:underline;"><img
                                                                                    src="{{$_SERVER['HTTP_HOST']}}/images/inst.png" alt="{{$_SERVER['HTTP_HOST']}}/images/inst.png"
                                                                                    style="outline-style:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:auto;max-width:100%;clear:both;display:inline-block;border-style:none;"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            <a href="{{config('db.socials.youtube')}}"
                                                                               style="font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;line-height:1.3;color:#00dcd5;font-size:14px;text-decoration:underline;"><img
                                                                                    src="{{$_SERVER['HTTP_HOST']}}/images/youtube.png" alt="{{$_SERVER['HTTP_HOST']}}/images/youtube.png"
                                                                                    style="outline-style:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:auto;max-width:100%;clear:both;display:inline-block;border-style:none;"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                    </table>
                                                </th>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </center>
        </td>
    </tr>
</table>
<!-- prevent Gmail on iOS font size manipulation -->
<div
    style="display:none;white-space:nowrap;font-style:normal;font-variant:normal;font-weight:normal;font-size:15px;font-family:courier;line-height:0;">
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
</body>
</html>

