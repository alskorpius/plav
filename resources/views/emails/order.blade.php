@php
/** @var \App\Models\Order\Order $order */
@endphp

@extends('emails.layouts.main')

@section('content')
    <table align="center" class="container border-shadow border-shadow-1" style="margin:0 auto;background:#fff;border:1px solid #f3f3f3;border-collapse:collapse;border-spacing:0;margin:0;padding:0;text-align:inherit;vertical-align:top;width:586px">
        <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:20px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                <table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                    <tbody>
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:0;padding-bottom:10px;padding-left:25px;padding-right:25px;text-align:left;width:570px">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tr style="padding:0;text-align:left;vertical-align:top">
                                    <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left">
                                        <span class="content-text" style="color:#6d6d6d">{!! isset($text) ? $text : null !!}</span>
                                    </th>
                                    <th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
@stop
