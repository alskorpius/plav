@php
/** @var \App\Models\Order\Order $order */
@endphp
@include('emails.particials.spacer')
<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
    <tbody>
    <tr style="padding:0;text-align:left;vertical-align:top">
        <th class="small-12 large-12 columns first last"
            style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:0;padding-bottom:10px;padding-left:25px;padding-right:25px;text-align:left;width:570px">
            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                <tr style="padding:0;text-align:left;vertical-align:top">
                    <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left">
                        <table align="center" class="container table table--checkout-products" style="Margin:0 auto;background:#fff;border-collapse:collapse;border-spacing:0;margin:0 auto;padding:0;text-align:inherit;vertical-align:top;width:100%">
                            <tbody>
                            <tr style="padding:0;text-align:left;vertical-align:top">
                                <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:20px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                                    @foreach($order->items AS $key => $item)
                                        @include('emails.particials.order-item', ['item' => $item])
                                        @if($key + 1 < sizeof($order->items))
                                            <hr style="border-color:#f3f3f3;border-width:1px">
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </th>
                </tr>
            </table>
        </th>
    </tr>
    </tbody>
</table>
@include('emails.particials.spacer')
