@include('emails.particials.spacer')
@if(config('db.contacts.footer-phone') || config('db.db.contacts.phone'))
	<center align="center" class="float-center" style="min-width:586px;width:100%">
		<table align="center" class="menu small-vertical float-center"
			   style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:auto!important">
			<tr style="padding:0;text-align:left;vertical-align:top">
				<td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:20px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
					<table
						style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top">
						<tr style="padding:0;text-align:left;vertical-align:top">
							@if(config('db.contacts.phone'))
								<th class="menu-item phone-number float-center"
									style="Margin:0 auto;color:#0a0a0a;float:none;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:5px;padding-right:5px;text-align:center">
									<a href="tel:{{ preg_replace('/[^0-9]*/', '', config('db.contacts.phone')) }}"
									   style="Margin:0;color:#000;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left;text-decoration:none">
										{{ config('db.contacts.phone') }}
									</a>
								</th>
							@endif
							@if(config('db.contacts.footer-phone'))
								<th class="menu-item phone-number float-center"
									style="Margin:0 auto;color:#0a0a0a;float:none;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:5px;padding-right:5px;text-align:center">
									<a href="tel:{{ preg_replace('/[^0-9]*/', '', config('db.static.phone-2')) }}"
									   style="Margin:0;color:#000;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left;text-decoration:none">
										{{ config('db.contacts.footer-phone') }}
									</a>
								</th>
							@endif
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</center>
	@include('emails.particials.spacer')
@endif
@if(config('db.socials.instagram') || config('db.socials.fb') || config('db.socials.youtube'))
	<center align="center" class="float-center" data-parsed="" style="min-width:586px;width:100%">
		<table align="center" class="menu float-center"
			   style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:auto!important">
			<tr style="padding:0;text-align:left;vertical-align:top">
				<td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:20px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
					<table
						style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top">
						<tr style="padding:0;text-align:left;vertical-align:top">
							@if(config('db.socials.fb'))
								<th class="menu-item float-center"
									style="Margin:0 auto;color:#0a0a0a;float:none;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:5px;padding-right:5px;text-align:center">
									<a href="{{ config('db.socials.fb') }}"
									   style="Margin:0;color:#000;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left;text-decoration:underline">
										<img data-title="facebook.com"
											 src="https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/19961631_1373167952791394_8745247778497117933_n.jpg?oh=1367cdab9cecadf9a1be3fc6dc318a37&oe=5AFD3BA0"
											 alt="Facebook"
											 style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto">
									</a>
								</th>
							@endif
							@if(config('db.socials.instagram'))
								<th class="menu-item float-center"
									style="Margin:0 auto;color:#0a0a0a;float:none;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:5px;padding-right:5px;text-align:center">
									<a href="{{ config('db.socials.instagram') }}"
									   style="Margin:0;color:#000;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left;text-decoration:underline">
										<img data-title="vk.com"
											 src="https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/19961631_1373167952791394_8745247778497117933_n.jpg?oh=1367cdab9cecadf9a1be3fc6dc318a37&oe=5AFD3BA0"
											 alt="Instagram"
											 style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto">
									</a>
								</th>
							@endif
							@if(config('db.socials.youtube'))
								<th class="menu-item float-center"
									style="Margin:0 auto;color:#0a0a0a;float:none;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:5px;padding-right:5px;text-align:center">
									<a href="{{ config('db.socials.youtube') }}"
									   style="Margin:0;color:#000;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left;text-decoration:underline">
										<img data-title="twitter.com"
											 src="https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/19961631_1373167952791394_8745247778497117933_n.jpg?oh=1367cdab9cecadf9a1be3fc6dc318a37&oe=5AFD3BA0"
											 alt="Youtube"
											 style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto">
									</a>
								</th>
							@endif
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</center>
	@include('emails.particials.spacer')
@endif
<center align="center" class="float-center" data-parsed="" style="min-width:586px;width:100%">
	<a data-title="" href="{{route('site.index')}}" align="center" class="float-center"
	   style="Margin:0;color:#0600ff;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left;text-decoration:underline">
		{{ config('db.static.copyright') }}
	</a>
</center>
@include('emails.particials.spacer')
