@php
    /** @var \App\Models\Order\OrdersItem $item */
@endphp
<table class="spacer"
       style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
    <tbody>
    <tr style="padding:0;text-align:left;vertical-align:top">
        <td height="15px"
            style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:15px;font-weight:400;hyphens:auto;line-height:15px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
            &#xA0;
        </td>
    </tr>
    </tbody>
</table>
<table class="row"
       style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
    <tbody>
    <tr style="padding:0;text-align:left;vertical-align:top">
        <th class="small-12 large-12 columns last"
            style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:0;padding-bottom:10px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%">
            <table
                style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                <tr style="padding:0;text-align:left;vertical-align:top">
                    <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left">
                        <table align="center" class="container"
                               style="Margin:0 auto;background:#fff;border-collapse:collapse;border-spacing:0;margin:0 auto;padding:0;text-align:inherit;vertical-align:top;width:100%">
                            <tbody>
                            <tr style="padding:0;text-align:left;vertical-align:top">
                                <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:20px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                                    <table class="row"
                                           style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                        <tbody>
                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                            <th class="small-12 large-12 columns first last"
                                                style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:0;padding-bottom:10px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%">
                                                <table
                                                    style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left">
                                                            <a href="{{ route('product', ['slug' => $item->product->slug]) }}"
                                                               style="Margin:0;color:#0600ff;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left;text-decoration:underline">
                                                                {{ $item->product->name }}
                                                            </a>
                                                        </th>
                                                        <th class="expander"
                                                            style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                                                    </tr>
                                                </table>
                                            </th>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="row"
                                           style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                        <tbody>
                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                            <th class="small-2 large-2 columns"
                                                style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:0;padding-bottom:10px;padding-left:0!important;padding-right:0!important;text-align:left;width:29%">
                                                <table
                                                    style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left">
                                                            <p class="text-center content-text"
                                                               style="Margin:0;Margin-bottom:10px;color:#6d6d6d;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;margin-bottom:10px;padding:0;text-align:center">
                                                                <strong style="color:#000">
                                                                    {{ $item->price->price }} UAH
                                                                </strong>
                                                            </p>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </th>
                                            <th class="small-2 large-2 columns"
                                                style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:0;padding-bottom:10px;padding-left:0!important;padding-right:0!important;text-align:left;width:23%">
                                                <table
                                                    style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left">
                                                            <img
                                                                src="{{$_SERVER['HTTP_HOST']}}/uploads/images/products/icon/{{$item->product->image}}"
                                                                alt="{{ $item->product->name }}"
                                                                style="outline-style:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:auto;max-width:100%;clear:both;display:inline-block;border-style:none;">
                                                        </th>
                                                    </tr>
                                                </table>
                                            </th>
                                            <th class="small-2 large-2 columns columns"
                                                style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:0;padding-bottom:10px;padding-left:0!important;padding-right:0!important;text-align:left;width:15%">
                                                <table
                                                    style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left">
                                                            <p class="text-left count"
                                                               style="Margin:0;Margin-bottom:10px;color:#6d6d6d;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;margin-bottom:10px;padding:0;text-align:left">
                                                                {{ $item->count }} шт.
                                                            </p>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </th>
                                            <th class="small-3 large-3 columns columns"
                                                style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:0;padding-bottom:10px;padding-left:0!important;padding-right:0!important;text-align:left;width:33%">
                                                <table
                                                    style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left">
                                                            <p class="text-left count"
                                                               style="Margin:0;Margin-bottom:10px;color:#6d6d6d;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;margin-bottom:10px;padding:0;text-align:left">
                                                                {{ $item->price->volume }} мл | {{ $item->price->year }} год
                                                            </p>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </th>
                                            <th class="small-2 large-2 columns last"
                                                style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:0;padding-bottom:10px;padding-left:0!important;padding-right:0!important;text-align:left;width:33%">
                                                <table
                                                    style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left">
                                                            <p class="text-center content-text"
                                                               style="Margin:0;Margin-bottom:10px;color:#6d6d6d;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;margin-bottom:10px;padding:0;text-align:center">
                                                                {{ $item->price->price * $item->count }} UAH
                                                            </p>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </th>
                </tr>
            </table>
        </th>
    </tr>
    </tbody>
</table>
