@php
/** @var \App\Models\Order\Order $order */

$information = [];
$information = [
    ['№ заказа', $order->id],
    ['Дата заказа', $order->created_at->format('d.m.Y, H:i')],
];
if ($order->user_name !== null) {
    $information[] = ['Имя заказчика', $order->user_name];
    $information[] = ['Телефон заказчика', $order->phone];
    $information[] = ['Email заказчика', $order->email];
} elseif ($order->user_id !== null) {
    $information[] = ['Имя заказчика', $order->user->full_name];
    $information[] = ['Телефон заказчика', $order->user->phone];
    $information[] = ['Email заказчика', $order->user->email];
}

if ($order->delivery_type !== null) {
    $information[] = ['Тип доставки', $order->delivery_type == 0 ? 'В отделение новой почты' : 'Доставка курьером'];
}
if ($order->delivery_type == 0) {
    $information[] = ['Адрес доставки', $order->delivery_warehouse_name];
} else {
    $information[] = ['Адрес доставки', $order->delivery_address];
}
if ($order->payment_type == 0) {
    $information[] = ['Способ оплаты', 'Оплата при получении'];
} else {
    $information[] = ['Способ оплаты', $order->payment_system_id == 0 ? 'Liqpay' : 'Portmone'];
}

@endphp

@include('emails.particials.spacer')

<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
    <tbody>
    <tr style="padding:0;text-align:left;vertical-align:top">
        <th class="wide small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:0;padding-bottom:10px;padding-left:12px!important;padding-right:12px!important;text-align:left;width:570px">
            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                <tr style="padding:0;text-align:left;vertical-align:top">
                    <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left">
                        <table align="center" class="container client-and-shipping-info" style="Margin:0 auto;background:#fff;border-collapse:collapse;border-spacing:0;margin:0 auto;padding:0;text-align:inherit;vertical-align:top;width:100%">
                            <tbody>
                            <tr style="padding:0;text-align:left;vertical-align:top">
                                <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:20px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                                    @foreach($information AS $key => $data)
                                            <table class="row" style="background-color:{{ $key % 2 ? '#f3f3f3' : '#fff' }};border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                                <tbody>
                                                <tr style="padding:0;text-align:left;vertical-align:top">
                                                    <th class="key small-4 large-4 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:10px;padding-bottom:10px;padding-left:0!important;padding-right:0!important;text-align:left;width:33.33333%">
                                                        <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                            <tr style="padding:0;text-align:left;vertical-align:top">
                                                                <th style="Margin:0;color:#000;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;padding:0;padding-left:14px;padding-right:7px;text-align:left">
                                                                    {{ $data[0] }}
                                                                </th>
                                                            </tr>
                                                        </table>
                                                    </th>
                                                    <th class="value small-8 large-8 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:10px;padding-bottom:10px;padding-left:0!important;padding-right:0!important;text-align:left;width:66.66667%">
                                                        <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                            <tr style="padding:0;text-align:left;vertical-align:top">
                                                                <th style="Margin:0;color:#6d6d6d;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;padding:0;padding-left:7px;padding-right:14px;text-align:left">
                                                                    {{ $data[1] }}
                                                                </th>
                                                            </tr>
                                                        </table>
                                                    </th>
                                                </tr>
                                                </tbody>
                                            </table>
                                    @endforeach
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </th>
                </tr>
            </table>
        </th>
    </tr>
    </tbody>
</table>

@include('emails.particials.spacer')
