@extends('emails.layouts.main')

@section('content')
    @if($content !== null && $content !== '')
        <table align="center" class="container border-shadow border-shadow-1"
               style="margin:0 auto;background:#fff;border:1px solid #f3f3f3;border-collapse:collapse;border-spacing:0;margin:0;padding:0;text-align:inherit;vertical-align:top;width:586px">
            <tbody>
            <tr style="padding:0;text-align:left;vertical-align:top">
                <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:20px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                    <table class="row"
                           style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                        <tbody>
                        <tr style="padding:0;text-align:left;vertical-align:top">
                            <th class="small-12 large-12 columns first last"
                                style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:0;padding-bottom:10px;padding-left:25px;padding-right:25px;text-align:left;width:570px">
                                <table
                                    style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left">
                                            {!! $content !!}
                                        </th>
                                        <th class="expander"
                                            style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                                    </tr>
                                </table>
                            </th>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    @elseif($news !== null)
        <table align="center" class="container border-shadow border-shadow-1"
               style="margin:0 auto;background:#fff;border:1px solid #f3f3f3;border-collapse:collapse;border-spacing:0;margin:0;padding:0;text-align:inherit;vertical-align:top;width:586px">
            <tbody>
            <tr style="padding:0;text-align:left;vertical-align:top">
                <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:20px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                    <table class="row"
                           style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                        <tbody>
                        <tr style="padding:0;text-align:left;vertical-align:top">
                            <th class="small-12 large-12 columns first last"
                                style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0 auto;padding:0;padding-bottom:10px;padding-left:25px;padding-right:25px;text-align:left;width:570px">
                                <table
                                    style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left">
                                            @foreach($news as $new)
                                                <div style="height: 150px; display: block;">
                                                    <div style="float: left; width: 30%; padding-top: 20px; display:inline-block;">
                                                        <img
                                                            src="{{$_SERVER['HTTP_HOST']}}/uploads/images/articles/small/{{$new->image}}"
                                                            alt="{{ $new->name }}"
                                                            width="30%"
                                                            style="outline-style:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:auto;max-width:100%;clear:both;display:inline-block;border-style:none;">
                                                    </div>
                                                    <div style="float: left; padding-left:20px; padding-top: 25px; display:inline-block;">
                                                        @if($new->type == 0)
                                                            <h3 style="color: #73dad3;">
                                                                <a style="color: #73dad3;" href="{{$_SERVER['HTTP_HOST']}}/blog/news/{{$new->articlesCategory->slug}}/{{$new->slug}}">{{$new->name}}</a>
                                                            </h3>
                                                            <br>
                                                        @elseif($new->type == 1)
                                                            <h3 style="color: #73dad3;">
                                                                <a style="color: #73dad3;" href="{{$_SERVER['HTTP_HOST']}}/blog/articles/{{$new->articlesCategory->slug}}/{{$new->slug}}">{{$new->name}}</a>
                                                            </h3>
                                                            <br>
                                                        @elseif($new->type == 2)
                                                            <h3 style="color: #73dad3;">
                                                                <a style="color: #73dad3;" href="{{$_SERVER['HTTP_HOST']}}/blog/events/{{$new->articlesCategory->slug}}/{{$new->slug}}">{{$new->name}}</a>
                                                            </h3>
                                                            <br>
                                                        @endif
                                                        {{$new->articlesCategory->name}}<br>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endforeach
                                        </th>
                                        <th class="expander"
                                            style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:20px;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                                    </tr>
                                </table>
                            </th>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    @endif
@stop
