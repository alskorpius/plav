@extends('admin.layouts.main')

@section('content')
    @widget('filter', ['fields' => ['name']])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Название</th>
                            <th></th>
                            {{--<th></th>--}}
                        </tr>
                        @foreach($cities AS $city)
                            <tr>
                                <td>{{ $city->name }}</td>
                                <td>@widget('status', ['status' => $city->status, 'table' => 'cities', 'id' => $city->id])</td>
                                {{--<td>@widget('listButtons', ['routeBase' => 'cities.', 'id' => $city->id, 'view' => false, 'delete' => false])</td>--}}
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            {{--<div class="text-center">{{ $cities->appends(Input::except('city'))->render() }}</div>--}}
        </div>
    </div>
@endsection
