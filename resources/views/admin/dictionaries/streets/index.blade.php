@extends('admin.layouts.main')

@section('content')
    @widget('filter', ['fields' => ['name']])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Название</th>
                            <th></th>
                            {{--<th></th>--}}
                        </tr>
                        @foreach($streets AS $street)
                            <tr>
                                <td>{{ $street->name }}</td>
                                <td>@widget('status', ['status' => $street->status, 'table' => 'streets', 'id' => $street->id])</td>
                                {{--<td>@widget('listButtons', ['routeBase' => 'streets.', 'id' => $street->id, 'view' => false, 'delete' => false])</td>--}}
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            {{--<div class="text-center">{{ $streets->appends(Input::except('street'))->render() }}</div>--}}
        </div>
    </div>
@endsection
