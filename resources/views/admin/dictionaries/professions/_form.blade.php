<?php
/** @var \App\Models\Profession $profession */
$object = isset($profession) ? $profession : null;
?>
<div class="row">
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.input', ['name' => 'name', 'label' => 'Название', 'object' => $object, 'options' => ['required', 'data-slug-origin']])
                @include('admin.widgets.form.input', ['name' => 'slug', 'label' => 'Алиас', 'object' => $object, 'options' => ['required', 'data-slug-destination']])
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="box box-warning">
            <div class="box-body">
                <div class="form-group">
                    {!! Form::label('status', 'Статус') !!}
                    <div class="radio">
                        <label>{!! Form::radio('status', 0, isset($object->status) && $object->status == 0, ['class' => 'minimal']) !!} Неопубликовано</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('status', 1, isset($object->status) && $object->status == 1, ['class' => 'minimal']) !!} Опубликовано</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
