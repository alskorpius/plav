@extends('admin.layouts.main')

@section('custom-buttons')
    <a href="{{ route('admin.professions.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' => ['name']])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Название</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($professions AS $profession)
                            <tr>
                                <td>{{ $profession->name }}</td>
                                <td>@widget('status', ['status' => $profession->status, 'table' => 'professions', 'id' => $profession->id])</td>
                                <td>@widget('listButtons', ['routeBase' => 'professions.', 'id' => $profession->id, 'view' => false, 'delete' => true])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $professions->appends(Input::except('department'))->render() }}</div>
        </div>
    </div>
@endsection
