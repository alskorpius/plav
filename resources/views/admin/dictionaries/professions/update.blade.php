@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.professions.update', 'profession' => $profession->id], 'method' => 'PUT']) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.dictionaries.professions._form', ['profession' => $profession])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop
