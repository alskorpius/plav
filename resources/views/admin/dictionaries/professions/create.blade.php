@extends('admin.layouts.main')
@section('content')
    {!! Form::open(['route' => 'admin.professions.store']) !!}
    @include('admin.widgets.form.buttons', ['create' => true])
    @include('admin.dictionaries.professions._formNew')
    @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop
