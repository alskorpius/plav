@extends('admin.layouts.main')

@section('content')
    @widget('filter', ['fields' => ['name']])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Код</th>
                            <th>Название</th>
                            <th></th>
                            {{--<th></th>--}}
                        </tr>
                        @foreach($departments AS $department)
                            <tr>
                                <td>{{ $department->code }}</td>
                                <td>{{ $department->name }}</td>
                                <td>@widget('status', ['status' => $department->status, 'table' => 'departments', 'id' => $department->id])</td>
                                {{--<td>@widget('listButtons', ['routeBase' => 'departments.', 'id' => $department->id, 'view' => false, 'delete' => false])</td>--}}
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            {{--<div class="text-center">{{ $departments->appends(Input::except('department'))->render() }}</div>--}}
        </div>
    </div>
@endsection
