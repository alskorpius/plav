@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.where-buy-spots.update', 'spot' => $spot->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.map.spots._form', ['spot' => $spot])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop
