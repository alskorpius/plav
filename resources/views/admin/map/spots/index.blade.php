@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
    <a href="{{ route('admin.where-buy-spots.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' => [
        ['name' => 'name', 'placeholder' => 'Название'],
        ['name' => 'address', 'placeholder' => 'Адрес'],
        ['name' => 'region_id', 'type' => 'select', 'data' => $regions, 'label' => 'Регион'],
        ['name' => 'type', 'type' => 'select', 'data' => [
        App\Models\Map\MapSpot::TYPE_SHOP => 'Винный магазин',
        App\Models\Map\MapSpot::TYPE_CAFE => 'Ресторан',
        App\Models\Map\MapSpot::TYPE_SUPERMARKET => 'Супермаркет',
        App\Models\Map\MapSpot::TYPE_GAS => 'Заправка',
        ], 'label' => 'Тип'],
    ]])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox"
                                       data-table='map_spots'>
                            </th>
                            <th>Название</th>
                            <th>Адрес</th>
                            <th>Регион</th>
                            <th>Тип</th>
                            <th></th>
                        </tr>
                        @foreach($spots AS $spot)
                            @php
                                if(!empty($spot->current)){
                                    $spot->name =  $spot->current->name;
                                    $spot->address =  $spot->current->address;
                                } else{
                                    $id = $spot->id;
                                    $spot->id = $spot->row_id;
                                    $spot->row_id = $id;
                                }
                            @endphp
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox"
                                           data-id='{{$spot->id}}'>
                                </td>
                                <td><a
                                        href="{{route('admin.where-buy-spots.edit', ['id' => $spot->id])}}">{{ $spot->name }}</a>
                                </td>
                                <td>
                                    {{$spot->address}}
                                </td>
                                <td>{{$spot->region}}</td>
                                @if($spot->type == 0)
                                    <td><span class="label label-danger">Винный магазин</span></td>
                                @elseif($spot->type == 1)
                                    <td><span class="label label-success">Ресторан</span></td>
                                @elseif($spot->type == 2)
                                    <td><span class="label label-warning">Супермаркет</span></td>
                                @elseif($spot->type == 3)
                                    <td><span class="label label-warning">Заправка</span></td>
                                @endif
                                <td>@widget('listButtons', ['routeBase' => 'where-buy-spots.', 'id' => $spot->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $spots->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
