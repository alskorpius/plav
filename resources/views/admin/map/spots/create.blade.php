@extends('admin.layouts.main')
@section('content')
    {!! Form::open(['route' => 'admin.where-buy-spots.store', 'files' => true]) !!}
    @include('admin.widgets.form.buttons', ['create' => true])
    @include('admin.map.spots._form')
    @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop
