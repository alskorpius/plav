<?php
/** @var \App\Models\Map\MapSpot $object **/
$object = isset($spot) ? $spot : null;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}">
                            @include('admin.widgets.form.input', ['name' => 'name', 'lang' => $language['slug'], 'label' => 'Название', 'object' => $langObject, 'options' => ['required']])
                            @include('admin.widgets.form.input', ['name' => 'address', 'lang' => $language['slug'], 'label' => 'Адрес', 'value' => isset($langObject->address) ? $langObject->address : '', 'options' => ['required']])
                        </div>
                    @endforeach
                </div>
                @include('admin.widgets.form.select', ['name' => 'region_id', 'label' => 'Регион', 'object' => $object, 'data' => $regions, 'options' => ['required', 'class' => 'form-control select2']])
                <div class="form-group">
                    {!! Form::label('type', 'Тип') !!}
                    <div class="radio">
                        <label>{!! Form::radio('type', 0, (isset($object->type) && !$object->type) || ( isset($object->type) && $object->type == 0), ['class' => 'minimal']) !!} Винный магазин</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('type', 1, !isset($object->type) || $object->type == 1, ['class' => 'minimal']) !!} Ресторан</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('type', 2, !isset($object->type) || $object->type == 2, ['class' => 'minimal']) !!} Супермаркет</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('type', 3, !isset($object->type) || $object->type == 3, ['class' => 'minimal']) !!} Заправка</label>
                    </div>
                </div>
                @include('admin.widgets.form.select', ['name' => 'line_id[]', 'label' => 'Линейки',  'data' => $lines, 'value' => isset($selectedLines) ? $selectedLines : null, 'options' => ['required', 'class' => 'form-control select2', 'multiple' => 'multiple',]])
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="dropModule">
            <div class="box box-solid dropBox">
                <div class="box-header with-border">
                    <i class="fa fa-map"></i>
                    Настройка карты
                </div>
                <div class="box-body">
                    <div class="mapWrap" style="height: 450px;">
                        <input type="text" id="address"
                               class="form-control ui-autocomplete-input"
                               placeholder="Начните писать адрес..."
                               autocomplete="off">
                        <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                        <div style="width: 100%; height: calc(100% - 30px); position: relative; background-color: rgb(229, 227, 223); overflow: hidden;"
                             id="gmap"></div>
                    </div>
                    @include('admin.widgets.form.hidden', ['name' => 'SETTINGS[latitude]', 'value' => isset($object->latitude) ? $object->latitude : 46.637326, 'options' => ['id' => 'latitude']])
                    @include('admin.widgets.form.hidden', ['name' => 'SETTINGS[longitude]', 'value' => isset($object->longitude) ? $object->longitude : 32.6125771, 'options' => ['id' => 'longitude']])
                    @include('admin.widgets.form.hidden', ['name' => 'SETTINGS[zoom]', 'value' => isset($object->zoom) ? $object->zoom : 17, 'options' => ['id' => 'zoom']])
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script src="https://maps.google.com/maps/api/js?key={{ config('db.services.google-maps-api-key', config('services.google.maps.key')) }}&language=ru"
        type="text/javascript"></script>
<script type="text/javascript">
    $('#address').val('');
    var map = false, pointt,
        geocoder = false,
        myLatlng = new google.maps.LatLng(parseFloat('{{ isset($object->latitude) ? $object->latitude : 46.637326 }}'), parseFloat('{{ isset($object->longitude) ? $object->longitude : 32.6125771 }}')),
        zoom = parseInt('{{ isset($object->zoom) ? $object->zoom : 17 }}'),
        marker = true;

    function initialize() {
        var mapOptions = {
            center: myLatlng,
            zoom: zoom,
            styles: [
                {
                    "featureType": "administrative",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": "-100"
                        }
                    ]
                },
                {
                    "featureType": "administrative.province",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 65
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": "50"
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": "-100"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "all",
                    "stylers": [
                        {
                            "lightness": "30"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "all",
                    "stylers": [
                        {
                            "lightness": "40"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "hue": "#ffff00"
                        },
                        {
                            "lightness": -25
                        },
                        {
                            "saturation": -97
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "lightness": -25
                        },
                        {
                            "saturation": -100
                        }
                    ]
                }
            ]
        };
        map = new google.maps.Map(document.getElementById("gmap"), mapOptions);
        geocoder = new google.maps.Geocoder();

        map.addListener('zoom_changed', function () {
            $('#zoom').val(map.getZoom());
        });
    }

    function bind_location(location) {
        $('#latitude').val(location.lat());
        $('#longitude').val(location.lng());
    }

    function placeMarker(location) {
        if (marker) {
            marker.setMap(null);
        }
        bind_location(location);
        pointt = new google.maps.Point(18, 54);
        marker = new google.maps.Marker({
            position: location,
            map: map,
            draggable: true
        });
        google.maps.event.addListener(marker, 'dragend', function (cc) {
            bind_location(cc.latLng);
        });
    }

    $(document).ready(function () {
        initialize();
        google.maps.event.addListener(map, 'click', function (cc) {
            placeMarker(cc.latLng);
        });

        if (marker) {
            pointt = new google.maps.Point(18, 54);
            marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                draggable: true
            });
            google.maps.event.addListener(marker, 'dragend', function (cc) {
                bind_location(cc.latLng);
            });
        }

        $('#address').autocomplete({
            //Определяем значение для адреса при геокодировании
            source: function (request, response) {
                geocoder.geocode({'address': request.term}, function (results, status) {
                    var resp = [];
                    for (var i in results) {
                        resp.push({
                            label: results[i].formatted_address,
                            value: results[i].formatted_address,
                            latitude: results[i].geometry.location.lat(),
                            longitude: results[i].geometry.location.lng()
                        });
                    }
                    response(resp);
                });
            },
            select: function (event, ui) {
                $('#latitude').val(ui.item.latitude);
                $('#longitude').val(ui.item.longitude);
                var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
                map.setCenter(location);
                map.setZoom(zoom);
                placeMarker(location);
            }
        });
    });
</script>
@endpush
