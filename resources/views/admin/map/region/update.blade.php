@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.where-buy-regions.update', 'region' => $region->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.map.region._form', ['region' => $region])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop
