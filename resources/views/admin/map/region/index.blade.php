@extends('admin.layouts.main')

@section('custom-buttons')
{{--
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
--}}
    <a href="{{ route('admin.where-buy-regions.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' => ['name']])
    <div class="row">
        <div class="col-xs-12">
            <div class="dd pageList" id="myNest" data-depth="1">
                <ol class="dd-list">
                    @include('admin.map.region.items', ['regions' => $regions])
                </ol>
            </div>
            <span id="parameters" data-table="map_regions"></span>
            <input type="hidden" id="myNestJson">
        </div>
    </div>
@endsection
