<?php
$object = isset($region) ? $region : null;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.input', ['name' => 'slug', 'label' => 'Алиас', 'object' => $object, 'options' => ['required', 'data-slug-destination']])
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}">
                            @include('admin.widgets.form.input', ['name' => 'name', 'lang' => $language['slug'], 'label' => 'Название', 'object' => $langObject, 'options' => ['required','data-slug-origin']])
                        </div>
                    @endforeach
                </div>
                @include('admin.widgets.form.input', ['name' => 'url', 'label' => 'Url', 'object' => $object, 'options' => ['']])
                <div class="form-group">
                    {!! Form::label('is_checked', 'Отмечен на фронте') !!}
                    <div class="radio">
                        <label>{!! Form::radio('is_checked', 0, !isset($object->is_checked) || (isset($object->is_checked) && $object->is_checked == 0), ['class' => 'minimal']) !!}
                            Не отмечен</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('is_checked', 1, isset($object->is_checked) && ($object->is_checked == 1), ['class' => 'minimal']) !!}
                            Отмечен</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
