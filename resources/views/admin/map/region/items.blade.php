<?php
/**
 * @var \App\Models\regions[] regions
 */
?>
@if(isset($regions) and sizeof($regions))
    @foreach ($regions as $region)
        @php
            if(!empty($region->current)){
                $region->name =  $region->current->name;
            } else{
                //$id = $region->id;
                //$region->id = $region->row_id;
               // $region->row_id = $id;
            }
        @endphp
        <li class="dd-item dd3-item" data-id="{{ $region->id }}">
            <div title="Переместить строку" class="dd-handle dd3-handle">Drag</div>
            <div class="dd3-content">
                <table style="width: 100%;">
                    <tr>
                        <td class="column-drag" width="1%"></td>
                        <td width="20%">
                            <a class="pageLinkEdit" href="{{route('admin.where-buy-regions.edit', ['id' => $region->id])}}">
                                <p>{{ $region->name }}</p>
                            </a>
                        </td>
                        <td width="10%" valign="top" class="icon-column status-column">
                            {{$region->url}}
                        </td>
                        <td class="nav-column icon-column" valign="top" width="10%">
                            @widget('listButtons', ['routeBase' => 'where-buy-regions.', 'id' => $region->id, 'view' => false])
                        </td>
                    </tr>
                </table>
            </div>
        </li>
    @endforeach
@endif
