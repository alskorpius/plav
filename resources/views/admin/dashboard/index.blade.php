@extends('admin.layouts.main')

@section('content')
    {{--<div class="row">

        <!-- Orders Count -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{$countNewOrders}}</h3>
                    <p>Новые заказы</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="{{route('admin.orders.index')}}" class="small-box-footer">{{ __('custom.more') }} <i
                        class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- Product Reviews Count -->
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{$reviewsProductsCount}}</h3>

                    <p>Отзывы к товарам</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="{{route('admin.products-reviews.index')}}" class="small-box-footer">{{ __('custom.more') }} <i
                        class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- Registered Users Count -->
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{$countRegisteredUsers}}</h3>

                    <p>Зарегестрированные пользователи</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="{{route('admin.customers.index')}}" class="small-box-footer">{{ __('custom.more') }} <i
                        class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- Unique Users Count -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-maroon">
                <div class="inner">
                    <h3>{{$visitors}}</h3>

                    <p>Уникальные посетители</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="{{route('admin.visitors.index')}}" class="small-box-footer">{{ __('custom.more') }} <i
                        class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

    </div>
    <div class="row" style="margin: 0em;">
        <div class="col-md-6 col-xs-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Статистика продаж</h3>
            </div>
            <div class="box-body">
                    <div class="selects-for-orders-graph">
                        {!! Form::open(['route' => 'admin.dashboard', 'files' => true, 'name' => 'form1', 'method' => 'GET']) !!}
                        @include('admin.widgets.form.select', ['name' => 'product_id', 'label' => 'Товар', 'data' => $products, 'options' => ['class' => 'form-control select2 for-graph-orders-item-select']])
                        @include('admin.widgets.form.dateRange', ['name' => 'dateRange', 'label' => 'Диапазон дат', 'options' => ['class' => 'form-control dateRangePicker for-graph-orders-item', 'style' => 'padding-top: 2px;']])
                        <div class="form-group">
                            <button title="Принять" type="submit" class="btn btn-block btn-success btn-submit-search"><i class="fa fa-search"
                                                                                                                         aria-hidden="true"></i>
                            </button>
                        </div>
                        <div class="form-group">
                            <a href="{{route('admin.dashboard')}}" title="Продажи за последние 30 дней" class="btn btn-block btn-danger btn-refresh-search"><i
                                    class="fa fa-close"
                                    aria-hidden="true"></i></a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="orders-graph-main-page">
                        <canvas id="line-chart" class="main-page-orders-graph"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-6">

        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Статистика отзывов по товарам</h3>
            </div>
            <div class="box-body">
                    <div class="selects-for-orders-graph">
                        {!! Form::open(['route' => 'admin.dashboard', 'files' => true, 'name' => 'form2', 'method' => 'GET']) !!}
                        @include('admin.widgets.form.select', ['name' => 'product_id_reviews', 'label' => 'Товар', 'data' => $products, 'options' => ['class' => 'form-control select2 for-graph-orders-item-select']])
                        @include('admin.widgets.form.dateRange', ['name' => 'dateRange_reviews', 'label' => 'Диапазон дат', 'options' => ['class' => 'form-control dateRangePicker for-graph-orders-item', 'style' => 'padding-top: 2px;']])
                        <div class="form-group">
                            <button title="Принять" type="submit" class="btn btn-block btn-success btn-submit-search"><i class="fa fa-search"
                                                                                                                         aria-hidden="true"></i>
                            </button>
                        </div>
                        <div class="form-group">
                            <a href="{{route('admin.dashboard')}}" title="Продажи за последние 30 дней" class="btn btn-block btn-danger btn-refresh-search"><i
                                    class="fa fa-close"
                                    aria-hidden="true"></i></a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="orders-graph-main-page">
                        <canvas id="line-chart-reviews" class="main-page-orders-graph"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
    <script>
        new Chart(document.getElementById("line-chart"), {
            type: 'line',
            data: {
                labels: {!! json_encode($labelsForChartOrders) !!},
                datasets: [{
                    data: {!! json_encode($valuesForChartOrders) !!},
                    label: 'Количество продаж',
                    borderColor: "#3e95cd",
                    fill: true,
                    backgroundColor: 'rgba(62, 149, 205, .4)',
                    pointBackgroundColor: '#0c537f',
                    pointRadius: 4,
                }]
            },
            options: {
                title: {
                    display: true,
                    text: {!! json_encode($titleLabelOrders) !!}
                },
                scales: {
                    yAxes: [{
                        display: true,
                        ticks: {
                            max: {!! $maxTicksOrders !!},
                            stepSize: {!! $stepSize !!},
                            beginAtZero: true,
                        }
                    }]
                },
                legend: {
                    display: false
                },
            }
        });

        new Chart(document.getElementById("line-chart-reviews"), {
            type: 'line',
            data: {
                labels: {!! json_encode($labelsForChartReviews) !!},
                datasets: [{
                    data: {!! json_encode($valuesForChartReviews) !!},
                    label: 'Количество отзывов',
                    borderColor: "#dd4b39",
                    fill: true,
                    backgroundColor: 'rgba(221, 75, 57, .4)',
                    pointBackgroundColor: '#ad2d1f',
                    pointRadius: 4,
                }]
            },
            options: {
                title: {
                    display: true,
                    text: {!! json_encode($titleLabelReviews) !!}
                },
                scales: {
                    yAxes: [{
                        display: true,
                        ticks: {
                            max: {!! $maxTicksReviews !!},
                            stepSize: {!! $stepSize !!},
                            beginAtZero: true,
                        }
                    }]
                },
                legend: {
                    display: false
                },
            }
        });
    </script>
    @endpush--}}
@endsection
