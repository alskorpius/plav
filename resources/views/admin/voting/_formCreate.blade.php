<?php
/** @var \App\Models\News $object */
$object = isset($voting) ? $voting : null;
$status = isset($voting) ? $voting->status : null;
?>
<div class="row">
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="box-body">
                <div class="col-md-12">
                    @include('admin.widgets.form.input', ['name' => 'name', 'label' => 'Название', 'object' => $object, 'options' => ['required']])
                    @include('admin.widgets.form.input', ['name' => 'description', 'label' => 'Описание', 'object' => $object])
                    @include('admin.widgets.form.dateTime', ['name' => 'date_from', 'label' => 'С', 'object' => $object])
                    @include('admin.widgets.form.dateTime', ['name' => 'date_to', 'label' => 'По', 'object' => $object])

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="box box-warning">
            <div class="box-body">
                <div class="form-group">
                    {!! Form::label('status', 'Статус') !!}
                    <div class="radio">
                        <label>{!! Form::radio('status', 0, $status == 0, ['class' => 'minimal']) !!}
                            Неопубликовано</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('status', 1, $status == 1, ['class' => 'minimal']) !!}
                            Опубликовано</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('status', 2, $status == 2, ['class' => 'minimal']) !!}
                            Отложен</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('status', 3, $status == 3, ['class' => 'minimal']) !!}
                            Истек</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>