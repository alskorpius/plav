@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.voting.update', 'voting' => $voting->id], 'method' => 'PUT', 'files' => true, 'name' => 'editForm']) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.voting._form', ['voting' => $voting])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop