<?php
/**
 * @var \App\Models\Slider[] $slider
 */
?>
@if(isset($answers) and sizeof($answers))
    @foreach ($answers as $element)
        <li class="dd-item dd3-item" data-id="{{ $element->id }}">
            <div title="Переместить строку" class="dd-handle dd3-handle">Drag</div>
            <div class="dd3-content">
                <table style="width: 100%;">
                    <tr>
                        <td class="column-drag" width="1%"></td>
                        <td>
                            <p>{{$element->current->name}}</p>
                        </td>
                        <td width="45" valign="top" class="icon-column status-column">
                            @widget('status', ['status' => $element->status, 'table' => 'voting_answers', 'id' => $element->id])
                        </td>
                        <td class="nav-column icon-column" valign="top" width="20%">
                            <a class="btn btn-xs btn-danger deleteAnswerAction" data-question='{{$votingId}}'
                               data-id="{{$element->id}}">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </li>
    @endforeach
@endif
