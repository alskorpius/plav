@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.voting.store', 'files' => false,]) !!}
    {{csrf_field()}}
    @include('admin.widgets.form.buttons')
    @include('admin.voting._formCreate')
    {!! Form::close() !!}
@stop