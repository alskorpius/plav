@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
	<button class="btn bg-yellow btn-flat margin" id="statusChecked">Снять с публикации</button>
	<button class="btn bg-blue btn-flat margin" id="statusCheckedEnable">Опубликовать</button>
	<a href="{{ route('admin.voting.create') }}" class="btn bg-olive btn-flat margin">Создать</a>
@endsection

@section('content')
    @widget('filter', ['fields' => 'votings'])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox" data-table = 'voting_questions'>
                            </th>
                            <th>Название опроса</th>
                            <th>Дата создания</th>
                            <th>Опубликовано с</th>
                            <th>Опубликовано по</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($questions AS $question)
                            @php
                                if(!empty($question->current)){
                                    $question->name =  $question->current->name;
                                } else{
                                    $id = $question->id;
                                    $question->id = $question->row_id;
                                    $question->row_id = $id;
                                }
                            @endphp
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox" data-id = '{{$question->id}}'>
                                </td>
                                <td width="25%">{{ $question->name }}</td>
                                <td width="15%">{{ $question->created_at->format('Y.m.d') }}</td>
                                <td width="15%">{{ $question->date_from }}</td>
                                <td width="15%">{{ $question->date_to }}</td>
                                @if($question->status == 0)
                                    <td width="15%">Не опубликован</td>
                                @elseif($question->status == 1)
                                    <td width="15%">Опубликован</td>
                                @elseif($question->status == 2)
                                    <td width="15%">Отложен</td>
                                @elseif($question->status == 3)
                                    <td width="15%">Истек</td>
                                @endif
                                <td width="15%">@widget('listButtons', ['routeBase' => 'voting.', 'id' => $question->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $questions->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
