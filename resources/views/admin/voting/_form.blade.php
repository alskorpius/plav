<?php
/** @var \App\Models\VotingQuestion $object */
$object = isset($voting) ? $voting : null;
$status = isset($voting) ? $voting->status : null;
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                <div class="col-md-12">
                    <ul class="nav nav-tabs">
                        @foreach($languages AS $language)
                            <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}"
                                                                                        data-toggle="tab">{{ $language['name'] }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($languages AS $language)
                            <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                            <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}">
                                @include('admin.widgets.form.input', ['name' => 'name', 'lang' => $language['slug'], 'label' => 'Название', 'object' => $langObject, 'options' => ['required']])
                                @include('admin.widgets.form.input', ['name' => 'description', 'lang' => $language['slug'], 'label' => 'Описание', 'object' => $langObject])
                            </div>
                        @endforeach
                    </div>
                    @include('admin.widgets.form.dateTime', ['name' => 'date_from', 'label' => 'С', 'object' => $object])
                    @include('admin.widgets.form.dateTime', ['name' => 'date_to', 'label' => 'По', 'object' => $object])
                </div>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Ответы на вопрос</h3>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="dd pageList" id="myNest" data-depth="1">
                        <ol class="dd-list answerList">
                            @include('admin.voting.answerItems', ['answers' => $answers, 'cur' => 0])
                        </ol>
                    </div>
                    <span id="parameters" data-table="voting_answers"></span>
                    <input type="hidden" id="myNestJson">
                </div>
            </div>
            <div class="input-group input-group-sm answerAdd" data-question="{{$object->id}}">
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}add"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}add">
                            <input type="text" class="form-control answerAddInput" data-lang="{{ $language['slug'] }}">
                        </div>
                    @endforeach
                </div>
                <span class="input-group-btn">
                      <button type="button"
                              class="btn btn-info btn-flat answerAddButton">Добавить</button>
                    </span>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-warning">
            <div class="box-body">
                <div class="form-group">
                    {!! Form::label('status', 'Статус') !!}
                    <div class="radio">
                        <label>{!! Form::radio('status', 0, $status == 0, ['class' => 'minimal']) !!}
                            Неопубликовано</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('status', 1, $status == 1, ['class' => 'minimal']) !!}
                            Опубликовано</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('status', 2, $status == 2, ['class' => 'minimal']) !!}
                            Отложен</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('status', 3, $status == 3, ['class' => 'minimal']) !!}
                            Истек</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Результаты опроса (проголосовало: {{$count}})</h3>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <th>Вариант</th>
                                <th>Результаты</th>
                                <th>Свой %</th>
                            </tr>
                            @foreach($answers as $answer)
                                <tr>
                                    <td width="60%">{{$answer->current->name}}</td>
                                    <td width="15%">{{round(($resultAnswersArr[$answer->id] / $resultAnswersAll) * 100)}}
                                        %
                                    </td>
                                    <td width="15%"><input data-answer="{{$answer->id}}" name="newPercent"
                                                           class="form-control newPercent" type="text"
                                                           value="{{isset($answer->own_percent) ? $answer->own_percent : ''}}">
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <button style="margin-top: 10px; float: right;" type="button" class="btn btn-block btn-success newPercentBtn">
                            Сохранить
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
