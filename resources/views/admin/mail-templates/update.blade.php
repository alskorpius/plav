@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.mail-templates.update', 'mail-template' => $template->id], 'method' => 'PUT']) !!}
        @include('admin.widgets.form.buttons', ['except' => ['add']])
        @include('admin.mail-templates._form', ['template' => $template])
        @include('admin.widgets.form.buttons', ['except' => ['add']])
    {!! Form::close() !!}
@stop