<?php
/** @var \App\Models\MailTemplate $object */
$object = isset($template) ? $template : null;
?>
<div class="row">
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.input', ['name' => 'name', 'label' => 'Название', 'object' => $object, 'options' => ['required']])
                @include('admin.widgets.form.input', ['name' => 'subject', 'label' => 'Заголовок письма', 'object' => $object, 'options' => ['required']])
                @include('admin.widgets.form.tiny', ['name' => 'content', 'label' => 'Текст письма', 'object' => $object])
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="box box-warning">
            <div class="box-body">
                @include('admin.widgets.form.status', ['status' => old('status', isset($object) ? $object->status : 0)])
            </div>
        </div>
        @if(count($object->data) > 0)
            <div class="box box-danger">
                <div class="box-header">
                    <h3>Метки</h3>
                </div>
                <div class="box-body">
                    <ul>
                    @foreach($object->data AS $key => $name)
                        <li><b>{{ '{' . $key . '}' }}</b> - {{ $name }}</li>
                    @endforeach
                    </ul>
                </div>
            </div>
        @endif
    </div>
</div>