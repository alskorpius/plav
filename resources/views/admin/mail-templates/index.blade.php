@extends('admin.layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Название</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($templates AS $template)
                            <tr>
                                <td>{{ $template->name }}</td>
                                <td>@widget('status', ['status' => $template->status, 'table' => $template->getTable(), 'id' => $template->id])</td>
                                <td>@widget('listButtons', ['routeBase' => 'mail-templates.', 'id' => $template->id, 'view' => false, 'delete' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection