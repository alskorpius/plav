@php
/** @var Notification[] $notifications */
@endphp
@extends('admin.layouts.main')

@section('content')
    @widget('filter', ['fields' => [
        ['name' => 'date_from', 'label' => 'Дата от', 'type' => 'date'],
        ['name' => 'date_to', 'label' => 'Дата до', 'type' => 'date'],
    ]])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Уведомление</th>
                                <th>Дата</th>
                            </tr>
                        </thead>
                        <tbody id="notifications">
                            @foreach($notifications AS $notification)
                                <tr class="{{ $notification->read_at ? 'read' : null }}" data-id="{{ $notification->id }}">
                                    <td>{!! $notification->icon !!} {{ Html::link($notification->link, $notification->message, ['target' => '_blank']) }}</td>
                                    <td>{{ $notification->created_at->format('d.m.Y') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $notifications->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            var ids = [];
            $('#notifications').find('tr').each(function () {
                if ($(this).hasClass('read') === false) {
                    ids.push($(this).data('id'));
                }
            });
            if (ids.length > 0) {
                $.ajax({
                    url: '{{ route('ajax.read-notifications') }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        notificationsIds: ids
                    }
                }).done(function () {
                    var timeout = 0;
                    $('#notifications').find('tr').each(function () {
                        var tr = $(this);
                        setTimeout(function () {
                            tr.addClass('read');
                        }, timeout);
                        timeout += 250;
                    });
                });
            }
        });
    </script>
@endpush

@push('styles')
    <style>
        #notifications tr {
            transition: background-color 0.75s;
            background-color: #eee;
        }
        #notifications tr.read {
            background-color: #ffffff;
        }
    </style>
@endpush