@extends('admin.layouts.main')

@section('content')

    <div class="row">
        @include('admin.widgets.user-left-menu')

        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-body">
                    {!! Form::open(['method' => 'delete', 'route' => 'admin.delete-avatar', 'accept-charset' => 'UTF-8']) !!}
                        <a href="{{ Auth::user()->showImage('original', null, true) }}" data-lightbox="image">
                            <img src="{{ Auth::user()->showImage('icon', null, true) }}" alt="{{ Auth::user()->full_name }}">
                        </a>
                        <a type="submit" class="btn btn-s btn-danger confirmation-window align-text-top" data-message="Вы действительно хотите удалить аватар?"><i class="fa fa-trash"></i> Удалить аватар</a>
                        <a href="{{ route('admin.crop', ['id' => Auth::user()->id, 'back' => URL::current(), 'model' => Auth::user()->getMorphClass()]) }}" class="btn btn-s btn-twitter align-text-top">Изменить изображение</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@stop