@extends('admin.layouts.main')

@section('content')

    <div class="row">
        @include('admin.widgets.user-left-menu')

        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-body">
                    {{ Form::open(['route' => 'admin.save-new-avatar', 'method' => 'put', 'files' => true]) }}
                        @include('admin.widgets.form.image')
                        <button type="submit" class="btn btn-primary btn-large">Загрузить аватар</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@stop