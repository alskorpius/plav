@extends('admin.layouts.main')

@section('content')

    <div class="row">
        @include('admin.widgets.user-left-menu')

        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-body">
                    {{ Form::open(['route' => 'admin.save-personal-data', 'method' => 'put']) }}
                        @include('admin.widgets.form.input', ['name' => 'first_name', 'label' => 'Имя', 'object' => Auth::user()])
                        @include('admin.widgets.form.input', ['name' => 'second_name', 'label' => 'Фамилия', 'object' => Auth::user()])
                        @include('admin.widgets.form.input', ['name' => 'email', 'label' => 'Email', 'object' => Auth::user()])
                        <button type="submit" class="btn btn-primary btn-large">Сохранить данные</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@stop