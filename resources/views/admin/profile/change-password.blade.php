@extends('admin.layouts.main')

@section('content')

    <div class="row">
        @include('admin.widgets.user-left-menu')

        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-body">
                    {{ Form::open(['route' => 'admin.save-new-password', 'method' => 'put']) }}
                        @include('admin.widgets.form.password', ['name' => 'current_password', 'label' => 'Текущий пароль'])
                        @include('admin.widgets.form.password', ['name' => 'new_password', 'label' => 'Новый пароль'])
                        @include('admin.widgets.form.password', ['name' => 'new_password_confirmation', 'label' => 'Подтвердите пароль'])
                        <button type="submit" class="btn btn-primary btn-large">Сохранить данные</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@stop