@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.articles.store', 'files' => true]) !!}
        @include('admin.widgets.form.buttons', ['create' => true])
        @include('admin.content.articles.item._form')
        @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop