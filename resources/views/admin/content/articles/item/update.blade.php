@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.articles.update', 'news' => $news->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.content.articles.item._form', ['news' => $news])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop