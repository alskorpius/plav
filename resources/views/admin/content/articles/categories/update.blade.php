@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.articles-categories.update', 'categories' => $categories->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.content.articles.categories._form', ['categories' => $categories])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop