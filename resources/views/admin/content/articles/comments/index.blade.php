@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
    <a href="{{ route('admin.articles-comments.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' => 'comments'])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox"
                                       data-table='comments'>
                            </th>
                            <th>Комментарий</th>
                            <th>Имя статьи</th>
                            <th>Дата</th>
                            <th>Имя пользователя</th>
                            <th>Email пользователя</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($comments AS $comment)
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox"
                                           data-id='{{$comment->id}}'>
                                </td>
                                @if(strlen($comment->text) >= 60)
                                    <td width="25%"><a
                                            href="{{route('admin.articles-comments.edit', ['id' => $comment->id])}}">{{ substr($comment->text, 0, 60) }}
                                            ...</a></td>
                                @else
                                    <td width="25%"><a
                                            href="{{route('admin.articles-comments.edit', ['id' => $comment->id])}}">{{$comment->text}}</a>
                                    </td>
                                @endif
                                @if(strlen($comment->article->name) >= 40)
                                    <td width="15%"><a
                                            href="{{ route('admin.articles.edit', ['article' => $comment->article->id]) }}">{{ substr($comment->article->current->name, 0, 40) }}
                                            ...</a></td>
                                @else
                                    <td width="15%"><a
                                            href="{{ route('admin.articles.edit', ['article' => $comment->article->id]) }}">{{ $comment->article->current->name }}
                                        </a></td>
                                @endif
                                <td width="10%">{{isset($comment->created_at) ? $comment->created_at->format('d.m.Y') : '---'}}</td>
                                @if(isset($comment->user_id))
                                    <td width="15%"><a
                                            href="{{ route('admin.customers.edit', ['user' => $comment->user->id]) }}">{{ $comment->user->first_name . ' ' . $comment->user->second_name }}</a>
                                    </td>
                                    <td width="13%">{{$comment->user->email}}</td>
                                @elseif($comment->user_name)
                                    <td width="15%">{{$comment->user_name}}</td>
                                    <td width="13%">{{$comment->email}}</td>
                                @else
                                    <td width="15%">------</td>
                                    <td width="13%">------</td>
                                @endif
                                @if($comment->status == 0)
                                    <td width="10%"><span class="label label-danger">Не опубликовано</span></td>
                                @elseif($comment->status == 1)
                                    <td width="10%"><span class="label label-success">Опубликовано</span></td>
                                @elseif($comment->status == 2)
                                    <td width="10%"><span class="label label-warning">На модерации</span></td>
                                @endif
                                <td width="7%">@widget('listButtons', ['routeBase' => 'articles-comments.', 'id' => $comment->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $comments->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
