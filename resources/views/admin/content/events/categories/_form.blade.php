<?php
/** @var \App\Models\ArticlesCategory $object */
$object = isset($categories) ? $categories : null;
//dd($object);
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}">
                            @include('admin.widgets.form.input', ['name' => 'name', 'lang' => $language['slug'], 'label' => 'Название', 'object' => $langObject, 'options' => ['required', 'data-slug-origin']])
                        </div>
                    @endforeach
                </div>

                @include('admin.widgets.form.input', ['name' => 'slug', 'label' => 'Алиас', 'object' => $object, 'options' => ['required', 'data-slug-destination']])
                @include('admin.widgets.form.input', ['name' => 'position', 'label' => 'Позиция', 'object' => $object, 'options' => ['required', 'data-position']])
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-warning">
            <div class="box-body">
                @include('admin.widgets.form.status', ['status' => old('status', isset($object) ? $object->status : 0)])

                <div class="form-group">
                    {!! Form::label('menu', 'Отображать в меню') !!}
                    <div class="radio">
                        <label>{!! Form::radio('menu', 0, isset($object->menu) && $object->menu == 0, ['class' => 'minimal']) !!} Не отображать</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('menu', 1, isset($object->menu) && $object->menu == 1, ['class' => 'minimal']) !!} Отображать</label>
                    </div>
                </div>
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}meta"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}meta">
                            @include('admin.widgets.form.input', ['name' => 'h1', 'lang' => $language['slug'], 'object' => $langObject])
                            @include('admin.widgets.form.input', ['name' => 'title', 'lang' => $language['slug'], 'object' => $langObject])
                            @include('admin.widgets.form.textarea', ['name' => 'description', 'lang' => $language['slug'], 'object' => $langObject])
                            @include('admin.widgets.form.textarea', ['name' => 'keywords', 'lang' => $language['slug'], 'object' => $langObject])
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
