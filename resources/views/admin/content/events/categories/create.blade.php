@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.events-categories.store', 'files' => true]) !!}
        @include('admin.widgets.form.buttons', ['create' => true])
        @include('admin.content.events.categories._form')
        @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop