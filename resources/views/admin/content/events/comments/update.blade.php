@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.events-comments.update', 'categories' => $comments->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.content.events.comments._form', ['comments' => $comments])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop