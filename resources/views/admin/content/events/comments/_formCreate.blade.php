<?php
/** @var \App\Models\ArticlesCategory $object */
$object = isset($comments) ? $comments : null;
//dd($object);
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.input', ['name' => 'text', 'label' => 'Текст комментария', 'object' => $object, 'options' => ['required']])
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-warning">
            <div class="box-body">
                @include('admin.widgets.form.status', ['status' => old('status', isset($object) ? $object->status : 0)])
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-body">
                @include('admin.widgets.form.date', ['name' => 'created_at', 'label' => 'Дата комментария',])
                @include('admin.widgets.form.select', ['additionalClass' => 'articlesSelected', 'name' => 'article_id', 'label' => 'Статья',  'data' => $articles, 'options' => ['required']])
                @include('admin.widgets.form.select', ['name' => 'user_id', 'label' => 'Пользователь',  'data' => $users, 'options' => ['class' => 'form-control select2']])
            </div>
        </div>
    </div>
</div>