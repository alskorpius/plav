<?php
/** @var \App\Models\ArticlesCategory $object */
$object = isset($comments) ? $comments : null;
//dd($object);
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.input', ['name' => 'text', 'label' => 'Текст комментария', 'object' => $object, 'options' => ['required']])
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-warning">
            <div class="box-body">
                @include('admin.widgets.form.status', ['status' => old('status', isset($object) ? $object->status : 0)])
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-body">
                @include('admin.widgets.form.date', ['name' => 'created_at', 'label' => 'Дата создания', 'object' => $object, 'options' => ['']])
                <div class="form-group  ">
                    <label class="control-label">Статья</label>
                    <div>
                        <a href="{{route('admin.news.edit', ['id' => $article->id])}}">{{$article->name}}</a>
                    </div>
                </div>
                <div class="form-group  ">
                    <label class="control-label">Пользователь</label>
                    <div>
                        @if(isset($user))
                            <a href="{{route('admin.customers.edit', ['id' => $user->id])}}">{{$user->first_name . ' ' . $user->second_name}}</a>
                        @else
                            <p>---</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
