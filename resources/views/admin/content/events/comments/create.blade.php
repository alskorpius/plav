@extends('admin.layouts.main')
@section('content')
    {!! Form::open(['route' => 'admin.events-comments.store', 'files' => true]) !!}
    @include('admin.widgets.form.buttons', ['create' => true])
    @include('admin.content.events.comments._formCreate')
    @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop