@extends('admin.layouts.main')

@section('custom-buttons')
    <a href="{{ route('admin.events-comments.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' => 'comments'])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Комментарий</th>
                            <th>Имя статьи</th>
                            <th>Имя пользователя</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($comments AS $comment)
                            <tr>
                                <td width="60%">{!! $comment->text !!}</td>
                                <td width="10%"><a
                                            href="{{ route('admin.news.edit', ['article' => $comment->article->id]) }}">{{ substr($comment->article->current->name, 0, 25) }}
                                        ...</a></td>
                                @if(isset($comment->user_id))
                                    <td width="10%"><a href="{{ route('admin.customers.edit', ['user' => $comment->user->id]) }}">{{ $comment->user->first_name . ' ' . $comment->user->second_name }}</a></td>
                                @else
                                    <td>---------</td>
                                @endif

                                <td width="5%">@widget('status', ['status' => $comment->status, 'table' => 'comments', 'id' => $comment->id])</td>
                                <td width="5%">@widget('listButtons', ['routeBase' => 'events-comments.', 'id' => $comment->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $comments->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
