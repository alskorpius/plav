@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
	<button class="btn bg-yellow btn-flat margin" id="statusChecked">Снять с публикации</button>
	<button class="btn bg-blue btn-flat margin" id="statusCheckedEnable">Опубликовать</button>
	<a href="{{ route('admin.events.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' => [
        ['name' => 'name', 'placeholder' => 'Имя'],
        ['name' => 'published_at', 'type' => 'date', 'placeholder' => 'Дата публикации'],
        ['name' => 'category_id', 'type' => 'select', 'data' => $categories, 'placeholder' => 'Тип']
    ]])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox"
                                       data-table='articles'>
                            </th>
                            <th>Название</th>
                            <th>Дата публикации</th>
                            <th>Количество просмотров</th>
                            <th>Количество регистраций</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($news AS $article)
                            @php
                                if(!empty($article->current)){
                                    $article->name =  $article->current->name;
                                } else{
                                    $id = $article->id;
                                    $article->id = $article->row_id;
                                    $article->row_id = $id;
                                }
                            @endphp
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox"
                                           data-id='{{$article->id}}'>
                                </td>
                                <td>
                                    <a href="{{route('admin.events.edit', ['id' => $article->id])}}">{{ $article->name }}</a>
                                </td>
                                <td>{{ $article->published_at->format('d.m.Y') }}</td>
                                <td>&emsp;&emsp;&emsp;&emsp;{{ $article->view_count }}</td>
                                <td>
                                    &emsp;&emsp;&emsp;&emsp; <a href="{{route('admin.events-sub.index', ['idArticle' => $article->id])}}">{{ count($article->eventsRegistration) }}</a>
                                </td>
								@if(isset( $article->articlesCategory) && $article->articlesCategory->status == 0 && $article->status !== 0)
									<td>
										<span class="label label-danger grey-imp" title="Для отображение опубликуйте категорию">Не отображается</span>
									</td>
								@else
									@if($article->status == 0)
										<td>
											<span class="label label-danger">Не опубликовано</span>
										</td>
									@elseif($article->status == 1)
										<td>
											<span class="label label-success">Опубликовано</span>
										</td>
									@else
										<td>
											<span class="label label-warning">Отложено</span>
										</td>
									@endif
								@endif
                                <td>@widget('listButtons', ['routeBase' => 'events.', 'id' => $article->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $news->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
