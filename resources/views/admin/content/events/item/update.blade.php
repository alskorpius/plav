@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.events.update', 'news' => $news->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.content.events.item._form', ['news' => $news])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop