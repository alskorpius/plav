@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.news-categories.store', 'files' => true]) !!}
        @include('admin.widgets.form.buttons', ['create' => true])
        @include('admin.content.news.categories._form')
        @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop