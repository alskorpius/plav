@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
	<button class="btn bg-yellow btn-flat margin" id="statusChecked">Снять с публикации</button>
	<button class="btn bg-blue btn-flat margin" id="statusCheckedEnable">Опубликовать</button>
    <a href="{{ route('admin.news-categories.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' => ['name']])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox" data-table = 'articles_categories'>
                            </th>
                            <th>Название</th>
                            <th>Позиция</th>
                            <th></th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($categories AS $category)
                            @php
                                if(!empty($category->current)){
                                    $category->name =  $category->current->name;
                                } else{
                                    $id = $category->id;
                                    $category->id = $category->row_id;
                                    $category->row_id = $id;
                                }
                            @endphp
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox" data-id = '{{$category->id}}'>
                                </td>
                                <td width="40%"><a href="{{route('admin.news-categories.edit', ['id' => $category->id])}}">{{ $category->name }}</a></td>
                                <td width="5%"><input value="{{$category->position}}" type="text" class="form-control set-position-category" data-id = '{{$category->id}}'></td>
                                <td width="30%"></td>
                                <td width="10%">@widget('status', ['status' => $category->status, 'table' => 'articles_categories', 'id' => $category->id])</td>
                                <td width="10%">@widget('listButtons', ['routeBase' => 'news-categories.', 'id' => $category->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $categories->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
