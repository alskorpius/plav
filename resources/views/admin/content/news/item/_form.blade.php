<?php
/** @var \App\Models\News $object */
$object = isset($news) ? $news : null;
?>
<div class="row">
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.input', ['name' => 'slug', 'label' => 'Алиас', 'object' => $object, 'options' => ['required', 'data-slug-destination']])
                @include('admin.widgets.form.dateTime', ['name' => 'published_at', 'label' => 'Дата публикации',  'object' => $object, 'options' => ['required', 'class' => 'form-control dateTimePicker']])
                @include('admin.widgets.form.select', ['name' => 'articles_categories_id', 'label' => 'Категория', 'data' => $categories, 'object' => $object, 'options' => ['']])
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}">
                            @include('admin.widgets.form.input', ['name' => 'name', 'lang' => $language['slug'], 'label' => 'Название', 'object' => $langObject, 'options' => ['required', 'data-slug-origin']])
                            @include('admin.widgets.form.tiny', ['name' => 'text', 'lang' => $language['slug'], 'label' => 'Контент', 'object' => $langObject])
                        </div>
                    @endforeach
                </div>
                @if(isset($object))
                    <a href="{{ route('admin.news-comments.index', ['idArticle' => $object->id]) }}"
                       class="btn bg-olive btn-flat margin">Отзывы к новости</a>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="box box-warning">
            <div class="box-body">
                <div class="form-group">
                    {!! Form::label('status', 'Статус') !!}
                    <div class="radio">
                        <label>{!! Form::radio('status', 0, isset($object->status) && $object->status == 0, ['class' => 'minimal']) !!}
                            Неопубликовано</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('status', 1, isset($object->status) && $object->status == 1, ['class' => 'minimal']) !!}
                            Опубликовано</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('status', 2, isset($object->status) && $object->status == 2, ['class' => 'minimal']) !!}
                            Отложено</label>
                    </div>
                </div>
                @if(isset($object) && $object->image && $object->imageExists('small'))
                    @include('admin.widgets.form.image', ['big' => $object->showImage('original', null, true), 'preview' => $object->showImage('small', null, true), 'id' => $news->id, 'model' => $news->getMorphClass()])
                @else
                    @include('admin.widgets.form.image')
                @endif
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}meta"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}meta">
                            @include('admin.widgets.form.input', ['name' => 'h1', 'lang' => $language['slug'], 'object' => $langObject])
                            @include('admin.widgets.form.input', ['name' => 'title', 'lang' => $language['slug'], 'object' => $langObject])
                            @include('admin.widgets.form.textarea', ['name' => 'description', 'lang' => $language['slug'], 'object' => $langObject])
                            @include('admin.widgets.form.textarea', ['name' => 'keywords', 'lang' => $language['slug'], 'object' => $langObject])
                        </div>
                    @endforeach
                </div>
                @include('admin.widgets.form.select', ['name' => 'products[]', 'value' => isset($selected) ? $selected : [], 'label' => 'Рекомендуемые товары', 'data' => $products, 'options' => ['class' => 'form-control select2', 'multiple' => 'multiple']])
            </div>
        </div>
    </div>
</div>
