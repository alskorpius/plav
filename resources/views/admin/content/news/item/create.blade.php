@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.news.store', 'files' => true]) !!}
        @include('admin.widgets.form.buttons', ['create' => true])
        @include('admin.content.news.item._form')
        @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop