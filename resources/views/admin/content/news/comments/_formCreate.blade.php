<?php
/** @var \App\Models\ArticlesCategory $object */
$object = isset($comments) ? $comments : null;
//dd($object);
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.input', ['name' => 'text', 'label' => 'Текст комментария', 'object' => $object, 'options' => ['required']])
                @include('admin.widgets.form.date', ['name' => 'created_at', 'label' => 'Дата комментария',])
                @include('admin.widgets.form.select', ['additionalClass' => 'articlesSelected', 'name' => 'article_id', 'label' => 'Новость',  'data' => $articles, 'options' => ['required']])
                @include('admin.widgets.form.select', ['name' => 'user_id', 'label' => 'Пользователь',  'data' => $users, 'options' => ['class' => 'form-control select2']])
                @include('admin.widgets.form.input', ['name' => 'user_name', 'label' => 'Имя пользователя (если он не зарегистрирован)', 'object' => $object, 'options' => ['']])
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-warning">
            <div class="box-body">
                <div class="form-group">
                    {!! Form::label('status', 'Статус') !!}
                    <div class="radio">
                        <label>{!! Form::radio('status', 0, isset($object) && $object->status == 0, ['class' => 'minimal']) !!}
                            Неопубликовано</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('status', 1, isset($object) && $object->status == 1, ['class' => 'minimal']) !!}
                            Опубликовано</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('status', 2, isset($object) && $object->status == 2, ['class' => 'minimal']) !!} На
                            модерации</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>