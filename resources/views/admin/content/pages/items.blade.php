<?php
/**
 * @var \App\Models\Page[] $pages
 * @var integer $cur
 */
?>

@if(isset($pages) and sizeof($pages))
    @foreach ($pages as $page)
        <li class="dd-item dd3-item" data-id="{{ $page->id }}">
            <div title="Переместить строку" class="dd-handle dd3-handle">Drag</div>
            <div class="dd3-content">
                <table style="width: 100%;">
                    <tr>
                        <td class="column-drag" width="1%"></td>
                        <td valign="top" class="pagename-column" width="60%">
                            <div class="clearFix">
                                <div class="pull-left">
                                    <div class="overflow-20">
                                        <a class="pageLinkEdit"
                                           href="{{ route('admin.pages.edit', ['page' => $page->id]) }}">{{ $page->current->name }}</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td width="45" valign="top" class="icon-column status-column" width="19%">
                            @widget('status', ['status' => $page->status, 'table' => 'pages', 'id' => $page->id])
                        </td>
                        <td class="nav-column icon-column" valign="top" width="20%">
                                @widget('listButtons', ['routeBase' => 'pages.', 'id' => $page->id, 'view' => false])
                        </td>
                    </tr>
                </table>
            </div>
        </li>
    @endforeach
@endif
