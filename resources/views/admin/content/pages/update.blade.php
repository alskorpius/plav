@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.pages.update', 'page' => $page->id], 'method' => 'PUT']) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.content.pages._form', ['page' => $page])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop