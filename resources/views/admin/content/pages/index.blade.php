@extends('admin.layouts.main')

@section('custom-buttons')
    <a href="{{ route('admin.pages.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' => ['name']])
    <div class="row">
        <div class="col-xs-12">
            <div class="dd pageList" id="myNest" data-depth="1">
                <ol class="dd-list">
                    @include('admin.content.pages.items', ['pages' => $pages, 'cur' => 0])
                </ol>
            </div>
            <span id="parameters" data-table="pages"></span>
            <input type="hidden" id="myNestJson">
        </div>
    </div>
@endsection