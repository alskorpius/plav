<?php
$object = isset($page) ? $page : null;
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.input', ['placeholder' => 'Системная сраница','type' => 'hidden', 'name' => 'static_page', 'label' => false, 'object' => $object, 'options' => []])
                @include('admin.widgets.form.input', ['name' => 'slug', 'label' => 'Алиас', 'object' => $object, 'options' => ['required', 'data-slug-destination']])
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}">
                            @include('admin.widgets.form.input', ['name' => 'name', 'lang' => $language['slug'], 'label' => 'Название', 'object' => $langObject, 'options' => ['required', 'data-slug-origin']])
                            @include('admin.widgets.form.tiny', ['name' => 'content', 'lang' => $language['slug'], 'label' => 'Контент', 'object' => $langObject])
                        </div>
                    @endforeach
                </div>
                @if(isset($object))
                    <a href="{{ route('admin.news-comments.index', ['idArticle' => $object->id]) }}"
                       class="btn bg-olive btn-flat margin">Отзывы к статье</a>
                @endif

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-warning">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
                        @include('admin.widgets.form.status', ['status' => old('status', isset($page) ? $page->status : 0)])
                    </div>
                </div>
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}meta"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}meta">
                            @include('admin.widgets.form.input', ['name' => 'h1', 'lang' => $language['slug'], 'object' => $langObject])
                            @include('admin.widgets.form.input', ['name' => 'title', 'lang' => $language['slug'], 'object' => $langObject])
                            @include('admin.widgets.form.textarea', ['name' => 'description', 'lang' => $language['slug'], 'object' => $langObject])
                            @include('admin.widgets.form.textarea', ['name' => 'keywords', 'lang' => $language['slug'], 'object' => $langObject])
                        </div>
                    @endforeach
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
