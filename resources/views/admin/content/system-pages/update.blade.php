@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.system-pages.update', 'system_page' => $page->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons', ['except' => ['add']])
        @include('admin.content.system-pages._form', ['page' => $page])
        @include('admin.widgets.form.buttons', ['except' => ['add']])
    {!! Form::close() !!}
@stop