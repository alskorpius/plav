@extends('admin.layouts.main')

@section('content')
    @widget('filter', ['fields' => ['name', 'slug']])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Название</th>
                            <th>Алиас</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach($pages AS $page)
                            <tr>
                                <td>{{ $page->current->name }}</td>
                                <td>{{ $page->slug }}</td>
                                <td>@widget('status', ['status' => $page->status, 'table' => 'system_pages', 'id' => $page->id])</td>
                                <td>@widget('listButtons', ['routeBase' => 'system-pages.', 'id' => $page->id, 'view' => false, 'delete' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $pages->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
