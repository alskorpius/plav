<?php
/** @var \App\Models\SystemPage $object */
$object = isset($page) ? $page : null;
?>
<div class="row">
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="box-body">
                <div class="form-group">
                    {!! Form::label('status', 'Отображать на английской версии сайта?') !!}
                    <div class="radio">
                        <label>{!! Form::radio('status', 1, isset($object->status) && $object->status == 1, ['class' => 'minimal']) !!}
                            Да</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('status', 0, isset($object->status) && $object->status == 0, ['class' => 'minimal']) !!}
                            Нет</label>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('Алиас') }}
                    <div class="form-control disabled">{{ $object->slug }}</div>
                </div>
                <div class="form-group">
                    {{ Form::label('Имя') }}
                    <ul class="nav nav-tabs">
                        @foreach($languages AS $language)
                            <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}"
                                                                                        data-toggle="tab">{{ $language['name'] }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($languages AS $language)
                            <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                            <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}">
                                <div class="form-control disabled">{{ $langObject->name }}</div>
                                @include('admin.widgets.form.tiny', ['name' => 'content', 'lang'=>$language['slug'], 'label' => 'Контент', 'object' => $langObject])
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="box box-warning">
            <div class="box-body">
                @if($object->slug === '/about-us/who-are-we' || $object->slug === '/about-us/quality')
                    @if(isset($object) && $object->image && $object->imageExists('small'))
                        @include('admin.widgets.form.image', ['big' => $object->showImage('original', null, true), 'preview' => $object->showImage('small', null, true), 'id' => $page->id, 'model' => $page->getMorphClass()])
                    @else
                        @include('admin.widgets.form.image')
                    @endif
                @endif
                    <ul class="nav nav-tabs">
                        @foreach($languages AS $language)
                            <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}meta"
                                                                                        data-toggle="tab">{{ $language['name'] }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($languages AS $language)
                            <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                            <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}meta">
                                @include('admin.widgets.form.input', ['name' => 'h1', 'lang'=>$language['slug'], 'object' => $langObject])
                                @include('admin.widgets.form.input', ['name' => 'title', 'lang'=>$language['slug'], 'object' => $langObject])
                                @include('admin.widgets.form.textarea', ['name' => 'description', 'lang'=>$language['slug'], 'object' => $langObject])
                                @include('admin.widgets.form.textarea', ['name' => 'keywords', 'lang'=>$language['slug'], 'object' => $langObject])
                            </div>
                        @endforeach
                    </div>

            </div>
        </div>
    </div>
</div>
@if($object->slug === '/contacts')
    <div class="row">
        <div class="col-md-12">
            <div class="dropModule">
                <div class="box box-solid dropBox">
                    <div class="box-header with-border">
                        <i class="fa fa-map"></i>
                        Настройка карты
                    </div>
                    <div class="box-body">
                        <div class="mapWrap" style="height: 450px;">
                            <input type="text" id="address"
                                   class="form-control ui-autocomplete-input"
                                   placeholder="Начните писать адрес..."
                                   autocomplete="off">
                            <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                            <div style="width: 100%; height: calc(100% - 30px); position: relative; background-color: rgb(229, 227, 223); overflow: hidden;"
                                 id="gmap"></div>
                        </div>
                        @include('admin.widgets.form.hidden', ['name' => 'SETTINGS[latitude]', 'value' => $object->latitude, 'options' => ['id' => 'latitude']])
                        @include('admin.widgets.form.hidden', ['name' => 'SETTINGS[longitude]', 'value' => $object->longitude, 'options' => ['id' => 'longitude']])
                        @include('admin.widgets.form.hidden', ['name' => 'SETTINGS[zoom]', 'value' => $object->zoom, 'options' => ['id' => 'zoom']])
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
    <script src="https://maps.google.com/maps/api/js?key={{ config('db.services.google-maps-api-key', config('services.google.maps.key')) }}"
            type="text/javascript"></script>
    <script type="text/javascript">
        $('#address').val('');
        var map = false, pointt,
            geocoder = false,
            myLatlng = new google.maps.LatLng(parseFloat('{{ $object->latitude }}'), parseFloat('{{ $object->longitude }}')),
            zoom = parseInt('{{ $object->zoom }}'),
            marker = true;

        function initialize() {
            var mapOptions = {
                center: myLatlng,
                zoom: zoom,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("gmap"), mapOptions);
            geocoder = new google.maps.Geocoder();

            map.addListener('zoom_changed', function () {
                $('#zoom').val(map.getZoom());
            });
        }

        function bind_location(location) {
            $('#latitude').val(location.lat());
            $('#longitude').val(location.lng());
        }

        function placeMarker(location) {
            if (marker) {
                marker.setMap(null);
            }
            bind_location(location);
            pointt = new google.maps.Point(18, 54);
            marker = new google.maps.Marker({
                position: location,
                map: map,
                draggable: true
            });
            google.maps.event.addListener(marker, 'dragend', function (cc) {
                bind_location(cc.latLng);
            });
        }

        $(document).ready(function () {
            initialize();
            google.maps.event.addListener(map, 'click', function (cc) {
                placeMarker(cc.latLng);
            });

            if (marker) {
                pointt = new google.maps.Point(18, 54);
                marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    draggable: true
                });
                google.maps.event.addListener(marker, 'dragend', function (cc) {
                    bind_location(cc.latLng);
                });
            }

            $('#address').autocomplete({
                //Определяем значение для адреса при геокодировании
                source: function (request, response) {
                    geocoder.geocode({'address': request.term}, function (results, status) {
                        var resp = [];
                        for (var i in results) {
                            resp.push({
                                label: results[i].formatted_address,
                                value: results[i].formatted_address,
                                latitude: results[i].geometry.location.lat(),
                                longitude: results[i].geometry.location.lng()
                            });
                        }
                        response(resp);
                    });
                },
                select: function (event, ui) {
                    $('#latitude').val(ui.item.latitude);
                    $('#longitude').val(ui.item.longitude);
                    var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
                    map.setCenter(location);
                    map.setZoom(zoom);
                    placeMarker(location);
                }
            });
        });
    </script>
    @endpush
@endif
