@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.callbacks.update', 'callback' => $callback->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.callback.callbacks._form', ['callback' => $callback])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop