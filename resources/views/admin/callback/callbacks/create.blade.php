@extends('admin.layouts.main')
@section('content')
    {!! Form::open(['route' => 'admin.callbacks.store', 'files' => true]) !!}
    @include('admin.widgets.form.buttons', ['create' => true])
    @include('admin.callback.callbacks._form')
    @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop