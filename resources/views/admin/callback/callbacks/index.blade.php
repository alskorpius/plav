@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
    <a href="{{ route('admin.callbacks.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' =>[
        ['name' => 'name', 'label' => 'Имя'],
        ['name' => 'phone', 'label' => 'Телефон'],
        ['name' => 'created_at', 'type' => 'date', 'label' => 'Дата'],]])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox"
                                       data-table='callbacks'>
                            </th>
                            <th>Имя</th>
                            <th>Телефон</th>
                            <th>Дата</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($callbacks AS $callback)
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox"
                                           data-id='{{$callback->id}}'>
                                </td>
                                <td><a
                                            href="{{route('admin.callbacks.edit', ['id' => $callback->id])}}">{{ $callback->name }}</a>
                                </td>
                                <td>{{ $callback->phone }}</td>
                                <td>{{ $callback->created_at->format('d.m.Y h:i:s') }}</td>
                                <td>@widget('status', ['status' => $callback->status, 'table' => 'callbacks', 'id' => $callback->id])</td>
                                <td>@widget('listButtons', ['routeBase' => 'callbacks.', 'id' => $callback->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $callbacks->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection