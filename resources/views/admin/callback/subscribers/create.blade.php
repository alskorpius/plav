@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.subscribers.store', 'files' => false, 'name' => 'mailForm']) !!}
    {{csrf_field()}}
    @include('admin.widgets.form.buttons', ['send' => true, 'except' => ['add']])
    @include('admin.callback.subscribers._form')
    {!! Form::close() !!}
@stop