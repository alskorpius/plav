@extends('admin.layouts.main')

@section('custom-buttons')
    <a href="{{ route('admin.subscribers.create') }}" class="btn bg-olive btn-flat margin">Отправить сообщение</a>
    <a href="{{ route('admin.subscribers.createNew') }}" class="btn bg-blue btn-flat margin">Добавить подписчика</a>
    <a href="{{ route('admin.send-news.index') }}" class="btn bg-orange btn-flat margin">Разослать новости</a>
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
@endsection

@section('content')
    @widget('filter', ['fields' => 'subscribers'])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox" data-table = 'subscribers'>
                            </th>
                            <th>Email</th>
                            <th>Дата подписки</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($subscribers AS $subscriber)
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox" data-id = '{{$subscriber->id}}'>
                                </td>
                                <td>{{ $subscriber->email }}</td>
                                <td>{{ $subscriber->created_at->format('d.m.Y') }}</td>
                                <td>@widget('status', ['status' => $subscriber->status, 'table' => 'subscribers', 'id' => $subscriber->id])</td>
                                <td>@widget('listButtons', ['routeBase' => 'subscribers.', 'id' => $subscriber->id, 'view' => false, 'edit' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $subscribers->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
