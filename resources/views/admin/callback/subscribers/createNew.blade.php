@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.subscribers.storeNew', 'files' => false, 'name' => 'mailForm']) !!}
    {{csrf_field()}}
    @include('admin.widgets.form.buttons', ['create' => true, 'except' => ['add']])
    @include('admin.callback.subscribers._formNew')
    {!! Form::close() !!}
@stop