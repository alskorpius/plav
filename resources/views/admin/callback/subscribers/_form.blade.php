<?php
/** @var \App\Models\News $object */
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.input', ['name' => 'subject', 'label' => 'Тема', 'options' => ['required']])
                @include('admin.widgets.form.tiny', ['name' => 'letter', 'label' => 'Текст', 'options' => []])
            </div>
        </div>
    </div>
</div>