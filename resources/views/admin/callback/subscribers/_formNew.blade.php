<?php
/** @var \App\Models\News $object */
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="col-md-8">
                    @include('admin.widgets.form.input', ['name' => 'email', 'label' => 'Email', 'options' => ['required']])
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('status', 'Статус') !!}
                        <div class="radio">
                            <label>{!! Form::radio('status', 0, isset($status) && !$status, ['class' => 'minimal']) !!}
                                Не подписан</label>
                        </div>
                        <div class="radio">
                            <label>{!! Form::radio('status', 1, !isset($status) || $status, ['class' => 'minimal']) !!}
                                Подписан</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>