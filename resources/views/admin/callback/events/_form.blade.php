<?php
/** @var \App\Models\ArticlesCategory $object */
$object = isset($eventsSub) ? $eventsSub : null;
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.input', ['name' => 'name', 'label' => 'Имя', 'object' => $object, 'options' => ['required']])
                @include('admin.widgets.form.input', ['name' => 'phone', 'label' => 'Телефон', 'object' => $object, 'options' => ['']])
                @include('admin.widgets.form.input', ['name' => 'email', 'label' => 'Email', 'object' => $object, 'options' => ['required']])

                @include('admin.widgets.form.select', ['name' => 'article_id', 'label' => 'Событие', 'data' => $events, 'object' => $object, 'options' => ['required']])
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-warning">
            <div class="box-body">
                <div class="form-group">
                    {!! Form::label('status', 'Статус') !!}
                    <div class="radio">
                        <label>{!! Form::radio('status', 0, isset($object->status) && !$object->status, ['class' => 'minimal']) !!}
                            Не обработан</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('status', 1, !isset($object->status) || $object->status, ['class' => 'minimal']) !!}
                            Обработан</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>