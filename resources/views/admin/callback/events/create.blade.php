@extends('admin.layouts.main')
@section('content')
    {!! Form::open(['route' => 'admin.events-sub.store', 'files' => true]) !!}
    @include('admin.widgets.form.buttons', ['create' => true])
    @include('admin.callback.events._form')
    @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop