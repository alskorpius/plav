@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.events-sub.update', 'eventsSub' => $eventsSub->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.callback.events._form', ['eventsSub' => $eventsSub])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop