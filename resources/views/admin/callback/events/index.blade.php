@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
    <a href="{{ route('admin.events-sub.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' =>[
        ['name' => 'name', 'label' => 'Имя'],
        ['name' => 'phone', 'label' => 'Телефон'],
        ['name' => 'email', 'label' => 'Email'],
        ['name' => 'event', 'type' => 'select', 'data' => $events, 'placeholder' => 'Событие'],
        ['name' => 'created_at', 'type' => 'date', 'label' => 'Дата регистрации'],]])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox"
                                       data-table='events_registrations'>
                            </th>
                            <th>Имя</th>
                            <th>Телефон</th>
                            <th>Email</th>
                            <th>Дата</th>
                            <th>Событие</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($eventSubs AS $eventSub)
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox"
                                           data-id='{{$eventSub->id}}'>
                                </td>
                                <td width="15%"><a
                                        href="{{route('admin.events-sub.edit', ['id' => $eventSub->id])}}">{{ $eventSub->name }}</a>
                                </td>
                                <td width="15%">{{ $eventSub->phone }}</td>
                                <td width="15%">{{ $eventSub->email }}</td>
                                <td width="15%">{{ $eventSub->created_at->format('d.m.Y') }}</td>
                                @if(strlen($eventSub->article->name) >= 40)
                                    <td width="20%"><a
                                            href="{{route('admin.events.edit', ['id' => $eventSub->article->id])}}">{{ substr($eventSub->article->current->name, 0, 40) }} ...</a></td>
                                @else
                                    <td width="20%"><a
                                            href="{{route('admin.events.edit', ['id' => $eventSub->article->id])}}">{{ $eventSub->article->current->name }} </a></td>
                                @endif
                                <td width="10%">@widget('status', ['status' => $eventSub->status, 'table' => 'events_registrations', 'id' => $eventSub->id])</td>
                                <td width="10%">@widget('listButtons', ['routeBase' => 'events-sub.', 'id' => $eventSub->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $eventSubs->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
