@extends('admin.layouts.main')

@section('custom-buttons')
    <a href="{{ route('admin.slider.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="dd pageList" id="myNest" data-depth="1">
                <ol class="dd-list">
                    @include('admin.slider.items', ['slider' => $slider])
                </ol>
            </div>
            <span id="parameters" data-table="slider"></span>
            <input type="hidden" id="myNestJson">
        </div>
    </div>
@endsection