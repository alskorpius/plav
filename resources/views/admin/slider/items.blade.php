<?php
/**
 * @var \App\Models\Slider[] $slider
 */
?>

@if(isset($slider) and sizeof($slider))
    @foreach ($slider as $element)
        <li class="dd-item dd3-item" data-id="{{ $element->id }}">
            <div title="Переместить строку" class="dd-handle dd3-handle">Drag</div>
            <div class="dd3-content">
                <table style="width: 100%;">
                    <tr>
                        <td class="column-drag" width="1%"></td>
                        <td valign="top" class="pagename-column" width="20%">
                            <div class="clearFix">
                                <div class="pull-left">
                                    <div class="overflow-20">
                                        <a class="pageLinkEdit"
                                           href="{{ route('admin.slider.edit', ['slider' => $element->id]) }}">
                                            <img src="{{ $element->showImage('icon') }}" alt="{{ $element->alt }}"
                                                 title="{{ $element->current->title }}" style="max-height:20px;">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <a class="pageLinkEdit" href="{{ route('admin.slider.edit', ['slider' => $element->id]) }}">
                                <p>{{$element->current->title}}</p>
                            </a>
                        </td>
                        <td width="45" valign="top" class="icon-column status-column" width="19%">
                            @widget('status', ['status' => $element->status, 'table' => 'sliders', 'id' => $element->id])
                        </td>
                        <td class="nav-column icon-column" valign="top" width="20%">
                            @widget('listButtons', ['routeBase' => 'slider.', 'id' => $element->id, 'view' => false])
                        </td>
                    </tr>
                </table>
            </div>
        </li>
    @endforeach
@endif
