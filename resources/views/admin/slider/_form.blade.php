<?php
/**
 * @var \App\Models\Slider $slide
 */
$object = isset($slide) ? $slide : null;
?>
<div class="row">
    <div class="col-md-9">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.status', ['status' => old('status', isset($object) ? $object->status : 0)])
                @include('admin.widgets.form.input', ['name' => 'url', 'label' => 'Ссылка', 'object' => $object, 'options' => ['']])
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}">
                            @include('admin.widgets.form.input', ['name' => 'alt', 'lang' => $language['slug'], 'label' => 'Alt', 'object' => $langObject, 'options' => ['']])
                            @include('admin.widgets.form.input', ['name' => 'title', 'lang' => $language['slug'], 'label' => 'Title', 'object' => $langObject, 'options' => ['']])
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box box-warning">
            <div class="box-body">
                @if(isset($object) && $object->image)
                    @include('admin.widgets.form.image', [
                        'big' => $object->showImage('big', null, true),
                        'preview' => $object->showImage('icon', null, true),
                        'id' => $object->id,
                        'model' => $object->getMorphClass(),
                    ])
                @else
                    @include('admin.widgets.form.image', ['options' => ['required']])
                @endif
            </div>
        </div>
    </div>
</div>
