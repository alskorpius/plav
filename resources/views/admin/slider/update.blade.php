@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.slider.update', 'slider' => $slide->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.slider._form', ['slide' => $slide])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop