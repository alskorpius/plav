@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.slider.store', 'files' => true]) !!}
        @include('admin.widgets.form.buttons', ['create' => true])
        @include('admin.slider._form')
        @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop