<?php
/** @var \App\Models\ArticlesCategory $object */
$object = isset($blacklist) ? $blacklist : null;
$email = isset($email) ? $email : null;
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.date', ['name' => 'until_at', 'label' => 'Заблокировать до (оставте поле пустым, что бы заблокировать навсегда)', 'object' => $object,])
                @include('admin.widgets.form.input', ['name' => 'text', 'label' => 'Причина блокировки', 'object' => $object, 'options' => ['required']])
                @include('admin.widgets.form.select', ['name' => 'type', 'label' => 'Тип блокировки', 'data' => [
                \App\Models\Blacklist::TYPE_IP => 'IP',
                \App\Models\Blacklist::TYPE_EMAIL => 'EMAIL',
                ], 'object' => $object, 'options' => ['required', 'id' => 'blockSelect',]])
                @include('admin.widgets.form.input', ['additionalClass' => 'blockEmail','name' => 'email', 'label' => 'Email', 'object' => $object, 'options' => []])
                @include('admin.widgets.form.input', ['additionalClass' => 'blockIP', 'name' => 'ip', 'label' => 'IP', 'object' => $object, 'options' => []])
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-warning">
            <div class="box-body">
                <div class="form-group">
                    {!! Form::label('status', 'Статус') !!}
                    <div class="radio">
                        <label>{!! Form::radio('status', 0, isset($object->status) && !$object->status, ['class' => 'minimal']) !!}
                            Не заблокирован</label>
                    </div>
                    <div class="radio">
                        <label>{!! Form::radio('status', 1, !isset($object->status) || $object->status, ['class' => 'minimal']) !!}
                            Заблокирован</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
