@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.blacklist.update', 'blacklist' => $blacklist->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.blacklist._form', ['blacklist' => $blacklist])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop