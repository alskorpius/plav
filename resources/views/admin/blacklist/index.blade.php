@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
    <a href="{{ route('admin.blacklist.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' => 'blacklist'])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox" data-table = 'blacklist'>
                            </th>
                            <th>Email</th>
                            <th>IP</th>
                            <th>Причина бана</th>
                            <th>Забаннен до</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($bannedUsers AS $bannedUser)
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox" data-id = '{{$bannedUser->id}}'>
                                </td>
                                <td width="20%">{{ $bannedUser->email ? $bannedUser->email : '---' }}</td>
                                <td width="20%">{{ $bannedUser->ip ? $bannedUser->ip : '---' }}</td>
                                <td width="20%">{{ $bannedUser->text }}</td>
                                <td width="20%">{{ $bannedUser->until_at ? substr($bannedUser->until_at,0,strpos(trim($bannedUser->until_at),' ')) : 'Навсегда' }}</td>
                                <td width="10%">@widget('status', ['status' => $bannedUser->status, 'table' => 'blacklist', 'id' => $bannedUser->id])</td>
                                <td width="10%">@widget('listButtons', ['routeBase' => 'blacklist.', 'id' => $bannedUser->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $bannedUsers->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection