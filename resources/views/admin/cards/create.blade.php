@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.cards.store']) !!}
        @include('admin.widgets.form.buttons', ['create' => true])
        @include('admin.cards._form')
        @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop