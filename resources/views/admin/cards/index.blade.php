@extends('admin.layouts.main')

@section('custom-buttons')
    <a href="{{ route('admin.cards.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' => ['name']])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>ФИО</th>
                            <th>Дата рождения</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($cards AS $card)
                            <tr>
                                <td>{{ $card->full_name }}</td>
                                <td>{{ $card->birthday->format('d.m.Y') }}</td>
                                <td>@widget('status', ['status' => $card->status, 'table' => 'cards', 'id' => $card->id])</td>
                                <td>@widget('listButtons', ['routeBase' => 'cards.', 'id' => $card->id, 'view' => false, 'delete' => true])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            {{--<div class="text-center">{{ $cards->appends(Input::except('cards'))->render() }}</div>--}}
        </div>
    </div>
@endsection
