@extends('admin.layouts.main')

@push('styles')
<link rel="stylesheet" href="<?php echo asset('admin/dev/dropzone.css'); ?>">
@endpush
@push('scripts')
<script src="{{ asset('admin/dev/dropzone.js') }}"></script>
@endpush

@section('content')
    {!! Form::open(['route' => ['admin.cards.update', 'card' => $card->id], 'method' => 'PUT', 'files' => true]) !!}
    @include('admin.widgets.form.buttons')
    @include('admin.cards._formNew', ['card' => $card])
    @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop
