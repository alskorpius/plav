<?php
/** @var \App\Models\Card $card */
$object = isset($card) ? $card : null;

?>
<div class="row">
    <div class="col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Личные данные</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div class="col-md-3">
                        @include('admin.widgets.form.input', ['name' => 'last_name', 'label' => 'Фамилия', 'object' => $object, 'options' => ['required']])
                    </div>
                    <div class="col-md-3">
                        @include('admin.widgets.form.input', ['name' => 'first_name', 'label' => 'Имя', 'object' => $object, 'options' => ['required']])
                    </div>
                    <div class="col-md-3">
                        @include('admin.widgets.form.input', ['name' => 'middle_name', 'label' => 'Отчество', 'object' => $object, 'options' => ['required']])
                    </div>
                    <div class="col-md-3">
                        @include('admin.widgets.form.date', ['name' => 'birthday', 'label' => 'Дата рождения', 'object' => $object, 'options' => ['required', 'class' => 'form-control datePicker']])
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Адрес прописки</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    {!! Form::label('is_registration_address', 'Живёт по месту прописки?') !!}
                    <div class="form-group">
                        <div class="radio" style="display:inline-block;">
                            <label>{!! Form::radio('is_registration_address', 1, (isset($object->is_registration_address) && $object->is_registration_address == 1) or !isset($object->is_registration_address), []) !!} Да</label>
                        </div>
                        <div class="radio" style="display:inline-block;">
                            <label>{!! Form::radio('is_registration_address', 0, isset($object->is_registration_address) && $object->is_registration_address == 0, []) !!} Нет</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        @include('admin.widgets.form.select', ['name' => 'registration_address[city_id]', 'value' => isset($selectedAddresses['registration']->city_id) ? $selectedAddresses['registration']->city_id : null, 'label' => 'Город', 'data' => $cities, 'object' => $object, 'options' => ['class' => 'form-control select2', 'required']])
                    </div>
                    <div class="col-md-6">
                        @include('admin.widgets.form.select', ['name' => 'registration_address[street_id]', 'value' => isset($selectedAddresses['registration']->street_id) ? $selectedAddresses['registration']->street_id : null, 'label' => 'Улица', 'data' => $streets, 'object' => $object, 'options' => ['class' => 'form-control select2', 'required']])
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        @include('admin.widgets.form.input', ['name' => 'registration_address[house_number]', 'value' => isset($selectedAddresses['registration']->house_number) ? $selectedAddresses['registration']->house_number : null, 'label' => '№ дома', 'object' => $object, 'options' => ['required']])
                    </div>
                    <div class="col-md-4">
                        @include('admin.widgets.form.input', ['name' => 'registration_address[housing_number]', 'value' => isset($selectedAddresses['registration']->housing_number) ? $selectedAddresses['registration']->housing_number : null, 'label' => 'Корпус', 'object' => $object, 'options' => ['']])
                    </div>
                    <div class="col-md-4">
                        @include('admin.widgets.form.input', ['name' => 'registration_address[apartment_number]', 'value' => isset($selectedAddresses['registration']->apartment_number) ? $selectedAddresses['registration']->apartment_number : null, 'label' => 'Квартира', 'object' => $object, 'options' => []])
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-primary hide-for-fact-address {{ isset($object->is_registration_address) && $object->is_registration_address ? 'active' : null }}">
            <div class="box-header with-border">
                <h3 class="box-title">Фактический адрес</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div class="col-md-6">
                        @include('admin.widgets.form.select', ['name' => 'fact_address[city_id]', 'value' => isset($selectedAddresses['fact']->city_id) ? $selectedAddresses['fact']->city_id : null, 'label' => 'Город', 'data' => $cities, 'object' => $object, 'options' => ['class' => 'form-control select2']])
                    </div>
                    <div class="col-md-6">
                        @include('admin.widgets.form.select', ['name' => 'fact_address[street_id]', 'value' => isset($selectedAddresses['fact']->street_id) ? $selectedAddresses['fact']->street_id : null, 'label' => 'Улица', 'data' => $streets, 'object' => $object, 'options' => ['class' => 'form-control select2']])
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        @include('admin.widgets.form.input', ['name' => 'fact_address[house_number]', 'value' => isset($selectedAddresses['fact']->house_number) ? $selectedAddresses['fact']->house_number : null, 'label' => '№ дома', 'object' => $object, 'options' => ['required']])
                    </div>
                    <div class="col-md-4">
                        @include('admin.widgets.form.input', ['name' => 'fact_address[housing_number]', 'value' => isset($selectedAddresses['fact']->housing_number) ? $selectedAddresses['fact']->housing_number : null, 'label' => 'Корпус', 'object' => $object, 'options' => ['']])
                    </div>
                    <div class="col-md-4">
                        @include('admin.widgets.form.input', ['name' => 'fact_address[apartment_number]', 'value' => isset($selectedAddresses['fact']->apartment_number) ? $selectedAddresses['fact']->apartment_number : null, 'label' => 'Квартира', 'object' => $object, 'options' => []])
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box box-danger">
            <div class="box-body">
                {{--@include('admin.widgets.form.status', ['status' => old('status', isset($object) ? $object->status : 0)])--}}

                <div class="form-group">
                    {!! Form::label('gender', 'Выберите пол') !!}
                    <div class="form-group">
                        <div class="radio" style="display:inline-block;">
                            <label>{!! Form::radio('gender', 1, 1, ['class' => 'minimal']) !!} Мужской</label>
                        </div>
                        <div class="radio" style="display:inline-block;">
                            <label>{!! Form::radio('gender', 0, 0, ['class' => 'minimal']) !!} Женский</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('married', 'В браке') !!}
                    <div class="form-group">
                        <div class="radio" style="display:inline-block;">
                            <label>{!! Form::radio('married', 1, 0, ['class' => 'minimal']) !!} Да</label>
                        </div>
                        <div class="radio" style="display:inline-block;">
                            <label>{!! Form::radio('married', 0, 1, ['class' => 'minimal']) !!} Нет</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('fisherman', 'Рыбак') !!}
                    <div class="form-group">
                        <div class="radio" style="display:inline-block;">
                            <label>{!! Form::radio('fisherman', 1, 0, ['class' => 'minimal']) !!} Да</label>
                        </div>
                        <div class="radio" style="display:inline-block;">
                            <label>{!! Form::radio('fisherman', 0, 1, ['class' => 'minimal']) !!} Нет</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('payment', 'Оплата') !!}
                    <div class="form-group">
                        <div class="radio" style="display:inline-block;">
                            <label>{!! Form::radio('payment', 0, 1, ['class' => 'minimal']) !!} 100%</label>
                        </div>
                        <div class="radio" style="display:inline-block;">
                            <label>{!! Form::radio('payment', 1, 0, ['class' => 'minimal']) !!} 50%</label>
                        </div>
                        <div class="radio" style="display:inline-block;">
                            <label>{!! Form::radio('payment', 2, 0, ['class' => 'minimal']) !!} Бесплатно</label>
                        </div>
                    </div>
                </div>
                @include('admin.widgets.form.select', ['name' => 'department_id', 'label' => 'Цех', 'data' => $departments, 'object' => $object, 'options' => ['']])
                @include('admin.widgets.form.select', ['name' => 'profession_id', 'label' => 'Профессия', 'data' => $professions, 'object' => $object, 'options' => ['']])
                @include('admin.widgets.form.select', ['name' => 'factor_id', 'label' => 'Вредный фактор', 'data' => $factors, 'object' => $object, 'options' => ['']])
            </div>
        </div>
    </div>
</div>
