@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.providers.update', 'provider' => $provider->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.providers.providers._form', ['provider' => $provider])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop