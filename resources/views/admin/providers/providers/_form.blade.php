<?php
/** @var App\Models\Providers\ShopCountry $country */
$object = isset($provider) ? $provider : null;
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.input', ['name' => 'slug', 'label' => 'Алиас', 'object' => $object, 'options' => ['required', 'data-slug-destination']])
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        @foreach($languages AS $language)
                            <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}"
                                                                                        data-toggle="tab">{{ $language['name'] }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($languages AS $language)
                            <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                            <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}">
                                @include('admin.widgets.form.input', ['name' => 'name', 'lang' => $language['slug'], 'label' => 'Название', 'object' => $langObject, 'options' => ['required', 'data-slug-origin']])
                                @include('admin.widgets.form.tiny', ['name' => 'description', 'lang' => $language['slug'], 'label' => 'Описание', 'object' => $langObject, 'options' => ['']])
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-body">
                @include('admin.widgets.form.select', ['name' => 'country_id', 'label' => 'Страна',  'data' => $countries, 'options' => ['required', 'class' => 'form-control countryProvide']])
                <div class="regionsSelected">
                    @include('admin.widgets.form.select', ['name' => 'region_id[]', 'label' => 'Регион',  'data' => $regions, 'value' => isset($selectedRegions) ? $selectedRegions : [], 'options' => ['required', 'class' => 'form-control regionProvide select2', 'multiple' => 'multiple']])
                </div>
            </div>
        </div>
    </div>
</div>
