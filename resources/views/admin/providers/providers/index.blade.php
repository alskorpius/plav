@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
    <a href="{{ route('admin.providers.create') }}" class="btn bg-olive btn-flat margin">Создать</a>
@endsection

@section('content')
    @widget('filter', ['fields' => ['name' => 'name']])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox"
                                       data-table='shop_providers'>
                            </th>
                            <th>Производитель</th>
                            <th>Страна</th>
                            <th>Регионы</th>
                            <th></th>
                        </tr>
                        @foreach($providers AS $provider)
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox"
                                           data-id='{{$provider->id}}'>
                                </td>
                                <td width="30%">{{ $provider->current->name }}</td>
                                <td width="20%">
                                    @if(isset($provider->shopCountry))
                                        <a href="{{route('admin.providers-countries.edit', ['id' => $provider->shopCountry->id])}}">{{ $provider->shopCountry->current->name }}</a>
                                    @else
                                        ---
                                    @endif
                                </td>
                                <td width="20%">
                                    @if(sizeof($provider->regions) > 0)
                                        @foreach($provider->regions as $region)
                                            @if($region->region)
                                                @if($loop->last)
                                                    <a href="{{route('admin.providers-regions.edit', ['id' => $region->region->id])}}">{{ $region->region->current->name }}</a>
                                                @else
                                                    <a href="{{route('admin.providers-regions.edit', ['id' => $region->region->id])}}">{{ $region->region->current->name }}</a>,
                                                @endif
                                            @endif
                                        @endforeach
                                    @else
                                        ---
                                    @endif
                                </td>
                                <td width="15%">@widget('listButtons', ['routeBase' => 'providers.', 'id' => $provider->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $providers->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
