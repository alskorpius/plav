@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.providers.store', 'files' => true]) !!}
        @include('admin.widgets.form.buttons', ['create' => true])
        @include('admin.providers.providers._form')
        @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop