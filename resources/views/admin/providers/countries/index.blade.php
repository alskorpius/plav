@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
    <a href="{{ route('admin.providers-countries.create') }}" class="btn bg-olive btn-flat margin">Создать</a>
@endsection

@section('content')
    @widget('filter', ['fields' => ['name' => 'name']])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox" data-table = 'shop_countries'>
                            </th>
                            <th></th>
                            <th>Страна</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($countries AS $country)
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox" data-id = '{{$country->id}}'>
                                </td>
                                <td width="5%"><img src="{{$country->getImage('icon')}}" alt="{{ $country->current->name }}"></td>
                                <td width="70%">{{ $country->current->name }}</td>
                                <td>@widget('status', ['status' => $country->status, 'table' => 'shop_countries', 'id' => $country->id])</td>
                                <td width="15%">@widget('listButtons', ['routeBase' => 'providers-countries.', 'id' => $country->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $countries->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
