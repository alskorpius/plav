<?php
/** @var App\Models\Providers\ShopCountry $country */
$object = isset($country) ? $country : null;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}">
                            @include('admin.widgets.form.input', ['name' => 'name', 'lang' => $language['slug'], 'label' => 'Название', 'object' => $langObject, 'options' => ['required', 'data-slug-origin']])
                        </div>
                    @endforeach
                </div>
                @if(isset($object) && $object->image && $object->imageExists('small'))
                    @include('admin.widgets.form.image', ['big' => $object->showImage('original', null, true), 'preview' => $object->showImage('small', null, true), 'id' => $country->id, 'model' => $country->getMorphClass()])
                @else
                    @include('admin.widgets.form.image')
                @endif
            </div>
        </div>
    </div>
</div>
