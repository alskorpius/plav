@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.providers-countries.update', 'country' => $country->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.providers.countries._form', ['country' => $country])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop