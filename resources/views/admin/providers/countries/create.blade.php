@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.providers-countries.store', 'files' => true]) !!}
        @include('admin.widgets.form.buttons', ['create' => true])
        @include('admin.providers.countries._form')
        @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop