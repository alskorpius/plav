@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
    <a href="{{ route('admin.providers-areas.create') }}" class="btn bg-olive btn-flat margin">Создать</a>
@endsection

@section('content')
    @widget('filter', ['fields' => ['name' => 'name']])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox" data-table = 'shop_areas'>
                            </th>
                            <th>Подрегион</th>
                            <th>Регион</th>
                            <th></th>
                        </tr>
                        @foreach($areas AS $area)
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox" data-id = '{{$area->id}}'>
                                </td>
                                <td width="50%">{{ $area->name }}</td>
                                <td width="20%"><a href="{{route('admin.providers-regions.edit', ['id' => $area->shopRegion->id])}}">{{ $area->shopRegion->name }}</a></td>
                                <td width="15%">@widget('listButtons', ['routeBase' => 'providers-areas.', 'id' => $area->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $areas->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection