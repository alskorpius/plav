@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.providers-areas.store', 'files' => true]) !!}
        @include('admin.widgets.form.buttons', ['create' => true])
        @include('admin.providers.areas._form')
        @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop