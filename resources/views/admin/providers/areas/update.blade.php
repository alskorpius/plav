@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.providers-areas.update', 'area' => $area->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.providers.areas._form', ['area' => $area])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop