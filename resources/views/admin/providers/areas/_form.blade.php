<?php
/** @var App\Models\Providers\ShopCountry $country */
$object = isset($area) ? $area : null;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.input', ['name' => 'name', 'label' => 'Название', 'object' => $object, 'options' => ['required']])
                @include('admin.widgets.form.select', ['name' => 'region_id', 'label' => 'Регион',  'data' => $regions, 'options' => ['required']])
            </div>
        </div>
    </div>
</div>