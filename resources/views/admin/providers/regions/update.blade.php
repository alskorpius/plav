@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.providers-regions.update', 'region' => $region->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.providers.regions._form', ['region' => $region])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop