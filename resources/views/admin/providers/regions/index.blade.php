@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
    <a href="{{ route('admin.providers-regions.create') }}" class="btn bg-olive btn-flat margin">Создать</a>
@endsection

@section('content')
    @widget('filter', ['fields' => ['name' => 'name']])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox" data-table = 'shop_regions'>
                            </th>
                            <th>Регион</th>
                            <th>Страна</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($regions AS $region)
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox" data-id = '{{$region->id}}'>
                                </td>
                                <td width="50%">{{ $region->current ? $region->current->name : "" }}</td>
                                <td width="20%">
                                    @if(isset($region->shopCountry))
                                        <a href="{{route('admin.providers-countries.edit', ['id' => $region->shopCountry->id])}}">
                                            {{ $region->shopCountry->current->name }}
                                        </a>
                                    @endif
                                </td>
                                <td>@widget('status', ['status' => $region->status, 'table' => 'shop_regions', 'id' => $region->id])</td>
                                <td width="15%">@widget('listButtons', ['routeBase' => 'providers-regions.', 'id' => $region->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $regions->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
