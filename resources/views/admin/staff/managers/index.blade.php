@extends('admin.layouts.main')

@section('custom-buttons')
    <a href="{{ route('admin.staff-managers.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' => 'staff'])
    <div class="row">
        <div class="col-xs-12">
            <div class="dd pageList" id="myNest" data-depth="1">
                <ol class="dd-list">
                    @include('admin.staff.managers.items', ['staff' => $staff])
                </ol>
            </div>
            <span id="parameters" data-table="staff"></span>
            <input type="hidden" id="myNestJson">
        </div>
    </div>
@endsection