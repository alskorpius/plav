<?php
/** @var \App\Models\News $object */
$object = isset($staff) ? $staff : null;
?>
<div class="row">
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="box-body">
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}">
                            @include('admin.widgets.form.input', ['name' => 'name', 'lang' => $language['slug'], 'label' => 'Имя', 'object' => $langObject, 'options' => ['required',]])
                            @include('admin.widgets.form.input', ['name' => 'seat', 'lang' => $language['slug'], 'label' => 'Должность', 'object' => $langObject, 'options' => ['required',]])
                        </div>
                    @endforeach
                </div>

                @include('admin.widgets.form.input', ['name' => 'email', 'label' => 'Email', 'object' => $object, 'options' => ['required',]])
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="box box-warning">
            <div class="box-body">
                @include('admin.widgets.form.status', ['status' => old('status', isset($object) ? $object->status : 0)])

                @if(isset($object) && $object->image && $object->imageExists('small'))
                    @include('admin.widgets.form.image', ['big' => $object->showImage('original', null, true), 'preview' => $object->showImage('small', null, true), 'id' => $staff->id, 'model' => $staff->getMorphClass()])
                @else
                    @include('admin.widgets.form.image')
                @endif
            </div>
        </div>
    </div>
</div>
