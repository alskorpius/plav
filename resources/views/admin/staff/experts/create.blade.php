@extends('admin.layouts.main')
@section('content')
    {!! Form::open(['route' => 'admin.staff-experts.store', 'files' => true]) !!}
    @include('admin.widgets.form.buttons', ['create' => true])
    @include('admin.staff.experts._form')
    @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop