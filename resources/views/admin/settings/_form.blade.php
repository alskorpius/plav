<?php
/**
 * @var \App\Models\SettingsGroup[] $groups
 */
?>
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <?php $active = false; ?>
                @foreach($groups AS $group)
                    @if(sizeof($group->settings) > 0)
                        <li class="{{ $active === false ? 'active' : null }}"><a href="#{{ $group->alias }}" data-toggle="tab">{{ $group->name }}</a></li>
                        <?php $active = true; ?>
                    @endif
                @endforeach
            </ul>
            <div class="tab-content">
                <?php $active = false; ?>
                @foreach($groups AS $group)
                    @if(sizeof($group->settings) > 0)
                        <div class="{{ $active === false ? 'active' : null }} tab-pane" id="{{ $group->alias }}">
                            @foreach($group->settings AS $setting)
                                @if($setting->type === 'text')
                                    @include('admin.widgets.form.textarea', [
                                        'name' => $setting->key,
                                        'value' => old($setting->key, $setting->value),
                                        'label' => $setting->name,
                                        'options' => ['required' => in_array('required', $setting->validation)],
                                    ])
                                @elseif($setting->type === 'date')
                                    @include('admin.widgets.form.date', [
                                        'name' => $setting->key,
                                        'value' => old($setting->key, $setting->value),
                                        'label' => $setting->name,
                                        'options' => ['required' => in_array('required', $setting->validation)],
                                    ])
                                @elseif($setting->type === 'password')
                                    @include('admin.widgets.form.password', [
                                        'name' => $setting->key,
                                        'label' => $setting->name,
                                        'options' => ['required' => in_array('required', $setting->validation)],
                                    ])
                                @elseif($setting->type === 'select')
                                    @include('admin.widgets.form.select', [
                                        'name' => $setting->key,
                                        'value' => $setting->value,
                                        'data' => $setting->data,
                                        'label' => $setting->name,
                                        'options' => ['required' => in_array('required', $setting->validation)],
                                    ])
                                @elseif($setting->type === 'radio')
                                    @include('admin.widgets.form.radio', [
                                        'name' => $setting->key,
                                        'value' => $setting->value,
                                        'data' => $setting->data,
                                        'label' => $setting->name,
                                        'options' => ['required' => in_array('required', $setting->validation)],
                                    ])
                                @else
                                    @include('admin.widgets.form.input', [
                                        'name' => $setting->key,
                                        'value' => old($setting->key, $setting->value),
                                        'label' => $setting->name,
                                        'options' => ['required' => in_array('required', $setting->validation)],
                                    ])
                                @endif
                            @endforeach
                        </div>
                        <?php $active = true; ?>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
