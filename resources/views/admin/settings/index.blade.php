@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.settings.store', 'files' => true]) !!}
        @include('admin.widgets.form.buttons', ['except' => ['close', 'add', 'back']])
        @include('admin.settings._form', ['groups' => $groups])
        @include('admin.widgets.form.buttons', ['except' => ['close', 'add', 'back']])
    {!! Form::close() !!}
@stop