@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.seo-links.store']) !!}
        @include('admin.widgets.form.buttons', ['create' => true])
        @include('admin.seo.links._form')
        @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop