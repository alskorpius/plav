<?php
$object = isset($seoLink) ? $seoLink : null;
?>
<div class="row">
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.tiny', ['name' => 'content', 'label' => 'Контент', 'object' => $object])
                @include('admin.widgets.form.input', ['name' => 'h1', 'object' => $object])
                @include('admin.widgets.form.input', ['name' => 'title', 'object' => $object])
                @include('admin.widgets.form.textarea', ['name' => 'description', 'object' => $object])
                @include('admin.widgets.form.textarea', ['name' => 'keywords', 'object' => $object])
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="box box-warning">
            <div class="box-body">
                @include('admin.widgets.form.status', ['status' => old('status', isset($object) ? $object->status : 0)])
                @include('admin.widgets.form.input', ['name' => 'url', 'label' => 'URL', 'object' => $object, 'options' => ['required']])
            </div>
        </div>
    </div>
</div>