@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.seo-links.update', 'seoLink' => $seoLink->id], 'method' => 'PUT']) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.seo.links._form', ['seoLink' => $seoLink])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop