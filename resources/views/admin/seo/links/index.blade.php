<?php
/**
 * @var \App\Models\SeoLink[] $seoLinks
 */
?>
@extends('admin.layouts.main')

@section('custom-buttons')
    <a href="{{ route('admin.seo-links.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' => ['url']])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>URL</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($seoLinks AS $seoLink)
                            <tr>
                                <td>{{ $seoLink->url }}</td>
                                <td>@widget('status', ['status' => $seoLink->status, 'table' => 'seo_links', 'id' => $seoLink->id])</td>
                                <td>@widget('listButtons', ['routeBase' => 'seo-links.', 'id' => $seoLink->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $seoLinks->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection