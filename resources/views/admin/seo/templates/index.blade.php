<?php
/**
 * @var \App\Models\SeoTemplate[] $seoTemplates
 */
?>
@extends('admin.layouts.main')

@section('content')
    @widget('filter', ['fields' => [
        ['name' => 'name', 'placeholder' => 'Имя'],
    ]])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Название</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($seoTemplates AS $seoTemplate)
                            <tr>
                                <td>{{ $seoTemplate->current->name }}</td>
                                <td>@widget('status', ['status' => $seoTemplate->status, 'table' => 'seo_templates', 'id' => $seoTemplate->id])</td>
                                <td>@widget('listButtons', ['routeBase' => 'seo-templates.', 'id' => $seoTemplate->id, 'view' => false, 'delete' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $seoTemplates->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
