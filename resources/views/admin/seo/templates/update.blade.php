@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.seo-templates.update', 'seoTemplate' => $seoTemplate->id], 'method' => 'PUT']) !!}
        @include('admin.widgets.form.buttons', ['except' => ['add']])
        @include('admin.seo.templates._form', ['seoTemplate' => $seoTemplate])
        @include('admin.widgets.form.buttons', ['except' => ['add']])
    {!! Form::close() !!}
@stop