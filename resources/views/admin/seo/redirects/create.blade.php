@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.seo-redirects.store']) !!}
        @include('admin.widgets.form.buttons', ['create' => true])
        @include('admin.seo.redirects._form')
        @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop