@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.seo-redirects.update', 'seoRedirect' => $seoRedirect->id], 'method' => 'PUT']) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.seo.redirects._form', ['seoRedirect' => $seoRedirect])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop