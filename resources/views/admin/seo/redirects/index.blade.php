<?php
/**
 * @var \App\Models\SeoRedirect[] $seoRedirects
 */
?>
@extends('admin.layouts.main')

@section('custom-buttons')
    <a href="{{ route('admin.seo-redirects.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Ссылка с</th>
                            <th>Ссылка на</th>
                            <th>Тип</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($seoRedirects AS $seoRedirect)
                            <tr>
                                <td>{{ $seoRedirect->from }}</td>
                                <td>{{ $seoRedirect->to }}</td>
                                <td>{{ config('custom.redirects.' . $seoRedirect->type) }}</td>
                                <td>@widget('status', ['status' => $seoRedirect->status, 'table' => 'seo_redirects', 'id' => $seoRedirect->id])</td>
                                <td>@widget('listButtons', ['routeBase' => 'seo-redirects.', 'id' => $seoRedirect->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $seoRedirects->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection