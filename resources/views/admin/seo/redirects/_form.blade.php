<?php
/**
 * @var \App\Models\SeoRedirect $seoRedirect
 */
$object = isset($seoRedirect) ? $seoRedirect : null;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.status', ['status' => old('status', isset($object) ? $object->status : 0)])
                @include('admin.widgets.form.input', ['name' => 'from', 'label' => 'Ссылка с', 'object' => $object, 'options' => ['required']])
                @include('admin.widgets.form.input', ['name' => 'to', 'label' => 'Ссылка на', 'object' => $object, 'options' => ['required']])
                @include('admin.widgets.form.select', [
                    'name' => 'type',
                    'label' => 'Тип',
                    'object' => $object,
                    'options' => ['required'],
                    'data' => config('custom.redirects', []),
                ])
            </div>
        </div>
    </div>
</div>