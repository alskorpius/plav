@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.seo-scripts.update', 'seoScript' => $seoScript->id], 'method' => 'PUT']) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.seo.scripts._form', ['seoScript' => $seoScript])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop