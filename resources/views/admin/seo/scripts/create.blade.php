@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.seo-scripts.store']) !!}
        @include('admin.widgets.form.buttons', ['create' => true])
        @include('admin.seo.scripts._form')
        @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop