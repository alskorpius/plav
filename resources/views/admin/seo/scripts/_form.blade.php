<?php
/**
 * @var \App\Models\SeoScript $seoScript
 */
$object = isset($seoScript) ? $seoScript : null;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.status', ['status' => old('status', isset($object) ? $object->status : 0)])
                @include('admin.widgets.form.select', ['name' => 'place', 'label' => 'Место', 'object' => $object, 'options' => ['required'], 'data' => config('custom.seo-scripts-places')])
                @include('admin.widgets.form.input', ['name' => 'name', 'label' => 'Название', 'object' => $object, 'options' => ['required']])
                @include('admin.widgets.form.textarea', ['name' => 'script', 'label' => 'Код', 'object' => $object, 'options' => ['required']])
            </div>
        </div>
    </div>
</div>