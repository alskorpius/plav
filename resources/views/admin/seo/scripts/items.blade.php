<?php
/**
 * @var \App\Models\SeoScript[] $seoScripts
 */
?>

@if(isset($seoScripts) and sizeof($seoScripts))
    @foreach ($seoScripts as $element)
        <li class="dd-item dd3-item" data-id="{{ $element->id }}">
            <div title="Переместить строку" class="dd-handle dd3-handle">Drag</div>
            <div class="dd3-content">
                <table style="width: 100%;">
                    <tr>
                        <td class="column-drag" width="1%"></td>
                        <td valign="top" class="pagename-column" width="60%">
                            <div class="clearFix">
                                <div class="overflow-20">
                                    <a class="pageLinkEdit" href="{{ route('admin.seo-scripts.edit', ['seoScript' => $element->id]) }}">{{ $element->name }}</a>
                                </div>
                            </div>
                        </td>
                        <td width="45" valign="top" class="icon-column status-column" width="19%">
                            @widget('status', ['status' => $element->status, 'table' => 'seo_scripts', 'id' => $element->id])
                        </td>
                        <td class="nav-column icon-column" valign="top" width="20%">
                            @widget('listButtons', ['routeBase' => 'seo-scripts.', 'id' => $element->id, 'view' => false])
                        </td>
                    </tr>
                </table>
            </div>
        </li>
    @endforeach
@endif