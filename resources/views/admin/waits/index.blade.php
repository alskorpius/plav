@extends('admin.layouts.main')

@section('content')
    {{--@widget('filter', ['fields' => 'blacklist'])--}}
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Пользователь</th>
                            <th>Телефон</th>
                            <th>Дата</th>
                            <th></th>
                        </tr>
                        @foreach($waits AS $wait)
                            <tr>
                                <td width="20%">{{isset($wait->user->full_name) ? $wait->user->full_name : '---'}}</td>
                                <td width="20%">{{isset($wait->user->phone) ? $wait->user->phone : '---'}}</td>
                                <td width="20%">{{isset($wait->created_at) ? $wait->created_at : '---' }}</td>
                                <td width="10%">@widget('listButtons', ['routeBase' => 'waits.', 'id' => $wait->user_id, 'view' => true, 'edit' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            {{--<div class="text-center">{{ $waits->appends(Input::except('page'))->render() }}</div>--}}
        </div>
    </div>
@endsection
