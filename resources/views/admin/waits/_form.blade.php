@extends('admin.layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Информация о запросе</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div style="margin-left: 50px;">
                                <h4>Пользователь:</h4>
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th>Имя</th>
                                        <th>Телефон</th>
                                        <th>Email</th>
                                        <th>Дата</th>
                                        <th>Статус</th>
                                    </tr>
                                    <tr>
                                        <td>{{isset($user->full_name) ? $user->full_name : '---'}}</td>
                                        <td>{{isset($user->phone) ? $user->phone : '---'}}</td>
                                        <td>{{isset($user->email) ? $user->email : '---'}}</td>
                                        <td>{{$date}}</td>
                                        <td class="order-waits-select">@include('admin.widgets.form.select', ['name' => 'status', 'label' => false, 'data' => [1 => 'Выполнен', 2 => 'Отменен', 0 => 'Новый'], 'value' => $status, 'options' => ['id' => 'order-wait-status', 'data-user-id' => $user->id]])</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div style="margin-right: 50px;">
                                <h4>Товары:</h4>
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th></th>
                                        <th>Название</th>
                                        <th>Цена</th>
                                    </tr>
                                    @foreach($products as $product)
                                        <tr>
                                            <td><img src="{{$product->getImage('icon')}}" alt="{{$product->current->name}}"></td>
                                            <td>
                                                <a href="{{route('admin.products.edit', ['id' => $product->id])}}">{{$product->current->name}}</a>
                                            </td>
                                            <td>{{$product->checked_price->price}} грн.</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
