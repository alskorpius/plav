<?php
/**
 * @var \App\Models\Menu[] $menu_items
 * @var integer $cur
 */
?>
@if(isset($menu_items[$cur]) and sizeof($menu_items[$cur]))
    @if($cur > 0)
        <ol>
            @endif
            @foreach ($menu_items[$cur] as $menu)
                <li class="dd-item dd3-item" data-id="{{ $menu->id }}">
                    <div title="Переместить строку" class="dd-handle dd3-handle">Drag</div>
                    <div class="dd3-content">
                        <table style="width: 100%;">
                            <tr>
                                <td class="column-drag" width="1%"></td>
                                <td valign="top" class="pagename-column" width="35%">
                                    <div class="clearFix">
                                        <div class="pull-left">
                                            <div class="overflow-20">
                                                {{ $menu->current->name ? $menu->current->name : $menu->id }}
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td valign="top" class="pagename-column" width="35%">
                                    <div class="clearFix">
                                        <div class="pull-left">
                                            <div class="overflow-20">{!! $menu->last_action_short !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td width="30" valign="top" class="icon-column status-column">
                                    @widget('status', ['status' => $menu->status, 'table' => 'menus', 'id' => $menu->id])
                                </td>
                                <td class="nav-column icon-column" valign="top" width="15%">
                                    @widget('listButtons', ['routeBase' => 'menu.', 'id' => $menu->id, 'view' => false])
                                </td>
                            </tr>
                        </table>
                    </div>
                    @include('admin.menu.items', ['menu_items' => $menu_items, 'cur' => $menu->id])
                </li>
            @endforeach
            @if($cur > 0)
        </ol>
    @endif
@endif
