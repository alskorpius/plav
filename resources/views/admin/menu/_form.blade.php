<?php
$object = isset($menu) ? $menu : null;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.status', ['status' => old('status', isset($menu) ? $menu->status : 0)])
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}">
                            @include('admin.widgets.form.input', ['name' => 'name', 'lang'=>$language['slug'], 'label' => 'Название', 'object' => $langObject, 'options' => ['required']])
                        </div>
                    @endforeach
                </div>

                @include('admin.widgets.form.input', ['name' => 'url', 'label' => 'URL', 'object' => $object, 'options' => ['required']])
            </div>
        </div>
    </div>
</div>
