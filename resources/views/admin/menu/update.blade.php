@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.menu.update', 'menu' => $menu->id], 'method' => 'PUT']) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.menu._form', ['menu' => $menu])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop