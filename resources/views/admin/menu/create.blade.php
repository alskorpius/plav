@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.menu.store']) !!}
        @include('admin.widgets.form.buttons', ['create' => true])
        @include('admin.menu._form')
        @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop