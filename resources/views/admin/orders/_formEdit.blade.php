<?php
/** @var \App\Models\Order\Order $object */
/** @var \App\Models\Order\Order $order */
/** @var \App\Models\User $users */
$object = isset($order) ? $order : null;
?>
<div class="row">
    <div class="col-md-5">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Покупатель</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <b>Дата создания: {{$order->created_at->format('d-m-Y H:i:s')}}</b>
                    <br>
                    <br>
                    {!! Form::label('status', 'Статус') !!}
                    <br>
                    <div class="radio-group">
                        <div class="radio">
                            <label>{!! Form::radio('status', 0, !isset($object->status) || (isset($object->status) && $object->status == 0), ['class' => 'minimal']) !!}
                                Новый</label>
                        </div>
                        <div class="radio">
                            <label>{!! Form::radio('status', 1, isset($object->status) && $object->status == 1, ['class' => 'minimal']) !!}
                                В обработке</label>
                        </div>
                    </div>
                    <div class="radio-group">
                        <div class="radio">
                            <label>{!! Form::radio('status', 2, isset($object->status) && $object->status == 2, ['class' => 'minimal']) !!}
                                Оплачен</label>
                        </div>
                        <div class="radio">
                            <label>{!! Form::radio('status', 3, isset($object->status) && $object->status == 3, ['class' => 'minimal']) !!}
                                Отправлен</label>
                        </div>
                    </div>
                    <div class="radio-group">
                        <div class="radio">
                            <label>{!! Form::radio('status', 4, isset($object->status) && $object->status == 4, ['class' => 'minimal']) !!}
                                Отправлен и оплачен</label>
                        </div>
                        <div class="radio">
                            <label>{!! Form::radio('status', 5, isset($object->status) && $object->status == 5, ['class' => 'minimal']) !!}
                                Завершен</label>
                        </div>
                    </div>
                    <div class="radio-group">
                        <div class="radio">
                            <label>{!! Form::radio('status', 6, isset($object->status) && $object->status == 6, ['class' => 'minimal']) !!}
                                Не окончен</label>
                        </div>
                    </div>
                </div>
                @include('admin.widgets.form.select', ['name' => 'user_id', 'label' => 'Пользователь',  'data' => $users, 'options' => ['class' => 'form-control select2', 'id' => 'user-select']])
                @include('admin.widgets.form.input', ['name' => 'user_name', 'label' => 'Имя покупателя', 'object' => $object, 'options' => ['required', 'class' => 'form-control', 'id' => 'user-name']])
                @include('admin.widgets.form.input', ['name' => 'phone', 'label' => 'Телефон покупателя', 'object' => $object, 'options' => ['required', 'class' => 'form-control', 'id' => 'user-phone']])
                @include('admin.widgets.form.input', ['name' => 'email', 'label' => 'Email покупателя', 'object' => $object, 'options' => ['required', 'class' => 'form-control', 'id' => 'user-email']])
                @include('admin.widgets.form.select', ['name' => 'city', 'label' => 'Город', 'object' => $object,  'data' => $cities, 'options' => ['required', 'class' => 'form-control select2', 'id' => 'order-user-city']])
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Оплата и доставка</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    {!! Form::label('delivery_type', 'Способ доставки') !!}
                    <div class="radio" style="display:inline-block;">
                        <label>{!! Form::radio('delivery_type', 0, isset($object->delivery_type) && !$object->delivery_type, ['id' => 'delivery-type-radio-0']) !!}
                            В отделение новой почты</label>
                    </div>
                    <div class="radio" style="display:inline-block;">
                        <label>{!! Form::radio('delivery_type', 1, !isset($object->delivery_type) || $object->delivery_type, ['id' => 'delivery-type-radio-1']) !!}
                            Курьерская доставка</label>
                    </div>
                </div>
                <span id="select-warehouses-ajax">
					@include('admin.widgets.form.select', ['name' => 'delivery_warehouse', 'label' => 'Отделение новой почты', 'object' => $object,  'data' => $warehouses, 'options' => ['class' => 'form-control select2', 'id' => 'user-warehouse', $object->delivery_type == 0 ? '' : 'disabled']])
				</span>
                @include('admin.widgets.form.input', ['name' => 'delivery_address', 'label' => 'Адресс курьерской доставки', 'object' => $object, 'options' => ['class' => 'form-control', 'id' => 'user-address', $object->delivery_type == 1 ? '' : 'disabled']])
                <div class="form-group">
                    {!! Form::label('payment_type', 'Способ оплаты') !!}
                    <div class="radio" style="display:inline-block;">
                        <label>{!! Form::radio('payment_type', 0, isset($object->payment_type) && !$object->payment_type, ['class' => '', 'id' => 'payment-type-radio-0']) !!}
                            Наложеный платеж</label>
                    </div>
                    <div class="radio" style="display:inline-block;">
                        <label>{!! Form::radio('payment_type', 1, !isset($object->payment_type) || $object->payment_type, ['class' => '', 'id' => 'payment-type-radio-1']) !!}
                            Безналичный платеж
                            @if(isset($object->payment_type) && $object->payment_type == 1 && $object->payment_system_id !== null)
                                {{$object->payment_system_id == 0 ? '(Liqpay)' : '(Portmone)'}}
                            @endif
                        </label>
                    </div>
                </div>
                @include('admin.widgets.form.input', ['name' => 'comment', 'label' => 'Комментарий к заказу', 'object' => $object, 'options' => ['class' => 'form-control']])
                <div class="box-header with-border">
                    <h3 class="box-title">Стоимость</h3>
                </div>
                <div style="margin: 0 0;">
                    <label>Общая стоимость заказа</label>
                    <span id="total-products-price-order" style="color: red; font-weight: bold">
					- {{$order->total_price . ' грн.' ?: '---'}}
				</span>
                </div>
                <div>
                    <label>Стоимость доставки</label>
                    <span id="total-delivery-price-order" style="color: red; font-weight: bold">
					- {{$order->delivery_cost . ' грн.' ?: '---'}}
				</span>
                </div>
                <div>
                    <label>Ориентировочный вес посылки</label>
                    <span id="total-delivery-price-order" style="color: red; font-weight: bold">
					- {{isset($weightItems) ? $weightItems : 0}} кг.
				</span>
                </div>
                <a target="_blank" href="{{route('admin.print-order.index', ['id' => $order->id])}}"
                   class="btn btn-block btn-primary">Печать заказа</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Товары</h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered" id="orders-item-table">
                    <tr>
                        <th></th>
                        <th>Товар</th>
                        <th>Цена - цена со скидкой - цена со скидкой *количество</th>
                        <th>Скидка</th>
                        <th>Количество</th>
                        <th></th>
                    </tr>
                    @forelse($order->items AS $item)
                        <tr>
                            <td>
                                <img src="{{$item->product->getImage('icon_small')}}" alt="{{$item->product->current->name}}">
                            </td>
                            <td>
                                <a target="_blank"
                                   href="{{route('admin.products.edit', ['id' => $item->product->id])}}">{{$item->product->current->name}}</a>
                            </td>
                            <td>
                                {{$item->price->price}} грн. - {{$item->total_price}} грн
                                - {{$item->total_price * $item->count}} грн.
                            </td>
                            <td>
                                {{isset($item->price->sale) ? $item->price->sale : 0}} %
                            </td>
                            <td>
                                <input style="width: 50px; display: inline-block;" type="text" value="{{$item->count}}"
                                       name="order-item-count" data-id="{{$item->id}}"
                                       class="form-control order-item-count"> шт.
                            </td>
                            <td>
                                <button type="button" class="btn btn-xs btn-danger orders-delete-items"
                                        data-id="{{$item->id}}"><i
                                        class="fa fa-trash-o"></i>
                                </button>
                            </td>
                        </tr>
                    @empty
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    @endforelse
                </table>
                <br>
                <div class="box-header with-border">
                    <h3 class="box-title">Добавить товар</h3>
                </div>
                @include('admin.widgets.form.select', ['name' => 'order-product', 'label' => 'Товар', 'data' => $products, 'options' => ['class' => 'form-control select2', 'id' => 'order-products', 'data-order-id' => $order->id]])
                <div id="order-product-prices" style="display:inline-block;">

                </div>
                <div id="order-product-submit" style="display:block;">

                </div>
            </div>
        </div>
    </div>
</div>
