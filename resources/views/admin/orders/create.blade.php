@extends('admin.layouts.main')

@section('content')
	{!! Form::open(['route' => 'admin.orders.store', 'files' => true]) !!}
	@include('admin.widgets.form.buttons', ['create' => true])
	@include('admin.orders._form')
	@include('admin.widgets.form.buttons', ['create' => true])
	{!! Form::close() !!}
@stop
