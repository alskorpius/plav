@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.orders.update', 'order' => $order->id], 'method' => 'PUT', 'files' => true]) !!}
    @include('admin.widgets.form.buttons')
    @include('admin.orders._formEdit', ['order' => $order])
    @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop
