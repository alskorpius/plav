<?php
/** @var \App\Models\Order\Order $object */
/** @var \App\Models\Order\Order $order */
/** @var \App\Models\User $users */
$object = isset($order) ? $order : null;
?>
<div class="row">
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-body">
				@include('admin.widgets.form.select', ['name' => 'user_id', 'label' => 'Пользователь',  'data' => $users, 'options' => ['class' => 'form-control select2', 'id' => 'user-select']])
				@include('admin.widgets.form.input', ['name' => 'user_name', 'label' => 'Имя покупателя', 'object' => $object, 'options' => ['required', 'class' => 'form-control', 'id' => 'user-name']])
				@include('admin.widgets.form.input', ['name' => 'phone', 'label' => 'Телефон покупателя', 'object' => $object, 'options' => ['required', 'class' => 'form-control', 'id' => 'user-phone']])
				@include('admin.widgets.form.input', ['name' => 'email', 'label' => 'Email покупателя', 'object' => $object, 'options' => ['required', 'class' => 'form-control', 'id' => 'user-email']])
				@include('admin.widgets.form.select', ['name' => 'city', 'label' => 'Город', 'object' => $object,  'data' => $cities, 'options' => ['required', 'class' => 'form-control select2', 'id' => 'order-user-city']])
				<div class="form-group">
					{!! Form::label('delivery_type', 'Способ доставки') !!}
					<div class="radio" style="display:inline-block;">
						<label>{!! Form::radio('delivery_type', 0, isset($object->delivery_type) && !$object->delivery_type, ['id' => 'delivery-type-radio-0']) !!}
							В отделение новой почты</label>
					</div>
					<div class="radio" style="display:inline-block;">
						<label>{!! Form::radio('delivery_type', 1, !isset($object->delivery_type) || $object->delivery_type, ['id' => 'delivery-type-radio-1']) !!}
							Курьерская доставка</label>
					</div>
				</div>
				<span id="select-warehouses-ajax">
					@include('admin.widgets.form.select', ['name' => 'delivery_warehouse', 'label' => 'Отделение новой почты', 'object' => $object,  'data' => [], 'options' => ['class' => 'form-control select2', 'id' => 'user-warehouse', !isset($object->payment_type) || $object->payment_type == 1 ? 'disabled' : '']])
				</span>
				@include('admin.widgets.form.input', ['name' => 'delivery_address', 'label' => 'Адресс курьерской доставки', 'object' => $object, 'options' => ['class' => 'form-control', 'id' => 'user-address']])
				<div class="form-group">
					{!! Form::label('payment_type', 'Способ оплаты') !!}
					<div class="radio" style="display:inline-block;">
						<label>{!! Form::radio('payment_type', 0, isset($object->payment_type) && !$object->payment_type, ['class' => '']) !!}
							Наложеный платеж</label>
					</div>
					<div class="radio" style="display:inline-block;">
						<label>{!! Form::radio('payment_type', 1, !isset($object->payment_type) || $object->payment_type, ['class' => '']) !!}
							Безналичный платеж</label>
					</div>
				</div>
				@include('admin.widgets.form.input', ['name' => 'comment', 'label' => 'Комментарий к заказу', 'object' => $object, 'options' => ['class' => 'form-control']])
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="box box-warning">
			<div class="box-body">
				<div class="form-group">
					{!! Form::label('status', 'Статус') !!}
					<br>
					<div class="radio-group">
						<div class="radio">
							<label>{!! Form::radio('status', 0, !isset($object->status) || (isset($object->status) && $object->status == 0), ['class' => 'minimal']) !!}
								Новый</label>
						</div>
						<div class="radio">
							<label>{!! Form::radio('status', 1, isset($object->status) && $object->status == 1, ['class' => 'minimal']) !!}
								В обработке</label>
						</div>
					</div>
					<div class="radio-group">
						<div class="radio">
							<label>{!! Form::radio('status', 2, isset($object->status) && $object->status == 2, ['class' => 'minimal']) !!}
								Оплачен</label>
						</div>
						<div class="radio">
							<label>{!! Form::radio('status', 3, isset($object->status) && $object->status == 3, ['class' => 'minimal']) !!}
								Отправлен</label>
						</div>
					</div>
					<div class="radio-group">
						<div class="radio">
							<label>{!! Form::radio('status', 4, isset($object->status) && $object->status == 4, ['class' => 'minimal']) !!}
								Отправлен и оплачен</label>
						</div>
						<div class="radio">
							<label>{!! Form::radio('status', 5, isset($object->status) && $object->status == 5, ['class' => 'minimal']) !!}
								Завершен</label>
						</div>
					</div>
                    <div class="radio-group">
                        <div class="radio">
                            <label>{!! Form::radio('status', 6, isset($object->status) && $object->status == 6, ['class' => 'minimal']) !!}
                                Не окончен</label>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
