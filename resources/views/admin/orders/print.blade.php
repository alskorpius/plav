<?php
/**
 * @var \App\Models\Order\Order $order
 */
?>
<body>
<div style="margin: 50px;">
    <h2>Заказ №{{$order->id}}</h2>
    <!-- <img src="" alt="" width="200" class="logo_2" />-->

    <strong>Информация о заказе</strong>
    <table class="table2" width="100%" border="1" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td width="50%">Номер заказа:</td>
            <td>{{$order->id}}</td>
        </tr>
        <tr>
            <td>Дата заказа:</td>
            <td>{{$order->created_at}}</td>
        </tr>
        <tr>
            <td>Статус заказа:</td>
            @if($order->status == 0)
                <td>Новый</td>
            @elseif($order->status == 1)
                <td>В обработке</td>
            @elseif($order->status == 2)
                <td>Оплачен</td>
            @elseif($order->status == 3)
                <td>Отправлен</td>
            @elseif($order->status == 4)
                <td>Оплачен и отправлен</td>
            @elseif($order->status == 5)
                <td>Завершен</td>
            @endif
        </tr>
        @if(isset($order->comment))
            <tr>
                <td>Комментарий к заказу:</td>
                <td>{{$order->comment}}</td>
            </tr>
            @endif
        </tbody>
    </table>
    <br>

    <strong>Информация о плательщике</strong>
    <table class="table2" width="100%" border="1" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td width="50%">Адресат</td>
            <td>{{$order->user_name}}</td>
        </tr>
        <tr>
            <td>E-Mail</td>
            <td>{{$order->email}}</td>
        </tr>
        <tr>
            <td>Номер телефона</td>
            <td>{{$order->phone}}</td>
        </tr>
        </tbody>
    </table>
    <br>

    <strong>Информация о доставке</strong>
    <table class="table2" width="100%" border="1" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td width="50%">Способ доставки</td>
            @if($order->delivery_type == 0)
                <td>В отделение новой почты</td>
            @else
                <td>Курьерская доставка</td>
            @endif
        </tr>
        <tr>
            <td>Адрес</td>
            @if($order->delivery_type == 0)
                <td>{{$order->warehouse_name}}</td>
            @else
                <td>{{$order->delivery_address}}</td>
            @endif
        </tr>
        </tbody>
    </table>
    <br>

    <strong>Содержание заказа</strong>
    <table class="table2" width="100%" border="1" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <th>Товар</th>
            <th>Цена</th>
            <th>Количество</th>
            <th>Итог</th>
        </tr>
        @foreach($order->items AS $item)
            <tr>
                <td style="text-align: center">
                    {{$item->product->current->name . ' Год - ' . $item->price->year . 'год | Объем - ' . $item->price->volume . ' мл'}}
                </td>
                <td style="text-align: center">{{$item->total_price}} грн.</td>
                <td width="8%" style="text-align: center">{{$item->count}}</td>
                <td style="text-align: center">{{$item->total_price * $item->count}} грн.</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <br>

    <div align="right">
        <strong>Стоимость доставки:</strong> <span style="text-decoration: underline;">{{$order->delivery_cost}}</span>
        грн
        <br>
        <strong>Итого:</strong> <span style="text-decoration: underline;">{{$order->total_price}}</span> грн
    </div>
</div>
</body>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        window.print();
    });
</script>
