<?php
/**
 * @var \App\Models\Order\Order $orders
 * @var \App\Models\Order\Order $order
 */
?>
@extends('admin.layouts.main')

@section('custom-buttons')
	<button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
	<button class="btn bg-danger btn-flat margin" id="statusChecked">Статус "новый"</button>
	<button class="btn bg-yellow btn-flat margin" id="statusCheckedEnable">В обработке</button>
	<button class="btn bg-blue btn-flat margin" id="statusChecked-2">Оплачен</button>
	<button class="btn bg-purple btn-flat margin" id="statusChecked-3">Отправлен</button>
	<button class="btn bg-brown btn-flat margin" id="statusChecked-4">Оплачен и отправлен</button>
	<button class="btn bg-green btn-flat margin" id="statusChecked-5">Статуc завершен</button>
	<a style="margin-left: 7%" href="{{ route('admin.orders.create') }}" class="btn bg-olive btn-flat margin">Добавить
		заказ</a>
@endsection

@section('content')
	@widget('filter', ['fields' => 'orders'])

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tr>
							<th>
								<input type="checkbox" class="icheckbox_minimal-aero mainCheckbox" data-table='orders'>
							</th>
							<th>#</th>
							<th>Имя покупателя</th>
							<th>Телефон</th>
							<th>Email</th>
                                                        <th>Дата</th>
							<th>Город</th>
							<th>Статус</th>
							<th>Доставка</th>
							<th>Сумма заказа</th>
							<th></th>
						</tr>
						@foreach($orders AS $order)
							<tr>
								<td>
									<input type="checkbox" class="icheckbox_minimal-aero subCheckbox"
										   data-id='{{$order->id}}'>
								</td>
								<td><a href="{{route('admin.orders.edit', ['id' => $order->id])}}"
									   title="{{$order->created_at !== null ? $order->created_at->format('d-m-Y H:i:s') : ''}}">{{$order->id}}</a>
								</td>
								<td>{{$order->user_name}}</td>
								<td>{{$order->phone}}</td>
								<td>{{$order->email}}</td>
                                                                <td>{{$order->created_at->format('d-m-Y H:i:s')}}</td>
								<td>{{$order->city_name}}</td>
								@if($order->status == 0)
									<td width="10%"><span class="label label-danger">Новый</span></td>
								@elseif($order->status == 1)
									<td width="10%"><span class="label label-warning">В обработке</span></td>
								@elseif($order->status == 2)
									<td width="10%"><span class="label label-blue">Оплачен</span></td>
								@elseif($order->status == 3)
									<td width="10%"><span class="label label-violate">Отправлен</span></td>
								@elseif($order->status == 4)
									<td width="10%"><span class="label label-shit">Оплачен и отправлен</span></td>
								@elseif($order->status == 5)
									<td width="10%"><span class="label label-success">Завершен</span></td>
                                @elseif($order->status == 6)
                                    <td width="10%"><span class="label label-not-finish">Не окончен</span></td>
								@endif
								@if($order->delivery_type == 0)
									<td width="10%"><span class="label label-success"
														  title="{{$order->delivery_warehouse_name}}">Отделение</span></td>
								@elseif($order->delivery_type == 1)
									<td width="10%"><span class="label label-danger"
														  title="{{$order->delivery_address}}">Курьер</span></td>
								@endif
								<td>{{$order->total_price ?: '---'}} грн.</td>
								<td width="7%">@widget('listButtons', ['routeBase' => 'orders.', 'id' => $order->id, 'view' => false])</td>
							</tr>
						@endforeach
					</table>
				</div>
			</div>
			<div class="text-center">{{ $orders->appends(Input::except('page'))->render() }}</div>
		</div>
	</div>
@endsection
