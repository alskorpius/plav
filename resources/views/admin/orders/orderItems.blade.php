<tbody>
<tr>
	<th></th>
	<th>Товар</th>
	<th>Цена - цена со скидкой - цена со скидкой *количество</th>
	<th>Скидка</th>
	<th>Количество</th>
	<th></th>
</tr>
@forelse($order->items AS $item)
	<tr>
		<td>
			<img src="{{$item->product->getImage('icon_small')}}" alt="{{$item->product->current->name}}">
		</td>
		<td>
			<a target="_blank" href="{{route('admin.products.edit', ['id' => $item->product->id])}}">{{$item->product->current->name}}</a>
		</td>
		<td>
			{{$item->price->price}} грн. - {{$item->totalPrice}} грн
			- {{$item->totalPrice * $item->count}} грн.
		</td>
		<td>
			{{isset($item->price->sale) ? $item->price->sale : 0}} %
		</td>
		<td>
			<input style="width: 50px; display: inline-block;" type="text" value="{{$item->count}}"
				   name="order-item-count" data-id="{{$item->id}}"
				   class="form-control order-item-count"> шт.
		</td>
		<td>
			<button type="button" class="btn btn-xs btn-danger orders-delete-items"
					data-id="{{$item->id}}"><i
					class="fa fa-trash-o"></i>
			</button>
		</td>
	</tr>
@empty
	<p>Заказ пустой</p>
@endforelse
</tbody>
