@extends('admin.layouts.main')

@section('custom-buttons')
@endsection

@section('content')
	<div class="row">
		<div class="col-md-4">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Слоган 1</h3>
				</div>

				<div class="box-body">
					<table class="table table-bordered">
						<tbody>
						<tr>
							<th>Текст строки</th>
							<th></th>
						</tr>
						@foreach($slogan1 as $item)
							<tr>
								<td><input type="text" value="{{$item->text}}" data-id="{{$item->id}}" data-slogan="1" class="form-control slogan-text"></td>
								<td class="nav-column icon-column" valign="top" style="padding: 4%;">
									<button type="button" class="btn btn-xs btn-success save-slogan" data-slogan="1" data-id="{{$item->id}}"><i class="fa fa-floppy-o"></i>
									</button>
									<button type="button" class="btn btn-xs btn-danger del-slogan" data-slogan="1" data-id="{{$item->id}}"><i class="fa fa-trash-o"></i>
									</button>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
					<div class="input-group input-group-sm answerAdd">
						<input type="text" class="form-control new-slogan-item" data-slogan="1" {{(count($slogan1) < 4) ? '' : 'disabled'}}>
						<span class="input-group-btn">
                      <button type="button"
							  class="btn btn-info btn-flat add-slogan-item" data-slogan="1" {{(count($slogan1) < 4) ? '' : 'disabled'}}>Добавить</button>
                    </span>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Слоган 2</h3>
				</div>

				<div class="box-body">
					<table class="table table-bordered">
						<tbody>
						<tr>
							<th>Текст строки</th>
							<th></th>
						</tr>
						@foreach($slogan2 as $item)
							<tr>
								<td><input type="text" value="{{$item->text}}" data-id="{{$item->id}}" data-slogan="2" class="form-control slogan-text"></td>
								<td class="nav-column icon-column" valign="top" style="padding: 4%;">
									<button type="button" class="btn btn-xs btn-success save-slogan" data-slogan="2" data-id="{{$item->id}}"><i class="fa fa-floppy-o"></i>
									</button>
									<button type="button" class="btn btn-xs btn-danger del-slogan" data-slogan="2" data-id="{{$item->id}}"><i class="fa fa-trash-o"></i>
									</button>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
					<div class="input-group input-group-sm answerAdd">
						<input type="text" class="form-control new-slogan-item" data-slogan="2" {{(count($slogan2) < 4) ? '' : 'disabled'}}>
						<span class="input-group-btn">
                      <button type="button"
							  class="btn btn-info btn-flat add-slogan-item" data-slogan="2" {{(count($slogan2) < 4) ? '' : 'disabled'}}>Добавить</button>
                    </span>
					</div>
				</div>

			</div>
		</div>

		<div class="col-md-4">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Слоган 3</h3>
				</div>

				<div class="box-body">
					<table class="table table-bordered">
						<tbody>
						<tr>
							<th>Текст строки</th>
							<th></th>
						</tr>
						@foreach($slogan3 as $item)
							<tr>
								<td><input type="text" value="{{$item->text}}" data-id="{{$item->id}}" data-slogan="3" class="form-control slogan-text"></td>
								<td class="nav-column icon-column" valign="top" style="padding: 4%;">
									<button type="button" class="btn btn-xs btn-success save-slogan" data-slogan="3" data-id="{{$item->id}}"><i class="fa fa-floppy-o"></i>
									</button>
									<button type="button" class="btn btn-xs btn-danger del-slogan" data-slogan="3" data-id="{{$item->id}}"><i class="fa fa-trash-o"></i>
									</button>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
					<div class="input-group input-group-sm slogan-add">
						<input type="text" class="form-control new-slogan-item" data-slogan="3" {{(count($slogan3) < 4) ? '' : 'disabled'}}>
						<span class="input-group-btn">
                      <button type="button"
							  class="btn btn-info btn-flat add-slogan-item" data-slogan="3" {{(count($slogan3) < 4) ? '' : 'disabled'}}>Добавить</button>
                    </span>
					</div>
				</div>

			</div>
		</div>
	</div>
@endsection
