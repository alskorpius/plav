<?php
/**
 * @var \App\Models\staff[] staff
 */
?>
@if(isset($slogan) and sizeof($slogan))
	@foreach ($slogan as $element)
		<li class="dd-item dd3-item" data-id="{{ $element->id }}">
			<div title="Переместить строку" class="dd-handle dd3-handle">Drag</div>
			<div class="dd3-content">
				<table style="width: 100%;">
					<tr>
						<td class="column-drag" width="1%"></td>
						<td width="20%">
							{{$element->text}}
						</td>
						<td class="nav-column icon-column" valign="top" width="20%">
							<a class="btn btn-xs btn-danger deleteSlogan"
							   data-id="{{$element->id}}">
								<i class="fa fa-trash"></i>
							</a>
						</td>
					</tr>
				</table>
			</div>
		</li>
	@endforeach
@endif
