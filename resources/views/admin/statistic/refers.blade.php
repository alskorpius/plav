@extends('admin.layouts.main')

{{--@section('custom-buttons')--}}
{{--<button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>--}}
{{--@endsection--}}

@section('content')
    @widget('filter', ['fields' => 'refers'])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            {{--<th>--}}
                            {{--<input type="checkbox" class="icheckbox_minimal-aero mainCheckbox" data-table = 'kslStatistics'>--}}
                            {{--</th>--}}
                            <th>IP</th>
                            <th>URL</th>
                            <th>Дата</th>
                        </tr>
                        @foreach($refers as $refer)
                            <tr>
                                {{--<td>--}}
                                {{--<input type="checkbox" class="icheckbox_minimal-aero subCheckbox" data-id = '{{$visitor->id}}'>--}}
                                {{--</td>--}}
                                <td width="20%">{{$refer->ip}}</td>
                                @if(strlen($refer->refer) >= 100)
                                    <td width="10%">{{substr($refer->refer, 0, 100)}} ...</td>
                                @else
                                    <td width="10%">{{$refer->refer}}</td>
                                @endif
                                <td width="10%">{{$refer->created_at}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $refers->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
