@extends('admin.layouts.main')

{{--@section('custom-buttons')--}}
    {{--<button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>--}}
{{--@endsection--}}

@section('content')
    @widget('filter', ['fields' => 'hits'])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            {{--<th>--}}
                                {{--<input type="checkbox" class="icheckbox_minimal-aero mainCheckbox" data-table = 'kslStatistics'>--}}
                            {{--</th>--}}
                            <th>IP</th>
                            <th>URL</th>
                            <th>Ответ сервера</th>
                            <th>Устройство</th>
                            <th>Дата посещения</th>
                            <th>Последнее обновление</th>
                            <th>Количество обновлений</th>
                        </tr>
                        @foreach($hits as $hit)
                            <tr>
                                {{--<td>--}}
                                    {{--<input type="checkbox" class="icheckbox_minimal-aero subCheckbox" data-id = '{{$visitor->id}}'>--}}
                                {{--</td>--}}
                                <td width="10%"><a href="{{route('admin.visitors.index', ['ip' => $hit->ip])}}">{{$hit->ip}}</a></td>
                                <td width="20%"><a href="{{$hit->str_url}}" target="_blank">{{str_replace(\Request::getSchemeAndHttpHost(),'',$hit->str_url)}}</a></td>
                                <td width="5%">{{$hit->response}}</td>
                                <td width="30%">{{$hit->device . '|' . $hit->platform . '|' . $hit->browser}}</td>
                                <td width="20%">{{$hit->first}}</td>
                                <td width="20%">{{$hit->last}}</td>
                                <td width="10%">{{$hit->total}}</td>
                            </tr>
                            @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $hits->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection