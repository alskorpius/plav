@extends('admin.layouts.main')

{{--@section('custom-buttons')--}}
    {{--<button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>--}}
{{--@endsection--}}

@section('content')
    @widget('filter', ['fields' => 'visitors'])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            {{--<th>--}}
                                {{--<input type="checkbox" class="icheckbox_minimal-aero mainCheckbox" data-table = 'kslStatistics'>--}}
                            {{--</th>--}}
                            <th>IP</th>
                            <th>Местоположение</th>
                            <th>Первое посещение</th>
                            <th>Последнее посещение</th>
                            <th>Количество посещений</th>
                        </tr>
                        @foreach($visitors as $visitor)
                            <tr>
                                {{--<td>--}}
                                    {{--<input type="checkbox" class="icheckbox_minimal-aero subCheckbox" data-id = '{{$visitor->id}}'>--}}
                                {{--</td>--}}
                                <td width="20%">{{$visitor->ip}}</td>
                                <td width="30%">{{implode(', ', \App\Http\Controllers\Admin\Statistic\VisitorsController::ip_info($visitor->ip))}}</td>
                                <td width="20%">{{$visitor->first}}</td>
                                <td width="20%">{{$visitor->last}}</td>
                                <td width="10%">{{$visitor->total}}</td>
                            </tr>
                            @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $visitors->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
