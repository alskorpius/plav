<?php
/** @var Illuminate\Support\ViewErrorBag $errors */
/** @var string $name */
/** @var string $label */
/** @var object $object */

use Illuminate\Support\Str;
use App\Exceptions\WrongParametersException;

if (!isset($name) || !$name) {
    throw new WrongParametersException('`name` parameter is required');
}
if (isset($lang) && $lang) {
    $elementName = $lang . '[' . $name . ']';
    $error = $errors->first($lang . '.' . $name);
} else {
    $elementName = $name;
    $error = $errors->first($name);
}
$options = (isset($options) && is_array($options)) ? $options : [];
if (array_key_exists('style', $options) === false) {
    $options['style'] = 'height: 200px;';
} else {
    $options['style'] .= 'height: 200px;';
}
$text = Lang::has('validation.attributes.' . $name) ? __('validation.attributes.' . $name) : Str::ucfirst(str_replace('_', ' ', $name));
$label = isset($label) ? $label : $text;
if (isset($placeholder) === false) {
    $placeholder = $label === false ? $text : false;
}
$value = isset($value) ? $value : old($name, isset($object) ? $object->{$name} : null);
?>
<div class="form-group {{ $error ? 'has-error' : null }} {{ isset($additionalClass) ? $additionalClass : null }}">
    {!! Form::label($elementName, $label . (in_array('required', $options) ? ' *' : null), ['class' => 'control-label']) !!}
    {!! Form::textarea($elementName, $value, array_merge(['class' => 'form-control tinymceEditor'], $options)) !!}
    @if($error)
        <span class="help-block">{{ $error }}</span>
    @elseif(isset($help))
        <span class="help-block">{{ $help }}</span>
    @endif
</div>
