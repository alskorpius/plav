<?php
$currentYear = date('Y');
$year = '2019';
if ($currentYear !== $year) {
    $year .= '-' . $currentYear;
}
?>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.4.0
    </div>
    <strong>Copyright &copy; {{ $year }}</strong> PLAVSOSTAV
</footer>
<div class="control-sidebar-bg"></div>