<?php
/** @var \App\Models\Catalog\ShopProductsDictionary $object */
/** @var \App\Models\Catalog\ShopProductsDictionariesValue $value */
/** @var \App\Models\Catalog\ShopProductsDictionariesValue $specification */
$object = isset($specification) ? $specification : null;
?>
<div class="row">
	<div class="col-md-7">
		<div class="box box-primary">
			<div class="box-body">
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}">
                            <div
                                class="col-md-8">@include('admin.widgets.form.input', ['name' => 'name', 'lang' => $language['slug'], 'label' => 'Название', 'object' => $langObject, 'options' => ['required']])</div>

                        </div>
                    @endforeach
                </div>
				<div
					class="col-md-4">@include('admin.widgets.form.input', ['name' => 'slug', 'label' => 'Алиас', 'object' => $object, 'options' => ['disabled']])</div>
			</div>
		</div>
	</div>
	<div class="col-md-5">
		<div class="box box-warning">
			<div class="box-body">
				@include('admin.widgets.form.status', ['status' => old('status', isset($object) ? $object->status : 0)])
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Добавить новое значение</h3>
			</div>
			<div class="box-body">
				<table class="table table-bordered">
					<tbody>
					<tr>
						<th width="25%">
                            <span class="col-md-6">Имя</span>
                            <ul class="nav nav-tabs col-md-6">
                                @foreach($languages AS $language)
                                    <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}new"
                                                                                                data-toggle="tab">{{ $language['name'] }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </th>
						<th width="47%">Алиас</th>
						@if($specification->slug == 'tsvet')
							<th>Цвет</th>
						@endif
						<th width="6%"></th>
					</tr>
					<tr>
						<td>
                            <div class="tab-content">
                                @foreach($languages AS $language)
                                    <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                                    <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}new">
                                        @if ($language['slug'] == 'ru')
                                            @include('admin.widgets.form.input', ['name' => 'nameValue', 'lang' => $language['slug'], 'label' => false, 'object' => $langObject, 'placeholder' => false,'options' => ['data-lang' => $language['slug']], 'class' => 'form-control translitSourceNew newName'])
                                        @else
                                            @include('admin.widgets.form.input', ['name' => 'nameValue', 'lang' => $language['slug'], 'label' => false, 'object' => $langObject, 'placeholder' => false,'options' => ['data-lang' => $language['slug']], 'class' =>  'form-control newName'])
                                        @endif

                                    </div>
                                @endforeach
                            </div>
                        </td>
						<td>
							<div class="input-group">
								<input class="form-control translitResultNew" required value="">
								<span class="input-group-btn">
                                        <button type="button" class="btn btn-info btn-flat translitButton">Транслитерировать</button>
                                    </span>
							</div>
						</td>
						@if($specification->slug == 'tsvet')
							<td>
								<input type="text" class="form-control color-picker value-color-new"/>
							</td>
						@endif
						<td class="nav-column icon-column" valign="top" style="padding: 1%;">
							<button data-table="{{$specification->id}}" type="button" id="newValue"
									class="btn btn-xs btn-success"><i class="fa fa-floppy-o"></i></button>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="box-header with-border">
				<h3 class="box-title">Список значений</h3>
			</div>
			<div class="box-body">
				<table data-table="{{$specification->id}}" class="table table-bordered" id="values-list">
					<tbody>
					<tr>
						<th width="25%">
                            <span class="col-md-6">Имя (ru)</span>
                            <span class="col-md-6">Имя (en)</span>
                        </th>
						<th width="47%">Алиас</th>
						@if($specification->slug == 'tsvet')
							<th>Цвет</th>
						@endif
                        <th width="4%">Статус</th>
                        <th width="6%"></th>
					</tr>
					@foreach($object->dictionaryValues as $value)
						<tr>
							<td>
                                @foreach($languages AS $language)
                                    <?php $langObject = ($value ? $value->dataFor(array_get($language, 'slug')) : null); ?>
                                    <div class="col-md-6">
                                        @if ($language['slug'] == 'ru')
                                            @include('admin.widgets.form.input', [
                                            'name' => 'nameValue',
                                            'lang' => $language['slug'],
                                            'label' => false,
                                            'value' => $langObject->name,
                                            'placeholder' => false,
                                            'options' => [
                                                'data-lang' => $language['slug'],
                                                'data-id'=> $value->id
                                            ],
                                            'class' => 'form-control translitSource tableName'
                                            ])
                                        @else
                                            @include('admin.widgets.form.input', [
                                            'name' => 'nameValue',
                                            'lang' => $language['slug'],
                                            'label' => false,
                                            'value' => $langObject->name,
                                            'placeholder' => false,
                                            'options' => [
                                                'data-lang' => $language['slug'],
                                                'data-id'=> $value->id
                                            ],
                                            'class' =>  'form-control tableName'
                                            ])
                                        @endif

                                    </div>
                                @endforeach
                            </td>
							<td>
								<div class="input-group">
									<input data-id="{{$value->id}}" class="form-control translitResult" required
										   value="{{$value->slug}}">
									<span class="input-group-btn">
                                        <button type="button" class="btn btn-info btn-flat translitButton"
												data-id="{{$value->id}}">Транслитерировать</button>
                                    </span>
								</div>
							</td>
							@if($specification->slug == 'tsvet')
								<td>
									<input type="text" class="form-control color-picker value-color-edit" data-id="{{$value->id}}" value="{{$value->option}}"/>
								</td>
							@endif
                            <td>@widget('status', ['status' => $value->status, 'table' => 'shop_products_dictionaries_values', 'id' => $value->id])</td>
                            <td class="nav-column icon-column" valign="top" style="padding: 1%;">
								<button type="button" class="btn btn-xs btn-success edit-value"
										data-table="{{$specification->id}}" data-id="{{$value->id}}"><i
										class="fa fa-floppy-o"></i>
								</button>
								<button type="button" class="btn btn-xs btn-danger del-value"
										data-table="{{$specification->id}}" data-id="{{$value->id}}"><i
										class="fa fa-trash-o"></i>
								</button>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
</div>
