@extends('admin.layouts.main')

@section('custom-buttons')
@endsection

@section('content')
    @widget('filter', ['fields' => ['name']])

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Название</th>
                            <th>Алиас</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($specifications AS $specification)
                            <tr>
                                <td>{{ $specification->current->name }}</td>
                                <td>{{ $specification->slug }}</td>
                                <td>@widget('status', ['status' => $specification->status, 'table' => 'shop_products_dictionaries', 'id' => $specification->id])</td>
                                <td>@widget('listButtons', ['routeBase' => 'specifications.', 'id' => $specification->id, 'view' => false, 'delete' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            {{--<div class="text-center">{{ $news->appends(Input::except('page'))->render() }}</div>--}}
        </div>
    </div>
@endsection
