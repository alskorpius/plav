@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.specifications.update', 'specification' => $specification->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.catalog.specifications._form', ['specification' => $specification])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop