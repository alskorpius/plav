<tbody>
<tr>
	<th width="25%">
        <span class="col-md-6">Имя (ru)</span>
        <span class="col-md-6">Имя (en)</span>
    </th>
	<th width="47%">Алиас</th>
	@if($specification->slug == 'tsvet')
		<th>Цвет</th>
	@endif
    <th width="4%">Статус</th>
    <th width="6%"></th>
</tr>
@foreach($specification->dictionaryValues as $value)
    <tr>
        <td>
            <div class="tab-content">
                @foreach($languages AS $language)
                    <?php $langObject = ($value ? $value->dataFor(array_get($language, 'slug')) : null); ?>
                    <div class="col-md-6">
                        @if ($language['slug'] == 'ru')
                            @include('admin.widgets.form.input', [
							'name' => 'nameValue',
							'lang' => $language['slug'],
							'label' => false,
							'value' => $langObject->name,
							'placeholder' => false,
							'options' => [
								'data-lang' => $language['slug'],
								'data-id'=> $value->id
							],
							'class' => 'form-control translitSource tableName'
							])
                        @else
                            @include('admin.widgets.form.input', [
							'name' => 'nameValue',
							'lang' => $language['slug'],
							'label' => false,
							'value' => $langObject->name,
							'placeholder' => false,
							'options' => [
								'data-lang' => $language['slug'],
								'data-id'=> $value->id
							],
							'class' =>  'form-control tableName'
							])
                        @endif

                    </div>
                @endforeach
            </div>
        </td>
        <td>
            <div class="input-group">
                <input data-id="{{$value->id}}" class="form-control translitResult" required value="{{$value->slug}}">
                <span class="input-group-btn">
                                        <button type="button" class="btn btn-info btn-flat translitButton" data-id="{{$value->id}}">Транслитерировать</button>
                                    </span>
            </div>
        </td>
		@if($specification->slug == 'tsvet')
			<td>
				<input type="text" class="form-control color-picker value-color-edit" data-id="{{$value->id}}" value="{{$value->option}}"/>
			</td>
		@endif
        <td>@widget('status', ['status' => $value->status, 'table' => 'shop_products_dictionaries_values', 'id' => $value->id])</td>
        <td class="nav-column icon-column" valign="top" style="padding: 1%;">
            <button type="button" class="btn btn-xs btn-success edit-value" data-table="{{$specification->id}}" data-id="{{$value->id}}"><i class="fa fa-floppy-o"></i>
            </button>
            <button type="button" class="btn btn-xs btn-danger del-value" data-table="{{$specification->id}}" data-id="{{$value->id}}"><i class="fa fa-trash-o"></i>
            </button>
        </td>
    </tr>
@endforeach
</tbody>
