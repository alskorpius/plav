@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
    <button class="btn bg-yellow btn-flat margin" id="statusChecked">Снять с публикации</button>
    <button class="btn bg-blue btn-flat margin" id="statusCheckedEnable">Опубликовать</button>
    <a href="{{ route('admin.rewards.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox"
                                       data-table='rewards'>
                            </th>
                            <th>Название</th>
                            <th></th>
                        </tr>
                        @foreach($rewards AS $reward)
                            <tr>
                                <td width="5%">
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox"
                                           data-id='{{$reward->id}}'>
                                </td>
                                <td >
                                    @if(isset($reward->current->name))
                                        <a href="{{route('admin.rewards.edit', ['id' => $reward->id])}}">{{$reward->current->name}}</a>
                                    @else
                                        ---
                                    @endif
                                </td>
                                <td width="7%">@widget('listButtons', ['routeBase' => 'rewards.', 'id' => $reward->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
