<?php
/** @var \App\Models\Catalog\ProductsRewards $object */
$object = isset($reward) ? $reward : null;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}">
                            @include('admin.widgets.form.input', ['name' => 'name_short', 'lang' => $language['slug'], 'label' => 'Краткое название', 'object' => $langObject, 'options' => ['required']])
                            @include('admin.widgets.form.input', ['name' => 'name', 'lang' => $language['slug'], 'label' => 'Название', 'object' => $langObject, 'options' => ['required']])
                            @include('admin.widgets.form.tiny', ['name' => 'short', 'lang' => $language['slug'], 'label' => 'Краткое описание', 'object' => $langObject, 'options' => ['required']])
                            @include('admin.widgets.form.tiny', ['name' => 'text', 'lang' => $language['slug'], 'label' => 'Контент', 'object' => $langObject, 'options' => ['required']])
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                @if(isset($object) && $object->image && $object->imageExists('big'))
                    @include('admin.widgets.form.image', [
                    'label' => 'Изображение для карточки товара',
                    'big' => $object->showImage('original', null, true),
                    'preview' => $object->showImage('big', null, true),
                    'id' => $object->id,
                    'withoutRoute' => true,
                    'crop' => false,
                    'model' => $object->getMorphClass(),
                    ])
                @else
                    @include('admin.widgets.form.image', [
                    'label' => 'Выберите изображение для карточки товара'
                    ])
                @endif
            </div>
        </div>
    </div>
</div>
