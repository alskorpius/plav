@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
    <a href="{{ route('admin.compilations.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' => ['name']])

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox" data-table = 'products_compilations'>
                            </th>
                            <th>Название</th>
                            <th>Изображение</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($compilations AS $compilation)
                            @php
                                if(!empty($compilation->current)){
                                    $compilation->name =  $compilation->current->name;
                                } else{
                                    $id = $compilation->id;
                                    $compilation->id = $compilation->row_id;
                                    $compilation->row_id = $id;
                                }
                            @endphp
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox" data-id = '{{$compilation->id}}'>
                                </td>
                                <td>{{ $compilation->name }}</td>
                                <td valign="top" class="pagename-column" width="20%">
                                    <div class="clearFix">
                                        <div class="pull-left">
                                            <div class="overflow-20">
                                                <a class="pageLinkEdit"
                                                   href="{{ route('admin.compilations.edit', ['compilation' => $compilation->id]) }}">
                                                    <img src="{{ $compilation->showImage('small') }}" alt="{{ $compilation->alt }}"
                                                         title="{{ $compilation->title }}" style="max-height:20px; width:40px;">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>@widget('status', ['status' => $compilation->status, 'table' => 'products_compilations', 'id' => $compilation->id])</td>
                                <td>@widget('listButtons', ['routeBase' => 'compilations.', 'id' => $compilation->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $compilations->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
