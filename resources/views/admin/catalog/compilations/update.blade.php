@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.compilations.update', 'compilation' => $compilation->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.catalog.compilations._form', ['compilation' => $compilation])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop