@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.compilations.store', 'files' => true]) !!}
        @include('admin.widgets.form.buttons', ['create' => true])
        @include('admin.catalog.compilations._form')
        @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop