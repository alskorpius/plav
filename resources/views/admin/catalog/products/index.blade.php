@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
    <button class="btn bg-yellow btn-flat margin" id="statusChecked">Снять с публикации</button>
    <button class="btn bg-blue btn-flat margin" id="statusCheckedEnable">Опубликовать</button>
    <a href="{{ route('admin.products.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
    <a href="{{ route('admin.products.excel') }}" class="btn bg-olive btn-flat margin">
        <i class="fa fa-file-excel-o"></i>
        Выгрузить
    </a>
@endsection

@section('content')
    @widget('filter', ['fields' => [
        ['name' => 'name', 'placeholder' => 'Имя'],
        ['name' => 'brand_id', 'type' => 'select', 'data' => $brands, 'placeholder' => 'Бренд'],
        ['name' => 'line_id', 'type' => 'select', 'data' => $lines, 'placeholder' => 'Линейка'],
        ['name' => 'compilation_id', 'type' => 'select', 'data' => $compilations, 'placeholder' => 'Подборка'],
    ]])

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox"
                                       data-table='products'>
                            </th>
                            <th></th>
                            <th>Название</th>
                            <th>Бренд</th>
                            <th>Позиция</th>
                            <th>Позиция(shop)</th>
                            <th>Линейка</th>
                            <th>Подборка</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($products AS $product)
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox"
                                           data-id='{{$product->id}}'>
                                </td>
                                <td><a title="Редактировать"
                                       href="{{route('admin.products.edit', ['id' => $product->id])}}"><img
                                            src="{{$product->getImage('icon_small')}}" alt="{{$product->current->name}}"></a></td>
                                <td width="18%"><a title="Перейти на страницу товара" target="_blank"
                                                   href="{{route('product', ['slug' => $product->slug])}}">{{ $product->current->name }}</a>
                                </td>
                                <td width="15%">
                                    @if(isset($product->brand->name))
                                        <a href="{{route('admin.brands.edit', ['id' => $product->brand->id])}}">{{$product->brand->name}}</a>
                                    @else
                                        ---
                                    @endif
                                </td>
                                <td width="5%"><input value="{{$product->position}}" type="text" class="form-control set-position-product" data-id = '{{$product->id}}'></td>
                                <td width="5%"><input value="{{$product->position_front}}" type="text" class="form-control set-front-position-product" data-id = '{{$product->id}}'></td>
                                <td width="15%">
                                    @if(isset($product->line->name))
                                        <a href="{{route('admin.lines.edit', ['id' => $product->line->id])}}">{{$product->line->name}}</a>
                                    @else
                                        ---
                                    @endif
                                </td>

                                <td width="15%">
                                    @if(isset($product->compilation->name))
                                        <a href="{{route('admin.compilations.edit', ['id' => $product->compilation->id])}}">{{$product->compilation->name}}</a>
                                    @else
                                        ---
                                    @endif
                                </td>
                                <td width="10%">@widget('status', ['status' => $product->status, 'table' => 'products', 'id' => $product->id])</td>
                                <td width="7%">@widget('listButtons', ['routeBase' => 'products.', 'id' => $product->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $products->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
