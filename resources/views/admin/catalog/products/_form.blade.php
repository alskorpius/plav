<?php
/** @var \App\Models\Catalog\Product $object */
$object = isset($product) ? $product : null;
$images = isset($product->images) ? $product->images : null;
?>
<div class="row">
    <div class="col-md-{{ $object ? 6 : 6 }}">
        <div class="box box-warning">
            <div class="box-body">
                @include('admin.widgets.form.input', ['name' => 'slug', 'label' => 'Алиас', 'object' => $object, 'options' => ['required', 'data-slug-destination']])
                @include('admin.widgets.form.input', ['name' => 'position', 'label' => 'Позиция', 'object' => $object, 'options' => ['required', 'data-position']])
                @include('admin.widgets.form.input', ['name' => 'position_front', 'label' => 'Позиция (shop)', 'object' => $object, 'options' => ['required', 'data-position']])
            </div>
        </div>
        <div class="box box-info">
            <div class="box-body">
            <ul class="nav nav-tabs">
                @foreach($languages AS $language)
                    <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}"
                                                                                data-toggle="tab">{{ $language['name'] }}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach($languages AS $language)
                    <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                    <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}">
                        @include('admin.widgets.form.input', ['name' => 'name', 'lang' => $language['slug'], 'label' => 'Название', 'object' => $langObject, 'options' => ['required', 'data-slug-origin']])
                        @include('admin.widgets.form.tiny', ['name' => 'content', 'lang' => $language['slug'], 'label' => 'Описание', 'object' => $langObject])
                    </div>
                @endforeach
            </div>

                @include('admin.widgets.form.select', ['name' => 'products_recomm[]', 'value' => isset($selected) ? $selected : [], 'label' => 'С этим покупают', 'data' => $products, 'options' => ['class' => 'form-control select2', 'multiple' => 'multiple']])
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-warning">
            <div class="box-body">
                @include('admin.widgets.form.status', ['status' => old('status', isset($object) ? $object->status : 0)])
                <div class="form-group">
                    {!! Form::label('is_accessory', 'Аксессуар') !!}
                    <div class="radio" style="display:inline-block;">
                        <label>{!! Form::radio('is_accessory', 0, !isset($object->is_accessory) || !$object->is_accessory, []) !!}
                            Нет</label>
                    </div>
                    <div class="radio" style="display:inline-block;">
                        <label>{!! Form::radio('is_accessory', 1, isset($object->is_accessory) && $object->is_accessory, []) !!}
                            Да</label>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('is_avilable', 'Доступнутось') !!}
                    <div class="radio" style="display:inline-block;">
                        <label>{!! Form::radio('is_avilable', 0, isset($object->is_avilable) && $object->is_avilable == 0, []) !!}
                            Нет в наличии</label>
                    </div>
                    <div class="radio" style="display:inline-block;">
                        <label>{!! Form::radio('is_avilable', 1, isset($object->is_avilable) && $object->is_avilable == 1, []) !!}
                            В наличии</label>
                    </div>
                    <div class="radio" style="display:inline-block;">
                        <label>{!! Form::radio('is_avilable', 2, isset($object->is_avilable) && $object->is_avilable == 2, []) !!}
                            Ожидается поставка</label>
                    </div>
                </div>
                @if(isset($object) && $object->image && $object->imageExists('small'))
                    @include('admin.widgets.form.image', ['label' => 'Изображение для карточки товара', 'big' => $object->showImage('original', null, true), 'preview' => $object->showImage('small', null, true), 'id' => $product->id, 'model' => $product->getMorphClass()])
                @else
                    @include('admin.widgets.form.image', ['label' => 'Выберите изображение для карточки товара'])
                @endif
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}meta"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}meta">
                            @include('admin.widgets.form.input', ['name' => 'h1', 'lang' => $language['slug'], 'object' => $langObject])
                            @include('admin.widgets.form.input', ['name' => 'title', 'lang' => $language['slug'], 'object' => $langObject])
                            @include('admin.widgets.form.textarea', ['name' => 'description', 'lang' => $language['slug'], 'object' => $langObject])
                            @include('admin.widgets.form.textarea', ['name' => 'keywords', 'lang' => $language['slug'], 'object' => $langObject])
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 hide-for-accessories {{ isset($object->is_accessory) && $object->is_accessory ? 'active' : null }}">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Презентация товара</h3>
            </div>
            <div class="box-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        @foreach($languages AS $language)
                            <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}presentation"
                                                                                        data-toggle="tab">{{ $language['name'] }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($languages AS $language)
                            <?php $langObject = ($product->presentation ? $product->presentation->dataFor(array_get($language, 'slug')) : null); ?>
                            <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}presentation">
                                @include('admin.widgets.form.tiny', ['label' => 'Описание', 'name' => 'text', 'lang' => $language['slug'], 'value' => isset($langObject) ? $langObject->text : '', 'options' => ['']])
                                @include('admin.widgets.form.input', ['label' => 'Цвет', 'name' => 'color', 'lang' => $language['slug'], 'value' => isset($langObject) ? $langObject->color: '', 'options' => ['']])
                                @include('admin.widgets.form.input', ['label' => 'Аромат', 'name' => 'aroma', 'lang' => $language['slug'], 'value' => isset($langObject) ? $langObject->aroma: '', 'options' => ['']])
                                @include('admin.widgets.form.input', ['label' => 'Вкус', 'name' => 'taste', 'lang' => $language['slug'], 'value' => isset($langObject) ? $langObject->taste: '', 'options' => ['']])
                                @include('admin.widgets.form.input', ['label' => 'Гастрономические сочетания', 'name' => 'combination', 'lang' => $language['slug'], 'value' => isset($langObject) ? $langObject->combination: '', 'options' => ['']])
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Информация</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    {!! Form::label('novelty', 'Новинка') !!}
                    <div class="radio" style="display:inline-block;">
                        <label>{!! Form::radio('novelty', 0, isset($object->novelty) && !$object->novelty, ['class' => 'minimal']) !!}
                            Нет</label>
                    </div>
                    <div class="radio" style="display:inline-block;">
                        <label>{!! Form::radio('novelty', 1, !isset($object->novelty) || $object->novelty, ['class' => 'minimal']) !!}
                            Да</label>
                    </div>
                </div>
                @include('admin.widgets.form.select', ['name' => 'brand_id', 'label' => 'Бренд',  'data' => $brands, 'options' => ['required']])
                @include('admin.widgets.form.select', ['name' => 'line_id', 'label' => 'Линейка',  'data' => $lines, 'options' => []])
                @include('admin.widgets.form.select', ['name' => 'compilation_id[]', 'value' => $selectedCompilations, 'label' => 'Подборка',  'data' => $compilation, 'options' => ['class' => 'form-control select2', 'multiple' => 'multiple']])
            </div>
        </div>
    </div>
    <div class="col-md-6 hide-for-accessories {{ isset($object->is_accessory) && $object->is_accessory ? 'active' : null }}">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Характеристики</h3>
            </div>
            <div class="box-body">
                @include('admin.widgets.form.select', ['name' => 'color_id', 'label' => 'Цвет',  'data' => $colors, 'options' => []])
                @include('admin.widgets.form.select', ['name' => 'sweetness_id', 'label' => 'Сладость',  'data' => $sweetness, 'options' => []])
                @include('admin.widgets.form.select', ['name' => 'type_id', 'label' => 'Тип',  'data' => $types, 'options' => []])
                @include('admin.widgets.form.select', ['name' => 'kind_id', 'label' => 'Вид',  'data' => $kind, 'options' => []])
                @include('admin.widgets.form.select', ['name' => 'alcohol_id', 'label' => 'Алкоголь',  'data' => $alcohol, 'options' => []])
                @include('admin.widgets.form.select', ['name' => 'sort_id[]', 'value' => $selectedSorts, 'label' => 'Сорта', 'data' => $sort, 'options' => ['class' => 'form-control select2', 'multiple' => 'multiple']])
                @include('admin.widgets.form.select', ['name' => 'sugar_id', 'label' => 'Количество сахара',  'data' => $sugar, 'options' => []])
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Добавить цену</h3>
            </div>

            <div class="box-body">
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <th>Объем (мл)*</th>
                        <th>Год*</th>
                        <th>Цена*</th>
                        <th>Вес (кг)*</th>
                        <th>Скидка (%)</th>
                        <th>Артикул*</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td width="15%"><input type="text" id="newVolume" class="form-control" placeholder="Объем (мл)*">
                        </td>
                        <td width="15%"><input type="text" id="newYear" class="form-control" placeholder="Год*"></td>
                        <td width="15%"><input type="text" id="newPriceI" class="form-control" placeholder="Цена*"></td>
                        <td width="15%"><input type="text" id="newWeight" class="form-control" placeholder="Вес (кг)*">
                        </td>
                        <td width="15%"><input type="text" id="newSale" class="form-control" placeholder="Скидка (%)">
                        </td>
                        <td width="15%"><input type="text" id="newArtikul" class="form-control" placeholder="Артикул*">
                        </td>
                        <td>
                            <button data-table="{{$product->id}}" type="button" id="newPrice"
                                    class="btn btn-xs btn-success"><i class="fa fa-floppy-o"></i></button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="box-body">
                <div class="box-header with-border">
                    <h3 class="box-title">Список цен</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered" id="price-list">
                        <tbody>
                        <tr>
                            <th>Объем (мл)*</th>
                            <th>Год*</th>
                            <th>Цена*</th>
                            <th>Вес (кг)*</th>
                            <th>Скидка (%)</th>
                            <th>Артикул*</th>
                            <th>На карточку товара</th>
                            <th></th>
                        </tr>
                        @foreach($product->prices as $item)
                            <tr>
                                <td width="12%">
                                    <input type="text" value="{{$item->volume}}" class="form-control editVolume"
                                           data-id="{{$item->id}}">
                                </td>
                                <td width="12%">
                                    <input type="text" value="{{$item->year}}" class="form-control editYear"
                                           data-id="{{$item->id}}">
                                </td>
                                <td width="15%">
                                    <input type="text" value="{{$item->price}}" class="form-control editPrice"
                                           data-id="{{$item->id}}">
                                </td>
                                <td width="15%">
                                    <input type="text" value="{{$item->weight}}" class="form-control editWeight"
                                           data-id="{{$item->id}}">
                                </td>
                                <td width="15%">
                                    <input type="text" value="{{$item->sale}}" class="form-control editSale"
                                           data-id="{{$item->id}}">
                                </td>
                                <td width="15%">
                                    <input type="text" value="{{$item->artikul}}" class="form-control editArtikul"
                                           data-id="{{$item->id}}">
                                </td>
                                <td width="8%" style="padding: 2%;">
                                    <input type="radio" class="editCheck" name="is-checked-price" value="{{$item->id}}"
                                           data-id="{{$item->id}}" {{$item->is_checked == 1 ? 'checked' : ''}}>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-success edit-price"
                                            data-table="{{$product->id}}" data-id="{{$item->id}}"><i
                                            class="fa fa-floppy-o"></i>
                                    </button>
                                    <button type="button" class="btn btn-xs btn-danger del-price"
                                            data-table="{{$product->id}}" data-id="{{$item->id}}"><i
                                            class="fa fa-trash-o"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @include('admin.catalog.products.rewards', ['product' => $product, 'rewards' => $rewards])
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @include('admin.catalog.products.uploader', ['product' => $product])
    </div>
</div>

@if($object)
    @push('scripts')
    <script>
        $(document).ready(function () {
            var progress = '<div class="progress active time-to-time-loader" style="width: 100%; height: 34px;">' +
                '<div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>' +
                '</div>';
            $('body').on('click', 'ul.regions-list > li > span', function () {
                var $li = $(this).parent('li');
                $li.toggleClass('opened');
            });
            function preloader($li, start) {
                start = (start === false || start === true) ? start : true;
                if (start) {
                    $li.append(progress);
                    $li.addClass('updating');
                } else {
                    $li.find('.progress').remove();
                    $li.removeClass('updating');
                }
            }

            function generateRow(price, stock, id) {
                return '<li data-id="' + id + '" class="remote">' +
                    '<div class="form-group">' +
                    '<input type="text" class="form-control price-input" name="price" placeholder="Цена за тонну" value="' + price + '"> грн.' +
                    '<input type="text" class="form-control stock-input" name="price" placeholder="Цена за тонну" value="' + stock + '"> кол.' +
                    '<span class="btn btn-primary button-saving save-price"><i class="fa fa-save"></i></span>' +
                    '<span class="btn btn-danger button-saving delete-price"><i class="fa fa-close"></i></span>' +
                    '</div>' +
                    '</li>';
            }

        });
    </script>
    @endpush

    @push('styles')
    <style>
        ul.regions-list, ul.regions-list ul {
            list-style-type: none;
            padding: 0;
        }

        ul.regions-list > li {
            margin-bottom: 5px;
            padding: 5px;
            border: 1px solid #eee;
            cursor: pointer;
        }

        ul.regions-list li:hover {
            background-color: #eee;
        }

        ul.regions-list > li > span {
            font-weight: bold;
            padding-left: 2px;
            display: block;
        }

        ul.regions-list > li > ul {
            display: none;
        }

        ul.regions-list > li.opened > ul {
            display: block;
        }

        ul.regions-list > li.opened > span {
            margin-bottom: 10px;
        }

        .button-saving {
            width: 40px;
            display: inline-block;
            vertical-align: top;
            margin-left: 3px;
        }

        .price-input {
            display: inline-block;
            width: 30%;
            margin-right: 5px;
        }

        .stock-input {
            display: inline-block;
            width: 20%;
            margin-right: 5px;
        }

        .stock-select {
            width: calc(50% - 50px);
            display: inline-block;
            margin-left: 0;
            margin-right: 0;
        }

        li.updating .form-group {
            display: none;
        }

        ul.regions-list > li > ul > li:last-child .button-saving {
            margin-left: 0;
        }

        ul.regions-list > li > ul > li:last-child .price-input {
            margin-right: 2px;
        }
    </style>
    @endpush
@endif
