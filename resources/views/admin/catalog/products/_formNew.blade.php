<?php
/** @var \App\Models\News $object */
$object = isset($product) ? $product : null;
?>
<div class="row">
	<div class="col-md-7">
		<div class="box box-primary">
			<div class="box-body">
				@include('admin.widgets.form.input', ['name' => 'slug', 'label' => 'Алиас', 'object' => $object, 'options' => ['required', 'data-slug-destination']])
                @include('admin.widgets.form.input', ['name' => 'position', 'label' => 'Позиция', 'object' => $object, 'options' => ['required', 'data-position']])
                @include('admin.widgets.form.input', ['name' => 'position_front', 'label' => 'Позиция (shop)', 'object' => $object, 'options' => ['required', 'data-position']])
            </div>
		</div>
        <div class="box box-info">
            <div class="box-body">
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}">
                            @include('admin.widgets.form.input', ['name' => 'name', 'lang' => $language['slug'], 'label' => 'Название', 'object' => $langObject, 'options' => ['required', 'data-slug-origin']])
                            @include('admin.widgets.form.tiny', ['name' => 'content', 'lang' => $language['slug'], 'label' => 'Контент', 'object' => $langObject])
                        </div>
                    @endforeach
                </div>
                @include('admin.widgets.form.select', ['name' => 'products_recomm[]', 'value' => isset($selected) ? $selected : [], 'label' => 'С этим покупают', 'data' => $products, 'options' => ['class' => 'form-control select2', 'multiple' => 'multiple']])
            </div>
        </div>
	</div>
	<div class="col-md-5">
		<div class="box box-warning">
			<div class="box-body">
				@include('admin.widgets.form.status', ['status' => old('status', isset($object) ? $object->status : 0)])
                                <div class="form-group">
                                    {!! Form::label('is_accessory', 'Аксессуар') !!}
                                    <div class="radio" style="display:inline-block;">
                                        <label>{!! Form::radio('is_accessory', 0, !isset($object->is_accessory) || !$object->is_accessory, []) !!}
                                            Нет</label>
                                    </div>
                                    <div class="radio" style="display:inline-block;">
                                        <label>{!! Form::radio('is_accessory', 1, isset($object->is_accessory) && $object->is_accessory, []) !!}
                                            Да</label>
                                    </div>
                                </div>
				<div class="form-group">
					{!! Form::label('is_avilable', 'Доступнутось') !!}
					<div class="radio" style="display:inline-block;">
						<label>{!! Form::radio('is_avilable', 0, !isset($object->is_avilable) || $object->is_avilable == 0, []) !!}
							Нет в наличии</label>
					</div>
					<div class="radio" style="display:inline-block;">
						<label>{!! Form::radio('is_avilable', 1, isset($object->is_avilable) && $object->is_avilable == 1, []) !!}
							В наличии</label>
					</div>
					<div class="radio" style="display:inline-block;">
						<label>{!! Form::radio('is_avilable', 2, isset($object->is_avilable) && $object->is_avilable == 2, []) !!}
							Ожидается поставка</label>
					</div>
				</div>
				@if(isset($object) && $object->image && $object->imageExists('small'))
					@include('admin.widgets.form.image', [ 'label' => 'Изображение для карточки товара', 'big' => $object->showImage('original', null, true), 'preview' => $object->showImage('small', null, true), 'id' => $object->id, 'model' => $object->getMorphClass()])
				@else
					@include('admin.widgets.form.image', ['label' => 'Выберите изображение для карточки товара'])
				@endif
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}meta"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}meta">
                            @include('admin.widgets.form.input', ['name' => 'h1', 'lang' => $language['slug'], 'object' => $langObject])
                            @include('admin.widgets.form.input', ['name' => 'title', 'lang' => $language['slug'], 'object' => $langObject])
                            @include('admin.widgets.form.textarea', ['name' => 'description', 'lang' => $language['slug'], 'object' => $langObject])
                            @include('admin.widgets.form.textarea', ['name' => 'keywords', 'lang' => $language['slug'], 'object' => $langObject])
                        </div>
                    @endforeach
                </div>
			</div>
		</div>
	</div>
</div>
<div class="row" style="margin: 0 0">
    <div class="col-md-6 hide-for-accessories {{ isset($object->is_accessory) && $object->is_accessory ? 'active' : null }}">
	<div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title">Презентация товара</h3>
		</div>
		<div class="box-body">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    @foreach($languages AS $language)
                        <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}presentation"
                                                                                    data-toggle="tab">{{ $language['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages AS $language)
                        <?php $langObject = (isset($object->presentation) ? $object->presentation->dataFor(array_get($language, 'slug')) : null); ?>
                        <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}presentation">
                            @include('admin.widgets.form.tiny', ['label' => 'Описание', 'name' => 'text', 'lang' => $language['slug'], 'options' => ['']])
                            @include('admin.widgets.form.input', ['label' => 'Цвет', 'name' => 'color', 'lang' => $language['slug'], 'options' => ['']])
                            @include('admin.widgets.form.input', ['label' => 'Аромат', 'name' => 'aroma', 'lang' => $language['slug'], 'options' => ['']])
                            @include('admin.widgets.form.input', ['label' => 'Вкус', 'name' => 'taste', 'lang' => $language['slug'], 'options' => ['']])
                            @include('admin.widgets.form.input', ['label' => 'Гастрономические сочетания', 'name' => 'combination', 'lang' => $language['slug'], 'options' => ['']])
                        </div>
                    @endforeach
                </div>
            </div>
		</div>
	</div>
    </div>
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Информация</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    {!! Form::label('novelty', 'Новинка') !!}
                    <div class="radio" style="display:inline-block;">
                        <label>{!! Form::radio('novelty', 0, isset($object->novelty) && !$object->novelty, ['class' => 'minimal']) !!}
                            Нет</label>
                    </div>
                    <div class="radio" style="display:inline-block;">
                        <label>{!! Form::radio('novelty', 1, !isset($object->novelty) || $object->novelty, ['class' => 'minimal']) !!}
                            Да</label>
                    </div>
                </div>
                @include('admin.widgets.form.select', ['name' => 'brand_id', 'label' => 'Бренд',  'data' => $brands, 'options' => ['required']])
                @include('admin.widgets.form.select', ['name' => 'line_id', 'label' => 'Линейка',  'data' => $lines, 'options' => []])
                @include('admin.widgets.form.select', ['name' => 'compilation_id[]', 'label' => 'Подборка',  'data' => $compilation, 'options' => ['class' => 'form-control select2', 'multiple' => 'multiple']])
            </div>
        </div>
    </div>
    <div class="col-md-6 hide-for-accessories {{ isset($object->is_accessory) && $object->is_accessory ? 'active' : null }}">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Характеристики</h3>
            </div>
            <div class="box-body">
                @include('admin.widgets.form.select', ['name' => 'color_id', 'label' => 'Цвет',  'data' => $colors, 'options' => []])
                @include('admin.widgets.form.select', ['name' => 'sweetness_id', 'label' => 'Сладость',  'data' => $sweetness, 'options' => []])
                @include('admin.widgets.form.select', ['name' => 'type_id', 'label' => 'Тип',  'data' => $types, 'options' => []])
                @include('admin.widgets.form.select', ['name' => 'kind_id', 'label' => 'Вид',  'data' => $kind, 'options' => []])
                @include('admin.widgets.form.select', ['name' => 'alcohol_id', 'label' => 'Алкоголь',  'data' => $alcohol, 'options' => []])
                @include('admin.widgets.form.select', ['name' => 'sort_id[]', 'label' => 'Сорт винограда',  'data' => $sort, 'options' => ['class' => 'form-control select2', 'multiple' => 'multiple']])
                @include('admin.widgets.form.select', ['name' => 'sugar_id', 'label' => 'Количество сахара',  'data' => $sugar, 'options' => []])
            </div>
        </div>
    </div>
</div>
