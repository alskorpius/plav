<?php
/**
* @var \App\Models\DistributorPhotos[] $images
*/
?>
@if(count($images))
    @foreach($images AS $image)
        <div class="loadedBlockWithText loadedBlock {{ $image->status == 1 ? 'chk' : ''}}" data-image="{{ $image->id }}">
            <div class="loadedImage">
                <img src="{{ $image->getImage('icon') }}" />
            </div>
            <div class="loadedControl">
                <div class="loadedCtrl loadedCheck">
                    <label>
                        <input type="checkbox">
                        <ins></ins>
                        <span class="btn btn-info" alt="Отметить"><i class="fa fa-check"></i></span>
                        <div class="checkInfo"></div>
                    </label>
                </div>
                <div class="loadedCtrl loadedView">
                    <a class="btn btn-primary btnImage" alt="Просмотр" data-lightbox="gallery" href="{{ $image->getImage('big') }}">
                        <i class="fa fa-plus-square"></i>
                    </a>
                </div>
                {{--<div class="loadedCtrl">--}}{{-- ??--}}
                    {{--<a class="btn btn-warning" alt="Редактировать" href="{{ route('photos.edit', ['photo' => $image->id]) }}"><i class="fa fa-pencil"></i></a>--}}
                {{--</div>--}}
                <div class="loadedCtrl loadedDelete">
                    <button class="btn btn-danger" data-id="{{ $image->id }}" alt="Удалить"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <div class="loadedDrag"></div>
        </div>
    @endforeach
@else
    <div class="alert alert-warning">Нет загруженных фото!</div>
@endif