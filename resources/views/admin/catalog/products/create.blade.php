@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.products.store', 'files' => true]) !!}
        @include('admin.widgets.form.buttons', ['create' => true])
        @include('admin.catalog.products._formNew')
        @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop