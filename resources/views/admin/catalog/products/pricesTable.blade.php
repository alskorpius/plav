<tbody>
<tr>
    <th>Объем (мл)*</th>
    <th>Год*</th>
    <th>Цена*</th>
    <th>Вес (кг)*</th>
	<th>Скидка (%)</th>
    <th>Артикул*</th>
    <th>Вывод</th>
	<th></th>
</tr>
@foreach($product->prices as $item)
    <tr>
		<td width="12%">
			<input  type="text" value="{{$item->volume}}" class="form-control editVolume" data-id="{{$item->id}}">
		</td>
		<td width="12%">
			<input  type="text" value="{{$item->year}}" class="form-control editYear" data-id="{{$item->id}}">
		</td>
		<td width="15%">
			<input  type="text" value="{{$item->price}}" class="form-control editPrice" data-id="{{$item->id}}">
		</td>
		<td width="15%">
			<input  type="text" value="{{$item->weight}}" class="form-control editWeight" data-id="{{$item->id}}">
		</td>
		<td width="15%">
			<input  type="text" value="{{$item->sale}}" class="form-control editSale" data-id="{{$item->id}}">
		</td>
        <td width="15%">
            <input type="text" value="{{$item->artikul}}" class="form-control editArtikul"
                   data-id="{{$item->id}}">
        </td>
		<td width="5%" style="padding: 2%;">
			<input type="radio" class="editCheck" name="is-checked-price" value="{{$item->id}}" data-id="{{$item->id}}" {{$item->is_checked == 1 ? 'checked' : ''}}>
		</td>
        <td>
            <button type="button" class="btn btn-xs btn-success edit-price"
                    data-table="{{$product->id}}" data-id="{{$item->id}}"><i
                        class="fa fa-floppy-o"></i>
            </button>
            <button type="button" class="btn btn-xs btn-danger del-price"
                    data-table="{{$product->id}}" data-id="{{$item->id}}"><i
                        class="fa fa-trash-o"></i>
            </button>
        </td>
    </tr>
@endforeach
</tbody>
