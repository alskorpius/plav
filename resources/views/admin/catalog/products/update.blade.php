@extends('admin.layouts.main')

@push('styles')
<link rel="stylesheet" href="<?php echo asset('admin/dev/dropzone.css'); ?>">
@endpush
@push('scripts')
<script src="{{ asset('admin/dev/dropzone.js') }}"></script>
@endpush

@section('content')
    {!! Form::open(['route' => ['admin.products.update', 'product' => $product->id], 'method' => 'PUT', 'files' => true]) !!}
    @include('admin.widgets.form.buttons')
    @include('admin.catalog.products._form', ['product' => $product])
    @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop