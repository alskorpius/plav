<tbody>
<tr>
    <th>Название награды</th>
    <th>Количество баллов</th>
    <th></th>
</tr>
@foreach($product_reward as $item)
    <tr>
        <td width="75%">
            <select class="edit-reward-name" data-id="{{$item->id}}">
                @foreach($rewards as $reward)
                    @php
                        $selected = '';
                        if($reward->id == $item->reward_id){
                            $selected = 'selected';
                        }
                    @endphp

                    <option value="{{ $reward->id }}" {{ $selected }}>{{ $reward->current->name }}</option>
                @endforeach
            </select>
        </td>
        <td width="25%">
            <input type="text" value="{{$item->points}}" class="form-control editPoints"
                   data-id="{{$item->id}}">
        </td>
        <td>
            <button type="button" class="btn btn-xs btn-success edit-reward"
                    data-table="{{$item->product_id}}" data-id="{{$item->id}}"><i
                    class="fa fa-floppy-o"></i>
            </button>
            <button type="button" class="btn btn-xs btn-danger del-reward"
                    data-table="{{$item->product_id}}" data-id="{{$item->id}}"><i
                    class="fa fa-trash-o"></i>
            </button>
        </td>
    </tr>
@endforeach
</tbody>
