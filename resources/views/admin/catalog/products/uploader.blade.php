<div class="dropModule">
    <div class="box box-solid dropBox">
        <div class="box-header with-border">
            <i class="fa fa-download"></i>
            Загрузка изображений
        </div>
        <div class="box-body">
            <button class="btn btn-success dropAdd"><i class="fa fa-plus"></i> Добавить изображения</button>
            <button class="btn btn-info dropLoad" style="display: none;"><i class="fa fa-download"></i> Загрузить все (<span class="dropCount">0</span>)</button>
            <button class="btn btn-danger dropCancel" style="display: none;"><i class="fa fa-ban-circle"></i> Отменить все</button>
        </div>
        <div class="box-body">
            <div class="dropZone"
                 data-config="{{ asset('admin/dev/dropzone/photo-gallery.json') }}"
                 data-sortable="admin/ajax/gallery-sort-photos"
                 data-upload="admin/ajax/gallery-get-photos"
                 {{--data-default="admin/ajax/gallery-set-main"--}}
                 data-id="{{ $product->id }}"
            ></div>
        </div>
    </div>
    <div class="box box-solid loadedBox">
        <div class="box-header with-border">
            <i class="fa fa-file"></i>
            Загруженные изображения
        </div>
        <div class="box-body">
            <button class="btn btn-info checkAll" style="display: none;"><i class="fa fa-check"></i> Отметить все</button>
            <button class="btn btn-warning uncheckAll" style="display: none;"><i class="fa fa-ban-circle"></i> Снять все</button>
            <button class="btn btn-danger removeCheck" style="display: none;"><i class="fa fa-remove"></i> Удалить отмеченые</button>
        </div>
        <div class="box-body dropDownload">@include('admin.catalog.products.uploaded_images', ['images' => $product->images])</div>
    </div>
</div>