<?php
/** @var \App\Models\News $object */
$object = isset($brand) ? $brand : null;
?>
<div class="row">
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.input', ['name' => 'slug', 'label' => 'Алиас', 'object' => $object, 'options' => ['required', 'data-slug-destination']])
                @include('admin.widgets.form.select', ['name' => 'shop_provider_id', 'label' => 'Производитель', 'data' => $providers, 'object' => $object, 'options' => ['required']])
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        @foreach($languages AS $language)
                            <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}"
                                                                                        data-toggle="tab">{{ $language['name'] }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($languages AS $language)
                            <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                            <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}">
                                @include('admin.widgets.form.input', ['name' => 'name', 'lang' => $language['slug'], 'label' => 'Название', 'object' => $langObject, 'options' => ['required', 'data-slug-origin']])
                                @include('admin.widgets.form.tiny', ['name' => 'content', 'lang' => $language['slug'], 'label' => 'Описание', 'object' => $langObject, 'options' => ['']])
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="box box-warning">
            <div class="box-body">
                @include('admin.widgets.form.status', ['status' => old('status', isset($object) ? $object->status : 0)])
                @if(isset($object) && $object->image && $object->imageExists('small'))
                    @include('admin.widgets.form.image', ['big' => $object->showImage('original', null, true), 'preview' => $object->showImage('small', null, true), 'id' => $brand->id, 'model' => $brand->getMorphClass()])
                @else
                    @include('admin.widgets.form.image')
                @endif
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        @foreach($languages AS $language)
                            <li class="{{ $language['default'] ? 'active' : null }}"><a href="#{{ $language['slug'] }}meta"
                                                                                        data-toggle="tab">{{ $language['name'] }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($languages AS $language)
                            <?php $langObject = ($object ? $object->dataFor(array_get($language, 'slug')) : null); ?>
                            <div class="{{ $language['default'] ? 'active' : null }} tab-pane" id="{{ $language['slug'] }}meta">
                                @include('admin.widgets.form.input', ['name' => 'h1', 'lang' => $language['slug'], 'object' => $langObject])
                                @include('admin.widgets.form.input', ['name' => 'title', 'lang' => $language['slug'], 'object' => $langObject])
                                @include('admin.widgets.form.textarea', ['name' => 'description', 'lang' => $language['slug'], 'object' => $langObject])
                                @include('admin.widgets.form.textarea', ['name' => 'keywords', 'lang' => $language['slug'], 'object' => $langObject])

                            </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
