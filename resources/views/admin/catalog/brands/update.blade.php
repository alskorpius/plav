@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.brands.update', 'brand' => $brand->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.catalog.brands._form', ['brand' => $brand])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop