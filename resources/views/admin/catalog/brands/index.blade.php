@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
    <a href="{{ route('admin.brands.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' => ['name']])

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox" data-table = 'shop_brands'>
                            </th>
                            <th>Название</th>
                            <th>Производитель</th>
                            <th>Позиция</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($brands AS $brand)
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox" data-id = '{{$brand->id}}'>
                                </td>
                                <td>{{ $brand->current->name }}</td>
                                <td>{{ $brand->shopProvider ? $brand->shopProvider->current->name : '' }}</td>
                                <td width="5%"><input type="number" class="form-control set-brand-position" data-id="{{$brand->id}}" value="{{ $brand->position}}"></td>
                                <td>@widget('status', ['status' => $brand->status, 'table' => 'shop_brands', 'id' => $brand->id])</td>
                                <td>@widget('listButtons', ['routeBase' => 'brands.', 'id' => $brand->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $brands->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
