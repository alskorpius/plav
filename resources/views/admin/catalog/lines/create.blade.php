@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.lines.store', 'files' => true]) !!}
        @include('admin.widgets.form.buttons', ['create' => true])
        @include('admin.catalog.lines._form')
        @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop