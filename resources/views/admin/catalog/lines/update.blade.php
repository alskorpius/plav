@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.lines.update', 'line' => $line->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.catalog.lines._form', ['line' => $line])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop