@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
    <a href="{{ route('admin.lines.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' => ['name']])

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox" data-table = 'products_lines'>
                            </th>
                            <th>Название</th>
                            <th>Изображение</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($lines AS $line)
                            @php
                                if(!empty($line->current)){
                                    $line->name =  $line->current->name;
                                } else{
                                    $id = $line->id;
                                    $line->id = $line->row_id;
                                    $line->row_id = $id;
                                }
                            @endphp
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox" data-id = '{{$line->id}}'>
                                </td>
                                <td>{{ $line->name }}</td>
                                <td valign="top" class="pagename-column" width="20%">
                                    <div class="clearFix">
                                        <div class="pull-left">
                                            <div class="overflow-20">
                                                <a class="pageLinkEdit"
                                                   href="{{ route('admin.lines.edit', ['line' => $line->id]) }}">
                                                    <img src="{{ $line->showImage('small') }}" alt="{{ $line->alt }}"
                                                         title="{{ $line->title }}" style="max-height:20px; width:40px;">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>@widget('status', ['status' => $line->status, 'table' => 'products_lines', 'id' => $line->id])</td>
                                <td>@widget('listButtons', ['routeBase' => 'lines.', 'id' => $line->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $lines->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
