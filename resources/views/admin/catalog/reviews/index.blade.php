<?php
/** @var \App\Models\Catalog\ProductsReviews $review */
?>
@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
    <a href="{{ route('admin.products-reviews.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' => 'products_reviews'])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox"
                                       data-table='products_reviews'>
                            </th>
                            <th>Отзыв</th>
                            <th>Товар</th>
                            <th>Дата</th>
                            <th>Имя пользователя</th>
                            <th>Email пользователя</th>
                            <th>Оценка</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($reviews AS $review)
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox"
                                           data-id='{{$review->id}}'>
                                </td>
                                <td width="20%"><a
                                        href="{{route('admin.products-reviews.edit', ['id' => $review->id])}}">{{ substr($review->text, 0, 20) }}
                                        ...</a></td>
                                <td width="13%"><a
                                        href="{{ route('admin.products.edit', ['product' => $review->product->id]) }}">{{ substr($review->product->current->name, 0, 25) }}
                                        ...</a></td>
                                <td width="10%">{{isset($review->created_at) ? $review->created_at->format('d.m.Y') : '---'}}</td>
                                @if(isset($review->user_id))
                                    <td width="13%"><a
                                            href="{{ route('admin.customers.edit', ['user' => $review->user->id]) }}">{{ $review->user->first_name . ' ' . $review->user->second_name }}</a>
                                    </td>
                                    <td width="13%">{{$review->user->email}}</td>
                                @elseif($review->user_name)
                                    <td width="13%">{{$review->user_name}}</td>
                                    <td width="13%">{{$review->email}}</td>
                                @else
                                    <td width="13%">------</td>
                                    <td width="13%">------</td>
                                @endif
                                <td>
                                    @for($i = 0; $i < $review->mark; $i++)
                                        <i style="color: #ff9d00;" class="fa fa-star" aria-hidden="true" title="{{$review->mark}}"></i>
                                    @endfor
                                </td>
                                @if($review->status == 0)
                                    <td width="10%"><span class="label label-danger">Не опубликовано</span></td>
                                @elseif($review->status == 1)
                                    <td width="10%"><span class="label label-success">Опубликовано</span></td>
                                @elseif($review->status == 2)
                                    <td width="10%"><span class="label label-warning">На модерации</span></td>
                                @endif
                                <td width="7%">@widget('listButtons', ['routeBase' => 'products-reviews.', 'id' => $review->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $reviews->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection
