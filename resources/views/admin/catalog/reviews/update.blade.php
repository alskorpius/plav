@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.products-reviews.update', 'id' => $review->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.catalog.reviews._form', ['review' => $review])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop
