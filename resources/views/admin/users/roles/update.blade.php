@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.roles.update', 'role' => $role->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.users.roles._form', ['role' => $role])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop
