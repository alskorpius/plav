@extends('admin.layouts.main')

@section('custom-buttons')
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
    <a href="{{ route('admin.roles.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    {{--@widget('filter', ['fields' => 'blacklist'])--}}
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox" data-table = 'users_roles'>
                            </th>
                            <th>Имя</th>
                            <th>Алиас</th>
                            <th></th>
                        </tr>
                        @foreach($roles AS $role)
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox" data-id = '{{$role->id}}'>
                                </td>
                                <td>{{ $role->name }}</td>
                                <td>{{ $role->alias }}</td>
                                <td>@widget('listButtons', ['routeBase' => 'roles.', 'id' => $role->id, 'view' => false])</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
