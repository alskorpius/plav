@extends('admin.layouts.main')
@section('content')
    {!! Form::open(['route' => 'admin.roles.store', 'files' => true]) !!}
    @include('admin.widgets.form.buttons', ['create' => true])
    @include('admin.users.roles._formNew')
    @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop
