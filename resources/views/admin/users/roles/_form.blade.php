<?php
/** @var \App\Models\UsersRole $role */
$object = isset($role) ? $role : null;
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.input', ['name' => 'name', 'label' => 'Название', 'object' => $object, 'options' => ['required', 'data-slug-origin']])
                @include('admin.widgets.form.input', ['name' => 'alias', 'label' => 'Алиас', 'object' => $object, 'options' => ['required', 'data-slug-destination']])
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-warning">
            <div class="box-body">
                <div class="box-header with-border">
                    <h3 class="box-title">Доступы к разделам</h3>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('status1', 'Контентные страницы') !!}
                        <div class="radio">
                            <label>{!! Form::radio('status1', 0, $statusPages[1] == false, ['class' => 'minimal']) !!} Нет прав</label>
                        </div>
                        <div class="radio">
                            <label>{!! Form::radio('status1', 1,  $statusPages[1] == true, ['class' => 'minimal']) !!} Просмотр и редактирование</label>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('status2', 'Меню') !!}
                        <div class="radio">
                            <label>{!! Form::radio('status2', 0, $statusPages[2] == false, ['class' => 'minimal']) !!} Нет прав</label>
                        </div>
                        <div class="radio">
                            <label>{!! Form::radio('status2', 1,  $statusPages[2] == true, ['class' => 'minimal']) !!} Просмотр и редактирование</label>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('status9', 'Список пользователей') !!}
                        <div class="radio">
                            <label>{!! Form::radio('status9', 0, $statusPages[9] == false, ['class' => 'minimal']) !!} Нет прав</label>
                        </div>
                        <div class="radio">
                            <label>{!! Form::radio('status9', 1,  $statusPages[9] == true, ['class' => 'minimal']) !!} Просмотр и редактирование</label>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('status10', 'Черный список') !!}
                        <div class="radio">
                            <label>{!! Form::radio('status10', 0, $statusPages[10] == false, ['class' => 'minimal']) !!} Нет прав</label>
                        </div>
                        <div class="radio">
                            <label>{!! Form::radio('status10', 1,  $statusPages[10] == true, ['class' => 'minimal']) !!} Просмотр и редактирование</label>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('status13', 'Настройки сайта') !!}
                        <div class="radio">
                            <label>{!! Form::radio('status13', 0, $statusPages[13] == false, ['class' => 'minimal']) !!} Нет прав</label>
                        </div>
                        <div class="radio">
                            <label>{!! Form::radio('status13', 1,  $statusPages[13] == true, ['class' => 'minimal']) !!} Просмотр и редактирование</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
