<?php

?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.input', ['name' => 'name', 'label' => 'Название', 'options' => ['required', 'data-slug-origin']])
                @include('admin.widgets.form.input', ['name' => 'alias', 'label' => 'Алиас', 'options' => ['required', 'data-slug-destination']])
            </div>
        </div>
    </div>
</div>
