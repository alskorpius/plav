@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.customers.store']) !!}
        @include('admin.widgets.form.buttons', ['create' => true])
        @include('admin.users.customers._form')
        @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop