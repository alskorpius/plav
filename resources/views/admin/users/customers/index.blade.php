<?php
/** @var \App\Models\User[] $customers */
?>
@extends('admin.layouts.main')

@section('custom-buttons')
	<a href="{{ route('admin.customers.index') }}"
	   class="btn btn-info btn-flat {{ Route::currentRouteName() === 'admin.customers.index' ? 'disabled-opacity' : null }}">Список</a>
	<a href="{{ route('admin.customers.deleted') }}"
	   class="btn btn-danger btn-flat {{ Route::currentRouteName() === 'admin.customers.deleted' ? 'disabled-opacity' : null }}">Удаленные</a>
	<div class="free-space"></div>
	<button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
	<a href="{{ route('admin.customers.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
	@widget('filter', ['fields' => 'users'])
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tr>
							<th>
								<input type="checkbox" class="icheckbox_minimal-aero mainCheckbox" data-table='users'>
							</th>
							<th>Имя</th>
							<th>Email</th>
							<th>Телефон</th>
							<th>Регистрация</th>
							<th>Последний вход</th>
							<th>Статус</th>
							<th></th>
						</tr>
						@foreach($customers AS $customer)
							<tr>
								<td>
									<input type="checkbox" class="icheckbox_minimal-aero subCheckbox"
										   data-id='{{$customer->id}}'>
								</td>
								<td>{{ $customer->full_name }}</td>
								<td>{{ Html::mailto($customer->email) }}</td>
								<td>
									@if($customer->phone)
									{{ Html::tag('a', $customer->phone, ['href' => $customer->clear_phone]) }}
									@else
									&mdash;
									@endif
								</td>
								<td>{{ $customer->created_at->format('d.m.Y') }}</td>
								<td>{{ $customer->last_login ? $customer->last_login->format('d.m.Y') : '&mdash;' }}</td>
								<td>@widget('status', ['status' => $customer->status, 'table' => 'users', 'id' => $customer->id])</td>
								<td>
									@if($customer->trashed())
										<a type="href"
										   href="{{ route('admin.customers.edit', ['customer' => $customer->id]) }}"
										   class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
									@endif
									@widget('listButtons', ['routeBase' => 'customers.', 'id' => $customer->id, 'view' => false, 'trashed' => $customer->trashed()])
								</td>
							</tr>
						@endforeach
					</table>
				</div>
			</div>
			<div class="text-center">{{ $customers->appends(Input::except('page'))->render() }}</div>
		</div>
	</div>
@endsection
