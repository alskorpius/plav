@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.customers.update', 'customer' => $customer->id], 'method' => 'PUT']) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.users.customers._form', ['customer' => $customer])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop