<?php
/** @var \App\Models\User $customer */
$object = isset($customer) ? $customer : null;
?>
<div class="row">
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.widgets.form.input', ['name' => 'second_name', 'label' => 'Фамилия', 'object' => $object])
                @include('admin.widgets.form.input', ['name' => 'first_name', 'label' => 'Имя', 'object' => $object])
                @include('admin.widgets.form.input', ['name' => 'middle_name', 'label' => 'Отчество', 'object' => $object])
                @include('admin.widgets.form.input', ['name' => 'phone', 'label' => 'Номер телефона', 'object' => $object])
                @include('admin.widgets.form.input', ['name' => 'address', 'label' => 'Адрес', 'object' => $object])
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="box box-danger">
            <div class="box-body">
                @include('admin.widgets.form.status', ['status' => old('status', isset($object) ? $object->status : 0)])
                @include('admin.widgets.form.radio', [
                    'name' => 'subscription',
                    'label' => 'Подписка на рассылку',
                    'value' => old('subscription', isset($object) ? $object->subscription : 0),
                    'data' => [0 => 'Не подписан', 1 => 'Подписан'],
                ])
                @include('admin.widgets.form.input', ['name' => 'email', 'label' => 'Email', 'object' => $object, 'options' => ['required']])
                @include('admin.widgets.form.password', ['name' => 'password', 'label' => 'Пароль (оставте поле пустым для сохранения старого пароля)', 'object' => null])
            </div>
        </div>
        <div class="box box-info">
            <div class="box-body">
                <div class="form-group">
                    <label>ID</label>
                    <p>{{($object) ? $object->id : '---'}}</p>
                </div>
                <div class="form-group">
                    <label>Дата создания аккаунта</label>
                    <p>{{($object) ? $object->created_at->format('d.m.Y') : '---'}}</p>
                </div>
                <div class="form-group">
                    <label>Последнее IP</label>
                    <p>{{($object) ? $object->last_ip : '---'}}</p>
                </div>
                <div class="form-group">
                    <label>Дата последней авторизации</label>
                    <p>{{($object) ? $object->last_login : '---'}}</p>
                </div>
                <div class="form-group">
                    <label>Количество авторизаций</label>
                    <p>{{($object) ? $object->count_auth : '---'}}</p>
                </div>
                @if(isset($object))
                    <div class="form-group">
                        <a class="btn btn-m btn-success"
                           href="{{route('fake-login', ['user' => $object->id])}}" target="_blank">Авторизоваться</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
