@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => ['admin.managers.update', 'manager' => $manager->id], 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.widgets.form.buttons')
        @include('admin.users.managers._form', ['manager' => $manager])
        @include('admin.widgets.form.buttons')
    {!! Form::close() !!}
@stop