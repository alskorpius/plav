<?php
/** @var \App\Models\User $manager */
$object = isset($manager) ? $manager : null;
?>
<div class="row">
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="box-body">
                @if(isset($object) && $object->imageExists('icon'))
                    @include('admin.widgets.form.image', [
                        'big' => $object->showImage('original', null, true),
                        'preview' => $object->showImage('icon', null, true),
                        'id' => $object->id,
                        'model' => $object->getMorphClass(),
                        'field' => 'avatar',
                    ])
                @else
                    @include('admin.widgets.form.image')
                @endif
                @include('admin.widgets.form.input', ['name' => 'first_name', 'label' => 'Имя', 'object' => $object, 'options' => ['required']])
                @include('admin.widgets.form.input', ['name' => 'second_name', 'label' => 'Фамилия', 'object' => $object])
                @include('admin.widgets.form.input', ['name' => 'phone', 'label' => 'Телефон', 'object' => $object, 'options' => ['required']])
                @include('admin.widgets.form.select', ['name' => 'role_alias', 'label' => 'Роль',  'data' => $roles, 'options' => ['required']])

            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="box box-danger">
            <div class="box-body">
                @include('admin.widgets.form.status', ['status' => old('status', isset($object) ? $object->status : 0)])
                @include('admin.widgets.form.input', ['name' => 'email', 'label' => 'Email', 'object' => $object, 'options' => ['required']])
                @include('admin.widgets.form.password', ['name' => 'password', 'label' => 'Пароль (оставте поле пустым для сохранения старого пароля)', 'object' => null])
                @if(isset($object))
                    <div class="form-group">
                        <a class="btn btn-m btn-success"
                           href="{{route('admin.fake-auth', ['id' => $object->id])}}">Авторизоваться</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
