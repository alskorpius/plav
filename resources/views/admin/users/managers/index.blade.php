<?php
/** @var \App\Models\User[] $managers */
?>
@extends('admin.layouts.main')

@section('custom-buttons')
    <a href="{{ route('admin.managers.index') }}"
       class="btn btn-info btn-flat {{ Route::currentRouteName() === 'admin.managers.index' ? 'disabled-opacity' : null }}">Список</a>
    <a href="{{ route('admin.managers.deleted') }}"
       class="btn btn-danger btn-flat {{ Route::currentRouteName() === 'admin.managers.deleted' ? 'disabled-opacity' : null }}">Удаленные</a>
    <div class="free-space"></div>
    <button class="btn bg-red btn-flat margin" id="deleteChecked">Удалить выбранное</button>
    <a href="{{ route('admin.managers.create') }}" class="btn bg-olive btn-flat margin">Добавить</a>
@endsection

@section('content')
    @widget('filter', ['fields' => 'users'])
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>
                                <input type="checkbox" class="icheckbox_minimal-aero mainCheckbox" data-table = 'users'>
                            </th>
                            <th>Имя</th>
                            <th>Email</th>
                            <th>Телефон</th>
                            <th>Регистрация</th>
                            <th>Последний вход</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        @foreach($managers AS $manager)
                            <tr>
                                <td>
                                    <input type="checkbox" class="icheckbox_minimal-aero subCheckbox" data-id = '{{$manager->id}}'>
                                </td>
                                <td>{{ $manager->full_name }}</td>
                                <td>{{ Html::mailto($manager->email) }}</td>
                                <td>
                                    @if($manager->phone)
                                    {{ Html::tag('a', $manager->phone, ['href' => $manager->clear_phone]) }}
                                    @else
                                    &mdash;
                                    @endif
                                </td>
                                <td>{{ $manager->created_at->format('d.m.Y') }}</td>
                                <td>{{ $manager->last_login ? $manager->last_login->format('d.m.Y') : '&mdash;' }}</td>
                                <td>@widget('status', ['status' => $manager->status, 'table' => 'users', 'id' => $manager->id])</td>
                                <td>
                                    @if($manager->trashed())
                                        <a type="href"
                                           href="{{ route('admin.managers.edit', ['manager' => $manager->id]) }}"
                                           class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
                                    @endif
                                    @widget('listButtons', ['routeBase' => 'managers.', 'id' => $manager->id, 'view' => false, 'trashed' => $manager->trashed()])
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="text-center">{{ $managers->appends(Input::except('page'))->render() }}</div>
        </div>
    </div>
@endsection