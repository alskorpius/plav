@extends('admin.layouts.main')

@section('content')
    {!! Form::open(['route' => 'admin.managers.store', 'files' => true]) !!}
        @include('admin.widgets.form.buttons', ['create' => true])
        @include('admin.users.managers._form')
        @include('admin.widgets.form.buttons', ['create' => true])
    {!! Form::close() !!}
@stop