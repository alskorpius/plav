<?php
return [
    'news' => [
        'name',
        ['name' => 'created_at', 'type' => 'date', 'label' => 'Дата'],
    ],
    'users' => [
        ['name' => 'full_name', 'label' => 'Имя'],
        ['name' => 'email','label' => 'Email'],
        ['name' => 'phone', 'label' => 'Телефон'],
        ['name' => 'created_at', 'type' => 'date', 'label' => 'Дата регистрации'],
    ],
    'cards' => [
        ['name' => 'id', 'label' => 'Номер карточки', 'width' => '100'],
        ['name' => 'full_name', 'label' => 'ФИО', 'width' => '150'],
    ],
    'eventsSubs' => [
        ['name' => 'name', 'label' => 'Имя'],
        ['name' => 'phone', 'label' => 'Телефон'],
        ['name' => 'email','label' => 'Email'],
        ['name' => 'event','label' => 'Событие'],
        ['name' => 'created_at', 'type' => 'date', 'label' => 'Дата регистрации'],
    ],
    'votings' => [
        ['name' => 'name', 'label' => 'Имя'],
        ['name' => 'created_at', 'type' => 'date', 'label' => 'Дата создания'],
        ['name' => 'date_from', 'type' => 'date', 'label' => 'Опубликовано с'],
        ['name' => 'date_to', 'type' => 'date', 'label' => 'Опубликовано по'],
    ],
    'subscribers' => [
        ['name' => 'email', 'label' => 'Email'],
        ['name' => 'created_at', 'type' => 'date', 'label' => 'Дата'],
    ],
    'blacklist' => [
        ['name' => 'email','label' => 'Email'],
        ['name' => 'ip', 'label' => 'IP'],
        ['name' => 'text', 'label' => 'Причина бана'],
    ],
    'visitors' => [
        ['name' => 'ip', 'label' => 'IP'],
        ['name' => 'first_enter', 'type' => 'date', 'label' => 'Дата первого входа'],
        ['name' => 'last_enter', 'type' => 'date', 'label' => 'Дата последнего входа'],
    ],
    'hits' => [
        ['name' => 'ip', 'label' => 'IP'],
        ['name' => 'str_url', 'label' => 'URL'],
        ['name' => 'first_enter', 'type' => 'date', 'label' => 'Дата посещения'],
        ['name' => 'last_enter', 'type' => 'date', 'label' => 'Дата последнего обновления страницы'],
        ['name' => 'response', 'type' => 'select', 'label' => 'Ответ сервера', 'data' => [
            200 => '200 OK',
            404 => '404 Not Found',
        ]],
    ],
    'refers' => [
        ['name' => 'ip', 'label' => 'IP'],
        ['name' => 'refer', 'label' => 'URL'],
        ['name' => 'created_at', 'type' => 'date', 'label' => 'Дата'],
    ],
    'comments' => [
        ['name' => 'text','label' => 'Комментарий'],
        ['name' => 'article', 'label' => 'Название статьи'],
        ['name' => 'created_at', 'type' => 'date', 'label' => 'Дата'],
        ['name' => 'status', 'type' => 'select', 'label' => 'Статус', 'data' => [
            \App\Models\Comment::TYPE_NO => 'Не опубликовано',
            \App\Models\Comment::TYPE_YES => 'Опубликовано',
            \App\Models\Comment::TYPE_IN_PROCESS => 'На модерации',
        ]],
    ],
    'products_reviews' => [
        ['name' => 'text','label' => 'Отзыв'],
        ['name' => 'product', 'label' => 'Товар'],
        ['name' => 'created_at', 'type' => 'date', 'label' => 'Дата'],
        ['name' => 'mark', 'label' => 'Оценка'],
        ['name' => 'status', 'type' => 'select', 'label' => 'Статус', 'data' => [
            0 => 'Не опубликовано',
            1 => 'Опубликовано',
            2 => 'На модерации',
        ]],
    ],
    'orders' => [
        ['name' => 'id','label' => 'ID', 'width' => '100'],
        ['name' => 'user_name','label' => 'Имя покупателя'],
        ['name' => 'phone', 'label' => 'Телефон'],
        ['name' => 'status', 'type' => 'select', 'label' => 'Статус', 'data' => [
            0 => 'Новый',
            1 => 'В обработке',
            2 => 'Оплачен',
            3 => 'Отправлен',
            4 => 'Оплачено и отправлено',
            5 => 'Завершен',
            6 => 'Не окончен',
        ]],
        ['name' => 'created_at', 'type' => 'date', 'label' => 'Дата от', 'width' => '150'],
        ['name' => 'created_at_po', 'type' => 'date', 'label' => 'Дата до', 'width' => '150'],
        ['name' => 'amount', 'label' => 'Сумма', 'width' => '100'],
    ],
    'staff' => [
        ['name' => 'name', 'label' => 'Имя'],
        ['name' => 'email','label' => 'Email'],
    ],
    'authors' => [
        ['name' => 'name', 'label' => 'Имя'],
    ],
    'reviews' => [
        ['name' => 'date_from', 'type' => 'date'],
        ['name' => 'date_to', 'type' => 'date'],
    ],
    'seo-redirects' => [
        ['name' => 'from', 'label' => 'Ссылка с'],
        ['name' => 'to', 'label' => 'Ссылка на'],
    ],
];
