<?php

/*
 * Настройки пакета laravel-statistics
 * https://github.com/klisl/laravel-statistics
 */

return [

    'name_route' => ['site.index', 'votings', 'content', 'events', 'event', 'articles', 'article', 'news', 'newsItem', 'contacts', 'whoRwe', 'experts', 'team', 'quality', 'paymentDelivery', 'products', 'product'], //названия маршрутов по которым будет собираться статистика. Например ['posts','contact']

    'days_default' => 30, //кол-во дней для вывода статистики по-умолчанию (сегодня/вчера)

    'password' => false, //пароль для входа на страницу статистики. Если false (без кавычек) - вход без пароля

    'authentication' => false, //если true, то статистика доступна только аутентифицированным пользователям

    'auth_route' => 'login', //название маршрута для страницы аутентификации (по-умолчанию 'login')

];