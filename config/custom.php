<?php

return [
    'image-max-size' => 15 * 1024,
    'price-max-size' => 15 * 1024,
    'library-max-size' => 15 * 1024,

    'redirects' => [
        300 => '300 - Multiple Choices («множество выборов»)',
        301 => '301 - Moved Permanently («перемещено навсегда»)',
        302 => '302 - Moved Temporarily («перемещено временно»)',
        303 => '303 - See Other (смотреть другое)',
        304 => '304 - Not Modified (не изменялось)',
        305 => '305 - Use Proxy («использовать прокси»)',
        307 => '307 - Temporary Redirect («временное перенаправление»)',
    ],
    'seo-scripts-places' => [
        'head' => 'Перед закрывающим </head>',
        'body' => 'После открывающего <body>',
        'footer' => 'В подвале сайта',
    ],
];