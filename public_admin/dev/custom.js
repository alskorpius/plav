// Noty settings
var message = function (message, type) {
  type = type || 'info';
  new Noty({
    text: message,
    timeout: 5000,
    type: type
  }).show();
};

// Noty confirmation window settings
var confirmation = function (text, okCallback, cancelCallback) {
  okCallback = okCallback || function () {
    };
  cancelCallback = cancelCallback || function () {
    };
  var n = new Noty({
    text: text,
    type: 'information',
    buttons: [
      Noty.button('OK', 'btn btn-success', function () {
        okCallback();
      }),
      Noty.button('Отмена', 'btn btn-warning', function () {
        cancelCallback();
        n.close();
      })
    ]
  }).show();
};

$(document).ready(function () {

  $('.dateTimePicker').daterangepicker({
    "autoApply": true,
    "singleDatePicker": true,
    "timePicker24Hour": true,
    "timePickerSeconds": true,
    "showDropdowns": true,
    "timePicker": true,
    "locale": {
      "format": "YYYY-MM-DD HH:mm:ss",
      "separator": " - ",
      "applyLabel": "Принять",
      "cancelLabel": "Отменить",
      "fromLabel": "С",
      "toLabel": "По",
      "customRangeLabel": "Custom",
      "daysOfWeek": [
        "Вс",
        "Пн",
        "Вт",
        "Ср",
        "Чт",
        "Пт",
        "Сб"
      ],
      "monthNames": [
        "Январь",
        "Февраль",
        "Март",
        "Апрель",
        "Май",
        "Июнь",
        "Июль",
        "Август",
        "Сентябрь",
        "Октябрь",
        "Ноябрь",
        "Декабрь"
      ],
      "firstDay": 1
    },
  }, function (start, end, label) {
  });

  var date = new Date();
  var formated_date = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);

  $('.dateTimePicker').filter('.lastOff').daterangepicker({
    "autoApply": true,
    "singleDatePicker": true,
    "timePicker24Hour": true,
    "minDate": formated_date,
    "timePickerSeconds": true,
    "showDropdowns": true,
    "timePicker": true,
    "locale": {
      "format": "YYYY-MM-DD HH:mm:ss",
      "separator": " - ",
      "applyLabel": "Принять",
      "cancelLabel": "Отменить",
      "fromLabel": "С",
      "toLabel": "По",
      "customRangeLabel": "Custom",
      "daysOfWeek": [
        "Вс",
        "Пн",
        "Вт",
        "Ср",
        "Чт",
        "Пт",
        "Сб"
      ],
      "monthNames": [
        "Январь",
        "Февраль",
        "Март",
        "Апрель",
        "Май",
        "Июнь",
        "Июль",
        "Август",
        "Сентябрь",
        "Октябрь",
        "Ноябрь",
        "Декабрь"
      ],
      "firstDay": 1
    },
  }, function (start, end, label) {
  });

  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1; //January is 0!
  var yyyy = today.getFullYear();

  if (dd < 10) {
    dd = '0' + dd
  }

  if (mm < 10) {
    mm = '0' + mm
  }

  today = yyyy + '-' + mm + '-' + dd;
  var lastMonth = new Date();
  var lastMm = parseInt(mm) - 1;
  var lastYear = yyyy;
  if (lastMm < 10) {
    lastMm = '0' + lastMm;
  }
  if (lastMm == 0) {
    lastMm = '12';
    var lastYear = parseInt(yyyy) - 1;
  }
  lastMonth = lastYear + '-' + lastMm + '-' + dd;

  var url_string = window.location.href;
  var url = new URL(url_string);
  var datarange1 = url.searchParams.get("dateRange_reviews");
  var datarange2 = url.searchParams.get("dateRange");
  if (datarange1 !== null || datarange2 !== null) {
    var dateString = '';
    if (datarange1 !== null) {
      dateString = datarange1;
    }
    if (datarange2 !== null) {
      dateString = datarange2;
    }
    var datesArr = dateString.split(' - ');
    lastMonth = datesArr[0];
    today = datesArr[1];
  }
  $('.dateRangePicker').daterangepicker({
    "autoApply": true,
    "autoUpdateInput": true,
    "linkedCalendars": false,
    "showDropdowns": true,
    "startDate": lastMonth,
    "endDate": today,
    "locale": {
      "format": "YYYY-MM-DD",
      "separator": " - ",
      "applyLabel": "Принять",
      "cancelLabel": "Отменить",
      "fromLabel": "С",
      "toLabel": "По",
      "customRangeLabel": "Custom",
      "daysOfWeek": [
        "Вс",
        "Пн",
        "Вт",
        "Ср",
        "Чт",
        "Пт",
        "Сб"
      ],
      "monthNames": [
        "Январь",
        "Февраль",
        "Март",
        "Апрель",
        "Май",
        "Июнь",
        "Июль",
        "Август",
        "Сентябрь",
        "Октябрь",
        "Ноябрь",
        "Декабрь"
      ],
      "firstDay": 1,
    },
  }, function (start, end, label) {
  });
  var multi_select = function () {
    $('.multiSelectBlock select').each(function () {
      if ($(this).prop('multiple')) {
        $(this).multiSelect({
          keepOrder: true,
          selectableHeader: "<div class='multiSelectNavigation custom-header select-all'>Выбрать все</div>",
          selectionHeader: "<div class='multiSelectNavigation custom-header deselect-all'>Отменить все</div>"
        });
      }
    });
    $('.select-all').click(function () {
      $(this).closest('.multiSelectBlock').find('select').multiSelect('select_all');
      return false;
    });
    $('.deselect-all').click(function () {
      $(this).closest('.multiSelectBlock').find('select').multiSelect('deselect_all');
      return false;
    });
  };


  var $body = $('body');

  // $body.on('click', function () {
  //     $logo = $('.main-logo');
  //     console.log(11111);
  //     if (window.location.pathname === '/'){
  //         console.log(window.location.pathname);
  //         $logo.attr('href', '#');
  //     } else {
  //         console.log(window.location.pathname);
  //
  //         $logo.attr('href', '/');
  //     }
  // });

  var answersForVoting = $('.deleteAnswerAction');
  var answerInput = $('.answerAddInput');
  var answerBtn = $('.answerAddButton');
  if (answersForVoting.length >= 5) {
    answerInput.attr("disabled", "disabled");
    answerBtn.attr("disabled", "disabled");
  } else {
    answerInput.prop("disabled", false);
    answerBtn.prop("disabled", false);
  }

  if ($('.multiSelectBlock').length) {
    multi_select();
  }

  // CSRF token settings for ajax requests
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  // URLs list for ajax
  var urls = {
    'status': '/admin/ajax/change-status'
  };

  // Reverse status for element
  $body.on('click', '.change-status', function (e) {
    e.preventDefault();
    var $it = $(this), id = $it.data('id'), table = $it.data('table');
    $.ajax({
      url: urls.status,
      type: 'PUT',
      dataType: 'JSON',
      data: {
        id: id,
        table: table
      },
      success: function (data) {
        if (data.success) {
          if ($it.hasClass('label-success')) {
            $it.removeClass('label-success');
            $it.addClass('label-danger');
            $it.html('<i class="fa fa-close"></i>');
          } else {
            $it.removeClass('label-danger');
            $it.addClass('label-success');
            $it.html('<i class="fa fa-check"></i>');
          }
        } else {
          if (data.message) {
            message(data.message, 'error');
          }
        }
      },
      error: function () {
        message('Ошибка сервера!', 'error');
      }
    });
  });

  // Button as link
  $body.on('click', 'button[type="href"]', function () {
    var $this = $(this);

    if (!$this.hasClass('confirmation-window')) {
      window.location.href = $this.attr('href');
    }
  });

  // Link as submit button
  $body.on('click', 'a[type="submit"]', function () {
    var $it = $(this);
    if (!$it.hasClass('confirmation-window')) {
      var $form = $it.closest('form');
      if ($form.hasClass('filter-form')) {
        var arr = [];
        var key;
        var value;
        $form.find('input,select').each(function () {
          if (!$(this).attr('type') || $(this).attr('type') !== 'submit') {
            key = $(this).attr('name');
            value = $(this).val();
            if (value !== "") {
              arr.push(key + '=' + value);
            }
          }
        });
        var link = $form.attr('action');
        if (arr.length) {
          link += '?' + arr.join('&');
        }
        window.location.href = link;
      } else {
        $form.submit();
      }
    }
  });

  // Confirmation window
  $body.on('click', '.confirmation-window', function (e) {
    e.preventDefault();
    var $it = $(this), text = $it.data('message') || 'Пожалуйста, подтвердите действие';
    confirmation(text, function () {
      if ($it.is('a') && $it.attr('type') === 'submit' && $it.closest('form').length) {
        $it.closest('form').submit();
      } else if ($it.is('a') || ($it.is('button') && $it.attr('type') === 'href' && $it.attr('href'))) {
        window.location.href = $it.attr('href');
      }
    });
  });

  // Close message button
  $body.on('click', '.messagesBlock button.close', function () {
    $(this).closest('.callout').slideUp(350);
  });

  // Colorpicker initialization
  var $colorPicker = $(".colorPicker");
  if ($colorPicker.length) {
    $colorPicker.colorpicker();
  }

  // Date picker
  if ($('.datePicker').length) {
    $.each($('.datePicker'), function () {
      $(this).datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        language: 'ru'
      });
    });
  }

  // Initialize Select2 Elements
  if ($('.select2').length) {
    $(".select2").select2({
          width: '100%'
    });
  }

  $('.color-picker').colorpicker({
    format: 'hex',
  });

  // Save sidebar status open or collapse
  $('.sidebar-toggle').on('click', function () {
    if (wCookie.get('sidebar') === 'collapse') {
      wCookie.delete('sidebar');
    } else {
      wCookie.set('sidebar', 'collapse', {
        expires: 365 * 24 * 60 * 60
      });
    }
  });

  //iCheck for checkbox and radio inputs
  if ($('input[type="checkbox"].minimal, input[type="radio"].minimal').length) {
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
  }

  //Red color scheme for iCheck

  if ($('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').length) {
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
  }

  //Flat red color scheme for iCheck
  if ($('input[type="checkbox"].flat-red, input[type="radio"].flat-red').length) {
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
  }

  // Input mask
  if ($("[data-mask]").length) {
    $("[data-mask]").inputmask();
  }

  // TinyMCE editor
  if ($('.tinymceEditor').length) {
    tinymce.init({
      selector: "textarea.tinymceEditor",
      skin: "admin",
      language: 'ru',
      plugins: [
        "advlist autolink lists link image charmap print preview hr",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons paste textcolor colorpicker textpattern responsivefilemanager"
      ],
      table_class_list: [
        {title: 'По умолчанию', value: ''},
        {title: 'Без границ', value: 'table-null'},
        {title: 'Зебра', value: 'table-zebra'}
      ],
      image_class_list: [
        {title: 'Поумолчанию', value: ''}
      ],
      toolbar1: "undo redo pastetext | bold italic forecolor backcolor fontselect fontsizeselect styleselect | alignleft aligncenter alignright alignjustify",
      toolbar2: 'bullist numlist outdent indent | link unlink image responsivefilemanager fullscreen',
      image_advtab: true,
      external_filemanager_path: "/public_admin/plugins/tinymce/filemanager/",
      filemanager_title: "Менеджер файлов",
      external_plugins: {"filemanager": "filemanager/plugin.min.js"},
      document_base_url: "http://" + window.location.hostname + "/",
      convert_urls: false,
      plugin_preview_width: "1000",
      relative_urls: false,
      default_language: 'ru',
      fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt"
    });

    var wLastSmall, wLastBig;
    if ($(window).width() > 700) {
      wLastSmall = false;
      wLastBig = true;
    } else {
      wLastSmall = true;
      wLastBig = false;
    }

    $(window).on('resize', function () {
      if ($(window).width() > 700 && wLastSmall) {
        wLastSmall = false;
        wLastBig = true;
        parent.tinyMCE.activeEditor.windowManager.close(window);
      }
      if ($(window).width() < 700 && wLastBig) {
        wLastSmall = true;
        wLastBig = false;
        parent.tinyMCE.activeEditor.windowManager.close(window);
      }
    })
  }


  // MY NESTABLE SCRIPT START
  if ($('#myNest').length) {
    var depth = parseInt($('#myNest').data('depth'));
    if (!depth) {
      depth = 5;
    }
    var updateOutput = function (e) {
      var list = e.length ? e : $(e.target),
        output = list.data('output');
      if ($(e.target).length) {
        if (window.JSON) {
          output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
          output.val('JSON browser support required for this demo.');
        }
      }
    };

    //Кнопки для сворачивания/разворачивания всех списков
    $("#nestable_list_menu").on("click", function (e) {
      var target = $(e.target),
        action = target.data('action');
      if (action === 'expand-all') {
        $('.dd').nestable('expandAll');
      }
      if (action === 'collapse-all') {
        $('.dd').nestable('collapseAll');
      }
      return false;
    });
    //Кнопки для сворачивания/разворачивания всех списков
    $("[data-action=expand-all]").on("click", function (e) {
      $('.dd').nestable('expandAll');
      return false;
    });
    $("[data-action=collapse-all]").on("click", function (e) {
      $('.dd').nestable('collapseAll');
      return false;
    });

    var myUpdateOutput = function (e) {
      var list = e.length ? e : $(e.target),
        output = list.data('output');
      if ($(e.target).length) {
        if (window.JSON) {
          output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
          output.val('JSON browser support required for this demo.');
        }
      }
    };
    var mySortable = function (e) {
      if (e.target.outerHTML == '<input type="checkbox">') {
        return;
      }
      myUpdateOutput(e);
      var json = $("#myNestJson").val();
      var table = $('#parameters').data('table');
      $.ajax({
        url: '/admin/ajax/sortable',
        type: 'PUT',
        dataType: 'JSON',
        data: {
          json: json,
          table: table
        },
        success: function (data) {
          // console.log(data);
        }
      });
    };

    $("#myNest").not('.pageList-del').nestable({
      dragClass: 'pageList dd-dragel',
      itemClass: 'dd-item',
      group: 1,
      maxDepth: depth
    }).on("change", mySortable);
    myUpdateOutput($("#myNest").data("output", $("#myNestJson")));
  }
  // MY NESTABLE SCRIPT END

  var $slugOriginal = $('input[data-slug-origin]'),
    $slugDestination = $('input[data-slug-destination]'),
    $slugButton = $('button[data-slug-button]');
  if ($slugOriginal.length && $slugDestination.length) {
    var timeout = null;

    function makeSlug($origin) {
      if (timeout !== null) {
        clearTimeout(timeout);
      }
      timeout = setTimeout(function () {
        $.ajax({
          url: '/admin/ajax/make-slug',
          type: 'GET',
          dataType: 'JSON',
          data: {
            name: $origin.val()
          },
          success: function (data) {
            if (data.slug) {
              $slugDestination.val(data.slug);
            }
          }
        });
      }, 250);
    }

    $slugButton.on('click', function () {
      console.log(111);
      makeSlug($slugOriginal);
    });
  }


  //////////////// DAMN DROPZONE
  $(function () {
    var dropzone = $('.dropZone');
    if (dropzone.length) {
      var upl = dropzone.data('upload');
      var sort = dropzone.data('sortable');
      var def = dropzone.data('default');

      var getUploadedPhotos = function () {
        $.ajax({
          type: 'POST',
          url: '/' + upl,
          dataType: 'JSON',
          success: function (data) {
            $('.dropDownload').html(data.images);
            if (parseInt(data.count)) {
              $('.loadedBox .checkAll').fadeIn(300);
            }
          }
        });
      };
      //getUploadedPhotos();

      $('.dropDownload').sortable({
        connectWith: ".loadedBlock",
        handle: ".loadedDrag",
        cancel: '.loadedControl',
        placeholder: "loadedBlockPlaceholder",
        update: function () {
          var order = [];
          $(this).find('.loadedBlock').each(function () {
            order.push($(this).data('image'));
          });
          $.ajax({
            type: "POST",
            url: "/" + sort,
            data: {
              order: order
            }
          });
        }
      });

      $('.dropDownload').on('click', '.loadedCover .btn.btn-success', function () {
        var it = $(this),
          itP = it.closest('.loadedBlock'),
          id = itP.data('image');
        $.ajax({
          url: '/' + def,
          type: 'POST',
          data: {
            id: id
          }
        });
      });

      $('.dropModule').on('click', '.checkAll', function (event) {
        event.preventDefault();
        var block = $(this).closest('.loadedBox').find('.dropDownload').find('.loadedBlock');
        block.addClass('chk');
        block.find('.loadedCheck').find('input').prop('checked', true);
        $('.loadedBox .uncheckAll, .loadedBox .removeCheck').fadeIn(300);
      });

      $('.dropModule').on('click', '.uncheckAll', function (event) {
        event.preventDefault();
        var block = $(this).closest('.loadedBox').find('.dropDownload').find('.loadedBlock');
        block.removeClass('chk');
        block.find('.loadedCheck').find('input').prop('checked', false);
        $('.loadedBox .uncheckAll, .loadedBox .removeCheck').fadeOut(300);
      });

      $('.dropDownload').on('click', '.loadedCheck label', function (event) {
        if ($(this).children('input').is(':checked')) {
          $(this).closest('.loadedBlock').addClass('chk');
        } else {
          $(this).closest('.loadedBlock').removeClass('chk');
        }
        if ($('.dropDownload .chk').length > 0) {
          $('.loadedBox .uncheckAll, .loadedBox .removeCheck').fadeIn(300);
        } else {
          $('.loadedBox .uncheckAll, .loadedBox .removeCheck').fadeOut(300);
        }
      });
    }
  });
  //////////////// THE END OF DROPZONE

  //Group delete

  //Checked or unchecked all
  var mainCheckbox = $('.mainCheckbox');
  mainCheckbox.on('click', function () {
    var subCheckboxes = $('.subCheckbox');
    if ($(this).is(':checked')) {
      subCheckboxes.prop('checked', true);
    } else {
      subCheckboxes.prop('checked', false);
    }
  });

  // Remove checked
  $body.on('click', '#deleteChecked', function () {
    var subCheckbox = $('.subCheckbox:checked');
    var table = mainCheckbox.data('table');
    var id = subCheckbox.map(function () {
      return this.dataset.id;
    }).toArray();

    var url = '/admin/ajax/group-delete';
    console.log(table);
    console.log(id);
    if (id.length > 0) {
      $.ajax({
        url: url,
        method: 'post',
        dataType: 'JSON',
        data: {table: table, id: id},
        beforeSend: function () {
        }
      }).done(function (data) {
        // subCheckbox.closest('tr').fadeOut();
        message('Удаление прошло успешно', 'success');
        setTimeout(function () {
          window.location.reload();
        }, 1000);
      }).fail(function () {
        message('Ошибка сервера. Пожалуйста, обновите страницу и попробуйте еще раз', 'error');
      }).always(function () {

      });
    } else {
      message('Выберете хотя бы один элемент', 'error');
    }

  });

  // Status = 0 for checked
  $body.on('click', '#statusChecked', function () {
    var subCheckbox = $('.subCheckbox:checked');
    var table = mainCheckbox.data('table');
    var id = subCheckbox.map(function () {
      return this.dataset.id;
    }).toArray();

    var url = '/admin/ajax/group-status';
    console.log(table);
    console.log(id);
    if (id.length > 0) {
      $.ajax({
        url: url,
        method: 'post',
        dataType: 'JSON',
        data: {table: table, id: id},
        beforeSend: function () {
        }
      }).done(function (data) {
        // subCheckbox.closest('tr').fadeOut();
        message('Статус сменен', 'success');
        setTimeout(function () {
          window.location.reload();
        }, 1000);
      }).fail(function () {
        message('Ошибка сервера. Пожалуйста, обновите страницу и попробуйте еще раз', 'error');
      }).always(function () {

      });
    } else {
      message('Выберете хотя бы один элемент', 'error');
    }

  });

  // Status = 1 for checked
  $body.on('click', '#statusCheckedEnable', function () {
    var subCheckbox = $('.subCheckbox:checked');
    var table = mainCheckbox.data('table');
    var id = subCheckbox.map(function () {
      return this.dataset.id;
    }).toArray();

    var url = '/admin/ajax/group-status-enable';
    console.log(table);
    console.log(id);
    if (id.length > 0) {
      $.ajax({
        url: url,
        method: 'post',
        dataType: 'JSON',
        data: {table: table, id: id},
        beforeSend: function () {
        }
      }).done(function (data) {
        // subCheckbox.closest('tr').fadeOut();
        message('Статус сменен', 'success');
        setTimeout(function () {
          window.location.reload();
        }, 1000);
      }).fail(function () {
        message('Ошибка сервера. Пожалуйста, обновите страницу и попробуйте еще раз', 'error');
      }).always(function () {

      });
    } else {
      message('Выберете хотя бы один элемент', 'error');
    }

  });

  // Status = 2 for checked
  $body.on('click', '#statusChecked-2', function () {
    var subCheckbox = $('.subCheckbox:checked');
    var table = mainCheckbox.data('table');
    var id = subCheckbox.map(function () {
      return this.dataset.id;
    }).toArray();

    var url = '/admin/ajax/group-status-2';
    console.log(table);
    console.log(id);
    if (id.length > 0) {
      $.ajax({
        url: url,
        method: 'post',
        dataType: 'JSON',
        data: {table: table, id: id},
        beforeSend: function () {
        }
      }).done(function (data) {
        // subCheckbox.closest('tr').fadeOut();
        message('Статус сменен', 'success');
        setTimeout(function () {
          window.location.reload();
        }, 1000);
      }).fail(function () {
        message('Ошибка сервера. Пожалуйста, обновите страницу и попробуйте еще раз', 'error');
      }).always(function () {

      });
    } else {
      message('Выберете хотя бы один элемент', 'error');
    }

  });

  // Status = 3 for checked
  $body.on('click', '#statusChecked-3', function () {
    var subCheckbox = $('.subCheckbox:checked');
    var table = mainCheckbox.data('table');
    var id = subCheckbox.map(function () {
      return this.dataset.id;
    }).toArray();

    var url = '/admin/ajax/group-status-3';
    console.log(table);
    console.log(id);
    if (id.length > 0) {
      $.ajax({
        url: url,
        method: 'post',
        dataType: 'JSON',
        data: {table: table, id: id},
        beforeSend: function () {
        }
      }).done(function (data) {
        // subCheckbox.closest('tr').fadeOut();
        message('Статус сменен', 'success');
        setTimeout(function () {
          window.location.reload();
        }, 1000);
      }).fail(function () {
        message('Ошибка сервера. Пожалуйста, обновите страницу и попробуйте еще раз', 'error');
      }).always(function () {

      });
    } else {
      message('Выберете хотя бы один элемент', 'error');
    }

  });

  // Status = 4 for checked
  $body.on('click', '#statusChecked-4', function () {
    var subCheckbox = $('.subCheckbox:checked');
    var table = mainCheckbox.data('table');
    var id = subCheckbox.map(function () {
      return this.dataset.id;
    }).toArray();

    var url = '/admin/ajax/group-status-4';
    console.log(table);
    console.log(id);
    if (id.length > 0) {
      $.ajax({
        url: url,
        method: 'post',
        dataType: 'JSON',
        data: {table: table, id: id},
        beforeSend: function () {
        }
      }).done(function (data) {
        // subCheckbox.closest('tr').fadeOut();
        message('Статус сменен', 'success');
        setTimeout(function () {
          window.location.reload();
        }, 1000);
      }).fail(function () {
        message('Ошибка сервера. Пожалуйста, обновите страницу и попробуйте еще раз', 'error');
      }).always(function () {

      });
    } else {
      message('Выберете хотя бы один элемент', 'error');
    }

  });

  // Status = 5 for checked
  $body.on('click', '#statusChecked-5', function () {
    var subCheckbox = $('.subCheckbox:checked');
    var table = mainCheckbox.data('table');
    var id = subCheckbox.map(function () {
      return this.dataset.id;
    }).toArray();

    var url = '/admin/ajax/group-status-5';
    console.log(table);
    console.log(id);
    if (id.length > 0) {
      $.ajax({
        url: url,
        method: 'post',
        dataType: 'JSON',
        data: {table: table, id: id},
        beforeSend: function () {
        }
      }).done(function (data) {
        // subCheckbox.closest('tr').fadeOut();
        message('Статус сменен', 'success');
        setTimeout(function () {
          window.location.reload();
        }, 1000);
      }).fail(function () {
        message('Ошибка сервера. Пожалуйста, обновите страницу и попробуйте еще раз', 'error');
      }).always(function () {

      });
    } else {
      message('Выберете хотя бы один элемент', 'error');
    }

  });


  /// End of group delete

  var select = $("#blockSelect").val();

  if (parseInt(select) === 1) {
    $('.blockEmail').show();
    $('.blockIP').hide();
  } else {
    $('.blockEmail').hide();
    $('.blockIP').show();
  }

  $("#blockSelect").on('change', function () {
    var val = $(this).val();

    if (val == 1) {
      $('.blockEmail').show();
      $('.blockIP').hide();
    } else {
      $('.blockEmail').hide();
      $('.blockIP').show();
    }
  });

  $body.on('change', 'input[name="is_accessory"]', function () {
    var val = $(this).val();
    if (val == 1) {
      $('.hide-for-accessories').addClass('active');
    } else {
      $('.hide-for-accessories').removeClass('active');
    }
  });

  $body.on('change', 'input[name="is_registration_address"]', function () {
    var val = $(this).val();
    if (val == 1) {
      $('.hide-for-fact-address').addClass('active');
    } else {
      $('.hide-for-fact-address').removeClass('active');
    }
  });

// deleting answers
  $body.on('click', ".deleteAnswerAction", function () {
    console.log('click');
    var data = $(this).data();
    var json = new FormData();

    $.each(data, function (key, value) {
      json.append(key, value);
    });

    var url = '/admin/ajax/delete-answer';

    console.log(json);
    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: json,
      contentType: false,
      processData: false,
      beforeSend: function () {
      }
    }).done(function (data) {
      $(".answerList").empty().append(data.html);
      if (data.count < 5) {
        $('.answerAddInput').prop("disabled", false);
        $('.answerAddButton').prop("disabled", false);
      }
      message('Ответ успешно удален', 'success');
    }).fail(function () {
      message('Ошибка сервера. Пожалуйста, обновите страницу и попробуйте еще раз', 'error');
    }).always(function () {

    });
  });

  // adding answers
  $body.on('click', ".answerAddButton", function () {
    var data = {};
    $('.answerAddInput').each(function () {
      data[$(this).data('lang')] = $(this).val();
    });

    var question = $(".answerAdd").data('question');
    var url = '/admin/ajax/add-answer';

    console.log(JSON.stringify(data));
    console.log(question);
    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {answer: JSON.stringify(data), question: question},
      beforeSend: function () {
      }
    }).done(function (data) {
      if (data.fail) {
        message('Поле обязательно для заполнения', 'error');
      } else {
        $(".answerList").empty().append(data.html);
        message('Ответ успешно добавлен', 'success');
        $('.answerAddInput').val('');
        if (data.count >= 5) {
          $('.answerAddInput').attr("disabled", "disabled");
          $('.answerAddButton').attr("disabled", "disabled");
        }
      }
    }).fail(function () {
      message('Ошибка сервера. Пожалуйста, обновите страницу и попробуйте еще раз', 'error');
    }).always(function () {

    });
  });

  //Select type of article on comments control
  $body.on('change', '.typeArticle', function () {
    var type = $(".typeArticle").val();
    var url = '/admin/ajax/select-article';

    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {type: type},
      beforeSend: function () {
      }
    }).done(function (data) {
      $('.articlesSelected').replaceWith(data.html)
    }).fail(function () {
      message('Ошибка сервера. Пожалуйста, обновите страницу и попробуйте еще раз', 'error');
    }).always(function () {

    });
  });

  //Select regions by country
  $body.on('change', '.countryProvide', function () {
    var country_id = $(".countryProvide").val();
    var url = '/admin/ajax/select-regions';
    if ( country_id !== 0) {
      $.ajax({
        url: url,
        method: 'post',
        dataType: 'JSON',
        data: {country_id: country_id},
        beforeSend: function () {
        }
      }).done(function (data) {
        $('.regionsSelected').empty().append(data.html);
        $('.regionProvide').val(0);
        $('.select2').select2();
      }).fail(function () {
        message('Ошибка сервера. Добавьте данные для страны', 'error');
      }).always(function () {

      });
    }
  });


  //Autoentering slug
  $body.on('click', '.translitButton', function () {

    var url = '/admin/ajax/translit';
    var id = $(this).data('id');
    //List
    var resultNew = $(".translitResultNew");
    var result = $(".translitResult[data-id='" + id + "']");
    var source = $(".translitSource[data-id='" + id + "']").val();
    //New
    var sourceNew = $(".translitSourceNew").val();

    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {source: source, sourceNew: sourceNew},
      beforeSend: function () {
      }
    }).done(function (data) {
      result.val(data.translit);
      resultNew.val(data.translitNew);
    }).fail(function () {
      message('Ошибка сервера. Пожалуйста, обновите страницу и попробуйте еще раз', 'error');
    }).always(function () {

    });
  });

  //Add new value
  $body.on('click', '#newValue', function () {

    var url = '/admin/ajax/add-value';
    var id = $(this).data('table');
    var newNames = {};
    $('.newName').each(function () {
      newNames[$(this).data('lang')] = $(this).val();
    });
    var newSlug = $('.translitResultNew').val();
    var option = $('.value-color-new').val();

    if (newNames == [] || newSlug == '') {
      message('Пожалуйста, заполните все данные', 'error');
    } else {
      $.ajax({
        url: url,
        method: 'post',
        dataType: 'JSON',
        data: {id: id, newNames: JSON.stringify(newNames), newSlug: newSlug, option: option},
        beforeSend: function () {
        }
      }).done(function (data) {
        message('Данные успешно внесены', 'success');
        $("#values-list").empty().append(data.html);
        $('.newName').val('');
        $('.translitResultNew').val('');
        $('.value-color-new').val('');
        $('.color-picker').colorpicker();
      }).fail(function (data) {
        message('Ошибка ввода данных', 'error');
      }).always(function () {

      });
    }
  });

  //Remove value
  $body.on('click', '.del-value', function () {

    var url = '/admin/ajax/remove-value';
    var specification = $(this).data('table');
    var id = $(this).data('id');

    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {id: id, specification: specification},
      beforeSend: function () {
      }
    }).done(function (data) {
      message('Данные успешно удалены', 'success');
      $("#values-list").empty().append(data.html);
      $('.color-picker').colorpicker();
    }).fail(function () {
      message('Ошибка сервера', 'error');
    }).always(function () {

    });

  });

  //Edit value
  $body.on('click', '.edit-value', function () {

    var url = '/admin/ajax/edit-value';
    var specification = $(this).data('table');
    var id = $(this).data('id');
    //Values
    var names = {};
    $(".tableName[data-id='" + id + "']").each(function () {
      names[$(this).data('lang')] = $(this).val();
    });
    console.log(names);

    var slug = $(".translitResult[data-id='" + id + "']").val();
    var option = $(".value-color-edit[data-id='" + id + "']").val();

    var formData = new FormData();
    formData.append("id", id);
    formData.append("specification", specification);
    formData.append("newName", JSON.stringify(names));
    formData.append("newSlug", slug);
    formData.append("option", option);
    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: formData,
      processData: false,
      contentType: false,
      beforeSend: function () {
      }
    }).done(function (data) {
      message('Данные успешно обновлены', 'success');
      $("#values-list").empty().append(data.html);
      $('.color-picker').colorpicker();
    }).fail(function () {
      message('Ошибка ввода данных', 'error');
    }).always(function () {

    });

  });

  //Add new price
  $body.on('click', '#newPrice', function () {

    var url = '/admin/ajax/add-price';
    var id = $(this).data('table');
    var newVolume = $("#newVolume").val();
    var newYear = $("#newYear").val();
    var newPrice = $("#newPriceI").val();
    var newWeight = $("#newWeight").val();
    var newSale = $("#newSale").val();
    var newArtikul = $("#newArtikul").val();

    if (newVolume === '' || newYear === '' || newPrice === '' || newArtikul === '' || newWeight === '') {
       message('Пожалуйста, заполните все данные', 'error');
    } else {
      $.ajax({
        url: url,
        method: 'post',
        dataType: 'JSON',
        data: {
          product_id: id,
          volume: newVolume,
          year: newYear,
          price: newPrice,
          weight: newWeight.replace(/,/g, '.'),
          sale: newSale,
          artikul: newArtikul,
        },
        beforeSend: function () {
        }
      }).done(function (data) {
        message('Данные успешно внесены', 'success');
        $("#price-list").html(data.html);
        $('#newVolume').val('');
        $('#newYear').val('');
        $('#newPriceI').val('');
        $('#newWeight').val('');
        $('#newArtikul').val('');
        $('#newSale').val('');
      }).fail(function (data) {
        message('Ошибка ввода данных', 'error');
      }).always(function () {

      });
    }
  });

  //Remove price
  $body.on('click', '.del-price', function () {

    var url = '/admin/ajax/remove-price';
    var product_id = $(this).data('table');
    var id = $(this).data('id');

    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {id: id, product_id: product_id},
      beforeSend: function () {
      }
    }).done(function (data) {
      message('Данные успешно удалены', 'success');
      $("#price-list").html(data.html);
    }).fail(function () {
      message('Ошибка сервера', 'error');
    }).always(function () {

    });

  });
  //Edit price
  $("body").on('click', '.edit-price', function () {

    var url = '/admin/ajax/edit-price';
    var product_id = $(this).data('table');
    var id = $(this).data('id');
    //Values
    var volume = $(".editVolume[data-id='" + id + "']").val();
    var year = $(".editYear[data-id='" + id + "']").val();
    var price = $(".editPrice[data-id='" + id + "']").val();
    var weight = $(".editWeight[data-id='" + id + "']").val();
    var sale = $(".editSale[data-id='" + id + "']").val();
    var artikul = $(".editArtikul[data-id='" + id + "']").val();
    var isChecked = $(".editCheck[data-id='" + id + "']").is(':checked');
    var is_checked = isChecked ? 1 : 0;
    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {
        id: id,
        volume: volume,
        year: year,
        price: price,
        weight: weight.replace(/,/g, '.'),
        artikul: artikul,
        product_id: product_id,
        is_checked: is_checked,
        sale: sale,
      },
      beforeSend: function () {
      }
    }).done(function (data) {
      message('Данные успешно обновлены', 'success');
      $("#price-list").html(data.html);
    }).fail(function () {
      message('Ошибка ввода данных', 'error');
    }).always(function () {

    });

  });

  //Add new price
  $body.on('click', '#addReward', function () {

    var url = '/admin/ajax/add-reward';
    var id = $(this).data('table');
    var reward = $("#rewardName").val();
    var point = $("#points").val();

    if (reward === '' || reward === '' ) {
      message('Пожалуйста, заполните все данные', 'error');
    } else {
      $.ajax({
        url: url,
        method: 'post',
        dataType: 'JSON',
        data: {
          product_id: id,
          reward_id: reward,
          points: point,
        },
        beforeSend: function () {
        }
      }).done(function (data) {
        message('Данные успешно внесены', 'success');
        $("#reward-list").html(data.html);
        $('#rewardName').val(1);
        $('#points').val('');
      }).fail(function (data) {
        message('Ошибка ввода данных', 'error');
      }).always(function () {

      });
    }
  });

  //Remove price
  $body.on('click', '.del-reward', function () {

    var url = '/admin/ajax/remove-reward';
    var product_id = $(this).data('table');
    var id = $(this).data('id');
    var reward = $("#rewardName").val();
    var point = $("#points").val();

    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {
        id: id,
        product_id: product_id,
        reward_id: reward,
        points: point,
      },
      beforeSend: function () {
      }
    }).done(function (data) {
      message('Данные успешно удалены', 'success');
      $("#reward-list").html(data.html);
    }).fail(function () {
      message('Ошибка сервера', 'error');
    }).always(function () {

    });

  });
  //Edit price
  $("body").on('click', '.edit-reward', function () {

    var url = '/admin/ajax/edit-reward';
    var product_id = $(this).data('table');
    var id = $(this).data('id');
    var reward = $(".edit-reward-name[data-id='" + id + "']").val();
    var point = $(".editPoints[data-id='" + id + "']").val();

    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {
        id: id,
        product_id: product_id,
        reward_id: reward,
        points: point,
      },
      beforeSend: function () {
      }
    }).done(function (data) {
      message('Данные успешно обновлены', 'success');
      $("#reward-list").html(data.html);
    }).fail(function () {
      message('Ошибка ввода данных', 'error');
    }).always(function () {

    });

  });


  //Edit category position
  $body.on('input', '.set-position-category', function () {

    var url = '/admin/ajax/set-category-position';
    var question = $(this).data('id');
    var value = $(this).val();

    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {question: question, value: value},
      beforeSend: function () {
      }
    }).done(function (data) {
    }).fail(function () {
    }).always(function () {

    });

  });
	//Edit product position
	$body.on('input', '.set-position-product', function () {

		var url = '/admin/ajax/set-product-position';
		var question = $(this).data('id');
		var value = $(this).val();

		$.ajax({
			url: url,
			method: 'post',
			dataType: 'JSON',
			data: {question: question, value: value},
			beforeSend: function () {
			}
		}).done(function (data) {
		}).fail(function () {
		}).always(function () {

		});

	});
  //Edit product position
  $body.on('input', '.set-front-position-product', function () {

    var url = '/admin/ajax/set-front-position-product';
    var question = $(this).data('id');
    var value = $(this).val();

    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {question: question, value: value},
      beforeSend: function () {
      }
    }).done(function (data) {
    }).fail(function () {
    }).always(function () {

    });

  });

  //Edit brand position
  $body.on('input', '.set-brand-position', function () {

    var url = '/admin/ajax/set-brand-position';
    var brand = $(this).data('id');
    var value = $(this).val();

    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {brand: brand, value: value},
      beforeSend: function () {
      }
    }).done(function (data) {
    }).fail(function () {
    }).always(function () {

    });

  });


  //Edit voting result
  $body.on('click', '.newPercentBtn', function () {

    var url = '/admin/ajax/new-percent';

    //Values
    var value = $(".newPercent");
    var arr = {};

    var sum = 0;
    value.each(function (i, el) {

      var $this = $(el);
      var $thisVal = parseInt($this.val());

      if ($thisVal !== null) {
        arr[$this.data('answer')] = $thisVal;
      }
      sum += parseInt($this.val()) || 0;
    });


    if (sum !== 100 && sum !== 0) {
      message('Сумарный процент должин быть 100, сейчас ' + sum + ', расспределите еще ' + (100 - sum) + ' процентов', 'error');

    } else {
      $.ajax({
        url: url,
        method: 'post',
        dataType: 'JSON',
        data: arr
      }).done(function (data) {
        message('Данные успешно обновлены', 'success');
      }).fail(function () {
        message('Ошибка ввода данных', 'error');
      }).always(function () {

      });
    }
  });


  //Edit slogan
  $body.on('click', '.save-slogan', function () {

    var url = '/admin/ajax/save-slogan';
    var $this = $(this);
    var slogan = $this.data('slogan');
    var id = $this.data('id');

    //Values
    var text = $(".slogan-text[data-id='" + id + "']").val();

    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {id: id, slogan: slogan, text: text},
      beforeSend: function () {
      }
    }).done(function (data) {
      if (data.fail === true) {
        message('Ошибка ввода данных', 'error');
      } else {
        message('Данные успешно обновлены', 'success');
        $("#values-list").empty().append(data.html);
      }
    }).fail(function () {
    }).always(function () {

    });

  });

  //Del slogan
  $body.on('click', '.del-slogan', function () {

    var url = '/admin/ajax/del-slogan';
    var $this = $(this);
    var id = $this.data('id');


    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {id: id},
      beforeSend: function () {
      }
    }).done(function (data) {
      message('Данные успешно обновлены', 'success');
      location.reload();
    }).fail(function () {
      message('Ошибка сервера', 'error');
    }).always(function () {

    });

  });

  //Add slogan
  $body.on('click', '.add-slogan-item', function () {

    var url = '/admin/ajax/add-slogan';
    var $this = $(this);
    var slogan = $(this).data('slogan');
    var $input = $('.new-slogan-item[data-slogan=' + slogan + ']');
    var text = $input.val();

    console.log(text);

    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {text: text, slogan: slogan},
      beforeSend: function () {
      }
    }).done(function (data) {
      message('Данные успешно добавлены', 'success');
      location.reload();
    }).fail(function () {
      message('Ошибка сервера', 'error');
    }).always(function () {

    });

  });

  // Delete img without route
  $body.on('click', '.del-without-route', function () {
    var $this = $(this);
    var id = $this.data('id');
    var model = $this.data('model');
    var url = '/admin/ajax/del-image-without-route';
    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {id: id, model: model},
      beforeSend: function () {
      }
    }).done(function (data) {
      location.reload();
    }).fail(function () {
      message('Ошибка сервера', 'error');
    }).always(function () {

    });
  });

  $body.on('change', '#user-select', function () {
    var $this = $(this);
    var id = $this.val();
    var url = '/admin/ajax/orders-user-select';
    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {id: id},
      beforeSend: function () {
      }
    }).done(function (data) {
      var $userName = $('#user-name');
      var $userPhone = $('#user-phone');
      var $userEmail = $('#user-email');
      var $userAddress = $('#user-address');

      $userName.val(data.user.name);
      $userPhone.val(data.user.phone);
      $userEmail.val(data.user.email);
      $userAddress.val(data.user.address);
    }).fail(function () {
    }).always(function () {
    });
  });

  $('#delivery-type-radio-0').on('change', function () {
    var $this = $(this);
    var $wareHouse = $('#user-warehouse');
    var $address = $('#user-address');
    if ($this.is(':checked') === true) {
      $address.prop('disabled', true);
      $wareHouse.prop('disabled', false);
    }

  });

  $('#delivery-type-radio-1').on('change', function () {
    var $this = $(this);
    var $wareHouse = $('#user-warehouse');
    var $address = $('#user-address');
    if ($this.is(':checked') === true) {
      $address.prop('disabled', false);
      $wareHouse.prop('disabled', true);
    }

  });

  $body.on('change', '#order-user-city', function () {
    var $this = $(this);
    var ref = $this.val()
    var enable = 0;
    if ($('#delivery-type-radio-0').is(':checked') == true) {
      enable = 1;
    }
    var url = '/admin/ajax/orders-warehouses-select';
    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {ref: ref, enable: enable},
      beforeSend: function () {
      }
    }).done(function (data) {
      $('#select-warehouses-ajax').empty().append(data.html);
      $('#user-warehouse').select2();
    }).fail(function () {
    }).always(function () {
    });
  });

  $body.on('change', '.order-item-count', function () {
    var $this = $(this);
    var id = $this.data('id');
    var value = $this.val();
    var url = '/admin/ajax/orders-items-count';

    console.log(id, value);
    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {id, value},
      beforeSend: function () {
      }
    }).done(function (data) {
      message(data.message, 'success');
      $('#orders-item-table').empty().append(data.html);
      $('#total-products-price-order').empty().append(data.totalSumOrder + ' грн');
      $('#total-delivery-price-order').empty().append(data.deliveryCost + ' грн');
    }).fail(function () {
    }).always(function () {
    });
  });

  $body.on('click', '.orders-delete-items', function () {
    var $this = $(this);
    var id = $this.data('id');
    var url = '/admin/ajax/orders-items-delete';

    console.log(id);
    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {id},
      beforeSend: function () {
      }
    }).done(function (data) {
      message(data.message, 'success');
      $('#orders-item-table').empty().append(data.html);
      $('#total-products-price-order').empty().append(data.totalSumOrder + ' грн');
      $('#total-delivery-price-order').empty().append(data.deliveryCost + ' грн');
    }).fail(function () {
    }).always(function () {
    });
  });

  $body.on('change', '#order-products', function () {
    var $this = $(this);
    var id = $this.val();
    var url = '/admin/ajax/orders-product-select-prices';
    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {id},
      beforeSend: function () {
      }
    }).done(function (data) {
      if (data.success === true) {
        message(data.message, 'success');
        $('#order-product-prices').empty().append(data.html);
      } else {
        // message(data.message, 'error');
        $('#order-product-prices').empty();
      }
    }).fail(function () {
    }).always(function () {
    });
  });

  $body.on('input', '#order-products-count', function () {
    var $this = $(this);
    var value = $this.val();
    var url = '/admin/ajax/order-products-count';
    console.log(value);
    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {value},
      beforeSend: function () {
      }
    }).done(function (data) {
      if (data.success === true) {
        message(data.message, 'success');
        $('#order-product-submit').empty().append(data.html);
      } else {
        message(data.message, 'error');
        $('#order-product-submit').empty();
      }
    }).fail(function () {
    }).always(function () {
    });
  });

  $body.on('click', '#orders-add-new-product', function () {
    var product_id = $('#order-products').val();
    var order_id = $('#order-products').data('order-id');
    var price_id = $('#order-products-price').val();
    var count = $('#order-products-count').val();
    var url = '/admin/ajax/orders-save-product';
    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {order_id, product_id, price_id, count},
      beforeSend: function () {
      }
    }).done(function (data) {
      if (data.success === true) {
        message(data.message, 'success');
        $('#orders-item-table').empty().append(data.html);
        $('#total-products-price-order').empty().append(data.totalSumOrder + ' грн');
        $('#total-delivery-price-order').empty().append(data.deliveryCost + ' грн');
        $('#order-product-submit').empty();
        $('#order-product-prices').empty();
        $('#order-products').val('0').change();
      } else {
        message(data.message, 'error');
        $('#order-product-submit').empty();
        $('#order-product-prices').empty();
        $('#order-products').val('0').change();
      }
    }).fail(function () {
    }).always(function () {
    });
  });

  $body.on('change', '#order-wait-status', function () {
    var $this = $(this);
    var status = $this.val();
    var user_id = $this.data('user-id');
    console.log(status, user_id);
    var url = '/admin/ajax/orders-waits-status';
    $.ajax({
      url: url,
      method: 'post',
      dataType: 'JSON',
      data: {user_id, status},
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      beforeSend: function () {
      }
    }).done(function (data) {
      if (data.success === true) {
        message(data.message, 'success');
        $('.order-waits-select').empty().append(data.html);
      } else {
        message(data.message, 'error');
      }
    }).fail(function () {
    }).always(function () {
    });
  });

});

