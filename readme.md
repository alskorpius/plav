## Installation

- Копируем репозиторий
- Создаем БД (например db)
- Копируем/переименовываем файл .env. Меняем/добавляем такие строки:
    - DB_DATABASE=db
    - DB_USERNAME=root
    - DB_PASSWORD=
- Открываем консоль и там пишем:
    - php composer install // Необращаем внимания на ошибки в консоли composer dump-autoload
    - php artisan key:generate
    - php artisan vendor:publish
    - php artisan migrate // Если есть ошибки в консоли - обращаемся к разработчикам
    - php artisan db:seed // Если есть ошибки в консоли - обращаемся к разработчикам
    - php artisan ide-helper:generate
    - php artisan ide-helper:meta
    - php artisan optimize
