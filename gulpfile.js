const gulp = require('gulp')
const svgSprite = require('gulp-svg-sprite')
const svgmin = require('gulp-svgmin')
const cheerio = require('gulp-cheerio')
const replace = require('gulp-replace')
const gulpIf = require('gulp-if')
const rename = require('gulp-rename')
const through2 = require('through2')

let idList = []
gulp.task('svgSpriteBuild', function () {
    return gulp.src(['./resources/assets/svg/simple-icons/*.svg', './resources/assets/svg/simple-icons-no-clear/*.svg'])
    // minify svg
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        // remove all fill, style and stroke declarations in out shapes
        .pipe(gulpIf(function (file) {
            return !file.base.split('\\').includes('simple-icons-no-clear')
        }, cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill')
                $('[stroke]').removeAttr('stroke')
                $('[style]').removeAttr('style')
            },
            parserOptions: {xmlMode: true}
        })))
        // cheerio plugin create unnecessary string '&gt;', so replace it.
        .pipe(replace('&gt;', '>'))
        // build svg sprite
        .pipe(svgSprite({
            mode: {
                symbol: {
                    sprite: '../sprite.svg'
                }
            }
        }))
        .pipe(gulp.dest('./public/svg/'))
        .pipe(cheerio({
            run: function ($) {
                $('symbol').each((i, symbol) => {
                    idList.push($(symbol).attr('id'))
                })
            },
            parserOptions: {
                xmlMode: true
            }
        }))
        .pipe(through2.obj(function (file, ...args) {
            let cb = args[1]
            file.contents = Buffer.from(`<?php return ${JSON.stringify(idList, null, '\t')};`)
            cb(null, file)
        }))
        .pipe(rename(path => {
            path.basename = 'svg-icons-list'
            path.extname = '.php'
        }))
        .pipe(gulp.dest('./config'))
})
