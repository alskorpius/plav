<?php

Route::group(['middleware' => 'auth'], function () {
    Route::put('change-status', 'GeneralController@changeStatus');
    Route::put('sortable', 'GeneralController@sortable');
    Route::get('make-slug', 'GeneralController@makeSlug');
    Route::post('delete-answer', 'GeneralController@deleteAnswer');
    Route::post('select-regions', 'GeneralController@selectRegions');
    Route::post('select-areas', 'GeneralController@selectAreas');
    Route::post('add-answer', 'GeneralController@addAnswer');
    Route::post('select-article', 'GeneralController@selectArticle');
    Route::post('translit', 'GeneralController@translit');
    Route::post('add-value', 'SpecificationsValuesController@addValue');
    Route::post('remove-value', 'GeneralController@removeValue');
    Route::post('edit-value', 'GeneralController@editValue');
    Route::post('read-notifications', ['as' => 'ajax.read-notifications', 'uses' => 'GeneralController@readNotifications']);
    Route::get('search-users', ['as' => 'ajax.search-users', 'uses' => 'GeneralController@searchUsers']);
    Route::post('add-price', 'GeneralController@addPrice');
    Route::post('remove-price', 'GeneralController@removePrice');
    Route::post('edit-price', 'GeneralController@editPrice');
    Route::post('add-reward', 'GeneralController@addReward');
    Route::post('remove-reward', 'GeneralController@removeReward');
    Route::post('edit-reward', 'GeneralController@editReward');
    Route::post('select-region-by-area', 'GeneralController@regionByArea');
    Route::post('new-percent', 'GeneralController@newPercent');

    Route::post('group-delete', 'GeneralController@groupDelete');
    Route::post('group-status', 'GeneralController@groupStatus');
    Route::post('group-status-enable', 'GeneralController@groupStatusEnable');
    Route::post('group-status-2', 'GeneralController@groupStatus2');
    Route::post('group-status-3', 'GeneralController@groupStatus3');
    Route::post('group-status-4', 'GeneralController@groupStatus4');
    Route::post('group-status-5', 'GeneralController@groupStatus5');

    Route::post('set-category-position', 'GeneralController@setCategoryPosition');
    Route::post('set-product-position', 'GeneralController@setProductPosition');
    Route::post('set-front-position-product', 'GeneralController@setFrontProductPosition');
    Route::post('set-brand-position', 'GeneralController@setBrandPosition');

    Route::post('gallery-upload-photos', 'GalleryController@uploadPhotos');
    Route::post('gallery-get-photos', 'GalleryController@getPhotos');
    Route::post('gallery-delete-photo', 'GalleryController@deletePhoto');
    Route::post('gallery-sort-photos', 'GalleryController@sortPhotos');
    Route::post('gallery-set-main', 'GalleryController@setMain');

    Route::post('save-slogan', 'GeneralController@saveSlogan');
    Route::post('del-slogan', 'GeneralController@delSlogan');
    Route::post('add-slogan', 'GeneralController@addSlogan');

    Route::post('del-image-without-route', 'GeneralController@delImgWithoutRoute');

    Route::post('orders-user-select', 'GeneralController@ordersUserSelect');
    Route::post('orders-warehouses-select', 'GeneralController@ordersWarehousesSelect');
    Route::post('orders-items-count', 'GeneralController@ordersItemsCount');
    Route::post('orders-items-delete', 'GeneralController@ordersItemsDelete');
    Route::post('orders-product-select-prices', 'GeneralController@ordersProductSelectPrices');
    Route::post('order-products-count', 'GeneralController@ordersProductCount');
    Route::post('orders-save-product', 'GeneralController@ordersSaveProduct');
    Route::post('orders-waits-status', 'GeneralController@ordersWaitsStatus');
});
