<?php
Route::get('login', ['as' => 'admin.login', 'uses' => 'LoginController@showLoginForm']);
Route::post('login', ['uses' => 'LoginController@login']);

Route::group(['middleware' => ['admin.auth', 'access']], function () {
    Route::get('/', ['as' => 'admin.dashboard', 'uses' => 'DashboardController@index']);
    Route::post('logout', ['as' => 'admin.logout', 'uses' => 'LoginController@logout']);
    Route::get('crop', ['as' => 'admin.crop', 'uses' => 'CropController@edit']);
    Route::put('crop', ['as' => 'admin.crop.update', 'uses' => 'CropController@update']);
    Route::group(['prefix' => 'account'], function () {
        Route::get('/', ['as' => 'admin.profile', 'uses' => 'ProfileController@index']);
        Route::put('save-personal-data', ['as' => 'admin.save-personal-data', 'uses' => 'ProfileController@savePersonalData']);
        Route::get('change-password', ['as' => 'admin.change-password', 'uses' => 'ProfileController@changePassword']);
        Route::put('save-new-password', ['as' => 'admin.save-new-password', 'uses' => 'ProfileController@saveNewPassword']);
        Route::get('change-avatar', ['as' => 'admin.change-avatar', 'uses' => 'ProfileController@changeAvatar']);
        Route::put('save-new-avatar', ['as' => 'admin.save-new-avatar', 'uses' => 'ProfileController@saveNewAvatar']);
        Route::delete('delete-avatar', ['as' => 'admin.delete-avatar', 'uses' => 'ProfileController@deleteAvatar']);
    });

    Route::resource('pages', 'Content\PagesController', ['except' => ['show'], 'as' => 'admin']);
    Route::resource('cities', 'Dictionaries\CitiesController', ['except' => ['show'], 'as' => 'admin']);
    Route::resource('streets', 'Dictionaries\StreetsController', ['except' => ['show'], 'as' => 'admin']);
    Route::resource('departments', 'Dictionaries\DepartmentsController', ['except' => ['show'], 'as' => 'admin']);
    Route::resource('professions', 'Dictionaries\ProfessionsController', ['except' => ['show'], 'as' => 'admin']);

    Route::resource('cards', 'Cards\CardsController', ['except' => ['show'], 'as' => 'admin']);


    Route::resource('settings', 'SettingsController', ['only' => ['index', 'store'], 'as' => 'admin']);


    Route::resource('system-pages', 'Content\SystemPagesController', ['except' => ['show'], 'as' => 'admin']);
    Route::get('system-pages/{id}/delete-image', ['as' => 'admin.system-pages.delete-image', 'uses' => 'Content\SystemPagesController@deleteImage']);

    Route::resource('menu', 'MenuController', ['except' => ['show'], 'as' => 'admin']);


    Route::resource('roles', 'Users\RolesController', ['except' => ['show'], 'as' => 'admin']);
    Route::resource('customers', 'Users\CustomersController', ['except' => ['show'], 'as' => 'admin']);
    Route::get('customers/deleted', ['as' => 'admin.customers.deleted', 'uses' => 'Users\CustomersController@deleted']);
    Route::put('customers/{id}/restore', ['as' => 'admin.customers.restore', 'uses' => 'Users\CustomersController@restore']);
    Route::delete('customers/{id}/force-delete', ['as' => 'admin.customers.force-delete', 'uses' => 'Users\CustomersController@forceDelete']);
    Route::resource('managers', 'Users\ManagersController', ['except' => ['show'], 'as' => 'admin']);
    Route::get('managers/deleted', ['as' => 'admin.managers.deleted', 'uses' => 'Users\ManagersController@deleted']);
    Route::get('managers/{manager}/delete-avatar', ['as' => 'admin.managers.delete-image', 'uses' => 'Users\ManagersController@deleteAvatar']);
    Route::put('managers/{id}/restore', ['as' => 'admin.managers.restore', 'uses' => 'Users\ManagersController@restore']);
    Route::delete('managers/{id}/force-delete', ['as' => 'admin.managers.force-delete', 'uses' => 'Users\ManagersController@forceDelete']);

    Route::get('notifications', ['as' => 'admin.notifications', 'uses' => 'NotificationsController@index']);
    Route::get('notifications/read/{notification}', ['as' => 'admin.notifications.read', 'uses' => 'NotificationsController@read']);

    Route::resource('brands', 'Catalog\BrandsController', ['except' => ['show'], 'as' => 'admin']);
    Route::get('brands/{id}/delete-image', ['as' => 'admin.brands.delete-image', 'uses' => 'Catalog\BrandsController@deleteImage']);

    Route::resource('specifications', 'Catalog\SpecificationsController', ['except' => ['show', 'create', 'store', 'destroy'], 'as' => 'admin']);

    Route::resource('products', 'Catalog\ProductsController', ['except' => ['show'], 'as' => 'admin']);

    Route::get('products/excel', ['as' => 'admin.products.excel', 'uses' => 'Catalog\ProductsController@excel']);

    Route::resource('products-reviews', 'Catalog\ProductsReviewsController', ['except' => ['show'], 'as' => 'admin']);
    Route::get('products/{id}/delete-image', ['as' => 'admin.products.delete-image', 'uses' => 'Catalog\ProductsController@deleteImage']);

    Route::resource('rewards', 'Catalog\ProductsRewardsController', ['except' => ['show'], 'as' => 'admin']);
    Route::get('rewards/{id}/delete-image', ['as' => 'admin.rewards.delete-image', 'uses' => 'Catalog\ProductsRewardsController@deleteImage']);

    Route::get('visitors', ['as' => 'admin.visitors.index', 'uses' => 'Statistic\VisitorsController@index']);
    Route::get('hits', ['as' => 'admin.hits.index', 'uses' => 'Statistic\HitsController@index']);
    Route::get('refers', ['as' => 'admin.refers.index', 'uses' => 'Statistic\RefersController@index']);
    Route::get('fake-auth', ['as' => 'admin.fake-auth', 'uses' => 'Users\FakeAuthController@index']);

    Route::resource('orders', 'Orders\OrdersController', ['except' => ['show'], 'as' => 'admin']);
    Route::get('print-order/{id}', ['as' => 'admin.print-order.index', 'uses' => 'Orders\OrdersController@printOrder']);
});
