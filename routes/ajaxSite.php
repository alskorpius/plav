<?php

Route::post('favorites', 'GeneralController@favorites')->name('product.favorites');
Route::post('review-post', 'GeneralController@addReview')->name('review-post');
Route::post('event-post', 'GeneralController@addEvent')->name('event-post');
Route::post('review-product', 'GeneralController@addReviewProduct')->name('review-product');
Route::post('add-to-cart', 'GeneralController@addToCart')->name('cart.add');
Route::post('remove-from-cart', 'GeneralController@removeFromCart')->name('cart.remove-item');
Route::post('count-items', 'GeneralController@countItems')->name('cart.count-items');
Route::post('contacts', 'GeneralController@contacts')->name('contacts-post');
Route::post('select-category', 'GeneralController@selectCategory')->name('select-category');
Route::post('order-select-warehouses', 'GeneralController@selectWarehouses')->name('order.select-warehouses');
Route::post('order-step1', 'GeneralController@orderStep1')->name('order.step1');
Route::post('order-step2', 'GeneralController@orderStep2')->name('order.step2');
Route::post('order-step3', 'GeneralController@orderStep3')->name('order.step3');
Route::post('delivery-cost', 'GeneralController@deliveryCost')->name('order.delivery-cost');
Route::post('get-cart', 'GeneralController@getCart')->name('cart.get-cart');
Route::post('get-product-prices', 'GeneralController@getProductPrices')->name('product.get-prices');
Route::post('filter-query', 'GeneralController@productsFilterQuery')->name('products.filter');
Route::post('live-search', 'GeneralController@liveSearch')->name('live-search');
Route::post('add-wait', 'GeneralController@addWait')->name('add-wait');
Route::get('/where-buy-spots-get', 'GeneralController@getSpots')->name('where-buy.spots');
