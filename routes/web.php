<?php
Route::patch('/', function () {
    if (request()->input('password') === config('db.security.bot-password')) {
        Session::put('protectionPassed', true);
        return redirect()->back();
    } else {
        abort(401);
    }
});
Route::group(['prefix' => config('prefix')], function () {
    Config::set('lang', Lang::getLocale());
    Auth::routes();

    Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
    Route::get('register/confirmation/{token}', ['as' => 'register.confirm', 'uses' => 'Auth\RegisterController@confirmation']);

    Route::any('/', ['as' => 'site.index', 'uses' => 'IndexController@show']);

    if (!Request::is('admin')) {
        #Route::get('{slug}', ['as' => 'site.pages.show', 'uses' => 'Content\PagesController@show']);
    }

    Route::get('/social/{provider}', ['as' => 'socials.redirect', 'uses' => 'Auth\LoginController@socials']);
    Route::get('/social/{provider}/callback', ['as' => 'socials.callback', 'uses' => 'Auth\LoginController@callback']);
    Route::get('/forgot-password', ['as' => 'forgot-password', 'uses' => 'Auth\ForgotPasswordController@index']);
    Route::post('/forgot-password', ['as' => 'password.email.custom', 'uses' => 'Auth\ForgotPasswordController@send']);
    // Client side routers

    // Votings
    Route::get('/voting', 'VotingsController@index')->name('votings');
    Route::post('/voting', 'VotingsController@store')->name('votingPOST');
    //Subscribe action
    Route::post('subscribe', ['as' => 'subscribe', 'uses' => 'SubscribeController@index']);
    //Content block
    Route::get('/blog', 'Content\ContentController@index')->name('blog');
    Route::get('/blog/ajax', 'Content\ContentController@index')->name('blog.ajax');

    //blog
    Route::get('/blog/{type}', 'Content\BlogController@index')->name('content')->where('type', 'events|news|articles');
    Route::get('/blog/{type}/{category}', 'Content\BlogController@category')->name('content.category')->where('type', 'events|news|articles');
    Route::get('/blog/{type}/{category}/{slug}', 'Content\BlogController@show')->name('content.show')->where('type', 'events|news|articles');

    //Contacts
    Route::get('/contacts', 'ContactsController@index')->name('contacts');
    // About us
    Route::get('/about-us', 'AboutUs\About@index')->name('about');
    Route::get('/about-us/ajax', 'AboutUs\About@index')->name('about.ajax');
    Route::get('/about-us/who-are-we', 'AboutUs\WhoRweController@index')->name('whoRwe');
    Route::get('/about-us/our-experts', 'AboutUs\ExpertsContactsController@index')->name('experts');
    Route::get('/about-us/our-directors', 'AboutUs\TeamController@index')->name('team');
    Route::get('/about-us/quality', 'AboutUs\QualityController@index')->name('quality');


    //Delivery
    Route::get('/payment-and-delivery', 'PaymentDeliveryController@index')->name('paymentDelivery');

    Route::get('/reviews', 'ReviewsController@index')->name('reviews');


    Route::group(['middleware' => ['auth'], 'prefix' => 'profile'], function () {
    //Profile
    Route::get('/', 'ProfileController@index')->name('profile');
    Route::get('/edit-password', 'ProfileController@indexPassword')->name('profile.editPassword');
    Route::get('/favorites', 'ProfileController@indexFavorites')->name('profile.favorites');
    Route::get('/favorites-clear', 'ProfileController@clearFavorites')->name('profile.favorites.clear');
    Route::get('/orders', 'ProfileController@indexOrders')->name('profile.orders');
    Route::get('/wait', 'ProfileController@indexWait')->name('profile.wait');
    Route::get('/wait-clear', 'ProfileController@clearWait')->name('profile.wait.clear');
    Route::get('/settings', 'ProfileController@indexSettings')->name('profile.settings');
    Route::post('/edit-password', 'ProfileController@updatePassword')->name('profile.newPass');
    Route::post('/', 'ProfileController@update')->name('profile.profileUpd');
    });

    //Products
    Route::paginate('/products', 'Catalog\ProductsController@index')->name('products');
    Route::paginate('/products/filter/{filters}', 'Catalog\ProductsController@index')
    ->where('filters', '.*')
    ->name('products-with-filter');
    Route::get('/products/{slug}', 'Catalog\ProductsController@show')->name('product');
    Route::get('/cart', 'Catalog\CartController@index')->name('cart');
    Route::get('/compilation/{slug}/products', 'Catalog\ProductsController@compilation')->name('compilation');
    Route::get('/compilation/{slug}/products/filter/{filters}', 'Catalog\ProductsController@compilation')
    ->where('filters', '.*')
    ->name('compilation-with-filter');

    //Ordering
    Route::match(['GET', 'POST'], '/checkout', 'Catalog\OrderController@index')->name('ordering');
    Route::match(['GET', 'POST'], '/checkout/step-1/{id}', 'Catalog\OrderController@index')->name('ordering.step-1');
    Route::match(['GET', 'POST'], '/checkout/step-2/{id}', 'Catalog\OrderController@step2')->name('ordering.step-2');
    Route::match(['GET', 'POST'], '/checkout/step-3/{id}', 'Catalog\OrderController@step3')->name('ordering.step-3');
    Route::any('/checkout/check-payment', 'Catalog\OrderController@checkPayment')->name('check-payment');
    Route::any('/checkout/check-payment/portmone', 'Catalog\OrderController@checkPaymentPortmone')->name('check-payment.portmone');

    //Brand presentation
    Route::get('/brand/{slug}/products', 'Catalog\BrandsPresentController@index')->name('brand.products');
    Route::get('/brand/{slug}/products/filter/{filters}', 'Catalog\BrandsPresentController@index')
    ->where('filters', '.*')
    ->name('brand.products-with-filter');
    Route::get('/presentation/{slug}', 'Catalog\BrandsPresentController@product')->name('brand.present');


    Route::get('/brands', 'Catalog\BrandsController@index')->name('brands');
    Route::get('/brands/filter/{filters}', 'Catalog\BrandsController@index')
    ->where('filters', '.*')
    ->name('brands-with-filter');
    Route::get('/brands/ajax', 'Catalog\BrandsController@index')->name('brands.ajax');

    //Search
    Route::get('/products-search', 'Catalog\ProductsController@search')->name('products.search');

    //Where buy page
    Route::get('/where-buy', 'WhereBuyController@index')->name('where-buy');

    Route::get('/sitemap', 'SitemapController@index')->name('sitemap');
    Route::get('/sitemap-xml', 'SitemapController@xml')->name('sitemap');


    Route::get('/age-check', 'AgeCheck@index')->name('age-check');
    Route::get('/age-check/{access}', 'AgeCheck@access')->name('age-check-access');
    Route::get('/age-check-denied', 'AgeCheck@denied')->name('age-check-denied');

    //Fake login
    Route::get('/fake-login', 'FakeLoginController@index')->name('fake-login');


    // Text pages
    if (!Request::is('admin')) {
        Route::get('{slug}', 'Content\TextPageController@index')->name('text-page');
    }
});
