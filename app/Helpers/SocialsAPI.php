<?php namespace App\Helpers;

class SocialsAPI
{

    public static function facebookPost()
    {
        $facebookData = file_get_contents('https://graph.facebook.com/v3.1/' . env('FACEBOOK_GRAPH_USER') . '/posts?fields=link%2Cmessage%2Cpicture%2Cfull_picture%2Ccreated_time&access_token=' . env('FACEBOOK_GRAPH_ACCESS_TOKEN'));
        $posts = \GuzzleHttp\json_decode($facebookData);
        $facebookPost = $posts->data[0];
        return $facebookPost;
    }

    public static function facebookGroupInfo()
    {
        $facebookData = file_get_contents('https://graph.facebook.com/v3.1/' . env('FACEBOOK_GRAPH_USER') . '?fields=picture%2Cname&access_token=' . env('FACEBOOK_GRAPH_ACCESS_TOKEN'));
        $info = \GuzzleHttp\json_decode($facebookData);
        $arr = [];
        $arr['name'] = $info->name;
        $arr['image'] = $info->picture->data->url;
        $arr = (object)$arr;
        return $arr;
    }
}
