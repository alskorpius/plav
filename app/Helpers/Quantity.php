<?php namespace App\Helpers;

class Quantity
{

    public static function getMbFromKb($kilobytes)
    {
        return (float)number_format((int)$kilobytes / 1024, 2, '.', '');
    }

}