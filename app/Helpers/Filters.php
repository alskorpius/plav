<?php namespace App\Helpers;

use App\Models\Catalog\Product;
use App\Models\Catalog\ProductsLine;
use App\Models\Catalog\ProductsPrice;
use App\Models\Catalog\ShopProductsDictionariesValue;
use App\Models\Catalog\ShopProductsDictionary;
use App\Models\Map\MapRegion;
use App\Models\Providers\ShopCountry;
use App\Models\Providers\ShopProvider;
use App\Models\Providers\ShopRegion;
use Illuminate\Support\Arr;

class Filters
{

    /**
     * @param $requestArr []
     * @param Product $products
     * @return array
     */
    public static function getProductsFilter($requestArr, $products)
    {
        $forIsDisabledArr = [];

        foreach ($products as $product) {
            
            if ($product->color) { $forIsDisabledArr['tsvet'][] = $product->color->slug; }
            if ($product->sweetness) { $forIsDisabledArr['sladost'][] = $product->sweetness->slug; }
            if ($product->type) { $forIsDisabledArr['tip'][] = $product->type->slug; }
            if ($product->kind) { $forIsDisabledArr['vid'][] = $product->kind->slug; }
            if ($product->country) { $forIsDisabledArr['strana'][] = $product->country->slug; }
            if ($product->provider) { $forIsDisabledArr['proizvoditel'][] = $product->provider->slug; }
            if (sizeof($product->sorts) > 0) {
                foreach ($product->sorts as $sort) {
                    $forIsDisabledArr['sort-vinograda'][] = $sort->value->slug;
                }
            } else {
                $forIsDisabledArr['sort-vinograda'][] = [];
            }
            if (sizeof($product->prices) > 0) {
                foreach ($product->prices as $vintage) {
                    $forIsDisabledArr['god'][] = $vintage->year;
                    $forIsDisabledArr['obem'][] = $vintage->volume;
                }
            } else {
                $forIsDisabledArr['god'][] = [];
                $forIsDisabledArr['obem'][] = [];
            }
            if (sizeof($product->regionsFilter) > 0) {
                foreach ($product->regionsFilter as $region) {
                    $forIsDisabledArr['region'][] = $region->slug;
                }
            } else {
                $forIsDisabledArr['region'][] = [];
            }
        }
        $yearsArr = [];
        $volumesArr = [];
        $vintages = ProductsPrice::all()->sortBy('year');
        $dictionaries = ShopProductsDictionary::with(['dictionaryValues'])->whereStatus(1)->get();
        $countries = ShopCountry::with(['products'])->where('status', 1)->get();
        $providers = ShopProvider::all();
        $regions = ShopRegion::with(['current'])->where('status', 1)->get();
        $filterResultArr = [];
        if ($requestArr !== null && !empty($requestArr) > 0 && !stristr($requestArr, 'page')) {
            $filterItemsGlobal = explode('/', $requestArr);
            $filterItems = [];
            foreach ($filterItemsGlobal as $filterItemsGlobalItem) {
                $filterItems[] = explode('_', $filterItemsGlobalItem);
            }
            foreach ($filterItems as $filterItem) {
                $filterResultArr[$filterItem[0]] = explode('-or-', $filterItem[1]);
            }
        }
        Product::$filter = $filterResultArr;
        $color_id = Arr::get($filterResultArr, 'tsvet', false) !== false ? Arr::get($filterResultArr, 'tsvet') : [];
        $sweetness_id = Arr::get($filterResultArr, 'sladost', false) !== false ? Arr::get($filterResultArr, 'sladost') : [];
        $type_id = Arr::get($filterResultArr, 'tip', false) !== false ? Arr::get($filterResultArr, 'tip') : [];
        $kind_id = Arr::get($filterResultArr, 'vid', false) !== false ? Arr::get($filterResultArr, 'vid') : [];
        $country_id = Arr::get($filterResultArr, 'strana', false) !== false ? Arr::get($filterResultArr, 'strana') : [];
        $provider_id = Arr::get($filterResultArr, 'proizvoditel', false) !== false ? Arr::get($filterResultArr, 'proizvoditel') : [];
        $sort_id = Arr::get($filterResultArr, 'sort-vinograda', false) !== false ? Arr::get($filterResultArr, 'sort-vinograda') : [];
        $year_idArr = Arr::get($filterResultArr, 'god', false) !== false ? Arr::get($filterResultArr, 'god') : [];
        $volume_idArr = Arr::get($filterResultArr, 'obem', false) !== false ? Arr::get($filterResultArr, 'obem') : [];
        $region_idArr = Arr::get($filterResultArr, 'region', false) !== false ? Arr::get($filterResultArr, 'region') : [];
        $urls = [];
        $forForeach = [
            'tsvet' => $color_id,
            'sladost' => $sweetness_id,
            'tip' => $type_id,
            'vid' => $kind_id,
        ];
        foreach ($vintages as $vintage) {
            $yearsArr[$vintage->year][] = $vintage->product_id;
            $volumesArr[$vintage->volume][] = $vintage->product_id;
        }
        foreach ($yearsArr as $key => $arr) {
            $yearsArr[$key] = array_unique($arr);
        }
        foreach ($volumesArr as $key => $arr) {
            $volumesArr[$key] = array_unique($arr);
        }
        /**
         * @var ShopProductsDictionary $dictionary
         */
        foreach ($dictionaries as $dictionary) {
            if ($dictionary->slug == 'krepost' || $dictionary->slug == 'kolichestvo-sahara') {
                continue;
            }
            $filterArr[$dictionary->slug]['title'] = $dictionary->current->name;
            $filterArr[$dictionary->slug]['name_prefix'] = $dictionary->slug;
            if (sizeof(Arr::get($forForeach, $dictionary->slug, [])) > 0) {
                $filterArr[$dictionary->slug]['is_open'] = true;
            } else {
                $filterArr[$dictionary->slug]['is_open'] = false;
            }
            if ($dictionary->slug == 'sort-vinograda') {
                $dictionary->dictionaryValues->load('sorts');
            }
            foreach ($dictionary->dictionaryValues as $value) {
                if ($value->status == 0) {
                    continue;
                }
                $filterArr[$dictionary->slug]['items'][$value->id]['text'] = $value->current->name;
                $filterArr[$dictionary->slug]['items'][$value->id]['slug'] = $dictionary->slug . '_' . $value->slug;
                $filterArr[$dictionary->slug]['items'][$value->id]['url'] = Filters::add($dictionary->slug, $value->slug, stristr(request()->url(), 'compilation') ? '/compilation/' . \Route::getCurrentRoute()->parameter('slug') . '/products' : '/products');
                if (in_array($value->slug, $color_id) || in_array($value->slug, $sweetness_id) ||
                    in_array($value->slug, $type_id) || in_array($value->slug, $kind_id) || in_array($value->slug, $sort_id)
                ) {
                    $filterArr[$dictionary->slug]['items'][$value->id]['is_checked'] = 1;
                } else {
                    $filterArr[$dictionary->slug]['items'][$value->id]['is_checked'] = 0;
                }
                $filterArr[$dictionary->slug]['items'][$value->id]['not-a'] = in_array($value->slug, Arr::get($forIsDisabledArr, $dictionary->slug, [])) ? false : true;
            }
        }

        $filterArr['country']['title'] = __('custom.country');
        $filterArr['country']['name_prefix'] = 'country';
        $filterArr['country']['is_open'] = sizeof($country_id) > 0 ? true : false;
        foreach ($countries as $country) {
            $filterArr['country']['items'][$country->id]['text'] = $country->current->name;
            $filterArr['country']['items'][$country->id]['slug'] = 'strana_' . $country->slug;
            $filterArr['country']['items'][$country->id]['url'] = Filters::add('strana', $country->slug, stristr(request()->url(), 'compilation') ? '/compilation/' . \Route::getCurrentRoute()->parameter('slug') . '/products' : '/products');
            $filterArr['country']['items'][$country->id]['is_checked'] = in_array($country->slug, $country_id) ? 1 : 0;
            $filterArr['country']['items'][$country->id]['not-a'] = in_array($country->slug, Arr::get($forIsDisabledArr, 'strana', [])) ? false : true;
        }

        $filterArr['provider']['title'] = __('custom.provider');
        $filterArr['provider']['name_prefix'] = 'provider';
        $filterArr['provider']['is_open'] = sizeof($provider_id) > 0 ? true : false;
        foreach ($providers as $provider) {
            $isOk = false;
            if ($provider->products) {
                foreach ($provider->products as $product) {
                    if (!$product->is_accessory && $product->status) $isOk = true;
                }
            }
            if ($isOk) {
                $filterArr['provider']['items'][$provider->id]['text'] = $provider->current->name;
                $filterArr['provider']['items'][$provider->id]['slug'] = 'proizvoditel_' . $provider->slug;
                $filterArr['provider']['items'][$provider->id]['url'] = Filters::add('proizvoditel', $provider->slug, stristr(request()->url(), 'compilation') ? '/compilation/' . \Route::getCurrentRoute()->parameter('slug') . '/products' : '/products');
                $filterArr['provider']['items'][$provider->id]['is_checked'] = in_array($provider->slug, $provider_id) ? 1 : 0;
                $filterArr['provider']['items'][$provider->id]['not-a'] = in_array($provider->slug, Arr::get($forIsDisabledArr, 'proizvoditel', [])) ? false : true;
            }
        }

        $filterArr['year']['title'] = __('custom.year');
        $filterArr['year']['name_prefix'] = 'year';
        $filterArr['year']['is_open'] = sizeof($year_idArr) > 0 ? true : false;
        foreach ($yearsArr as $key => $item) {
            $filterArr['year']['items'][$key]['text'] = $key;
            $filterArr['year']['items'][$key]['slug'] = 'god_' . $key;
            $filterArr['year']['items'][$key]['url'] = Filters::add('god', $key, stristr(request()->url(), 'compilation') ? '/compilation/' . \Route::getCurrentRoute()->parameter('slug') . '/products' : '/products');
            $filterArr['year']['items'][$key]['is_checked'] = in_array($key, $year_idArr) ? 1 : 0;
            $filterArr['year']['items'][$key]['not-a'] = in_array($key, Arr::get($forIsDisabledArr, 'god', [])) ? false : true;
        }

        $filterArr['volume']['title'] = __('custom.scope');
        $filterArr['volume']['name_prefix'] = 'volume';
        $filterArr['volume']['is_open'] = sizeof($volume_idArr) > 0 ? true : false;
        foreach ($volumesArr as $key => $item) {
            $filterArr['volume']['items'][$key]['text'] = $key . __('custom.scope_val');
            $filterArr['volume']['items'][$key]['slug'] = 'obem_' . $key;
            $filterArr['volume']['items'][$key]['url'] = Filters::add('obem', $key, stristr(request()->url(), 'compilation') ? '/compilation/' . \Route::getCurrentRoute()->parameter('slug') . '/products' : '/products');
            $filterArr['volume']['items'][$key]['is_checked'] = in_array($key, $volume_idArr) ? 1 : 0;
            $filterArr['volume']['items'][$key]['not-a'] = in_array($key, Arr::get($forIsDisabledArr, 'obem', [])) ? false : true;
        }

        $filterArr['region']['title'] = __('custom.region');
        $filterArr['region']['name_prefix'] = 'region';
        $filterArr['region']['is_open'] = sizeof($region_idArr) > 0 ? true : false;

        foreach ($regions as $region) {
            $filterArr['region']['items'][$region->id]['text'] = $region->current->name;
            $filterArr['region']['items'][$region->id]['slug'] = 'region_' . $region->slug;
            $filterArr['region']['items'][$region->id]['url'] = Filters::add('region', $region->slug, stristr(request()->url(), 'compilation') ? '/compilation/' . \Route::getCurrentRoute()->parameter('slug') . '/products' : '/products');
            $filterArr['region']['items'][$region->id]['is_checked'] = in_array($region->slug, $region_idArr) ? 1 : 0;
            $filterArr['region']['items'][$region->id]['not-a'] = in_array($region->slug, Arr::get($forIsDisabledArr, 'region', [])) ? false : true;
        }
        return $filterArr;
    }

    public static function getBrandsFilter($requestArr)
    {
        $yearsArr = [];
        $volumesArr = [];
        $vintages = ProductsPrice::all()->sortBy('year');
        $dictionaries = ShopProductsDictionariesValue::whereDictionaryId(4)->get();
        $countries = ShopCountry::all()->where('status', 1);
        $lines = ProductsLine::all()->where('status', 1);
        $providers = ShopProvider::all();
        $filterItemsGlobal = explode('/', $requestArr);
        $filterItems = [];
        $filterResultArr = [];
        if ($requestArr !== null) {
            $filterItemsGlobal = explode('/', $requestArr);
            $filterItems = [];
            foreach ($filterItemsGlobal as $filterItemsGlobalItem) {
                $filterItems[] = explode('_', $filterItemsGlobalItem);
            }
            foreach ($filterItems as $filterItem) {
                $filterResultArr[$filterItem[0]] = explode('-or-', $filterItem[1]);
            }
        }
        Product::$filter = $filterResultArr;
        $kind_id = Arr::get($filterResultArr, 'vid', false) !== false ? Arr::get($filterResultArr, 'vid') : [];
        $country_id = Arr::get($filterResultArr, 'strana', false) !== false ? Arr::get($filterResultArr, 'strana') : [];
        $provider_id = Arr::get($filterResultArr, 'proizvoditel', false) !== false ? Arr::get($filterResultArr, 'proizvoditel') : [];
        $lines_id = Arr::get($filterResultArr, 'lines', false) !== false ? Arr::get($filterResultArr, 'lines') : [];

        $filterArr['product']['title'] = __('custom.product-title');
        $filterArr['product']['name_prefix'] = 'product';
        $filterArr['product']['is_open'] = sizeof($kind_id) > 0 ? true : false;
        foreach ($dictionaries as $dictionary) {
            if ($dictionary->status == 0) {
                continue;
            }
            $filterArr['product']['items'][$dictionary->id]['text'] = $dictionary->current->name;
            $filterArr['product']['items'][$dictionary->id]['slug'] = 'vid_' . $dictionary->slug;
            $filterArr['product']['items'][$dictionary->id]['url'] = Filters::add('vid', $dictionary->slug, '/brands');
            $filterArr['product']['items'][$dictionary->id]['is_checked'] = in_array($dictionary->slug, $kind_id) ? 1 : 0;
        }

        $filterArr['lines']['title'] = __('custom.lines');
        $filterArr['lines']['name_prefix'] = 'lines';
        $filterArr['lines']['is_open'] = sizeof($lines_id) > 0 ? true : false;
        foreach ($lines as $line) {
            $filterArr['lines']['items'][$line->id]['text'] = $line->current->name;
            $filterArr['lines']['items'][$line->id]['slug'] = 'lines_' . $line->slug;
            $filterArr['lines']['items'][$line->id]['url'] = Filters::add('lines', $line->slug, '/brands');
            $filterArr['lines']['items'][$line->id]['is_checked'] = in_array($line->slug, $lines_id) ? 1 : 0;
        }

        $filterArr['country']['title'] = __('custom.country');
        $filterArr['country']['name_prefix'] = 'country';
        $filterArr['country']['is_open'] = sizeof($country_id) > 0 ? true : false;
        foreach ($countries as $country) {
            $filterArr['country']['items'][$country->id]['text'] = $country->current->name;
            $filterArr['country']['items'][$country->id]['slug'] = 'strana_' . $country->slug;
            $filterArr['country']['items'][$country->id]['url'] = Filters::add('strana', $country->slug, '/brands');
            $filterArr['country']['items'][$country->id]['is_checked'] = in_array($country->slug, $country_id) ? 1 : 0;

        }
        $filterArr['provider']['title'] = __('custom.provider');
        $filterArr['provider']['name_prefix'] = 'provider';
        $filterArr['provider']['is_open'] = sizeof($provider_id) > 0 ? true : false;
        foreach ($providers as $provider) {
            $isOk = false;
            if ($provider->products) {
                foreach ($provider->products as $product) {
                    if (!$product->is_accessory && $product->status) $isOk = true;
                }
            }
            if ($isOk) {
                $filterArr['provider']['items'][$provider->id]['text'] = $provider->current->name;
                $filterArr['provider']['items'][$provider->id]['slug'] = 'proizvoditel_' . $provider->slug;
                $filterArr['provider']['items'][$provider->id]['url'] = Filters::add('proizvoditel', $provider->slug, '/brands');
                $filterArr['provider']['items'][$provider->id]['is_checked'] = in_array($provider->slug, $provider_id) ? 1 : 0;
            }
        }
        return $filterArr;
    }

    public static function getBrandsPresentFilter($requestArr)
    {
        $dictionaries = ShopProductsDictionariesValue::whereDictionaryId(4)->get();
        $lines = ProductsLine::all()->where('status', 1);

        $filterItemsGlobal = explode('/', $requestArr);
        $filterItems = [];
        $filterResultArr = [];
        if ($requestArr !== null) {
            $filterItemsGlobal = explode('/', $requestArr);
            $filterItems = [];
            foreach ($filterItemsGlobal as $filterItemsGlobalItem) {
                $filterItems[] = explode('_', $filterItemsGlobalItem);
            }
            foreach ($filterItems as $filterItem) {
                $filterResultArr[$filterItem[0]] = explode('-or-', $filterItem[1]);
            }
        }
        Product::$filter = $filterResultArr;
        $kind_id = Arr::get($filterResultArr, 'vid', false) !== false ? Arr::get($filterResultArr, 'vid') : [];
        $lines_id = Arr::get($filterResultArr, 'lines', false) !== false ? Arr::get($filterResultArr, 'lines') : [];

        $filterArr['product']['title'] = __('custom.product-title');
        $filterArr['product']['name_prefix'] = 'product';
        $filterArr['product']['is_open'] = sizeof($kind_id) > 0 ? true : false;
        foreach ($dictionaries as $dictionary) {
            if ($dictionary->status == 0) {
                continue;
            }
            $filterArr['product']['items'][$dictionary->id]['text'] = $dictionary->current->name;
            $filterArr['product']['items'][$dictionary->id]['slug'] = 'vid_' . $dictionary->slug;
            $filterArr['product']['items'][$dictionary->id]['url'] = Filters::add('vid', $dictionary->slug, '/brand/' . \Route::getCurrentRoute()->parameter('slug') . '/products');
            $filterArr['product']['items'][$dictionary->id]['is_checked'] = in_array($dictionary->slug, $kind_id) ? 1 : 0;
        }

        $filterArr['lines']['title'] = __('custom.line');
        $filterArr['lines']['name_prefix'] = 'lines';
        $filterArr['lines']['is_open'] = sizeof($lines_id) > 0 ? true : false;
        foreach ($lines as $line) {
            $filterArr['lines']['items'][$line->id]['text'] = $line->current->name;
            $filterArr['lines']['items'][$line->id]['slug'] = 'lines_' . $line->slug;
            $filterArr['lines']['items'][$line->id]['url'] = Filters::add('lines', $line->slug, '/brand/' . \Route::getCurrentRoute()->parameter('slug') . '/products');
            $filterArr['lines']['items'][$line->id]['is_checked'] = in_array($line->slug, $lines_id) ? 1 : 0;
        }
        return $filterArr;
    }

    public static function getWhereBuyFilter()
    {
        $filterArr = [];
        $regions = MapRegion::getList(1000);
        $lines = ProductsLine::getList(1000, 1);

        $filterArr[1]['title'] = __('custom.filter.regions');
        $filterArr[1]['name_prefix'] = 'regions';
        $filterArr[1]['is_open'] = false;
        $iteratorChild = 0;
        foreach ($regions as $region) {
            $filterArr[1]['items'][$iteratorChild]['text'] = $region->name;
            $filterArr[1]['items'][$iteratorChild]['slug'] = 'regions_' . $region->row_id;
            $filterArr[1]['items'][$iteratorChild]['is_checked'] = $region->is_checked == 1 ? true : false;
            $iteratorChild++;
        }

        $filterArr[2]['title'] = __('custom.filter.locations');
        $filterArr[2]['name_prefix'] = 'locations';
        $filterArr[2]['is_open'] = false;
        $filterArr[2]['items'][0]['text'] = __('custom.filter.locations_0');
        $filterArr[2]['items'][0]['slug'] = 'locations_' . 0;
        $filterArr[2]['items'][0]['is_checked'] = false;
        $filterArr[2]['items'][0]['hint']['color'] = '#643336';
        $filterArr[2]['items'][0]['hint']['icon'] = 'shop';
        $filterArr[2]['items'][1]['text'] = __('custom.filter.locations_1');
        $filterArr[2]['items'][1]['slug'] = 'locations_' . 1;
        $filterArr[2]['items'][1]['is_checked'] = false;
        $filterArr[2]['items'][1]['hint']['color'] = '#00dcda';
        $filterArr[2]['items'][1]['hint']['icon'] = 'glass';
        $filterArr[2]['items'][2]['text'] = __('custom.filter.locations_2');
        $filterArr[2]['items'][2]['slug'] = 'locations_' . 2;
        $filterArr[2]['items'][2]['is_checked'] = false;
        $filterArr[2]['items'][2]['hint']['color'] = '#000';
        $filterArr[2]['items'][2]['hint']['icon'] = 'cart';

        $filterArr[2]['items'][3]['text'] = __('custom.filter.locations_3');
        $filterArr[2]['items'][3]['slug'] = 'locations_' . 3;
        $filterArr[2]['items'][3]['is_checked'] = false;
        $filterArr[2]['items'][3]['hint']['color'] = '#a7a8aa';
        $filterArr[2]['items'][3]['hint']['icon'] = 'gas-station-icon';

        $filterArr[3]['title'] = __('custom.filter.lines');
        $filterArr[3]['name_prefix'] = 'lines';
        $filterArr[3]['is_open'] = false;
        $iteratorChild = 0;
        foreach ($lines as $line) {
            $filterArr[3]['items'][$iteratorChild]['text'] = $line->current->name;
            $filterArr[3]['items'][$iteratorChild]['slug'] = 'lines_' . $line->id;
            $filterArr[3]['items'][$iteratorChild]['is_checked'] = false;
            $iteratorChild++;
        }
        return $filterArr;
    }

    public static function getFilteredProducts($products, $specifications, $priceForm, $priceTo)
    {
        $specificationAllList = ShopProductsDictionariesValue::getList();
        $countriesAllList = ShopCountry::getList(10000);
        $providersAllList = ShopProvider::getList(1000);
        $linesAllList = ProductsLine::getList(1000);
        $regionsAllList = ShopRegion::getList(1000);
        $providersAllListArr = [];
        $linesAllListArr = [];
        $countriesAllListArr = [];
        $regionsAllListArr = [];
        $specificationAllListArr = [];
        $specificationNewArr = [];
        foreach ($specificationAllList as $specificationAllListItem) {
            $specificationAllListArr[$specificationAllListItem->slug] = $specificationAllListItem->id;
        }
        foreach ($countriesAllList as $countriesAllListItem) {
            $countriesAllListArr[$countriesAllListItem->slug] = $countriesAllListItem->id;
        }
        foreach ($providersAllList as $providersAllListItem) {
            $providersAllListArr[$providersAllListItem->slug] = $providersAllListItem->id;
        }
        foreach ($linesAllList as $linesAllListItem) {
            $linesAllListArr[$linesAllListItem->slug] = $linesAllListItem->id;
        }
        foreach ($regionsAllList as $regionsAllListItem) {
            $regionsAllListArr[$regionsAllListItem->slug] = $regionsAllListItem->id;
        }
        foreach ($specifications as $key => $values) {
            if (sizeof($values) == 0) {
                $specificationNewArr[$key] = [];
            } else {
                foreach ($values as $value) {
                    if ($key == 'country_id') {
                        $specificationNewArr[$key][] = Arr::get($countriesAllListArr, $value);
                    } elseif ($key == 'provider_id') {
                        $specificationNewArr[$key][] = Arr::get($providersAllListArr, $value);
                    } elseif ($key == 'lines_id') {
                        $specificationNewArr[$key][] = Arr::get($linesAllListArr, $value);
                    } elseif ($key == 'year') {
                        $specificationNewArr[$key][] = $value;
                    } elseif ($key == 'volume') {
                        $specificationNewArr[$key][] = $value;
                    } elseif ($key == 'region') {
                        $specificationNewArr[$key][] = Arr::get($regionsAllListArr, $value);
                    } else {
                        $specificationNewArr[$key][] = Arr::get($specificationAllListArr, $value, []);
                    }
                }
            }
        }
        $specifications = $specificationNewArr;
        $filteredProducts = [];
        $cList = 0;
        foreach ($specifications AS $specification) {
            if (empty($specification) === false) {
                $cList++;
            }
        }
        foreach ($products as $product) {
            $equal = 0;
            foreach ($specifications as $key => $specification) {
                if ($key == 'sort_id' || $key == 'year' || $key == 'volume' || $key == 'region') {
                    continue;
                }
                if (in_array($product->$key, $specification) === true) {
                    $equal++;
                }
            }
            foreach ($specifications as $key => $specification) {
                if ($key == 'sort_id') {
                    foreach ($product->sorts as $sort) {
                        if (in_array($sort->sort_id, $specification) === true) {
                            $equal++;
                            break;
                        }
                    }
                }
                if ($key == 'year') {
                    foreach ($product->prices as $vintage) {
                        if (in_array($vintage->year, $specification) === true) {
                            $equal++;
                            break;
                        }
                    }
                }
                if ($key == 'volume') {
                    foreach ($product->prices as $vintage) {
                        if (in_array($vintage->volume, $specification) === true) {
                            $equal++;
                            break;
                        }
                    }
                }
                if ($key == 'region') {
                    foreach ($product->regions as $item) {
                        if (in_array($item, $specification) === true) {
                            $equal++;
                            break;
                        }
                    }
                }
                if ($key == 'lines_id') {
                    if (in_array($product->line_id, $specification) === true) {
                        $equal++;
                    }
                }
                if ($key == 'compilation_id') {
                    if ($product->compilation_id == $specification) {
                        $equal++;
                    }
                }
            }
            if ($equal == $cList) {
                if ($priceForm !== null && $priceTo !== null) {
                    if ($product->checked_price->price >= $priceForm && $product->checked_price->price <= $priceTo) {
                        $filteredProducts[] = $product;
                    }
                } else {
                    $filteredProducts[] = $product;
                }
            }
        }
        return $filteredProducts;
    }

    public static function add($spec, $value, $route)
    {
        $filter = Product::$filter;
        if (array_key_exists($spec, $filter) === false) {
            $filter[$spec] = [];
        }
        if (in_array($value, $filter[$spec]) === true) {
            unset($filter[$spec][array_search($value, $filter[$spec])]);
            if (empty($filter[$spec])) {
                unset($filter[$spec]);
            }
        } else {
            // add
            $filter[$spec][] = $value;
        }
        ksort($filter);
        // method to generate url
        $url = '';
        foreach ($filter as $key => $value) {
            $url .= $key . '_' . implode('-or-', $value) . '/';
        }
        $fullPath = sizeof($filter) > 0 ? $route . '/filter/' . $url : $route . $url;
        return $fullPath;
    }

    public static function filterAliases()
    {
        $vintages = ProductsPrice::all()->sortBy('year');
        $dictionaries = ShopProductsDictionary::with(['dictionaryValues'])->whereStatus(1)->get();
        $countries = ShopCountry::with(['products'])->where('status', 1)->get();
        $providers = ShopProvider::all();
        $regions = ShopRegion::all()->where('status', 1)->sortBy('name');
        $aliasesArr = [];
        $aliasesArr['god'] = __('custom.year');
        $aliasesArr['obem'] = __('custom.scope');
        $aliasesArr['strana'] = __('custom.country');
        $aliasesArr['region'] = __('custom.region');
        $aliasesArr['proizvoditel'] = __('custom.provider');
        foreach ($vintages as $vintage) {
            $aliasesArr[$vintage->year] = $vintage->year;
            $aliasesArr[$vintage->volume] = $vintage->volume;
        }
        foreach ($dictionaries as $dictionary) {
            $aliasesArr[$dictionary->slug] = $dictionary->current->name;
            foreach ( $dictionary->dictionaryValues as $dictionaryValue) {
                $aliasesArr[$dictionaryValue->slug] = $dictionaryValue->current->name;
            }
        }
        foreach ($countries as $country) {
            $aliasesArr[$country->slug] = $country->current->name;
        }
        foreach ($providers as $provider) {
            $aliasesArr[$provider->slug] = $provider->current->name;
        }
        foreach ($regions as $region) {
            $aliasesArr[$region->slug] = $region->current->name;
        }
        return $aliasesArr;
    }

}
