<?php namespace App\Helpers;

use Auth;
use Request;

class FilterUrl
{
    public static function makeSort($param)
    {
        $url = Request::url();
        $query = \Request::query();
        $query['sort'] = $param;
        $getParams = '';
        $getParams .= '?';
        foreach ($query as $key => $item) {
            $getParams .= "$key=$item&";
        }
        if (substr($getParams, -1) == '&') {
            $getParams = substr($getParams, 0, -1);
        }

        return $url . $getParams;
    }

    public static function makeLimit($param)
    {
        $url = Request::url();
        $query = \Request::query();
        $query['limit'] = $param;
        $getParams = '';
        $getParams .= '?';
        foreach ($query as $key => $item) {
            $getParams .= "$key=$item&";
        }
        if (substr($getParams, -1) == '&') {
            $getParams = substr($getParams, 0, -1);
        }

        return $url . $getParams;
    }
}
