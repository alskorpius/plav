<?php namespace App\Helpers;

use App\Models\SeoLink;
use App\Models\SeoTemplate;
use SEO;

class Meta
{

    const ARTICLES_TEMPLATE = 1;

    /**
     * @var object
     */
    static $object;

    /**
     * @var array
     */
    static $simple;

    /**
     * @var SeoTemplate
     */
    static $template;

    /**
     * @var SeoLink
     */
    static $highLevel;

    /**
     * @var array
     */
    static $breadcrumbs = [];

    /**
     * Обычные мета данные
     *
     * @param array $data
     */
    public static function simple(array $data)
    {
        static::$simple['name'] = array_get($data, 'name');
        static::$simple['h1'] = array_get($data, 'h1') ?: array_get($data, 'name');
        if (request()->query('page') !== null) {
            static::$simple['title'] = array_get($data, 'title') ? array_get($data, 'title') . ' Страница ' . request()->query('page') : array_get($data, 'name') . ' Страница ' .request()->query('page');
        } else {
            static::$simple['title'] = array_get($data, 'title') ?: array_get($data, 'name');
        }
        static::$simple['description'] = array_get($data, 'description');
        static::$simple['keywords'] = array_get($data, 'keywords');
        static::$simple['content'] = array_get($data, 'content');
    }

    /**
     * Шаблон мета данных
     *
     * @param int $id
     * @param object $object
     */
    public static function template($id, $object)
    {
        $template = SeoTemplate::whereId($id)->whereStatus(true)->first();
        if ($template && $template->exists) {
            static::$template = $template;
        }
        static::$object = $object;
    }

    /**
     * Мета теги по ссылке
     */
    public static function highLevel()
    {
        $link = SeoLink::whereUrl(request()->server('REQUEST_URI'))->whereStatus(true)->first();
        if ($link && $link->exists) {
            static::$highLevel = $link;
        }
    }

    /**
     * Формируем шапку с мета тегами
     *
     * @return string
     */
    public static function generate()
    {
        SEO::metatags()->setTitle(static::getTitle());
        if (!stristr(request()->fullUrl(), 'page')) {
            SEO::metatags()->setDescription(static::getDescription());
            SEO::metatags()->setKeywords(static::getKeywords());
        }

        return SEO::generate();
    }

    /**
     * Формируем title
     *
     * @return mixed|string
     */
    public static function getTitle()
    {
        if (static::$highLevel && static::$highLevel->exists) {
            return static::$highLevel->title;
        }
        if (!array_get(static::$simple, 'title')) {
            if (static::$template && static::$template->exists) {
                if (count(static::$template->params)) {
                    return str_replace(
                        static::$template->params_keys,
                        static::$template->paramsValues(static::$object),
                        static::$template->title
                    );
                } else {
                    return static::$template->title;
                }
            }
        }
        return array_get(static::$simple, 'title');
    }

    /**
     * Формируем description
     *
     * @return mixed|string
     */
    public static function getDescription()
    {
        if (static::$highLevel && static::$highLevel->exists) {
            return static::$highLevel->description;
        }
        if (!array_get(static::$simple, 'description')) {
            if (static::$template && static::$template->exists) {
                if (count(static::$template->params)) {
                    return str_replace(
                        static::$template->params_keys,
                        static::$template->paramsValues(static::$object),
                        static::$template->description
                    );
                } else {
                    return static::$template->description;
                }
            }
        }
        return array_get(static::$simple, 'description');
    }

    /**
     * Формируем keywords
     *
     * @return mixed|string
     */
    public static function getKeywords()
    {
        if (static::$highLevel && static::$highLevel->exists) {
            return static::$highLevel->keywords;
        }
        if (!array_get(static::$simple, 'keywords')) {
            if (static::$template && static::$template->exists) {
                if (count(static::$template->params)) {
                    return str_replace(
                        static::$template->params_keys,
                        static::$template->paramsValues(static::$object),
                        static::$template->keywords
                    );
                } else {
                    return static::$template->keywords;
                }
            }
        }
        return array_get(static::$simple, 'keywords');
    }

    /**
     * Формируем h1
     *
     * @return mixed|string
     */
    public static function getH1()
    {
        if (static::$highLevel && static::$highLevel->exists) {
            return static::$highLevel->h1 ?: array_get(static::$simple, 'name');
        }
        if (!array_get(static::$simple, 'h1')) {
            if (static::$template && static::$template->exists) {
                if (count(static::$template->params)) {
                    return str_replace(
                        static::$template->params_keys,
                        static::$template->paramsValues(static::$object),
                        static::$template->h1
                    ) ?: array_get(static::$simple, 'name');
                } else {
                    return static::$template->h1 ?: array_get(static::$simple, 'name');
                }
            }
        }
        return array_get(static::$simple, 'h1') ?: array_get(static::$simple, 'name');
    }

    /**
     * Формируем content
     *
     * @return mixed|string
     */
    public static function getContent()
    {
        if (static::$highLevel && static::$highLevel->exists) {
            return static::$highLevel->content;
        }
        if (!array_get(static::$simple, 'content')) {
            if (static::$template && static::$template->exists && trim(strip_tags(static::$template->content))) {
                if (count(static::$template->params)) {
                    return str_replace(
                        static::$template->params_keys,
                        static::$template->paramsValues(static::$object),
                        static::$template->content
                    );
                } else {
                    return static::$template->content;
                }
            }
        }
        return array_get(static::$simple, 'content');
    }

    /**
     * Добавляем элемент в хлебные крошки
     *
     * @param string $name
     * @param string|null $url
     */
    public static function addBreadcrumb($name, $url = null, $lang = true)
    {
        array_push(static::$breadcrumbs, ['name' => $name, 'url' => $url]);
    }

    public static function showBreadcrumbs()
    {
        if (count(static::$breadcrumbs) <= 1) {
            return '';
        }
        return view('site.widgets.breadcrumbs', ['breadcrumbs' => static::$breadcrumbs])->render();
    }

}
