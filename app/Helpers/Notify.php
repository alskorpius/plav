<?php namespace App\Helpers;

use App\Notifications\NotifyInterface;

class Notify
{

    public static function send(NotifyInterface $object)
    {
        return $object->run();
    }

}