<?php namespace App\Helpers;

use Illuminate\Support\Arr;

class Html
{
    /**
     * @var  array  preferred order of attributes
     */
    public static $attribute_order = array
    (
        'action',
        'method',
        'type',
        'id',
        'name',
        'value',
        'href',
        'src',
        'width',
        'height',
        'cols',
        'rows',
        'size',
        'maxlength',
        'rel',
        'media',
        'accept-charset',
        'accept',
        'tabindex',
        'accesskey',
        'alt',
        'title',
        'class',
        'style',
        'selected',
        'checked',
        'readonly',
        'disabled',
    );


    /**
     * @var  boolean  use strict XHtml mode?
     */
    public static $strict = TRUE;


    /**
     * Convert special characters to Html entities. All untrusted content
     * should be passed through this method to prevent XSS injections.
     *
     *     echo Html::chars($username);
     *
     * @param   string $value string to convert
     * @param   boolean $double_encode encode existing entities
     * @return  string
     */
    public static function chars($value, $double_encode = TRUE)
    {
        return htmlspecialchars((string)$value, ENT_QUOTES, 'UTF-8', $double_encode);
    }


    /**
     * Compiles an array of Html attributes into an attribute string.
     * Attributes will be sorted using Html::$attribute_order for consistency.
     *
     *     echo '<div'.Html::attributes($attrs).'>'.$content.'</div>';
     *
     * @param   array $attributes attribute list
     * @return  string
     */
    public static function attributes(array $attributes = NULL)
    {
        if (empty($attributes))
            return '';

        $sorted = array();
        foreach (Html::$attribute_order as $key) {
            if (isset($attributes[$key])) {
                // Add the attribute to the sorted list
                $sorted[$key] = $attributes[$key];
            }
        }

        // Combine the sorted attributes
        $attributes = $sorted + $attributes;

        $compiled = '';
        foreach ($attributes as $key => $value) {
            if ($value === NULL) {
                // Skip attributes that have NULL values
                continue;
            }

            if (is_int($key)) {
                // Assume non-associative keys are mirrored attributes
                $key = $value;

                if (!Html::$strict) {
                    // Just use a key
                    $value = FALSE;
                }
            }

            // Add the attribute key
            $compiled .= ' ' . $key;

            if ($value OR Html::$strict) {
                // Add the attribute value
                $compiled .= '="' . Html::chars($value) . '"';
            }
        }

        return $compiled;
    }


    /**
     * Формируем svg иконку на основании переданных параметров
     *
     * @param $icon
     * @param null $sClass
     * @return string
     */
    public static function svg($icon, $sClass = null)
    {
        $sClass = $sClass ? "class=\"svgi svgi--{$icon} {$sClass}\"" : "class=\"svgi svgi--{$icon}\"";

        return sprintf('<svg %s><use xlink:href="%s"></use></svg>', $sClass, asset("svg/sprite.svg#{$icon}"));
    }


    /**
     * @param $text
     * @param array $options
     * @return string
     */
    public static function button(array $options = [])
    {

        $tag = Arr::get($options, 'tag', 'button');
        $mainClass = Arr::get($options, 'mainClass', 'button');
        $modifierDelimiter = Arr::get($options, 'modifierDelimiter', '--');
        $type = Arr::get($options, 'type', 'button');
        $icon = Arr::get($options, 'icon', '');
        $rightIcon = Arr::get($options, 'rightIcon', false);
        $leftIcon = Arr::get($options, 'leftIcon', false);
        $class = Arr::get($options, 'class', '');
        $attrs = Arr::get($options, 'attrs', []);
        $href = Arr::get($options, 'href', '');
        $text = Arr::get($options, 'text', '');
        $modifiers = Arr::get($options, 'modifiers', []);

        if (!$rightIcon && $icon) {
            $rightIcon = $icon;
        }

        if ($tag === 'button') {
            $attrs['type'] = $type;
        } elseif ($tag === 'a') {
            $attrs['href'] = $href ?: ($attrs['href'] ?: '#');
        }

        $modifiers = is_array($modifiers) ? $modifiers : [$modifiers];

        if (!!$modifiers) {
            $modifierClasses = $mainClass . $modifierDelimiter . trim(implode(' ' . $mainClass . $modifierDelimiter, $modifiers));
        } else {
            $modifierClasses = '';
        }

        $buttonAttrs = Html::attributes(['class' => implode(' ', [$mainClass, $modifierClasses, $class])] + $attrs);
        $leftIcon = $leftIcon ? Html::svg($leftIcon) : '';
        $rightIcon = $rightIcon ? Html::svg($rightIcon) : '';
        $text = strlen($text) > 0   ? "<span>{$text}</span>" : '';
        return "<$tag {$buttonAttrs}><span>{$leftIcon}{$text}{$rightIcon}</span></{$tag}>";

    }

    /**
     * @param $mainClass
     * @param $modifiers
     */
    public static function generateClassWithModifiers($mainClass, $modifiers = [])
    {
        $modifiers = is_array($modifiers) ? $modifiers : [$modifiers];

        return implode(' ', array_merge([$mainClass], array_map(function ($modifier) use ($mainClass) {
            return $mainClass . '--' . $modifier;
        }, $modifiers)));
    }

}
