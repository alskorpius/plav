<?php namespace App\Helpers;

use App\Models\MailTemplate;
use Mail;
use Exception;

class Email
{

    static $testRecipient = 'komarov2809@gmail.com';
    //    static $testRecipient = ['test@gmail.com', 'admin@gmail.com'];

    public static function template($templateAlias, $recipient, array $data)
    {
        $recipient = $recipient ?: config('db.basic.admin_email');
        $template = MailTemplate::whereAlias($templateAlias)->whereStatus(true)->first();
        if ($template) {
            $keys = array_keys($data);
            foreach ($keys AS $key => $value) {
                $keys[$key] = '{' . $value . '}';
            }
            $subject = str_replace($keys, array_values($data), $template->subject);
            $content = str_replace($keys, array_values($data), $template->content);
            return static::send($recipient, $subject, $content);
        }
        return false;
    }

    public static function send($recipient, $subject, $content)
    {
        if (!trim(strip_tags($content)) || !$subject) {
            return false;
        }
        $recipient = env('MAIL_TEST', false) ? static::$testRecipient : $recipient;
        try {
            Mail::raw($content, function($message) use ($recipient, $subject)
            {
                $message->subject($subject);
                $message->to($recipient);
            });
        } catch (Exception $e) {
            //
        }
    }

    public static function templateInView($templateAlias, $recipient, array $data = [], $view = null, array $viewData = [])
    {
        $recipient = $recipient ?: config('db.basic.admin_email');
        $template = MailTemplate::whereAlias($templateAlias)->whereStatus(true)->first();
        if ($template) {
            $keys = array_keys($data);
            foreach ($keys AS $key => $value) {
                $keys[$key] = '{' . $value . '}';
            }
            $subject = str_replace($keys, array_values($data), $template->current->subject);
            $viewData = $viewData ?: $data;
            $viewData['text'] = str_replace($keys, array_values($data), $template->current->content);
            return static::sendView($recipient, $subject, $view, $viewData);
        }
        return false;
    }

    public static function sendView($recipient, $subject, $view = null, $data = [])
    {
        if (!$subject) {
            return false;
        }
        $data = is_array($data) ? $data : ['text' => $data];
        $recipient = env('MAIL_TEST', false) ? static::$testRecipient : $recipient;
        try {
            Mail::send($view ?: 'emails.general', $data, function ($message) use ($recipient, $subject) {
                $message->subject($subject);
                $message->to($recipient);
            });
        } catch (Exception $e) {
            //
        }
    }

}
