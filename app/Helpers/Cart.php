<?php namespace App\Helpers;

use Auth;
use App\Models\Cart\Cart AS ShoppingCart;
use App\Models\Cart\CartsItem;
use Illuminate\Support\Facades\Cookie;

class Cart
{

	/**
	 * @var ShoppingCart|null
	 */
	private static $instance = null;

	/**
	 * Существует ли корзина
	 *
	 * @return bool
	 */
	public static function loaded()
	{
		return static::$instance !== null;
	}

	/**
	 * Проверяем корзину на пустоту
	 *
	 * @return bool
	 */
	public static function isEmpty()
	{
		if (static::loaded() === false) {
			return true;
		}
		$count = 0;
		foreach (static::instance()->items AS $item) {
			$count += $item->count;
		}
		return $count > 0 ? false : true;
	}

	/**
	 * Корзина
	 *
	 * @return ShoppingCart|null
	 */
	public static function instance()
	{
		return static::$instance;
	}

	/**
	 * Достаем объект товара в корзине или создаем новый
	 *
	 * @param array $input
	 * @param $hash
	 * @return CartsItem|\Illuminate\Database\Eloquent\Builder
	 */
	public static function item(array $input)
	{
		$model = CartsItem::whereProductId(array_get($input, 'product_id'))
			->wherePriceId(array_get($input, 'price_id', null))
			->whereCartId(Cart::instance()->id)
			->first();

		if ($model === null) {
			$model = new CartsItem();
			$model->cart_id = Cart::instance()->id;
			$model->product_id = array_get($input, 'product_id');
			$model->price_id = array_get($input, 'price_id');
			$model->count = 1;
			$model->save();
		} else {
			$model->count++;
			$model->save();
		}
		return $model;
	}

	/**
	 * Устанавливаем корзину
	 *
	 * @param $cart
	 * @return mixed
	 */
	public static function set($cart)
	{
		return static::$instance = $cart;
	}

	public static function reload()
	{
		if (static::loaded()) {
			return static::$instance = static::load(static::$instance->hash);
		}
		static::create();
		return static::$instance;
	}

	/**
	 * Создаем новую корзину
	 */
	public static function create()
	{
		$realCart = (new \App\Models\Cart\Cart())->createRow();
		static::set($realCart);
	}

	/**
	 * Создаем/загружаем корзину
	 *
	 * @param $hash
	 *
	 * @internal param $cartPublicId
	 * @return null
	 */
	public static function load($hash)
	{
		$realCart = null;
		if ($hash) {
			$realCart = ShoppingCart::whereHash($hash)->with('items')->first();
		} elseif (Auth::check()) {
			$realCart = ShoppingCart::whereUserId(Auth::user()->id)->with('items')->first();
		}
		if ($realCart) {
			static::set($realCart);
		} else {
			static::create();
		}
		return $realCart;
	}

	/**
	 * Объект корзины для АПИ
	 *
	 * @return array|null
	 */
	public static function api()
	{
		if (static::loaded() === false) {
			return null;
		}
		$cart = static::instance()->api();
		$cart['total_amount'] = self::amount(); // Общее количество товаров в корзине
		$cart['total_weight'] = self::totalWeight(); // Общий вес товаров
		return $cart;
	}

	/**
	 * Очищаем корзину пользователя
	 */
	public static function clear()
	{
		foreach (self::instance()->items AS $item) {
			$item->delete();
		}
	}

	/**
	 * Считаем сумму корзины
	 * @return int
	 */
	public static function amount()
	{
		if (static::isEmpty()) {
			return 0;
		}
		$cartSum = 0;
		foreach (Cart::instance()->items as $item) {
		    if ($item->price && $item->price->exists) {
                $cartSum += $item->price->price * $item->count;
            }
		}
		return $cartSum;
	}

	/**
	 * Считаем сумму корзины
	 * @return int
	 */
	public static function totalWeight()
	{
		if (static::isEmpty()) {
			return 0;
		}
		$cartWeight = 0;
		foreach (Cart::instance()->items as $item) {
            if ($item->price && $item->price->exists) {
			    $cartWeight += (float) $item->price->weight * $item->count;
            }

        }
		return $cartWeight;
	}

    /**
     * Считаем количество товаров корзины
     * @return int
     */
    public static function totalCount()
    {
        if (static::isEmpty()) {
            return 0;
        }
        $cartCount = 0;
        foreach (Cart::instance()->items as $item) {
            $cartCount += $item->count;

        }
        return $cartCount;
    }

}
