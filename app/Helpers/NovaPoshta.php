<?php namespace App\Helpers;

use Illuminate\Support\Str;
use LisDev\Delivery\NovaPoshtaApi2;

class NovaPoshta
{
    /**
     * @var string
     */
    protected static $key = '0220eac8b90be2e405a1e1d225ec72a0';

    /**
     * @return array
     */
    public static function getCities()
    {
        $np = new NovaPoshtaApi2(self::$key);
        $cities = $np->getCities();
//        $i = 0;
//        foreach ($cities['data'] as $city){
//            $cities['data'][$i]['DescriptionEn'] = Str::ascii($city['DescriptionRu']);
//            $i++;
//        }
        $citiesArr = [];
        $citiesArr[''] = __('custom.form.city');
        $citiesArr['8d5a980d-391c-11dd-90d9-001a92567626'] = 'Киев';
        foreach ($cities['data'] as $city) {
            if ($city['Ref'] == '8d5a980d-391c-11dd-90d9-001a92567626') {
                continue;
            }
            $citiesArr[$city['Ref']] = $city['DescriptionRu'];
        }
        return $citiesArr;
    }

    /**
     * @return array
     */
    public static function getCitiesForProfile()
    {
        $np = new NovaPoshtaApi2(self::$key);
        $cities = $np->getCities();
        $citiesArr = [];
        $citiesArr[''] = __('custom.form.city');
        $citiesArr['8d5a980d-391c-11dd-90d9-001a92567626'] = 'Киев';
        foreach ($cities['data'] as $city) {
            if ($city['Ref'] == '8d5a980d-391c-11dd-90d9-001a92567626') {
                continue;
            }
            $citiesArr[$city['DescriptionRu']] = $city['DescriptionRu'];
        }
        return $citiesArr;
    }

    public static function getCity($ref)
    {
        $np = new NovaPoshtaApi2(self::$key);
        $result = $np
            ->model('Address')
            ->method('getCities')
            ->params(array(
                'Ref' => $ref,
            ))
            ->execute();
        return $result['data'][0]['DescriptionRu'];
    }

    public static function getWarehouse($ref)
    {
        $np = new NovaPoshtaApi2(self::$key);
        $result = $np
            ->model('Address')
            ->method('getWarehouses')
            ->params(array(
                'Ref' => $ref,
            ))
            ->execute();
        return isset($result['data'][0]['DescriptionRu']) ? $result['data'][0]['DescriptionRu'] : 0;
    }

    /**
     * @param $ref
     * @return mixed
     */
    public static function getWarehousesByCityRef($ref)
    {
        $np = new NovaPoshtaApi2(self::$key);
        $wareHouses = $np->getWarehouses($ref);

        $wareHousesArr = [];
        $wareHousesArr[''] = __('custom.checkout.delivery-NP-ware');

        foreach ($wareHouses['data'] as $wareHouse) {
            $wareHousesArr[$wareHouse['Ref']] = $wareHouse['DescriptionRu'];
        }
        return $wareHousesArr;
    }

    /**
     * Доставка в отделение
     * @param $ref
     * @param $weight
     * @param $price
     * @return mixed
     */
    public static function getDeliveryPriceW2WbyCityRef($ref, $weight, $price, $count = 1)
    {
        $bottles_1_3 = (int) config('db.pack_amount.1-3-bottles');
        $bottles_4_5 = (int) config('db.pack_amount.4-5-bottles');
        $bottles_6 = (int) config('db.pack_amount.6-bottles');
        $additionalPayment = (int) config('db.pack_amount.hand-payment');
        $packSum = 0;
        if ($count >= 1 && $count <= 3) {
            $packCount = $count == 3 ? 2 : 1;
            $packSum = $packCount * $bottles_1_3;
        } else if ($count >= 4 && $count <= 5) {
            $packSum = $bottles_4_5;
        } else if ($count == 6) {
            $packSum = $bottles_6;
        } else if ($count > 6) {
            $packCount = intval($count/6);
            $otherPart = $count%6;
            $packSum = $bottles_6 * $packCount;
            if ($otherPart >= 4) {
                $packSum += $bottles_4_5;
            } elseif ($otherPart == 3) {
                $packSum += $bottles_1_3 * 2;
            } elseif ($otherPart <= 2 && $otherPart!== 0) {
                $packSum += $bottles_1_3;
            }
        }
        $np = new NovaPoshtaApi2(self::$key);
        $sender_city = $np->getCity('Киев', 'Киевская');
        $sender_city_ref = $sender_city['data'][0]['Ref'];
        $recipient_city_ref = $ref;
        $result = $np->getDocumentPrice($sender_city_ref, $recipient_city_ref, 'WarehouseWarehouse', $weight, $price);
        return isset($result['data'][0]['Cost']) ? $result['data'][0]['Cost'] + $packSum : 0;
    }

    /**
     * Доставка курьером
     * @param $ref
     * @param $weight
     * @param $price
     * @return mixed
     */
    public static function getDeliveryPriceW2DbyCityRef($ref, $weight, $price, $count = 1)
    {
        $bottles_1_3 = (int) config('db.pack_amount.1-3-bottles');
        $bottles_4_5 = (int) config('db.pack_amount.4-5-bottles');
        $bottles_6 = (int) config('db.pack_amount.6-bottles');
        $additionalPayment = (int) config('db.pack_amount.hand-payment');
        $packSum = 0;
        if ($count >= 1 && $count <= 3) {
            $packCount = $count == 3 ? 2 : 1;
            $packSum = $packCount * $bottles_1_3;
        } else if ($count >= 4 && $count <= 5) {
            $packSum = $bottles_4_5;
        } else if ($count == 6) {
            $packSum = $bottles_6;
        } else if ($count > 6) {
            $packCount = intval($count/6);
            $otherPart = $count%6;
            $packSum = $bottles_6 * $packCount;
            if ($otherPart >= 4) {
                $packSum += $bottles_4_5;
            } elseif ($otherPart == 3) {
                $packSum += $bottles_1_3 * 2;
            } elseif ($otherPart <= 2 && $otherPart!== 0) {
                $packSum += $bottles_1_3;
            }
        }

        $np = new NovaPoshtaApi2(self::$key);
        $sender_city = $np->getCity('Киев', 'Киевская');
        $sender_city_ref = $sender_city['data'][0]['Ref']   ;
        $recipient_city_ref = $ref;
        $result = $np->getDocumentPrice($sender_city_ref, $recipient_city_ref, 'WarehouseDoors', $weight, $price);
        return isset($result['data'][0]['Cost']) ? $result['data'][0]['Cost'] + $packSum : 0;
    }

    /**
     * @param $ttn
     * @return mixed|null
     */
    public static function getBillByTTN($ttn)
    {
        $np = new NovaPoshtaApi2(self::$key);
        return $np->documentsTracking($ttn) ? $np->documentsTracking($ttn) : null;
    }

    public static function getTotalSumDelivery($ref, $weight, $price, $deliveryType, $paymentType, $count = 1)
    {
        $bottles_1_3 = (int) config('db.pack_amount.1-3-bottles');
        $bottles_4_5 = (int) config('db.pack_amount.4-5-bottles');
        $bottles_6 = (int) config('db.pack_amount.6-bottles');
        $additionalPayment = (int) config('db.pack_amount.hand-payment');
        $additionalPaymentAddress = (int) config('db.pack_amount.address-payment');
        $packSum = 0;
        if ($count >= 1 && $count <= 3) {
            $packCount = $count == 3 ? 2 : 1;
            $packSum = $packCount * $bottles_1_3;
        } else if ($count >= 4 && $count <= 5) {
            $packSum = $bottles_4_5;
        } else if ($count == 6) {
            $packSum = $bottles_6;
        } else if ($count > 6) {
            $packCount = intval($count/6);
            $otherPart = $count%6;
            $packSum = $bottles_6 * $packCount;
            if ($otherPart >= 4) {
                $packSum += $bottles_4_5;
            } elseif ($otherPart == 3) {
                $packSum += $bottles_1_3 * 2;
            } elseif ($otherPart <= 2 && $otherPart!== 0) {
                $packSum += $bottles_1_3;
            }
        }

        $np = new NovaPoshtaApi2(self::$key);
        $sender_city = $np->getCity('Киев', 'Киевская');
        $sender_city_ref = $sender_city['data'][0]['Ref'];
        $recipient_city_ref = $ref;
        $newPrice = $paymentType == 0 ? $price : 0;

        if ($deliveryType == 0) {
            $result = $np->getDocumentPrice($sender_city_ref, $recipient_city_ref, 'WarehouseWarehouse', $weight, $newPrice);
            $result =  isset($result['data'][0]['Cost']) ? $result['data'][0]['Cost'] + $packSum  : 0;
            $resultTotal = $paymentType == 0 ? $result + $additionalPayment : $result;
        } else {
            $result = $np->getDocumentPrice($sender_city_ref, $recipient_city_ref, 'WarehouseDoors', $weight, $newPrice);
            $result =  isset($result['data'][0]['Cost']) ? $result['data'][0]['Cost'] + $packSum  : 0;
            $resultTotal = $paymentType == 0 ? $result + $additionalPaymentAddress  : $result;
        }
        return $resultTotal;
    }

    public static function getTotalSumDeliveryNew($ref, $weight, $price, $deliveryType, $paymentType, $packType)
    {
        $np = new NovaPoshtaApi2(self::$key);
        $sender_city = $np->getCity('Киев', 'Киевская');
        $sender_city_ref = $sender_city['data'][0]['Ref'];
        $recipient_city_ref = $ref;
        $newPrice = $paymentType == 0 ? $price : 0;
        $serviceType = '';
        if ($deliveryType == 0) {
            $serviceType = 'WarehouseWarehouse';
        } else {
            $serviceType = 'WarehouseDoors';
        }
        $result = $np
            ->model('InternetDocument')
            ->method('getDocumentPrice')
            ->params([
                'CitySender' => $sender_city_ref,
                'CityRecipient' => $recipient_city_ref,
                'Weight' => $weight,
                'ServiceType' => $serviceType,
                'Cost' => $newPrice,
                'CargoType' => 'Cargo',
                'SeatsAmount' => '1',
                'PackCalculate' => [
                    'PackCount' => '1',
                    'PackRef' => $packType,
                ],
                'RedeliveryCalculate' => [
                    'CargoType' => 'Money',
                    'Amount' => $newPrice,
                ],
            ])
            ->execute();
        return isset($result['data'][0]['Cost']) ? $result['data'][0]['Cost'] : 0;
    }

    /**
     * Доставка в отделение новое
     * @param $ref
     * @param $weight
     * @param $price
     * @return mixed
     */
    public static function getDeliveryPriceW2WbyCityRefNew($ref, $weight, $price, $count)
    {
        $packCount = 1;
        $pacrRef = '';
        if ($count >= 1 && $count <= 3) {
            $pacrRef = 'ab0cd73f-da19-11e1-aa18-d4ae527baec9';  // - Коробка 2 кг с пенопластом
            $packCount = $count == 3 ? 2 : 1;
        } else if ($count >= 4 && $count <= 5) {
            $pacrRef = 'eb43d9a5-c206-46df-ba8d-3e3947457efc'; // - Пенопластnpm
        } else if ($count == 6) {
            $pacrRef = 'eb43d9a5-c206-46df-ba8d-3e3947457efc'; // - Пенопласт
        } else if ($count > 6) {

        }

        $np = new NovaPoshtaApi2(self::$key);
        $sender_city = $np->getCity('Киев', 'Киевская');
        $sender_city_ref = $sender_city['data'][0]['Ref'];
        $recipient_city_ref = $ref;
        $result = $np->getDocumentPrice($sender_city_ref, $recipient_city_ref, 'WarehouseWarehouse', $weight, $price);
        return isset($result['data'][0]['Cost']) ? $result['data'][0]['Cost'] : 0;
    }


    public static function getPackList($length = '', $width = '', $height = '', $typeOfPacking = '')
    {
        $np = new NovaPoshtaApi2(self::$key);
        return $np
            ->model('Common')
            ->method('getPackList')
            ->params([
                'Length' => $length,
                'Width' => $width,
                'Height' => $height,
                'TypeOfPacking' => $typeOfPacking,
            ])
            ->execute();
    }
}
