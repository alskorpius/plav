<?php namespace App\Helpers;

use App\Exceptions\WrongParametersException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Request;
use Image;
use File;

/**
 * Class Files
 * @package App\Helpers
 */
class Files
{

    /**
     * Image saving... by custom settings
     * @param Request|\Illuminate\Http\Request $request
     * @param array $settings
     * @param string $folder
     * @param string $name
     * @return null|string
     * @throws WrongParametersException
     */
    public static function uploadImage($request, array $settings, $folder, $name = 'image')
    {

        if (!($request instanceof Request) && !($request instanceof \Illuminate\Http\Request)) {
            throw new WrongParametersException('Parameter `request` is not an instance of Request');
        }
        if ($request->hasFile($name) === false) {
            return null;
        }
        $settings = array_get($settings, $folder, []);
        $image = $request->file($name);
        if ($image->isFile() === false || $image->isValid() === false) {
            return null;
        }
        $imageName = md5($image->getClientOriginalName() . $folder . Str::random(20) . time()) . '.' . $image->guessExtension();
        $mainDir = public_path('uploads/images/' . $folder);
        foreach ($settings AS $subFolder => $setting) {
            $file = Image::make($image->getRealPath());
            $dir = $mainDir . '/' . $subFolder;
            File::makeDirectory($dir, 0755, true, true);
            $newWidth = array_get($setting, 'width');
            $newHeight = array_get($setting, 'height');
            if (array_get($setting, 'crop') && $newWidth && $newHeight) {
                $file->fit($newWidth, $newHeight, function ($constraint) {
                    $constraint->upsize();
                });
            } elseif (array_get($setting, 'resize')) {
                if ($newWidth && $newHeight) {
                    $file->widen($newWidth, function ($constraint) {
                        $constraint->upsize();
                    });
                    if ($file->height() > $newHeight) {
                        $file->heighten($newHeight, function ($constraint) {
                            $constraint->upsize();
                        });
                    }
                } elseif ($newWidth) {
                    $file->widen($newWidth, function ($constraint) {
                        $constraint->upsize();
                    });
                } elseif ($newHeight) {
                    $file->heighten($newHeight, function ($constraint) {
                        $constraint->upsize();
                    });
                }
            }
            if (array_get($setting, 'watermark') && is_file(public_path(config('watermark.path')))) {
                $watermark = Image::make(public_path(config('watermark.path')));
                $watermark->widen(config('watermark.width') * $file->width());
                $watermark->opacity(config('watermark.opacity'));
                $file->insert($watermark, config('watermark.position'), config('watermark.x'), config('watermark.y'));
            }
            $file->save($dir . '/' . $imageName, 100);
        }
        return $imageName;
    }

    /**
     * Загружаем фото по объекту файла
     *
     * @param UploadedFile $image
     * @param array $settings
     * @param $folder
     * @param string $name
     * @return null|string
     */
    public static function uploadImageWithObject(UploadedFile $image, array $settings, $folder, $name = 'image', $filename = null)
    {
        $settings = array_get($settings, $folder, []);
        if ($image->isFile() === false || $image->isValid() === false) {
            return null;
        }
        if ($filename !== null) {
            $imageName = $filename;
        } else {
            $imageName = md5($image->getClientOriginalName() . $folder . Str::random(20) . time()) . '.' . $image->guessExtension();
        }
        $mainDir = public_path('uploads/images/' . $folder);
        foreach ($settings AS $subFolder => $setting) {
            $file = Image::make($image->getRealPath());
            $dir = $mainDir . '/' . $subFolder;
            File::makeDirectory($dir, 0755, true, true);
            $newWidth = array_get($setting, 'width');
            $newHeight = array_get($setting, 'height');
            if (array_get($setting, 'crop') && $newWidth && $newHeight) {
                $file->fit($newWidth, $newHeight, function ($constraint) {
                    $constraint->upsize();
                });
            } elseif (array_get($setting, 'resize')) {
                if ($newWidth && $newHeight) {
                    $file->widen($newWidth, function ($constraint) {
                        $constraint->upsize();
                    });
                    if ($file->height() > $newHeight) {
                        $file->heighten($newHeight, function ($constraint) {
                            $constraint->upsize();
                        });
                    }
                } elseif ($newWidth) {
                    $file->widen($newWidth, function ($constraint) {
                        $constraint->upsize();
                    });
                } elseif ($newHeight) {
                    $file->heighten($newHeight, function ($constraint) {
                        $constraint->upsize();
                    });
                }
            }
            if (array_get($setting, 'watermark') && is_file(public_path(config('watermark.path')))) {
                $watermark = Image::make(public_path(config('watermark.path')));
                $watermark->widen(config('watermark.width') * $file->width());
                $watermark->opacity(config('watermark.opacity'));
                $file->insert($watermark, config('watermark.position'), config('watermark.x'), config('watermark.y'));
            }
            $file->save($dir . '/' . $imageName);
        }
        return $imageName;
    }

    /**
     * @param $folder
     * @param $settings
     * @param $imageName
     */
    public static function deleteImage($folder, $settings, $imageName)
    {
        $basePath = public_path("uploads/images/{$folder}");
        foreach ($settings AS $subFolder => $setting) {
            @unlink("{$basePath}/{$subFolder}/{$imageName}");
        }
    }

}
