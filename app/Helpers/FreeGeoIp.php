<?php namespace App\Helpers;

use Request;

class FreeGeoIp
{
    static $url = 'http://freegeoip.net/json/';

    public static function get($ip = null)
    {
        $ip = $ip ?: Request::ip();

        $ch = curl_init(static::$url . $ip);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $output = curl_exec($ch);

        if( $output === false ){
            throw new \ErrorException( curl_error($ch) );
        }

        curl_close($ch);

        return json_decode($output, true);
    }

}