<?php namespace App\Helpers;

use App;
use App\Exceptions\WrongParametersException;
use Illuminate\Support\Str;

class Language
{

    /**
     * Меняем язык сайта
     *
     * @param $lang
     * @return array|string
     * @throws WrongParametersException
     */
    public static function switchTo($lang)
    {
        $language = config('languages.' . $lang, false);
        if (is_string($lang) === false || $language === false || is_array($language) === false) {
            throw new WrongParametersException('Языка [' . $lang . '] не существует в приложении');
        }
        if (array_get($language, 'slug') === App::getLocale()) {
            return request()->server('REQUEST_URI');
        }
        if (App::getLocale() === config('app.fallback_locale')) {
            return '/' . trim($lang . request()->server('REQUEST_URI'), '/');
        }
        if (array_get($language, 'default', false) === true) {
            return '/' . trim(Str::replaceFirst('/' . App::getLocale(), '', request()->server('REQUEST_URI')), '/');
        }
        return '/' . trim(Str::replaceFirst('/' . App::getLocale(), $lang, request()->server('REQUEST_URI')), '/');
    }

    /**
     * Формируем ссылку с учетом языковой версии сайта.
     * Скорее всего будет использоваться только для меню
     *
     * @param $url
     * @return string
     */
    public static function url($url)
    {
        if (App::getLocale() === config('app.fallback_locale')) {
            return '/' . trim($url, '/');
        }
        return '/' . App::getLocale() . '/' . trim($url, '/');
    }

    /**
     * @return array
     */
    public static function AlphabetArr()
    {
        return [
            'en' => ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
            'ru' => ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я']
        ];
    }

}
