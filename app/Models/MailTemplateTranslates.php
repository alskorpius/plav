<?php namespace App\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MailTemplateTranslates
 *
 * @package App\Models
 * @property integer $id
 * @property string $subject
 * @property string $content
 * @property string $language
 * @property integer $row_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MailTemplate whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MailTemplate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MailTemplate whereSubject($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\MailTemplate $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MailTemplateTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MailTemplateTranslates whereRowId($value)
 */
class MailTemplateTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'mail_templates_translates';

    public $timestamps = false;

    protected $fillable = ['subject', 'content'];

    public function rules()
    {
        return [
            'subject' => ['required', 'max:191'],
            'content' => ['required'],
        ];
    }

}
