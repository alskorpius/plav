<?php

namespace App\Models;

use App\Traits\HumanDate;
use App\Traits\ModelImages;
use App\Traits\ModelMain;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

/**
 * Class Pages
 *
 * @package App\Models
 * @property integer $id
 * @property string $slug
 * @property integer $status
 * @property string $image
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $date
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\NewsTranslates $current
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\NewsTranslates[] $data
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News published()
 * @property-read mixed $human_date
 * @property-read mixed $summary
 */
class News extends Model
{

    use ModelImages, HumanDate;
    use ModelMain;

    protected $table = 'news';

    protected $fillable = ['slug', 'date', 'image', 'status'];

    protected $dates = ['date'];

    protected $casts = [
        'status' => 'boolean',
    ];

    /**
     * @inheritdoc
     */
    protected function imagesFolders() {
        return [
            'news' => [
                'small' => [
                    'width' => 295,
                    'height' => 300,
                    'resize' => true,
                    'crop' => true,
                    'watermark' => false,
                ],
                'original' => [],
            ],
        ];
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function _rules() {
        return [
            'slug' => ['required', 'alpha_dash', 'max:191', Rule::unique($this->getTable())->ignore($this->id)],
            'date' => ['required', 'date'],
            'status' => ['required', 'boolean'],
            'image' => ['sometimes', 'image', 'max:' . config('custom.image-max-size')],
        ];
    }

    /**
     * @param array $ids
     * @param null $status
     * @return array|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getListByIds(array $ids, $status = null) {
        if (!$ids) {
            return [];
        }
        $news = static::whereIn('id', $ids);
        if ($status !== null) {
            $news->whereStatus($status);
        }
        return $news->latest('date')->get();
    }

    /**
     * Краткое описание статьи
     *
     * @return string
     */
    public function getSummaryAttribute() {
        return Str::limit(strip_tags($this->content), 120, '...');
    }

    /**
     * @return string
     */
    public function getHumanDateAttribute() {
        return $this->date->format('j ' . $this->shortNameOfTheMonth($this->date->format('n')) . ' Y');
    }

    /**
     * @param $limit
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getList($limit, $status = null) {
        $pages = self::with(['current'])->latest('date');
        if (request()->input('name', false) !== false) {
            $pages
                ->join('news_translates', 'news.id', '=', 'news_translates.row_id')
                ->where('news_translates.name', 'like', '%' . request()->input('name') . '%')
                ->where('news_translates.language', '=', \Lang::getLocale());
        }

        if (request()->input('date', false) !== false) {
            $pages->where('date', '=', request()->input('date'));
        }

        if ($status !== null) {
            $pages->whereStatus($status);
        }

        return $pages->paginate($limit);
    }

    /**
     * Формируем данные для АПИ
     *
     * @return array
     */
    public function api() {
        return [
            'id' => $this->id,
            'name' => $this->current->name,
            'slug' => $this->slug,
            'image' => $this->getImage('small'),
            'date' => $this->human_date,
            'summary' => $this->summary,
        ];
    }

    /**
     * Мета даныне
     *
     * @return array
     */
    public function meta() {
        // Мета теги по дефолту
        $meta = [
            'h1' => $this->h1,
            'title' => $this->title,
            'description' => $this->description,
            'keywords' => $this->keywords,
            'content' => $this->content,
        ];
        // Используем шаблон мета тегов для статтей
        if ($template = SeoTemplate::getTemplate(SeoTemplate::ARTICLES)) {
            if (!$meta['h1'] && $template->h1) {
                $meta['h1'] = str_replace(['{name}'], [$this->name], $template->h1);
            }
            if (!$meta['title'] && $template->title) {
                $meta['title'] = str_replace(['{name}'], [$this->name], $template->title);
            }
            if (!$meta['description'] && $template->description) {
                $meta['description'] = str_replace(['{name}'], [$this->name], $template->description);
            }
            if (!$meta['keywords'] && $template->keywords) {
                $meta['keywords'] = str_replace(['{name}'], [$this->name], $template->keywords);
            }
        }
        return $meta;
    }

    /**
     * Список статтей для возврата по АПИ
     *
     * @param $limit
     * @param null|bool $status
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getItems($limit, $status = null) {
        $article = static::where('date', '<=', Carbon::now());
        if ($status !== null) {
            $article->whereStatus((bool)$status);
        }
        return $article->where('date', '<=', Carbon::now())
            ->latest('date')
            ->paginate($limit);
    }

    /**
     * Достаем одну статью по алиасу страницы
     *
     * @param $slug
     * @param null|bool $status
     * @return Model|static
     */
    public static function getItemBySlug($slug, $status = null) {
        $article = static::whereSlug($slug);
        if ($status !== null) {
            $article->whereStatus((bool)$status);
        }
        return $article->where('date', '<=', Carbon::now())->firstOrFail();
    }

}
