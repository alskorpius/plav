<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PagesRole
 *
 * @property int $id
 * @property int $page_id
 * @property int $role_id
 * @property bool $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\AdminPage $page
 * @property-read \App\Models\UsersRole $role
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PagesRole whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PagesRole whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PagesRole wherePageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PagesRole whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PagesRole whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PagesRole whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PagesRole extends Model
{
    protected $table = 'pages_roles';

    protected $fillable = ['page_id', 'role_id', 'status'];

    protected $casts = [
        'status' => 'boolean',
    ];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page_id' => ['required'],
            'role_id' => ['required'],
            'status' => ['required', 'boolean'],
        ];
    }

    public function role()
    {
        return $this->hasOne(UsersRole::class, 'id', 'role_id');
    }

    public function page()
    {
        return $this->hasOne(AdminPage::class, 'id', 'page_id');
    }
}
