<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SettingsType
 *
 * @package App\Models
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Settings[] $settings
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SettingsType whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SettingsType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SettingsType whereName($value)
 */
class SettingsType extends Model
{
    protected $table = 'settings_types';

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function settings()
    {
        return $this->hasMany(Settings::class, 'type', 'alias')
            ->orderBy('position');
    }
}
