<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Crop
 *
 * @package App\Models
 * @property integer $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $x
 * @property string $y
 * @property string $width
 * @property string $height
 * @property integer $model_id
 * @property string $model_name
 * @property string $size
 * @property string $folder
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Crop whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Crop whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Crop whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Crop whereModelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Crop whereModelName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Crop whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Crop whereWidth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Crop whereX($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Crop whereY($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Crop whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Crop whereFolder($value)
 */
class Crop extends Model
{
    protected $table = 'crop';

    protected $fillable = ['x', 'y', 'width', 'height'];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'width' => ['required', 'numeric', 'min:1'],
            'height' => ['required', 'numeric', 'min:1'],
            'x' => ['required', 'numeric', 'min:0'],
            'y' => ['required', 'numeric', 'min:0'],
        ];
    }
}
