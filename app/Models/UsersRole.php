<?php

namespace App\Models;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

/**
 * Class Slider
 *
 * @package App\Models
 * @property integer $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $name
 * @property string $alias
 * @property User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsersRole whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsersRole whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsersRole whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsersRole whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsersRole whereAlias($value)
 * @mixin \Eloquent
 */
class UsersRole extends Model
{
    protected $table = 'users_roles';

    protected $fillable = ['name', 'alias'];

    const ADMIN = 'administrator';

    const USER = 'user';

    const MANAGER = 'manager';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class, 'role_alias', 'alias');
    }

    public function rules()
    {
        return [
            'name' => ['required', 'min:2', 'max:191'],
            'alias' => ['required', 'alpha_dash', 'max:191', Rule::unique($this->table)->ignore($this->id)],
        ];
    }

}
