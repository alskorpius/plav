<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Events_registration
 *
 * @package App\Models
 * @property int $id
 * @property int $article_id
 * @property string $name
 * @property bool $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Article[] $articles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventsRegistration whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventsRegistration whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventsRegistration whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventsRegistration whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventsRegistration whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventsRegistration whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $phone
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventsRegistration wherePhone($value)
 * @property string $email
 * @property-read \App\Models\Article $article
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventsRegistration whereEmail($value)
 */
class EventsRegistration extends Model
{
    protected $table = 'events_registrations';
    protected $fillable = ['article_id', 'name', 'phone', 'email', 'status'];
    protected $casts = [
        'status' => 'boolean',
    ];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:191'],
            'email' => ['required', 'email'],
            'phone' => ['nullable','numeric'],
        ];
    }

    /**
     * @param $limit
     * @param null $type
     * @param null $status
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList($limit, $status = null)
    {
        $pages = self::oldest('created_at');

        if (request()->input('name', false) !== false) {
            $pages->where('name', 'like', '%' . request()->input('name') . '%');
        }

        if (request()->input('email', false) !== false) {
            $pages->where('email', 'like', '%' . request()->input('email') . '%');
        }

        if (request()->input('phone', false) !== false) {
            $pages->where('phone', 'like', '%' . request()->input('phone') . '%');
        }

        if (request()->input('event', false) !== false) {
            $pages->where('article_id', request()->input('event'));
        }

        if (request()->input('created_at', false) !== false) {
            $pages->where('created_at', 'like', '%' . request()->input('created_at') . '%');
        }

        if ($status !== null) {
            $pages->whereStatus($status);
        }

        return $pages->paginate($limit);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function article()
    {
        return $this->hasOne(Article::class, 'id', 'article_id');
    }
}
