<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Comment
 *
 * @property int $id
 * @property bool $status
 * @property int $position
 * @property string $text
 * @property int $article_id
 * @property int $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Article $article
 * @property-read \App\Models\User $user
 * @property int $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereType($value)
 * @property string|null $user_name
 * @property string|null $answer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereUserName($value)
 * @property string|null $email
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereEmail($value)
 */
class Comment extends Model
{
    const TYPE_NO = 0;
    const TYPE_YES = 1;
    const TYPE_IN_PROCESS = 2;

    protected $table = 'comments';

    protected $fillable = ['position', 'status', 'text', 'article_id', 'user_id', 'user_name', 'answer'];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'text' => ['required'],
        ];
    }

    /**
     * Получаем комментарии
     * @param null $idArticle
     * @param null $idUser
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList($limit, $type = null,$idArticle = null, $idUser = null)
    {
        $comments = self::orderBy('updated_at', 'DESC');

        if (request()->input('text', false) !== false) {
            $comments->where('text', 'like', '%' . request()->input('text') . '%');
        }

        if (request()->input('created_at', false) !== false) {
            $comments->where('created_at', 'like', '%' . request()->input('created_at') . '%');
        }

        if (request()->input('status', false) !== false) {
            $comments->where('status', 'like', '%' . request()->input('status') . '%');
        }

        if (request()->input('article', false) !== false) {
            $articles = Article::getListByName(request()->input('article'));
            foreach ($articles as $article) {
                $comments->orWhere('article_id', 'like', $article->id );
            }
        }

        if (request()->input('articleType', false) !== false) {
            $articles = Article::getList(40,request()->input('articleType'));
            foreach ($articles as $article) {
                $comments->orWhere('article_id', '=', $article->id);
            }
        }

        if ($idArticle !== null){
            $comments->where('article_id', $idArticle);
        }
        if ($idUser !== null){
            $comments->where('user_id', $idUser);
        }
        if ($type !== null){
            $comments->where('type', $type);
        }
        return $comments->paginate($limit);
    }

    /**
     * @param null $status
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getItems($status = null)
    {
        $slides = static::oldest('position');
        if ($status !== null) {
            $slides->whereStatus((bool)$status);
        }
        return $slides->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function article() {
        return $this->hasOne(Article::class, 'id', 'article_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}
