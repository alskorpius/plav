<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

/**
 * Class Language
 *
 * @package App\Models
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property boolean $default
 * @property string $google_slug
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Language whereGoogleSlug($value)
 * @mixin \Eloquent
 */
class Language extends Model
{
    public $timestamps = false;

    protected $table = 'languages';

    protected $casts = [
        'default' => 'boolean',
    ];

    protected $fillable = ['name', 'default'];

    public function rules()
    {
        return [
            'name' => ['required', 'min:1', 'max:191'],
            'default' => ['required', 'boolean'],
        ];
    }
}
