<?php

namespace App\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MenuTranslates
 *
 * @package App\Models
 * @property integer $id
 * @property string $name
 * @property string $language
 * @property integer $row_id
 * @property-read \App\Models\Menu $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuTranslates whereRowId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 */
class MenuTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'menus_translates';

    public $timestamps = false;

    protected $fillable = ['name'];

    public function rules()
    {
        return [
            'name' => ['required', 'max:191'],
        ];
    }
}
