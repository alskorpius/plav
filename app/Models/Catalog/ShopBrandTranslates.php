<?php namespace App\Models\Catalog;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ShopBrandTranslates
 *
 * @package App\Models\Shop
 * @property integer $id
 * @property string $name
 * @property string|null $h1
 * @property string|null $title
 * @property string|null $keywords
 * @property string|null $description
 * @property string|null $content
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property integer $row_id
 * @property string $language
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopBrandTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopBrandTranslates whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopBrandTranslates whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopBrandTranslates whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopBrandTranslates whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopBrandTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopBrandTranslates whereTitle($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\Catalog\ShopBrand $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopBrandTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopBrandTranslates whereRowId($value)
 */
class ShopBrandTranslates extends Model
{
    use ModelTranslates;

    public $timestamps = false;

    protected $table = 'shop_brands_translates';

    protected $fillable = ['name', 'content', 'h1', 'title', 'keywords', 'description'];

    public function rules()
    {
        return [
            'name' => ['required', 'max:191'],
        ];
    }
}
