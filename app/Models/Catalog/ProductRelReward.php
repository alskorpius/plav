<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Catalog\ProductRelReward
 *
 * @property int $id
 * @property int $product_id
 * @property int $reward_id
 * @property int $points
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Catalog\Product $product
 * @property-read \App\Models\Catalog\ProductsCompilation $value
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductRelReward whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductRelReward whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductRelReward whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductRelReward whereCompilationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductRelReward whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ProductRelReward extends Model
{
    protected $table = 'products_rewards_rel';

    protected $fillable = ['product_id', 'reward_id', 'points'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => ['required', 'numeric'],
            'reward_id' => ['required', 'numeric'],
        ];
    }

}
