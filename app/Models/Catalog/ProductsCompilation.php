<?php

namespace App\Models\Catalog;

use App\Models\SeoTemplate;
use App\Traits\ModelMain;
use App\Traits\ModelImages;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

/**
 * App\Models\Catalog\ProductsCompilation
 *
 * @property int $id
 * @property string $slug
 * @property string $image
 * @property bool $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Catalog\ProductsCompilationTranslates $current
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsCompilation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsCompilation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsCompilation whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsCompilation whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsCompilation whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsCompilation whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ProductsCompilation[] $products
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ProductsCompilationTranslates[] $data
 */
class ProductsCompilation extends Model
{
    use ModelImages;
    use ModelMain;

    protected $table = 'products_compilations';

    protected $fillable = ['image', 'status', 'slug'];


    protected $casts = [
        'status' => 'boolean',
    ];

    /**
     * @inheritdoc
     */
    protected function imagesFolders()
    {
        return [
            'compilations' => [
                'small' => [
                    'width' => 350,
                    'height' => 350,
                    'resize' => true,
                    'crop' => true,
                    'watermark' => false,
                ],
                'original' => [],
            ],
        ];
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function _rules()
    {
        return [
            'slug' => ['required', 'alpha_dash', 'max:191', Rule::unique($this->getTable())->ignore($this->id)],
            'status' => ['required', 'boolean'],
            'image' => ['sometimes', 'image', 'max:' . config('custom.image-max-size')],
        ];
    }

    /**
     * @param null $status
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @internal param $limit
     */
    public static function getList($limit = null, $status = null)
    {
        $pages = static::with(['current'])->oldest('products_compilations.created_at');
        if (request()->input('name', false) !== false) {
            $pages
                ->join('products_compilations_translates', 'products_compilations.id', '=', 'products_compilations_translates.row_id')
                ->where('products_compilations_translates.name', 'like', '%' . request()->input('name') . '%')
                ->where('products_compilations_translates.language', '=', \Lang::getLocale());
        }

        if ($status !== null) {
            $pages->whereStatus($status);
        }

        if ($limit == null) {
            return $pages->get();
        }
        return $pages->paginate($limit);
    }

    /**
     * Мета даныне
     *
     * @return array
     */
    public function meta()
    {
        // Мета теги по дефолту
        $meta = [
            'h1' => $this->current->h1,
            'title' => $this->current->title,
            'description' => $this->current->description,
            'keywords' => $this->current->keywords,
            'content' => $this->current->content,
        ];
        // Используем шаблон мета тегов для статтей
        if ($template = SeoTemplate::getTemplate(SeoTemplate::COMPILATIONS)) {
            if (!$meta['h1'] && $template->h1) {
                $meta['h1'] = str_replace(['{name}'], [$this->name], $template->h1);
            }
            if (!$meta['title'] && $template->title) {
                $meta['title'] = str_replace(['{name}'], [$this->name], $template->title);
            }
            if (!$meta['description'] && $template->description) {
                $meta['description'] = str_replace(['{name}'], [$this->name], $template->description);
            }
            if (!$meta['keywords'] && $template->keywords) {
                $meta['keywords'] = str_replace(['{name}'], [$this->name], $template->keywords);
            }
        }
        return $meta;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(ProductsCompilation::class, 'compilation_id', 'id');
    }
}
