<?php

namespace App\Models\Catalog;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Catalog\ProductsPresentation
 *
 * @property int $id
 * @property string|null $text
 * @property string|null $color
 * @property string|null $aroma
 * @property string|null $taste
 * @property string|null $combination
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $language
 * @property integer $row_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentationTranslates whereAroma($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentationTranslates whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentationTranslates whereCombination($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentationTranslates whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentationTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentationTranslates whereTaste($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentationTranslates whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\Catalog\ProductsPresentation $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentationTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentationTranslates whereRowId($value)
 */
class ProductsPresentationTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'products_presentations_translates';

    protected $fillable = ['text', 'color', 'aroma', 'taste', 'combination'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => ['required'],
            'color' => ['required'],
            'aroma' => ['required'],
            'taste' => ['required'],
            'combination' => ['required'],
        ];
    }
}
