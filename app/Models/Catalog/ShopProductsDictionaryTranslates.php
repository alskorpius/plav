<?php

namespace App\Models\Catalog;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Catalog\ShopProductsDictionaryTranslates
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property integer $row_id
 * @property string $language
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionaryTranslates whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionaryTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionaryTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionaryTranslates whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\Catalog\ShopProductsDictionary $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionaryTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionaryTranslates whereRowId($value)
 */
class ShopProductsDictionaryTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'shop_products_dictionaries_translates';

    protected $fillable = ['name'];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:191'],
        ];
    }
}
