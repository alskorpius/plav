<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Catalog\ProductSort
 *
 * @property int $id
 * @property int|null $product_id
 * @property int|null $sort_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Catalog\Product $product
 * @property-read \App\Models\Catalog\ShopProductsDictionariesValue $value
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductSort whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductSort whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductSort whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductSort whereSortId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductSort whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ProductSort extends Model
{
	protected $table = 'product_sorts';

	protected $fillable = ['product_id', 'sort_id'];

	/**
	 * Правила валидации
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'product_id' => ['required', 'numeric'],
			'sort_id' => ['numeric'],
		];
	}

	public function product()
	{
		return $this->hasOne(Product::class, 'id', 'product_id');
	}

	public function value()
	{
		return $this->hasOne(ShopProductsDictionariesValue::class, 'id', 'sort_id');
	}
}
