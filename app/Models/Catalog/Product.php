<?php

namespace App\Models\Catalog;

use App\Helpers\Cart;
use App\Models\Article;
use App\Models\Cart\CartsItem;
use App\Models\Providers\ProvidersRegion;
use App\Models\Providers\ShopCountry;
use App\Models\Providers\ShopProvider;
use App\Models\Providers\ShopRegion;
use App\Models\SeoTemplate;
use App\Models\User;
use App\Traits\ModelImages;
use App\Traits\ModelMain;
use Cookie;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;
use ProductDictionariesValues;

/**
 * App\Models\Catalog\Product
 *
 * @property int $id
 * @property string $slug
 * @property string|null $image
 * @property bool $status
 * @property int $position
 * @property int $position_front
 * @property int|null $color_id
 * @property int|null $sweetness_id
 * @property int|null $type_id
 * @property int|null $kind_id
 * @property int|null $alcohol_id
 * @property int|null $sugar_id
 * @property int|null $brand_id
 * @property float $weight
 * @property int|null $novelty
 * @property int|null $sale
 * @property string $artikul
 * @property int|null $line_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Catalog\ProductTranslates $current
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ProductsCompilation[] $products
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereAlcoholId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereArtikul($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereColorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereCompilationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product wherePositionFront($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereKindId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereLineId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereNovelty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereSugarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereSweetnessId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereWeight($value)
 * @mixin \Eloquent
 * @property int|null $sort_id
 * @property-read \App\Models\Catalog\ShopProductsDictionariesValue $alcohol
 * @property-read \App\Models\Catalog\ShopBrand $brand
 * @property-read \App\Models\Catalog\ShopProductsDictionariesValue $color
 * @property-read \App\Models\Catalog\ProductsCompilation $compilation
 * @property-read \App\Models\Catalog\ShopProductsDictionariesValue $kind
 * @property-read \App\Models\Catalog\ProductsLine $line
 * @property-read \App\Models\Catalog\ShopProductsDictionariesValue $sort
 * @property-read \App\Models\Catalog\ShopProductsDictionariesValue $sugar
 * @property-read \App\Models\Catalog\ShopProductsDictionariesValue $sweetness
 * @property-read \App\Models\Catalog\ShopProductsDictionariesValue $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereSortId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ProductsPrice[] $prices
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Article[] $articles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ProductsImage[] $images
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cart\CartsItem[] $cartItems
 * @property-read mixed $checked_price
 * @property-read mixed $country
 * @property-read mixed $has_sale
 * @property int|null $country_id
 * @property int|null $provider_id
 * @property-read \App\Models\Providers\ShopProvider $provider
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereProviderId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ProductsReviews[] $reviews
 * @property-read mixed $rating
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ProductSort[] $sorts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ProductRelCompilation[] $compilations
 * @property int|null $is_avilable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\Product whereIsAvilable($value)
 * @property-read \App\Models\Catalog\ProductsPresentation $presentation
 * @property-read mixed $alcohol_item
 * @property-read mixed $color_item
 * @property-read mixed $kind_item
 * @property-read mixed $reviews_arr
 * @property-read mixed $sort_items
 * @property-read mixed $sweetness_item
 * @property-read mixed $type_item
 * @property-read mixed $in_cart
 * @property-read mixed $in_favorites
 * @property-read mixed $in_wait
 * @property-read mixed $regions
 * @property-read mixed $status_classes
 * @property-read mixed $recommendation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ProductTranslates[] $data
 */
class Product extends Model
{
    use ModelImages, ModelMain;
    protected $table = 'products';

    static $filter = [];

    protected $fillable = ['slug', 'position', 'position_front', 'is_avilable', 'image', 'status', 'is_accessory', 'country_id', 'provider_id', 'color_id', 'sweetness_id', 'type_id', 'kind_id', 'alcohol_id', 'sugar_id', 'brand_id', 'weight', 'novelty', 'line_id'];

    const AVILABLE_YES = 1;
    const AVILABLE_NO = 0;
    const AVILABLE_WAIT = 2;
    /**
     * @var ShopProductsDictionariesValue[]
     */
    public $valuesArr = [];

    protected $casts = [
        'status' => 'boolean',
        'is_accessory' => 'boolean',
        'novelty' => 'boolean',
    ];

    protected function imagesFolders()
    {
        return [
            'products' => [
                'icon' => [ // Иконки в корзине
                    'width' => 70,
                    'height' => 70,
                    'resize' => true,
                    'crop' => true,
                    'watermark' => false,
                ],
                'icon_small' => [
                    'width' => 50,
                    'height' => 50,
                    'resize' => true,
                    'crop' => true,
                    'watermark' => false,
                ],
                'small' => [ // Карточка товара
                    'width' => 350,
                    'height' => 350,
                    'resize' => false,
                    'crop' => true,
                    'watermark' => false,
                ],
                'big' => [
                    'width' => 960,
                    'height' => 650,
                    'resize' => true,
                    'crop' => false,
                    'watermark' => false,
                ],
                'original' => [],
            ],
        ];
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function _rules()
    {
        return [
            'slug' => ['required', 'alpha_dash', 'max:191', Rule::unique($this->getTable())->ignore($this->id)],
            'status' => ['required', 'boolean'],
//            'is_accessory' => ['required', 'boolean'],
            'brand_id' => ['required', 'not_in:0'],
            'image' => ['image'],
            'position' => ['required', 'numeric', 'min:1'],
            'position_front' => ['numeric'],
        ];
    }

    /**
     * @param $limit
     * @param null $status
     * @param null $front
     * @return self
     * @internal param $limit
     */
    public function getList($limit = null, $status = null, $front = null, $new = null, $search = null, $brand = null, $compilation = null, $accessory = null)
    {
        $pages = static::with(['current'])->select($this->getTable() . '.*')->orderBy('position_front');

        if (request()->input('name', '') !== '' && request()->input('name', '') !== null) {
            $pages
                ->join('products_translates', 'products.id', '=', 'products_translates.row_id')
                ->where('products_translates.name', 'like', '%' . request()->input('name') . '%')
                ->where('products_translates.language', '=', \Lang::getLocale());
        }

        if (request()->input('brand_id', false) !== false) {
            $pages->where('brand_id', request()->input('brand_id'));
        }

        if (request()->input('line_id', false) !== false) {
            $pages->where('line_id', request()->input('line_id'));
        }


        if (request()->input('compilation_id', false) !== false) {
            $compilations = ProductRelCompilation::whereCompilationId(request()->input('compilation_id'))->get();
            $idArr = [];
            foreach ($compilations as $obj) {
                $idArr[] = $obj->product_id;
            }
            if (count($idArr)){
                $pages->whereIn('id', $idArr);
            }
        }


        if ($front !== null) {
            $pricesArr = [];
            $prices = ProductsPrice::all();

            foreach ($prices as $price) {
                $pricesArr[] = $price->product_id;
            }
            $pages->whereIn('products.id', $pricesArr);
        }

        if ($accessory !== null) {
            $pages->where('is_accessory', '=', $accessory);
        }
        
        if ($status !== null) {
            $pages->whereStatus($status);
        }

        if ($new !== null) {
            $pages->whereNovelty($new);
        }

        if ($search !== null) {
            $pages
                ->join('products_translates', 'products.id', '=', 'products_translates.row_id')
                ->where('products_translates.name', 'like', '%' . $search . '%')
                ->where('products_translates.language', '=', \Lang::getLocale());
        }

        if ($brand !== null) {
            $pages->where('brand_id', $brand);
        }

       /* if ($compilation !== null) {
            $pages->whereCompilationId($compilation);
        }*/
        if ($compilation !== null) {
            $compilations = ProductRelCompilation::whereCompilationId($compilation)->get();
            $idArr = [];
            foreach ($compilations as $obj) {
                $idArr[] = $obj->product_id;
            }
            if (count($idArr)){

                $pages->whereIn('id', $idArr);
            }
        }

        if ($limit === null) {
            $getted = $pages->get();
        } else {
            $getted = $pages->paginate($limit);
        }
        $idArr = [];
        foreach ($getted as $item) {
            $idArr[] = $item->color_id;
            $idArr[] = $item->sweetness_id;
            $idArr[] = $item->type_id;
            $idArr[] = $item->kind_id;
            $idArr[] = $item->alcohol_id;
            $idArr[] = $item->sugar_id;
        }
        $dictionaryValues = ShopProductsDictionariesValue::with(['current'])->whereIn('id', $idArr)->get()->mapWithKeys(function (ShopProductsDictionariesValue $value) {
            return [$value->id => $value];
        });
        foreach ($getted as $key => $product) {
            if ($product->current == null) {
                unset($getted[$key]);
            }else{
                $getted[$key]->valuesArr['color'] = isset($product->color_id) ? array_get($dictionaryValues, $product->color_id) : null;
                $getted[$key]->valuesArr['sweetness'] = isset($product->sweetness_id) ? array_get($dictionaryValues, $product->sweetness_id) : null;
                $getted[$key]->valuesArr['type'] = isset($product->type_id) ? array_get($dictionaryValues, $product->type_id) : null;
                $getted[$key]->valuesArr['kind'] = isset($product->kind_id) ? array_get($dictionaryValues, $product->kind_id) : null;
                $getted[$key]->valuesArr['alcohol'] = isset($product->alcohol_id) ? array_get($dictionaryValues, $product->alcohol_id) : null;
                $getted[$key]->valuesArr['sugar'] = isset($product->sugar_id) ? array_get($dictionaryValues, $product->sugar_id) : null;
            }
        }
        return $getted;
    }

    public static function getListByName($name)
    {
        $pages = self::latest('created_at')->with('current')->where('name', 'like', '%' . $name . '%');
        return $pages->get();
    }

    public function getColorAttribute()
    {
        return array_get($this->valuesArr, 'color');
    }

    public function getSweetnessAttribute()
    {
        return array_get($this->valuesArr, 'sweetness');
    }

    public function getTypeAttribute()
    {
        return array_get($this->valuesArr, 'type');
    }

    public function getKindAttribute()
    {
        return array_get($this->valuesArr, 'kind');
    }

    public function getAlcoholAttribute()
    {
        return array_get($this->valuesArr, 'alcohol');
    }

    public function sorts()
    {
        return $this->hasMany(ProductSort::class, 'product_id', 'id');
    }
    public function compilations()
    {
        return $this->hasMany(ProductRelCompilation::class, 'product_id', 'id');
    }

    public function getSugarAttribute()
    {
        return array_get($this->valuesArr, 'sugar');
    }

    public function getCountryAttribute()
    {
        if (isset($this->country_id)) {
            return ShopCountry::find($this->country_id);
        }
        return null;
    }

    public function getHasSaleAttribute()
    {
        $prices = ProductsPrice::whereProductId($this->id)->get();
        $counter = 0;
        foreach ($prices as $price) {
            if ($price->sale > 0) {
                $counter++;
            }
        }
        return $counter > 0 ? true : false;
    }

    public static function getListForFilter(
        $limit, $color_id = [], $sweetness_id = [], $type_id = [], $kind_id = [],
        $alcohol_id = [], $sugar_id = [], $country_id = [], $provider_id = [],
        $sort_id = [], $year = [], $volume = [], $region = []
    )
    {

        $products = self::oldest('created_at');
        $pricesArr = [];
        $prices = ProductsPrice::all();

        foreach ($prices as $price) {
            $pricesArr[] = $price->product_id;
        }
        $products->whereIn('id', $pricesArr);

        if (sizeof($color_id) > 0) {
            $products->whereIn('color_id', $color_id);
        }

        if (sizeof($sweetness_id) > 0) {
            $products->whereIn('sweetness_id', $sweetness_id);
        }

        if (sizeof($type_id) > 0) {
            $products->whereIn('type_id', $type_id);
        }

        if (sizeof($kind_id) > 0) {
            $products->whereIn('kind_id', $kind_id);
        }

        if (sizeof($sugar_id) > 0) {
            $products->whereIn('sugar_id', $sugar_id);
        }

        if (sizeof($alcohol_id) > 0) {
            $products->whereIn('alcohol_id', $alcohol_id);
        }

        if (sizeof($country_id) > 0) {
            $products->whereIn('country_id', $country_id);
        }

        if (sizeof($provider_id) > 0) {
            $products->whereIn('provider_id', $provider_id);
        }

        if (sizeof($sort_id) > 0) {
            $sorts = ProductSort::whereIn('sort_id', $sort_id)->get();
            $idArr = [];
            foreach ($sorts as $sort) {
                $idArr[] = $sort->product_id;

            }
            $products->whereIn('id', $idArr);
        }

        if (sizeof($year) > 0) {
            $vintages = ProductsPrice::whereIn('year', $year)->get();
            $prodIds = [];
            foreach ($vintages as $vintage) {
                $prodIds [] = $vintage->product_id;
            }
            $products->whereIn('id', $prodIds);
        }

        if (sizeof($volume) > 0) {
            $vintages = ProductsPrice::whereIn('volume', $volume)->get();
            $prodIds = [];
            foreach ($vintages as $vintage) {
                $prodIds [] = $vintage->product_id;
            }
            $products->whereIn('id', $prodIds);
        }

        if (sizeof($region) > 0) {
            $providersRegions = ProvidersRegion::whereIn('region_id', $region)->get();
            $providersRegionsIds = [];
            foreach ($providersRegions as $providersRegion) {
                $providersRegionsIds[] = $providersRegion->provider_id;
            }
            $providers = ShopProvider::whereIn('id', $providersRegionsIds)->get();
            $providersIds = [];
            foreach ($providers as $provider) {
                $providersIds[] = $provider->id;
            }
            $products->whereIn('provider_id', $providersIds);

        }
        $getted = $products->get();
        $idArr = [];
        foreach ($getted as $item) {
            $idArr[] = $item->color_id;
            $idArr[] = $item->sweetness_id;
            $idArr[] = $item->type_id;
            $idArr[] = $item->kind_id;
            $idArr[] = $item->alcohol_id;
            $idArr[] = $item->sugar_id;
        }
        $dictionaryValues = ShopProductsDictionariesValue::with(['current'])->whereIn('id', $idArr)->get()->mapWithKeys(function (ShopProductsDictionariesValue $value) {
            return [$value->id => $value];
        });
        foreach ($getted as $key => $product) {
            $getted[$key]->valuesArr['color'] = isset($product->color_id) ? array_get($dictionaryValues, $product->color_id) : null;
            $getted[$key]->valuesArr['sweetness'] = isset($product->sweetness_id) ? array_get($dictionaryValues, $product->sweetness_id) : null;
            $getted[$key]->valuesArr['type'] = isset($product->type_id) ? array_get($dictionaryValues, $product->type_id) : null;
            $getted[$key]->valuesArr['kind'] = isset($product->kind_id) ? array_get($dictionaryValues, $product->kind_id) : null;
            $getted[$key]->valuesArr['alcohol'] = isset($product->alcohol_id) ? array_get($dictionaryValues, $product->alcohol_id) : null;
            $getted[$key]->valuesArr['sugar'] = isset($product->sugar_id) ? array_get($dictionaryValues, $product->sugar_id) : null;
        }
        return $getted;
    }

    /**
     * @param $color_id
     * @param null $status
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     *
     */
    public static function getListByColor($color_id, $status = null)
    {
        $pages = self::oldest('created_at')->whereColorId($color_id);

        if ($status !== null) {
            $pages->whereStatus($status);
        }

        return $pages->get();
    }

    /**
     * @param $sweetness_id
     * @param null $status
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getListBySweetness($sweetness_id, $status = null)
    {
        $pages = self::oldest('created_at')->whereSweetnessId($sweetness_id);

        if ($status !== null) {
            $pages->whereStatus($status);
        }

        return $pages->get();
    }

    /**
     * @param $type_id
     * @param null $status
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getListByType($type_id, $status = null)
    {
        $pages = self::oldest('created_at')->whereTypeId($type_id);

        if ($status !== null) {
            $pages->whereStatus($status);
        }

        return $pages->get();
    }

    /**
     * @param $kind_id
     * @param null $status
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getListByKind($kind_id, $status = null)
    {
        $pages = self::oldest('created_at')->whereKindId($kind_id);

        if ($status !== null) {
            $pages->whereStatus($status);
        }

        return $pages->get();
    }

    /**
     * @param $alcohol_id
     * @param null $status
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getListByAlcohol($alcohol_id, $status = null)
    {
        $pages = self::oldest('created_at')->whereAlcoholId($alcohol_id);

        if ($status !== null) {
            $pages->whereStatus($status);
        }

        return $pages->get();
    }

    /**
     * @param $sugar_id
     * @param null $status
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getListBySugar($sugar_id, $status = null)
    {
        $pages = self::oldest('created_at')->whereSugarId($sugar_id);

        if ($status !== null) {
            $pages->whereStatus($status);
        }

        return $pages->get();
    }

    /**
     * @param $brand_id
     * @param null $status
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getListByBrand($brand_id, $status = null)
    {
        $pages = self::oldest('created_at')->whereBrandId($brand_id);

        if ($status !== null) {
            $pages->whereStatus($status);
        }

        return $pages->get();
    }

    public function getRegionsAttribute()
    {
        $providerRegions = ProvidersRegion::whereProviderId($this->provider_id)->get();
        $regions = [];
        foreach ($providerRegions as $region) {
            $regions[] = $region->region_id;
        }
        return $regions;
    }

    public function getRegionsFilterAttribute()
    {
        $providerRegions = ProvidersRegion::whereProviderId($this->provider_id)->get();
        $regions = [];
        foreach ($providerRegions as $region) {
            $regions[] = $region->region_id;
        }
        $regionsModel = ShopRegion::whereIn('id', $regions)->get();
        return $regionsModel;
    }

    /**
     * Мета даныне
     *
     * @return array
     */
    public function meta()
    {
        // Мета теги по дефолту
        $meta = [
            'h1' => isset($this->current->h1) ? $this->current->h1 : $this->current->name,
            'title' => isset($this->current->title) ? $this->current->title : $this->current->name,
            'description' => isset($this->current->description) ? $this->current->description : $this->current->name,
            'keywords' => isset($this->current->keywords) ? $this->current->keywords : $this->current->name,
            'content' => isset($this->current->content) ? $this->current->content : $this->current->name,
        ];
        // Используем шаблон мета тегов для статтей
        if ($template = SeoTemplate::getTemplate(SeoTemplate::PRODUCTS)) {
            if (!$meta['h1'] && $template->h1) {
                $meta['h1'] = str_replace(['{name}'], [$this->name], $template->h1);
            }
            if (!$meta['title'] && $template->title) {
                $meta['title'] = str_replace(['{name}'], [$this->name], $template->title);
            }
            if (!$meta['description'] && $template->description) {
                $meta['description'] = str_replace(['{name}'], [$this->name], $template->description);
            }
            if (!$meta['keywords'] && $template->keywords) {
                $meta['keywords'] = str_replace(['{name}'], [$this->name], $template->keywords);
            }
        }
        return $meta;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function brand()
    {
        return $this->hasOne(ShopBrand::class, 'id', 'brand_id')->with('current');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function line()
    {
        return $this->hasOne(ProductsLine::class, 'id', 'line_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    /*public function compilation()
    {
        return $this->hasOne(ProductsCompilation::class, 'id', 'compilation_id');
    }*/

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prices()
    {
        return $this->hasMany(ProductsPrice::class, 'product_id', 'id')->orderBy('price');
    }

    public function getCheckedPriceAttribute()
    {
        if (sizeof($this->prices) > 0) {
            $counter = 0;
            foreach ($this->prices as $price) {
                if ($price->is_checked !== 0 && $price->is_checked !== null) {
                    $counter++;
                }
            }
            if ($counter !== 0) {
                return $this->prices()->where('is_checked', 1)->first();
            } else {
                return $this->prices()->orderBy('price')->first();
            }
        }
        return null;
    }

    public function getRatingAttribute()
    {
        if (sizeof($this->reviews) !== 0) {
            $sum = 0;
            foreach ($this->reviews as $review) {
                $sum += (int)$review->mark;
            }
            return ceil($sum / count($this->reviews));
        }
        return 0;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(ProductsImage::class, 'product_id', 'id')->orderBy('position', 'ASC');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function articles()
    {
        return $this->belongsToMany(Article::class, 'articles_products',
            'product_id', 'article_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'users_wishlists',
            'product_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cartItems()
    {
        return $this->hasMany(CartsItem::class, 'product_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviews()
    {
        return $this->hasMany(ProductsReviews::class, 'product_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function country()
    {
        return $this->hasOne(ShopCountry::class, 'id', 'country_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function provider()
    {
        return $this->hasOne(ShopProvider::class, 'id', 'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function presentation()
    {
        return $this->hasOne(ProductsPresentation::class, 'product_id', 'id');
    }

    public function getColorItemAttribute()
    {
        if ( ($item = ShopProductsDictionariesValue::find($this->color_id)) ) {
            return $item->current;
        }
        return false;
    }

    public function getSweetnessItemAttribute()
    {
        if ( ($item = ShopProductsDictionariesValue::find($this->sweetness_id)) ) {
            return $item->current;
        }
        return false;
    }

    public function getTypeItemAttribute()
    {
        if ( ($item = ShopProductsDictionariesValue::find($this->type_id)) ) {
            return $item->current;
        }
        return false;
    }

    public function getKindItemAttribute()
    {
        if ( ($item = ShopProductsDictionariesValue::find($this->kind_id)) ) {
            return $item->current;
        }
        return false;
    }

    public function getAlcoholItemAttribute()
    {
        if ( ($item = ShopProductsDictionariesValue::find($this->alcohol_id)) ) {
            return $item->current;
        }
        return false;
    }

    public function getSortItemsAttribute()
    {
        $result = [];
        $sorts = ProductSort::whereProductId($this->id)->get();
        $sortsArr = [];
        $i = 0;
        foreach ($sorts as $sort) {
            $sortsArr[$i] = $sort->sort_id;
            $i++;
        }
        $values = ShopProductsDictionariesValue::with(['current'])->whereIn('id', $sortsArr)->get();
        $i = 0;
        foreach ($values as $value) {
            $result[$i] = $value->current->name;
            $i++;
        }
        return $result;
    }

    /**
     * @return array
     */
    public function api()
    {
        return [
            'id' => $this->id,
            'name' => $this->current->name,
            'url' => route('product', ['slug' => $this->slug]),
            'artikul' => $this->artikul,
            'image' => $this->getImage('icon'),
        ];
    }

    public function getReviewsArrAttribute()
    {
        $result = [];
        $reviews = ProductsReviews::whereProductId($this->id)->whereStatus(1)->get();
        $i = 0;
        foreach ($reviews as $review) {
            $result[$i]['name'] = $review->user_name;
            $result[$i]['date'] = $review->created_at->format('d.m.Y');
            $result[$i]['rating'] = $review->mark;
            $result[$i]['text'] = $review->text;
            $i++;
        }
        return $result;
    }

    public function getInCartAttribute()
    {
        $cartHash = Cookie::get('cart_hash');
        Cart::load($cartHash);
        $items = Cart::api();
        foreach ($items['items'] as $item) {
            if ($this->id == $item['product']['id']) {
                return true;
            }
        }
        return false;
    }

    public function getInFavoritesAttribute()
    {
        if (\Auth::check()) {
            $wishLists = UsersWishlist::whereUserId(\Auth::user()->id)->get();
            foreach ($wishLists as $wishList) {
                if ($this->id == $wishList->product_id)
                    return true;
            }
        }
        return false;
    }

    public function getInWaitAttribute()
    {
        if (\Auth::check()) {
            $productsWaits = ProductsWait::whereUserId(\Auth::user()->id)->whereProductId($this->id)->get();
            foreach ($productsWaits as $productWait) {
                if ($this->id == $productWait->product_id)
                    return true;
            }
        }
        return false;
    }

    public function getRecommendationAttribute()
    {
        $prodRecoms = ProductsRecommendation::whereParentId($this->id)->get();
        $ids = [];
        foreach ($prodRecoms as $item) {
            $ids[] = $item->product_id;
        }
        return Product::whereIn('id', $ids)->get();
    }

    public function getStatusClassesAttribute()
    {
        return implode(' ', [
            $this->in_favorites ? 'in-favorites' : '',
            $this->in_cart ? 'in-cart' : '',
            $this->in_wait ? 'in-wait' : ''
        ]);
    }

    public static function workWithBD()
    {
        $products = (new Product)->getList(null, 1, 1);
        foreach ($products as $product) {
            if ($product->current->h1 == null) {
                $product->current->h1 = ($product->kind ? $product->kind->name : '') . ' ' . $product->current->name;
            }
            if ($product->current->title == null) {
                $product->current->title = ($product->kind ? $product->kind->name : '') . ' ' . $product->current->name . ' ' . $product->checked_price->volume . 'мл' . ', купить в Украине, цена ' . $product->checked_price->price . ' грн.';
            }
            if ($product->current->description == null) {
                $product->current->description = "Купить фирменное " . ($product->kind ? $product->kind->name : '') . ' ' . $product->current->name . ' ' . $product->checked_price->volume . 'мл, ' . $product->checked_price->price . ' грн. 🍷 Производство – ' . $product->provider->name .' ✈Доставка по Украине. Звоните ☎ ' . preg_replace('/[^0-9]*/', '', config('db.contacts.footer-phone'));
            }
            if ($product->current->keywords == null) {
                $product->current->keywords = ($product->kind ? $product->kind->name : '') . ',' . $product->current->name . ', купить, цена, интернет магазин, продажа, киев, украина';
            }
            $product->save();
        }
        return true;
    }

}
