<?php

namespace App\Models\Catalog;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Catalog\ProductsCompilationTranslates
 *
 * @property int $id
 * @property string $name
 * @property bool $status
 * @property string|null $h1
 * @property string|null $title
 * @property string|null $keywords
 * @property string|null $description
 * @property string|null $content
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $language
 * @property integer $row_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsCompilationTranslates whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsCompilationTranslates whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsCompilationTranslates whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsCompilationTranslates whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsCompilationTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsCompilationTranslates whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsCompilationTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsCompilationTranslates whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsCompilationTranslates whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\Catalog\ProductsCompilation $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsCompilationTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsCompilationTranslates whereRowId($value)
 */
class ProductsCompilationTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'products_compilations_translates';

    protected $fillable = ['name', 'content', 'h1', 'title', 'keywords', 'description'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:191'],
        ];
    }
}
