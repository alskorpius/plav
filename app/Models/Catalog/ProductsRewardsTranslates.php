<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelTranslates;

/**
 * App\Models\Catalog\ProductsRewardsTranslates
 *
 * @property int $id
 * @property string $name
 * @property string $name_short
 * @property string $short
 * @property string $text
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $language
 * @property integer $row_id
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsRewardsTranslates whereId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\Catalog\ProductsPresentation $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentationTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentationTranslates whereRowId($value)
 *
 */
class ProductsRewardsTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'products_rewards_translates';

    protected $fillable = ['name_short', 'name', 'short', 'text'];


    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_short' => ['required'],
            'name' => ['required'],
            'short' => ['required'],
            'text' => ['required'],
        ];
    }
}
