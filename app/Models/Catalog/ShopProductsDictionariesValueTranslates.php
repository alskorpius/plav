<?php

namespace App\Models\Catalog;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Catalog\ShopProductsDictionariesValueTranslates
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property integer $row_id
 * @property string $language
 * @property-read \App\Models\Catalog\ShopProductsDictionary $dictionary
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionariesValueTranslates whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionariesValueTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionariesValueTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionariesValueTranslates whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\Catalog\ShopProductsDictionary $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionaryTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionaryTranslates whereRowId($value)
 */
class ShopProductsDictionariesValueTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'shop_products_dictionaries_values_translates';

    protected $fillable = ['name'];

    /**
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
}
