<?php

namespace App\Models\Catalog;

use App\Traits\ModelMain;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

/**
 * App\Models\Catalog\ShopProductsDictionary
 *
 * @property int $id
 * @property string $slug
 * @property bool $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Catalog\ShopProductsDictionaryTranslates $current
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ShopProductsDictionariesValue[] $dictionaryValues
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionary whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionary whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionary whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionary whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionary whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ShopProductsDictionaryTranslates[] $data
 */
class ShopProductsDictionary extends Model
{
    use ModelMain;

    protected $table = 'shop_products_dictionaries';

    protected $casts = ['status' => 'boolean',];

    protected $fillable = ['slug', 'status'];

    /**
     * @return array
     */
    public function _rules()
    {
        return [
            'slug' => ['alpha_dash', 'max:191', Rule::unique($this->getTable())->ignore($this->id)],
            'status' => ['required', 'boolean'],
        ];
    }

    /**
     * Получаем список
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getList()
    {
        $dictionaries = self::with('current')->oldest('shop_products_dictionaries.created_at');

        if (request()->input('name', false) !== false) {
            $dictionaries
                ->join('shop_products_dictionaries_translates', 'shop_products_dictionaries.id', '=', 'shop_products_dictionaries_translates.row_id')
                ->where('shop_products_dictionaries_translates.name', 'like', '%' . request()->input('name') . '%')
                ->where('shop_products_dictionaries_translates.language', '=', \Lang::getLocale());
//                ->where('name', 'like', '%' . request()->input('name') . '%');
        }

        return $dictionaries->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dictionaryValues() {
        return $this->hasMany(ShopProductsDictionariesValue::class, 'dictionary_id', 'id')->with('current');
    }
}
