<?php

namespace App\Models\Catalog;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

/**
 * App\Models\Catalog\UsersWishlist
 *
 * @property int $id
 * @property int $user_id
 * @property int $product_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\Product[] $products
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\UsersWishlist whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\UsersWishlist whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\UsersWishlist whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\UsersWishlist whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\UsersWishlist whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Catalog\Product $product
 */
class UsersWishlist extends Model
{
    protected $table = 'users_wishlists';

    protected $fillable = ['user_id', 'product_id'];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => ['required', 'numeric',
                Rule::unique($this->table, 'product_id')
                    ->ignore($this->id)
                    ->where('user_id', \Auth::user()->id)],
        ];
    }

    /**
     * Получаем список
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getList()
    {
        $wishes = self::latest('created_at');

        return $wishes->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function users()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}
