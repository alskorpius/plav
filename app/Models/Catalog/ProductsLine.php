<?php

namespace App\Models\Catalog;

use App\Models\Map\MapProductLine;
use App\Models\SeoTemplate;
use App\Traits\ModelImages;
use App\Traits\ModelMain;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

/**
 * App\Models\Catalog\ProductsLine
 *
 * @property int $id
 * @property string $slug
 * @property string|null $image
 * @property int $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Catalog\ProductsLineTranslates $current
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsLine whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsLine whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsLine whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsLine whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsLine whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsLine whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ProductsCompilation[] $products
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Map\MapProductLine[] $spots
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ProductsLineTranslates[] $data
 */
class ProductsLine extends Model
{
    use ModelImages;
    use ModelMain;

    protected $table = 'products_lines';

    protected $fillable = ['image', 'status', 'slug'];


    protected $casts = [
        'status' => 'boolean',
    ];

    /**
     * @inheritdoc
     */
    protected function imagesFolders()
    {
        return [
            'lines' => [
                'small' => [
                    'width' => 350,
                    'height' => 350,
                    'resize' => true,
                    'crop' => true,
                    'watermark' => false,
                ],
                'icon' => [
                    'width' => 60,
                    'height' => 60,
                    'resize' => true,
                    'crop' => true,
                    'watermark' => false,
                ],
                'original' => [],
            ],
        ];
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function _rules()
    {
        return [
            'slug' => ['required', 'alpha_dash', 'max:191', Rule::unique($this->getTable())->ignore($this->id)],
            'status' => ['required', 'boolean'],
            'image' => ['sometimes', 'image', 'max:' . config('custom.image-max-size')],
        ];
    }

    /**
     * @param null $status
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @internal param $limit
     */
    public static function getList($limit, $status = null)
    {
        $pages = self::with('current')->oldest('products_lines.created_at');
        if (request()->input('name', false) !== false) {
            $pages
                ->join('products_lines_translates', 'products_lines.id', '=', 'products_lines_translates.row_id')
                ->where('products_lines_translates.name', 'like', '%' . request()->input('name') . '%')
                ->where('products_lines_translates.language', '=', \Lang::getLocale());
//                ->where('name', 'like', '%' . request()->input('name') . '%');
        }

        if ($status !== null) {
            $pages->whereStatus($status);
        }

        return $pages->paginate($limit);
    }

    /**
     * Мета даныне
     *
     * @return array
     */
    public function meta()
    {
        // Мета теги по дефолту
        $meta = [
            'h1' => $this->h1,
            'title' => $this->title,
            'description' => $this->description,
            'keywords' => $this->keywords,
            'content' => $this->content,
        ];
        // Используем шаблон мета тегов для статтей
        if ($template = SeoTemplate::getTemplate(SeoTemplate::LINES)) {
            if (!$meta['h1'] && $template->h1) {
                $meta['h1'] = str_replace(['{name}'], [$this->name], $template->h1);
            }
            if (!$meta['title'] && $template->title) {
                $meta['title'] = str_replace(['{name}'], [$this->name], $template->title);
            }
            if (!$meta['description'] && $template->description) {
                $meta['description'] = str_replace(['{name}'], [$this->name], $template->description);
            }
            if (!$meta['keywords'] && $template->keywords) {
                $meta['keywords'] = str_replace(['{name}'], [$this->name], $template->keywords);
            }
        }
        return $meta;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'line_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function spots()
    {
        return $this->hasMany(MapProductLine::class, 'line_id', 'id');
    }
}
