<?php

namespace App\Models\Catalog;

use App\Models\Providers\ShopProvider;
use App\Models\SeoTemplate;
use App\Traits\ModelImages;
use App\Traits\ModelMain;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

/**
 * App\Models\Catalog\ShopBrand
 *
 * @property int $id
 * @property string $slug
 * @property string|null $image
 * @property int|null $shop_provider_id
 * @property bool $status
 * @property int $position
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Catalog\ShopBrandTranslates $current
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopBrand whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopBrand whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopBrand whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopBrand wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopBrand whereShopProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopBrand whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopBrand whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopBrand whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Providers\ShopProvider $shopProvider
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\Product[] $products
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ShopBrandTranslates[] $data
 */
class ShopBrand extends Model
{
    use ModelImages;
    use ModelMain;

    protected $table = 'shop_brands';

    protected $fillable = ['image', 'shop_provider_id', 'slug', 'status', 'position'];

    protected $casts = [
        'status' => 'boolean',
    ];

    /**
     * @inheritdoc
     */
    protected function imagesFolders()
    {
        return [
            'brands' => [
                'small' => [
                    'width' => 350,
                    'height' => 350,
                    'resize' => true,
                    'crop' => true,
                    'watermark' => false,
                ],
                'original' => [],
            ],
        ];
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function _rules()
    {
        return [
            'slug' => ['required', 'alpha_dash', 'max:191', Rule::unique($this->getTable())->ignore($this->id)],
            'status' => ['required', 'boolean'],
            'image' => ['sometimes', 'image', 'max:' . config('custom.image-max-size')],
            'shop_provider_id' => ['required', 'numeric'],
        ];
    }

    /**
     * @param null $status
     * @param null $name
     * @param null $slug
     * @param null $shop_provider_id
     * @return \Illuminate\Support\Collection
     */
    public static function getList($limit = null, $status = null, $name = null, $slug = null, $shop_provider_id = null)
    {
        $pages = self::with('current')->oldest('position');

        if (request()->input('name', false) !== false) {
            $pages
                ->join('shop_brands_translates', 'shop_brands.id', '=', 'shop_brands_translates.row_id')
                ->where('shop_brands_translates.name', 'like', '%' . request()->input('name') . '%')
                ->where('shop_brands_translates.language', '=', \Lang::getLocale());
//                ->where('name', 'like', '%' . request()->input('name') . '%');
        }

        if ($status !== null) {
            $pages->whereStatus($status);
        }

        if ($name !== null) {
            $pages->whereName($name);
        }

        if ($slug !== null) {
            $pages->whereSlug($slug);
        }

        if ($shop_provider_id !== null) {
            $pages->whereShopProviderId($shop_provider_id);
        }

        if ($limit !== null) {
            return $pages->paginate($limit);
        }
        return $pages->get();
    }

    /**
     * Мета даныне
     * @return array
     **/

    public function meta()
    {
        // Мета теги по дефолту
        $meta = [
            'h1' => $this->current->h1,
            'title' => $this->current->title,
            'description' => $this->current->description,
            'keywords' => $this->current->keywords,
            'content' => $this->current->content,
        ];
        // Используем шаблон мета тегов для статтей
        if ($template = SeoTemplate::getTemplate(SeoTemplate::ARTICLES)) {
            if (!$meta['h1'] && $template->h1) {
                $meta['h1'] = str_replace(['{name}'], [$this->name], $template->h1);
            }
            if (!$meta['title'] && $template->title) {
                $meta['title'] = str_replace(['{name}'], [$this->name], $template->title);
            }
            if (!$meta['description'] && $template->description) {
                $meta['description'] = str_replace(['{name}'], [$this->name], $template->description);
            }
            if (!$meta['keywords'] && $template->keywords) {
                $meta['keywords'] = str_replace(['{name}'], [$this->name], $template->keywords);
            }
        }
        return $meta;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function shopProvider()
    {
        return $this->hasOne(ShopProvider::class, 'id', 'shop_provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'brand_id', 'id');
    }

    public static function workWithBD()
    {
        $brands = ShopBrand::getList();
        foreach ($brands as $brand) {
            if ($brand->current->title == null) {
                $brand->current->title = trans('custom.brands.title', ['name' => $brand->current->name]);
            }
            if ($brand->current->description == null) {
                $brand->current->description = trans('custom.brands.description', ['name' => $brand->current->name, 'phone' => preg_replace('/[^0-9]*/', '', config('db.contacts.footer-phone'))]);
            }
            if ($brand->current->keywords == null) {
                $brand->current->keywords = $brand->current->name . __('custom.brands.keywords');
            }
            if ($brand->current->h1 == null) {
                $brand->current->h1 = $brand->name;
            }
            $brand->save();
        }
        return true;
    }
}
