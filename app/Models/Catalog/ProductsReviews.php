<?php

namespace App\Models\Catalog;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Catalog\ProductsReviews
 *
 * @property int $id
 * @property int $status
 * @property string $text
 * @property string|null $user_name
 * @property string|null $answer
 * @property int $product_id
 * @property int|null $user_id
 * @property int|null $email
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsReviews whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsReviews whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsReviews whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsReviews whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsReviews whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsReviews whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsReviews whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsReviews whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsReviews whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsReviews whereUserName($value)
 * @mixin \Eloquent
 * @property int|null $mark
 * @property-read \App\Models\Catalog\Product $product
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsReviews whereMark($value)
 */
class ProductsReviews extends Model
{
	const TYPE_NO = 0;
	const TYPE_YES = 1;
	const TYPE_IN_PROCESS = 2;

	protected $table = 'products_reviews';

	protected $fillable = ['status', 'text', 'product_id', 'user_id', 'user_name', 'answer', 'email', 'mark'];

	/**
	 * @return array
	 */
	public function rules()
	{
		return [
			'text' => ['required'],
		];
	}

	/**
	 * Получаем комментарии
	 * @param $limit
	 * @param null $idProduct
	 * @param null $idUser
	 * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
	 */
	public static function getList($limit, $idProduct = null, $idUser = null)
	{
		$comments = self::orderBy('updated_at', 'DESC');

		if (request()->input('text', false) !== false) {
			$comments->where('text', 'like', '%' . request()->input('text') . '%');
		}

		if (request()->input('created_at', false) !== false) {
			$comments->where('created_at', 'like', '%' . request()->input('created_at') . '%');
		}

		if (request()->input('status', false) !== false) {
			$comments->where('status', 'like', '%' . request()->input('status') . '%');
		}

		if (request()->input('product', false) !== false) {
            $products = Product::getListByName(request()->input('product'));
            foreach ($products as $product) {
                $comments->orWhere('product_id', 'like', $product->id );
            }
		}

		if (request()->input('mark', false) !== false) {
			$comments->whereMark(request()->input('mark'));
		}

		if ($idProduct !== null){
			$comments->where('article_id', $idProduct);
		}
		if ($idUser !== null){
			$comments->where('user_id', $idUser);
		}
		return $comments->paginate($limit);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function product() {
		return $this->hasOne(Product::class, 'id', 'product_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function user() {
		return $this->hasOne(User::class, 'id', 'user_id');
	}

}
