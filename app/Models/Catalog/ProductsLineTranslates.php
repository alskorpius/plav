<?php

namespace App\Models\Catalog;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Catalog\ProductsLineTranslates
 *
 * @property int $id
 * @property string $name
 * @property bool $status
 * @property string|null $h1
 * @property string|null $title
 * @property string|null $keywords
 * @property string|null $description
 * @property string|null $content
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $language
 * @property integer $row_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsLineTranslates whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsLineTranslates whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsLineTranslates whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsLineTranslates whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsLineTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsLineTranslates whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsLineTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsLineTranslates whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsLineTranslates whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\Catalog\ProductsLine $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsLineTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsLineTranslates whereRowId($value)
 */
class ProductsLineTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'products_lines_translates';

    protected $fillable = ['name', 'content', 'h1', 'title', 'keywords', 'description'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:191'],
        ];
    }
}
