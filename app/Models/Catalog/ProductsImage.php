<?php

namespace App\Models\Catalog;

use App\Traits\ModelImages;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

/**
 * App\Models\Catalog\ProductsImage
 *
 * @property int $id
 * @property string $image
 * @property int $position
 * @property int $status
 * @property int $product_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsImage whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsImage wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsImage whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsImage whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsImage whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Catalog\Product $product
 */
class ProductsImage extends Model
{
    use ModelImages;

    protected $table = 'products_images';

    protected $fillable = ['image', 'status', 'position', 'product_id',];


    protected $casts = [
        'status' => 'boolean',
    ];

    /**
     * @inheritdoc
     */
    protected function imagesFolders()
    {
        return [
            'products_images' => [
                'small' => [
                    'width' => 350,
                    'height' => 670,
                    'resize' => true,
                    'crop' => true,
                    'watermark' => false,
                ],
                'icon' => [
                    'width' => 350,
                    'height' => 350,
                    'resize' => true,
                    'crop' => true,
                    'watermark' => false,
                ],
                'big' => [
                    'width' => 960,
                    'height' => 650,
                    'resize' => true,
                    'crop' => false,
                    'watermark' => false,
                ],
                'original' => [],
            ],
        ];
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => ['nullable', 'image', 'max:' . config('custom.image-max-size'),
                Rule::dimensions()
                    ->minWidth(array_get($this->imagesFolders(), 'products_images.small.width'))
                    ->minHeight(array_get($this->imagesFolders(), 'products_images.small.height')),],
        ];
    }

    /**
     * @param null $status
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     * @internal param $limit
     */
    public static function getList($product_id, $status = null)
    {
        $images = self::oldest('created_at')->whereProductId($product_id);

        if ($status !== null) {
            $images->whereStatus($status);
        }

        return $images->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
