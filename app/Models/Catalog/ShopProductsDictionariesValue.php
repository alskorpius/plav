<?php

namespace App\Models\Catalog;

use App\Traits\ModelImages;
use App\Traits\ModelMain;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

/**
 * App\Models\Catalog\ShopProductsDictionariesValue
 *
 * @property int $id
 * @property string $slug
 * @property int $dictionary_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Catalog\ShopProductsDictionary $dictionary
 * @property-read \App\Models\Catalog\ShopProductsDictionariesValueTranslates $current
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionariesValue whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionariesValue whereDictionaryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionariesValue whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionariesValue whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionariesValue whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $image
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionariesValue whereImage($value)
 * @property-read mixed $count_products
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ProductSort[] $sorts
 * @property string|null $option
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionariesValue whereOption($value)
 * @property int $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ShopProductsDictionariesValue whereStatus($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ShopProductsDictionariesValueTranslates[] $data
 */
class ShopProductsDictionariesValue extends Model
{
	use ModelImages;
	use ModelMain;

    protected $table = 'shop_products_dictionaries_values';

    protected $fillable = ['slug', 'dictionary_id', 'image', 'status'];

    /**
     * @return array
     */
    public function _rules()
    {
        return [
            'newSlug' => ['required', 'alpha_dash', 'min:1', 'max:191',
                Rule::unique($this->table, 'slug')
                ->ignore($this->id)
                ->where('dictionary_id', \Request::input('id'))],
        ];
    }

    /**
     * Получаем список
     * @param null $dictionary_id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getList($dictionary_id = null, $status = null)
    {
        $values = self::with(['current'])->oldest();

        if (request()->input('name', false) !== false) {
            $values
                ->join('shop_products_dictionaries_values_translates', 'shop_products_dictionaries_values.id', '=', 'shop_products_dictionaries_values_translates.row_id')
                ->where('shop_products_dictionaries_values_translates.name', 'like', '%' . request()->input('name') . '%')
                ->where('shop_products_dictionaries_values_translates.language', '=', \Lang::getLocale());
//                ->where('name', 'like', '%' . request()->input('name') . '%');
        }

        if ($dictionary_id !== null){
            $values->where('dictionary_id', '=', $dictionary_id);
        }

        if ($status !== null) {
            $values->whereStatus($status);
        }
        return $values->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function dictionary()
    {
        return $this->hasOne(ShopProductsDictionary::class, 'id', 'dictionary_id')->with(['current']);
    }

    public function sorts()
	{
		return $this->hasMany(ProductSort::class, 'sort_id', 'id');
	}

	public function sortsFilterCount($color_id = [], $sweetness_id = [], $type_id = [], $kind_id = [],  $alcohol_id = [], $sugar_id = [], $country_id = [], $provider_id = [], $sort_id = [], $year = [], $volume = [], $region = [])
    {
        $products = Product::getListForFilter(1000, $color_id, $sweetness_id, $type_id, $kind_id, $alcohol_id, $sugar_id, $country_id, $provider_id, $sort_id, $year, $volume, $region);
        $idsArr = [];
        foreach ($products as $product) {
            $idsArr[] = $product->id;
        }
        $count = 0;
        foreach ($this->sorts as $sort) {
            if (in_array($sort->product_id, $idsArr)){
             $count++;
            }
        }
        return $count;
    }

	public function getCountProductsAttribute()
	{
	    if ($this->dictionary_id == 6) {
	        return sizeof($this->sorts);
        }


		$products = (new Product)->getList(1000, 1, 1);
		$specificationArr = [];
		$iterator = 0;
		foreach ($products as $product) {
			$specificationArr[$iterator][] = $product->color_id;
			$specificationArr[$iterator][] = $product->sweetness_id;
			$specificationArr[$iterator][] = $product->type_id;
			$specificationArr[$iterator][] = $product->kind_id;
			$specificationArr[$iterator][] = $product->alcohol_id;
			$specificationArr[$iterator][] = $product->sugar_id;
			$iterator++;
		}
		$count = 0;
		foreach ($specificationArr as $item){
			if (in_array($this->id, $item)) {
				$count++;
			}
		}
		return $count;
	}

    public function countProductsFilter($color_id = [], $sweetness_id = [], $type_id = [], $kind_id = [],  $alcohol_id = [], $sugar_id = [], $country_id = [], $provider_id = [], $sort_id = [], $year = [], $volume = [], $region = [])
    {
        $products = Product::getListForFilter(1000, $color_id, $sweetness_id, $type_id, $kind_id, $alcohol_id, $sugar_id, $country_id, $provider_id, $sort_id, $year, $volume, $region);
        $specificationArr = [];
        $iterator = 0;
        foreach ($products as $product) {
            $specificationArr[$iterator][] = $product->color_id;
            $specificationArr[$iterator][] = $product->sweetness_id;
            $specificationArr[$iterator][] = $product->type_id;
            $specificationArr[$iterator][] = $product->kind_id;
            $specificationArr[$iterator][] = $product->alcohol_id;
            $specificationArr[$iterator][] = $product->sugar_id;
            $iterator++;
        }
        $count = 0;
        foreach ($specificationArr as $item){
            if (in_array($this->id, $item)) {
                $count++;
            }
        }
        return $count;
    }
}
