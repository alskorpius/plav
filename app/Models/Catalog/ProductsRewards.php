<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelMain;
use App\Traits\ModelImages;

/**
 * App\Models\Catalog\ProductsRewards
 *
 * @property int $id
 *
 * @property-read \App\Models\Catalog\ProductsRewardsTranslates $current
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsRewards whereId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ProductsRewardsTranslates[] $data
 */
class ProductsRewards extends Model
{
    use ModelImages;
    use ModelMain;

    protected $table = 'products_rewards';

    protected $fillable = ['image'];

    protected function imagesFolders()
    {
        return [
            'products_images' => [
                'small' => [
                    'width' => 350,
                    'height' => 670,
                    'resize' => true,
                    'crop' => true,
                    'watermark' => false,
                ],
                'icon' => [
                    'width' => 350,
                    'height' => 350,
                    'resize' => true,
                    'crop' => true,
                    'watermark' => false,
                ],
                'big' => [
                    'width' => 960,
                    'height' => 650,
                    'resize' => true,
                    'crop' => false,
                    'watermark' => false,
                ],
                'original' => [],
            ],
        ];
    }


    public function _rules()
    {
        return [
        ];
    }


    /**
     * Получаем список
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getList()
    {
        $values = static::with(['current'])->oldest('created_at');
        return $values->get();
    }

}
