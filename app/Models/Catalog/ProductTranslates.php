<?php namespace App\Models\Catalog;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductTranslates
 *
 * @package App\Models\Shop
 * @property integer $id
 * @property string $name
 * @property string|null $h1
 * @property string|null $title
 * @property string|null $keywords
 * @property string|null $description
 * @property string|null $content
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property integer $row_id
 * @property string $language
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductTranslates whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductTranslates whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductTranslates whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductTranslates whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductTranslates whereTitle($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\Catalog\Product $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductTranslates whereRowId($value)
 */
class ProductTranslates extends Model
{
    use ModelTranslates;

    public $timestamps = false;

    protected $table = 'products_translates';

    protected $fillable = ['name', 'content', 'h1', 'title', 'keywords', 'description'];

    public function rules()
    {
        return [
            'name' => ['required', 'max:191'],
        ];
    }
}
