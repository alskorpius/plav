<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelMain;

/**
 * App\Models\Catalog\ProductsPresentation
 *
 * @property int $id
 * @property int $product_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Catalog\Product $product
 * @property-read \App\Models\Catalog\ProductsPresentationTranslates $current
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentation whereAroma($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentation whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentation whereCombination($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentation whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentation whereTaste($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPresentation whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed $tastings
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ProductsPresentationTranslates[] $data
 */
class ProductsPresentation extends Model
{
    use ModelMain;

    protected $table = 'products_presentations';

    protected $fillable = ['product_id'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function _rules()
    {
        return [
            'product_id' => ['required', 'numeric'],
        ];
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function getTastingsAttribute()
    {
        if (isset($this->color) || isset($this->aroma) || isset($this->taste) || isset($this->combination)) {
            return true;
        }
        return false;
    }
}
