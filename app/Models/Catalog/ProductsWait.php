<?php

namespace App\Models\Catalog;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Catalog\ProductsWait
 *
 * @property int $id
 * @property int $product_id
 * @property int $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Catalog\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsWait whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsWait whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsWait whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsWait whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsWait whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $user_id
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsWait whereUserId($value)
 */
class ProductsWait extends Model
{
    protected $table = 'products_waits';

    protected $fillable = ['product_id', 'status', 'user_id'];


    /**
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => ['required', 'array'],
            'user_id' => ['required', 'numeric'],
            'status' => ['boolean'],
        ];
    }

    /**
     * Получаем список
     * @param null $dictionary_id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getList($status = null, $product_id = null)
    {
        $values = self::oldest('created_at');


        if ($product_id !== null) {
            $values->where('product_id', '=', $product_id);
        }

        if ($status !== null) {
            $values->where('status', '=', $status);
        }

        return $values->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
