<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Catalog\ProductsPrice
 *
 * @property int $id
 * @property float $volume
 * @property int $year
 * @property int $price
 * @property int $product_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Catalog\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPrice wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPrice whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPrice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPrice whereVolume($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPrice whereYear($value)
 * @mixin \Eloquent
 * @property float $weight
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPrice whereWeight($value)
 * @property int|null $is_checked
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPrice whereIsChecked($value)
 * @property int|null $sale
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPrice whereSale($value)
 * @property string $artikul
 * @property-read mixed $total_price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductsPrice whereArtikul($value)
 */
class ProductsPrice extends Model
{
    protected $table = 'products_prices';

    protected $fillable = ['volume', 'year', 'price', 'product_id', 'weight', 'sale', 'artikul'];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'volume' => ['required', 'numeric'],
            'year' => ['required', 'numeric'],
            'price' => ['required', 'numeric'],
            'product_id' => ['required'],
            'artikul' => ['required'],
        ];
    }

    /**
     * Получаем список
     * @param null $dictionary_id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function getList($product_id = null, $noAccessory = null)
    {
        $values =  static::oldest('created_at');


        if ($product_id !== null) {
            $values->where('product_id', '=', $product_id);
        }
        if ($noAccessory !== null) {
            $values->whereNotIn('product_id', function($values) {
                $values->select('products.id')
                        ->from('products')
                        ->where('is_accessory', '=', 1);
            });
        }
//        dd($values->toSql());
        return $values->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function getTotalPriceAttribute()
    {
        if ($this->sale !== null){
            return ceil($this->price - (($this->price / 100) * (int)$this->sale));
        }
        return $this->price;
    }
}
