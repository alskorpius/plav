<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Catalog\ProductRelCompilation
 *
 * @property int $id
 * @property int|null $product_id
 * @property int|null $compilation_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Catalog\Product $product
 * @property-read \App\Models\Catalog\ProductsCompilation $value
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductRelCompilation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductRelCompilation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductRelCompilation whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductRelCompilation whereCompilationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalog\ProductRelCompilation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ProductRelCompilation extends Model
{
	protected $table = 'product_rel_compilation';

	protected $fillable = ['product_id', 'compilation_id'];

	/**
	 * Правила валидации
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'product_id' => ['required', 'numeric'],
			'sort_id' => ['numeric'],
		];
	}

	public function product()
	{
		return $this->hasOne(Product::class, 'id', 'product_id');
	}

	public function value()
	{
		return $this->hasOne(ProductsCompilation::class, 'id', 'compilation_id');
	}
}
