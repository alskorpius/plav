<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SettingsGroup
 *
 * @package App\Models
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property integer $position
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Settings[] $settings
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SettingsGroup whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SettingsGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SettingsGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SettingsGroup wherePosition($value)
 */
class SettingsGroup extends Model
{
    protected $table = 'settings_groups';

    public $timestamps = false;

    /**
     * @return $this
     */
    public function settings()
    {
        return $this->hasMany(Settings::class, 'group', 'alias')
            ->orderBy('position');
    }
}
