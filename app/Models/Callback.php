<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Callback
 *
 * @property int $id
 * @property int $status
 * @property string $name
 * @property string $phone
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Callback whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Callback whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Callback whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Callback wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Callback whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Callback whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Callback extends Model
{
    protected $table = 'callbacks';

    protected $fillable = ['status', 'name', 'phone'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:191'],
            'phone' => ['required'],
            'status' => ['required'],
        ];
    }

    /**
     * @param $limit
     * @param null $type
     * @param null $status
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList($limit, $status = null)
    {
        $callbacks = self::orderBy('created_at', 'DESC');

        if (request()->input('name', false) !== false) {
            $callbacks->where('name', 'like', '%' . request()->input('name') . '%');
        }

        if (request()->input('phone', false) !== false) {
            $callbacks->where('phone', 'like', '%' . request()->input('phone') . '%');
        }

        if (request()->input('created_at', false) !== false) {
            $callbacks->where('created_at', 'like', '%' . request()->input('created_at') . '%');
        }

        if ($status !== null) {
            $callbacks->whereStatus($status);
        }

        return $callbacks->paginate($limit);
    }

}
