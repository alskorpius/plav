<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;
use App\Traits\ModelMain;

/**
 * Class Pages
 *
 * @package App\Models
 * @property integer $id
 * @property string $slug
 * @property boolean $status
 * @property boolean $advantages
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $position
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereAdvantages($value)
 * @mixin \Eloquent
 * @property int $static_page
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereStaticPage($value)
 * @property-read \App\Models\PageTranslates $current
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PageTranslates[] $data
 */
class Page extends Model
{
    use ModelMain;

    protected $table = 'pages';

//    protected $fillable = ['name', 'slug', 'content', 'h1', 'title', 'keywords', 'description', 'status', 'infographics', 'advantages'];

    protected $casts = [
        'status' => 'boolean',
    ];

    protected $fillable = ['slug', 'status', 'position'];
    /**
     * Правила валидации
     *
     * @return array
     */
    public function _rules()
    {
        return [
            'slug' => ['required_unless:static_page,1', 'alpha_dash', 'max:191', Rule::unique($this->table)->ignore($this->id)],
            'status' => ['required', 'boolean'],
        ];
    }

    /**
     * Список всех страниц
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getList()
    {
        $pages =  static::with(['current'])->oldest('position');

        return $pages->get();
    }

    /**
     * Данные о странице для вывода в АПИ
     *
     * @return array
     */
    public function api()
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
        ];
    }


    /**
     * Достаем данные о странице по алиасу
     *
     * @param $slug
     * @param null $status
     * @return Model|static
     */
    public static function getItem($slug, $status = null)
    {
        $page = static::whereSlug($slug);
        if ($status !== null) {
            $page->whereStatus((bool)$status);
        }
        return $page->firstOrFail();
    }

}
