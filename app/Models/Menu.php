<?php

namespace App\Models;

use App\Traits\ModelMain;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Lang;

/**
 * Class MailTemplate
 *
 * @package App\Models
 * @property integer $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property boolean $status
 * @property string $url
 * @property integer $position
 * @property integer $parent_id
 * @property-read \App\Models\MenuTranslates $current
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereUrl($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereParentId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MenuTranslates[] $data
 */
class Menu extends Model
{
    use ModelMain;
    protected $table = 'menus';

    /**
     * @var array|static[]
     */
    public $children = [];

    protected $casts = [
        'status' => 'boolean',
    ];

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $fillable = [ 'url', 'position', 'status', 'parent_id'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function _rules()
    {
        return [
//            'name' => ['required', 'max:191'],
            'url' => ['required', 'max:191'],
        ];
    }

    public function showLink() // нужно переделать логику
    {
        if ($this->url[0] === '/' && Lang::getLocale() !== 'ru') {
            return '/'.Lang::getLocale().$this->url;
        }
        return $this->url;
    }

    /**
     * Весь список элементов
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function getList()
    {
        return static::with(['current'])->oldest('position')->get();
    }

    /**
     * Список элементов для вывода в АПИ. Только name и url
     *
     * @param null $status
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
//    public static function getItems($status = null)
//    {
//        $menu = static::with(['current']);
//        if ($status !== null) {
//            $menu->whereStatus((bool)$status);
//        }
//        return $menu->oldest('position')->get();
//
//    }

    public static function getItems($status = null)
    {
        $menu = static::select(['url', 'parent_id', 'id']);
        if ($status !== null) {
            $menu->whereStatus((bool)$status);
        }
        $result = $menu->with(['current'])->oldest('position')->get();

        $response = [];
        foreach ($result AS $key => $value) {
            if (Lang::getLocale() !== 'ru'){
                $value->url = '/'.Lang::getLocale() .$value->url;
            }
            if (!$value->parent_id) {
                foreach ($result AS $element) {
                    if ($element->parent_id === $value->id) {
                        $value->children[] = $element;
                    }
                }
                $response[] = $value;
            }
        }

        return $response;
    }
}
