<?php

namespace App\Models\Order;

use App\Models\Catalog\Product;
use App\Models\Catalog\ProductsPrice;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Order\OrdersItem
 *
 * @property int $id
 * @property int $order_id
 * @property int|null $product_id
 * @property int $count
 * @property int $weight
 * @property int $price
 * @property string $comment
 * @property int $available
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\OrdersItem whereAvailable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\OrdersItem whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\OrdersItem whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\OrdersItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\OrdersItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\OrdersItem whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\OrdersItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\OrdersItem whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\OrdersItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\OrdersItem whereWeight($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Order\Order $order
 * @property-read \App\Models\Catalog\Product $product
 * @property int|null $price_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\OrdersItem wherePriceId($value)
 * @property-read float $total_price
 */
class OrdersItem extends Model
{
	protected $table = 'orders_items';

	protected $fillable = ['order_id', 'product_id', 'count', 'weight', 'price_id', 'comment', 'available'];

	/**
	 * Правила валидации
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'order_id' => ['required', 'numeric'],
			'product_id' => ['required', 'numeric'],
			'count' => ['required', 'numeric'],
			'price_id' => ['required', 'numeric'],
		];
	}

	/**
	 * @param $limit
	 * @param null $order_id
	 * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
	 * @internal param $limit
	 */
	public static function getList($limit, $order_id = null)
	{
		$pages = self::oldest('created_at');

		if ($order_id !== null) {
			$pages->whereOrderId($order_id);
		}

		return $pages->paginate($limit);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function order()
	{
		return $this->hasOne(Order::class, 'id', 'order_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function product()
	{
		return $this->hasOne(Product::class, 'id', 'product_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function price()
	{
		return $this->hasOne(ProductsPrice::class, 'id', 'price_id');
	}

	/**
	 * @return float
	 */
	public function getTotalPriceAttribute()
	{
		return $this->price->total_price;
	}
}
