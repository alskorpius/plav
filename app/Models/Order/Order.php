<?php

namespace App\Models\Order;

use App\Helpers\NovaPoshta;
use App\Models\Catalog\Product;
use App\Models\Catalog\ProductsReviews;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Order\Order
 *
 * @property int $id
 * @property int|null $user_id
 * @property int $status
 * @property string $user_name
 * @property string $phone
 * @property string $email
 * @property string $city
 * @property bool $delivery_type
 * @property string|null $delivery_warehouse
 * @property string|null $delivery_address
 * @property int $delivery_cost
 * @property bool $payment_type
 * @property int|null $payment_system_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereDeliveryAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereDeliveryCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereDeliveryType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereDeliveryWarehouse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order wherePaymentSystemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order wherePaymentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereUserName($value)
 * @mixin \Eloquent
 * @property string|null $comment
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order\OrdersItem[] $items
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereComment($value)
 * @property int|null $total_price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereTotalPrice($value)
 * @property string|null $city_name
 * @property string|null $delivery_warehouse_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereCityName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order\Order whereDeliveryWarehouseName($value)
 */
class Order extends Model
{
    const STATUS_NEW = 0;
    const STATUS_PROCESSED = 1;
    const STATUS_PAYED = 2;
    const STATUS_SENDED = 3;
    const STATUS_PAYED_AND_SENDED = 4;
    const STATUS_FINISHED = 5;
    const STATUS_NOT_FINISHED = 6;

    const DELIVERY_TYPE_WAREHOUSE = 0;
    const DELIVERY_TYPE_COURIER = 1;

    const PAYMENT_SYSTEM_LIQPAY = 0;
    const PAYMENT_SYSTEM_PORTMONE = 1;

    protected $table = 'orders';

    protected $fillable = [
        'user_id', 'status', 'user_name', 'phone', 'email', 'city', 'delivery_type', 'delivery_warehouse',
        'delivery_address', 'delivery_cost', 'payment_type', 'payment_system_id', 'cart_hash'
    ];

    protected $casts = [
        'delivery_type' => 'boolean',
        'payment_type' => 'boolean',
    ];


    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => ['numeric'],
            'status' => ['required'],
            'user_name' => ['required'],
            'phone' => ['required', ''],
            'email' => ['required', 'email'],
            'city' => ['required'],
            'delivery_type' => ['required', 'boolean'],
            'delivery_warehouse' => ['required_if:delivery_type,0'],
            'delivery_address' => ['required_if:delivery_type,1'],
            'delivery_cost' => ['', 'numeric'],
            'payment_type' => ['required', 'boolean'],
            'payment_system_id' => ['boolean'],
        ];
    }

    /**
     * @param null $status
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @internal param $limit
     */
    public static function getList($limit, $status = null)
    {
        $pages = self::latest('created_at');

        if (request()->input('user_name', false) !== false) {
            $pages->where('user_name', 'like', '%' . request()->input('user_name') . '%');
        }

        if (request()->input('id', false) !== false) {
            $pages->where('id',  request()->input('id'));
        }

        if (request()->input('phone', false) !== false) {
            $pages->where('phone', 'like', '%' . request()->input('phone') . '%');
        }

        if (request()->input('email', false) !== false) {
            $pages->where('email', 'like', '%' . request()->input('email') . '%');
        }

        if (request()->input('created_at', false) !== false) {
            $pages->where('created_at', 'like', '%' . request()->input('created_at') . '%');
        }

        if (request()->input('created_at_po', false) !== false) {
            $pages->where('created_at', 'like', '%' . request()->input('created_at_po') . '%');
        }

        if (request()->input('status', false) !== false) {
            $pages->where('status', request()->input('status'));
        }

        if (request()->input('amount', false) !== false) {
            $pages->where('total_price', request()->input('amount'));
        }

        if ($status !== null) {
            $pages->whereStatus($status);
        }

        return $pages->paginate($limit);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(OrdersItem::class, 'order_id', 'id');
    }

    /**
     * Получаю данные для графа заказов на dashboard
     * @param null $product_id
     * @param null $dateRange
     * @return array
     */
    public static function getOrdersGraph($product_id = null, $dateRange = null)
    {
        $rezult = [];
        if ($product_id == null && $dateRange == null) {
            $date = Carbon::now()->subDay(30)->format('Y-m-d H:i:s');
            $orders = self::where('created_at', '>=', $date)->groupBy('created_at')->get();
            $ordersArr = [];
            $i = 0;
            foreach ($orders as $order) {
                $ordersArr[$i] = $order->created_at->format('d.m.Y');
                $i++;
            }
            $ordersArr = array_count_values($ordersArr);
            $rezult[0] = array_keys($ordersArr);
            $rezult[1] = array_values($ordersArr);
            $rezult[2] = 'Продажи за последние 30 дней';
            return $rezult;
        } else {
            if ($product_id !== '0') {
                $dates = explode(' - ', $dateRange);
                $dates[0] .= ' 00:00:00';
                $dates[1] .= ' 23:59:59';
                $ordersItems = OrdersItem::where('product_id', $product_id)->groupBy('order_id')->get();
                $ordersItemsArr = [];
                $i = 0;
                foreach ($ordersItems as $ordersItem) {
                    $ordersItemsArr[$i] = $ordersItem->order_id;
                    $i++;
                }
                $orders = self::whereIn('id', $ordersItemsArr)
                    ->whereBetween('created_at', $dates)
                    ->orderBy('created_at')
                    ->get();
                $productForGraph = Product::find($product_id);
                $ordersArr = [];
                $i = 0;
                foreach ($orders as $order) {
                    $ordersArr[$i] = $order->created_at->format('d.m.Y');
                    $i++;
                }
                $ordersArr = array_count_values($ordersArr);
                $rezult[0] = array_keys($ordersArr);
                $rezult[1] = array_values($ordersArr);
                $rezult[2] = "Продажи $productForGraph->name за " . $dateRange;
                return $rezult;
            } else {
                $dates = explode(' - ', $dateRange);
                $dates[0] .= ' 00:00:00';
                $dates[1] .= ' 23:59:59';
                $orders = self::whereBetween('created_at', $dates)
                    ->orderBy('created_at')
                    ->get();
                $ordersArr = [];
                $i = 0;
                foreach ($orders as $order) {
                    $ordersArr[$i] = $order->created_at->format('d.m.Y');
                    $i++;
                }
                $ordersArr = array_count_values($ordersArr);
                $rezult[0] = array_keys($ordersArr);
                $rezult[1] = array_values($ordersArr);
                $rezult[2] = "Продажи за " . $dateRange;
                return $rezult;
            }
        }
    }

    /**
     * Получаю данные для графа отзывов на dashboard
     * @param null $product_id
     * @param null $dateRange
     * @return array
     */
    public static function getProductsReviewsGraph($product_id = null, $dateRange = null)
    {
        $rezult = [];
        if ($product_id == null && $dateRange == null) {
            $date = Carbon::now()->subDay(30)->format('Y-m-d H:i:s');
            $orders = ProductsReviews::where('created_at', '>=', $date)->groupBy('created_at')->get();
            $ordersArr = [];
            $i = 0;
            foreach ($orders as $order) {
                $ordersArr[$i] = $order->created_at->format('d.m.Y');
                $i++;
            }
            $ordersArr = array_count_values($ordersArr);
            $rezult[0] = array_keys($ordersArr);
            $rezult[1] = array_values($ordersArr);
            $rezult[2] = 'Отзывы за последние 30 дней';
            return $rezult;
        } else {
            if ($product_id !== '0') {
                $dates = explode(' - ', $dateRange);
                $dates[0] .= ' 00:00:00';
                $dates[1] .= ' 23:59:59';
                $reviews = ProductsReviews::where('product_id', $product_id)
                    ->whereBetween('created_at', $dates)
                    ->orderBy('created_at')
                    ->get();
                $productForGraph = Product::find($product_id);
                $reviewsArr = [];
                $i = 0;
                foreach ($reviews as $review) {
                    $reviewsArr[$i] = $review->created_at->format('d.m.Y');
                    $i++;
                }
                $reviewsArr = array_count_values($reviewsArr);
                $rezult[0] = array_keys($reviewsArr);
                $rezult[1] = array_values($reviewsArr);
                $rezult[2] = "Продажи $productForGraph->name за " . $dateRange;
                return $rezult;
            } else {
                $dates = explode(' - ', $dateRange);
                $dates[0] .= ' 00:00:00';
                $dates[1] .= ' 23:59:59';
                $reviews = ProductsReviews::whereBetween('created_at', $dates)
                    ->orderBy('created_at')
                    ->get();
                $reviewsArr = [];
                $i = 0;
                foreach ($reviews as $review) {
                    $reviewsArr[$i] = $review->created_at->format('d.m.Y');
                    $i++;
                }
                $reviewsArr = array_count_values($reviewsArr);
                $rezult[0] = array_keys($reviewsArr);
                $rezult[1] = array_values($reviewsArr);
                $rezult[2] = "Отзывы за " . $dateRange;
                return $rezult;
            }
        }
    }

}
