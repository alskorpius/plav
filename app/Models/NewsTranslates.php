<?php

namespace App\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * Class NewsTranslates
 *
 * @package App\Models
 * @property integer $id
 * @property string $language
 * @property string $name
 * @property string $h1
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $content
 * @property integer $row_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsTranslates whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsTranslates whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsTranslates whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsTranslates whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsTranslates whereTitle($value)
 * @mixin \Eloquent
 * @property-read \App\Models\News $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsTranslates whereRowId($value)
 * @property-read \App\Models\Language $lang
 */
class NewsTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'news_translates';

    public $timestamps = false;

    protected $fillable = ['name', 'h1', 'content', 'title', 'keywords', 'description'];

    public function rules()
    {
        return [
            'name' => ['required', 'max:191'],
        ];
    }
}
