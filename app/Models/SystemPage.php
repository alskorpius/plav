<?php

namespace App\Models;

use App\Traits\ModelImages;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelMain;

/**
 * Class SystemPage
 *
 * @package App\Models
 * @property integer $id
 * @property string $slug
 * @property array $settings
 * @property boolean $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read \App\Models\SystemPageTranslates $current
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemPage whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemPage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemPage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemPage whereSettings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemPage whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemPage whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemPage whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property float $latitude
 * @property float $longitude
 * @property int $zoom
 * @property string|null $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SystemPageTranslates[] $data
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemPage whereImage($value)
 */
class SystemPage extends Model
{
    use ModelImages;
    use ModelMain;

    protected $table = 'system_pages';

    protected $fillable = ['settings', 'image', 'status'];

    protected $casts = [
        'settings' => 'array',
    ];

    /**
     * @inheritdoc
     */
    protected function imagesFolders()
    {
        return [
            'systemPages' => [
                'small' => [
                    'width' => 295,
                    'height' => 300,
                    'resize' => true,
                    'crop' => true,
                    'watermark' => false,
                ],
                'big' => [
                    'width' => 960,
                    'height' => 650,
                    'resize' => true,
                    'crop' => false,
                    'watermark' => false,
                ],
                'original' => [],
            ],
        ];
    }

    /**
     * @return array
     */
    public function _rules()
    {
        return [
        ];
    }

    /**
     * @return float
     */
    public function getLongitudeAttribute()
    {
        if (array_get($this->settings, 'longitude', false) !== false) {
            return (float)array_get($this->settings, 'longitude', false);
        }
        return 32.6125771;
    }

    /**
     * @return float
     */
    public function getLatitudeAttribute()
    {
        if (array_get($this->settings, 'latitude', false) !== false) {
            return (float)array_get($this->settings, 'latitude', false);
        }
        return 46.637326;
    }

    /**
     * @return int
     */
    public function getZoomAttribute()
    {
        if (array_get($this->settings, 'zoom', false) !== false) {
            return (int)array_get($this->settings, 'zoom', false);
        }
        return 17;
    }

    /**
     * @param $limit
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getList($limit)
    {
        $system_pages = static::with(['current'])->select($this->getTable() . '.*');
        if (request()->input('slug', false) !== false) {
            $system_pages->where('system_pages.slug', 'like', '%' . request()->input('slug') . '%');
        }

        if (request()->input('name', false) !== false) {
            $system_pages
                ->join('system_pages_translates', $this->getTable() . '.id', '=', 'system_pages_translates.row_id')
                ->where('system_pages_translates.name', 'like', '%' . request()->input('name') . '%')
                ->where('system_pages_translates.language', '=', \Lang::getLocale());
        }

        return $system_pages->oldest($this->getTable() . '.id')->paginate($limit);
    }

    /**
     * @return array
     */
    public function api()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
        ];
    }

    /**
     * Достаем системную страницу по алиасу
     *
     * @param $slug
     * @return Model|null|static
     */
    public static function getItemBySlug($slug)
    {
        return static::whereSlug($slug)->with('current')->firstOrFail();
    }

}
