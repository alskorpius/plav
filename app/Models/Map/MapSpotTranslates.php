<?php

namespace App\Models\Map;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Map\MapSpotTranslates
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $language
 * @property integer $row_id
 * @property string $address
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapSpotTranslates whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapSpotTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapSpotTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapSpotTranslates whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapSpotTranslates whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapSpotTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapSpotTranslates whereRowId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\Map\MapSpot $row
 */
class MapSpotTranslates extends Model
{
    use ModelTranslates;
    protected $table = 'map_spots_translates';

    protected $fillable = ['name', 'address'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => ['required', 'max:191'],
        ];
    }

}
