<?php

namespace App\Models\Map;

use App\Traits\ModelMain;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Map\MapSpot
 *
 * @property int $id
 * @property string $settings
 * @property int $type 0 - Винный магазин, 1 - Ресторан, 2 - Супермаркет
 * @property int $region_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Map\MapSpotTranslates $current
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Map\MapProductLine[] $lines
 * @property-read \App\Models\Map\MapRegion $region
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapSpot whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapSpot whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapSpot whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapSpot whereSettings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapSpot whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapSpot whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Map\MapSpotTranslates[] $data
 * @property-read float $latitude
 * @property-read float $longitude
 * @property-read int $zoom
 */
class MapSpot extends Model
{
    use ModelMain;
    protected $table = 'map_spots';

    protected $fillable = [ 'settings', 'type', 'region_id'];

    protected $casts = [
        'settings' => 'array',
    ];

    const TYPE_SHOP = 0;
    const TYPE_CAFE = 1;
    const TYPE_SUPERMARKET = 2;
    const TYPE_GAS = 3;

    /**
     * Правила валидации
     *
     * @return array
     */
    public function _rules()
    {
        return [
            'SETTINGS' => ['required'],
            'type' => ['required'],
            'region_id' => ['required'],
            'line_id' => ['required'],
        ];
    }

    /**
     * @param null $name
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList($limit, $name = null, $region_id = null, $type = null)
    {
        $pages = self::with(['current'])->select('map_spots_translates.*', 'map_spots.*')->oldest('map_spots.created_at');
        $pages->join('map_spots_translates', 'map_spots.id', '=', 'map_spots_translates.row_id')->where('map_spots_translates.language', '=', \Lang::getLocale());
        if (request()->input('name', false) !== false) {
            $pages
                ->where('map_spots_translates.name', 'like', '%' . request()->input('name') . '%');
        }

        if (request()->input('address', false) !== false) {
            if (request()->input('name', false) !== false) {
                $pages->where('map_spots_translates.address', 'like', '%' . request()->input('address') . '%');
            }else{
                $pages->where('map_spots_translates.address', 'like', '%' . request()->input('address') . '%');

            }
        }

        if (request()->input('type', false) !== false) {
            $pages->where('type', request()->input('type'));
        }

        if (request()->input('region_id', false) !== false) {
            $pages->where('region_id', request()->input('region_id'));
        }

        if ($name !== null) {
            $pages->whereName($name);
        }

        if ($region_id !== null) {
            $pages->whereRegionId($region_id);
        }

        if ($type !== null) {
            $pages->whereType($type);
        }
        return $pages->paginate($limit);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function region()
    {
        return $this->hasOne(MapRegion::class, 'id', 'region_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lines()
    {
        return $this->hasMany(MapProductLine::class, 'spot_id', 'id');
    }

    /**
     * @return float
     */
    public function getLongitudeAttribute()
    {
        if (array_get($this->settings, 'longitude', false) !== false) {
            return (float)array_get($this->settings, 'longitude', false);
        }
        return 32.6125771;
    }

    /**
     * @return float
     */
    public function getLatitudeAttribute()
    {
        if (array_get($this->settings, 'latitude', false) !== false) {
            return (float)array_get($this->settings, 'latitude', false);
        }
        return 46.637326;
    }

    /**
     * @return int
     */
    public function getZoomAttribute()
    {
        if (array_get($this->settings, 'zoom', false) !== false) {
            return (int)array_get($this->settings, 'zoom', false);
        }
        return 17;
    }
}
