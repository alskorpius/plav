<?php

namespace App\Models\Map;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

/**
 * Class MapRegionTranslates
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $language
 * @property integer $row_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapRegionTranslates whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapRegionTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapRegionTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapRegionTranslates whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapRegionTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapRegionTranslates whereRowId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\Map\MapRegion $row

 */
class MapRegionTranslates extends Model
{
    use ModelTranslates;
    protected $table = 'map_regions_translates';

    protected $fillable = ['name', 'url', 'is_checked'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => ['required', 'max:191'],
        ];
    }

}
