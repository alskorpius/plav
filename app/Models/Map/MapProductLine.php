<?php

namespace App\Models\Map;

use App\Models\Catalog\ProductsLine;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Map\MapProductLine
 *
 * @property int $id
 * @property int $spot_id
 * @property int|null $line_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Catalog\ProductsLine $line
 * @property-read \App\Models\Map\MapSpot $spot
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapProductLine whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapProductLine whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapProductLine whereLineId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapProductLine whereSpotId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapProductLine whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MapProductLine extends Model
{
    protected $table = 'map_product_lines';

    protected $fillable = ['spot_id', 'line_id'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function _rules()
    {
        return [
            'spot_id' => ['required'],
            'line_id' => ['required'],
        ];
    }

    public function line()
    {
        return $this->hasOne(ProductsLine::class, 'id', 'line_id');
    }

    public function spot()
    {
        return $this->hasOne(MapSpot::class, 'id', 'spot_id');
    }
}
