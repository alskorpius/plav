<?php

namespace App\Models\Map;

use App\Traits\ModelMain;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

/**
 * App\Models\Map\MapRegion
 *
 * @property int $id
 * @property string $slug
 * @property int $position
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Map\MapRegionTranslates $current
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Map\MapSpot[] $spots
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapRegion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapRegion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Map\MapRegion whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Map\MapRegionTranslates[] $data
 */
class MapRegion extends Model
{
    use ModelMain;
    protected $table = 'map_regions';

    protected $fillable = ['slug','url', 'is_checked'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function _rules() {
        return [
            'slug' => ['required', 'alpha_dash', 'max:191', Rule::unique($this->getTable())->ignore($this->id)],
        ];
    }

    /**
     * @param null $name
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList($limit, $name = null) {
        $pages = self::select('map_regions_translates.*', 'map_regions.*')->oldest('map_regions.position');

        $pages->join('map_regions_translates', 'map_regions.id', '=', 'map_regions_translates.row_id') ;
        if (request()->input('name') != null) {
            $pages->where('map_regions_translates.name', 'like', '%' . request()->input('name') . '%');

        }
        $pages->where('map_regions_translates.language', '=', \Lang::getLocale());
        return $pages->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function spots() {
        return $this->hasMany(MapSpot::class, 'region_id', 'id');
    }
}