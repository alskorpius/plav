<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Request;

/**
 * App\Models\StatisticMy
 *
 * @property int $id
 * @property string $ip
 * @property string $str_url
 * @property string|null $response
 * @property string|null $device
 * @property string|null $platform
 * @property string|null $browser
 * @property string|null $refer
 * @property int $black_list_ip
 * @property string|null $comment
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatisticMy whereBlackListIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatisticMy whereBrowser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatisticMy whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatisticMy whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatisticMy whereDevice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatisticMy whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatisticMy whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatisticMy wherePlatform($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatisticMy whereRefer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatisticMy whereResponse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatisticMy whereStrUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StatisticMy whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class StatisticMy extends Model
{
	protected $table = 'kslStatistics';

	protected $fillable = ['ip', 'str_url', 'response', 'device', 'platform', 'browser', 'refer', 'black_list_ip', 'comment'];

	public static function getVisitorsList($limit)
	{
		$visitors = self::latest('created_at');

		$visitors->where('ip', '<>', '127.0.0.1');

		$visitors->groupBy('ip');

		$visitors->select('ip',
			DB::raw('count(*) as total'),
			DB::raw('min(created_at) as first'),
			DB::raw('max(created_at) as last'));

		return $visitors->paginate($limit);
	}

	public static function getHitsList($limit)
	{
		$visitors = self::latest('created_at');

		$visitors->where('ip', '<>', '127.0.0.1');

		$visitors->groupBy('str_url');

		$visitors->select('*',
			DB::raw('count(*) as total'),
			DB::raw('min(created_at) as first'),
			DB::raw('max(created_at) as last'));

		return $visitors->paginate($limit);
	}

	public static function getRefersList($limit)
	{
		$visitors = self::latest('created_at');

		$visitors->where('refer', '<>', null);

		return $visitors->paginate($limit);
	}

	/**
	 * Проверка наличия IP в черном списке (которые не надо выводить и сохранять в БД)
	 * если есть хоть одна строка, то вернет true
	 *
	 * @param string $ip
	 * @return bool
	 */
	public function inspection_black_list($ip)
	{

		$check = $this
			->where('ip', $ip)
			->where('black_list_ip', 1)
			->get();

		if (!$check->isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * @param string $ip
	 * @param string $str_url
	 * @param int $black_list_ip
	 * @return void
	 */
	public function setCount($ip, $str_url, $black_list_ip = 0, $response, $browser, $platform, $device){
		$this->ip = $ip;
		$this->str_url = $str_url;
		$this->black_list_ip = $black_list_ip;
		$this->response = $response;
		$this->browser = $browser;
		$this->platform = $platform;
		$this->device = $device;
		if (!stristr(Request::server('HTTP_REFERER'), 'plav')) {
			$this->refer = Request::server('HTTP_REFERER');
		}
		$this->save();
	}
}
