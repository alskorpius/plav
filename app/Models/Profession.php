<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class Profession extends Model
{

    protected $table = 'professions';

    protected $fillable = ['name', 'slug', 'status'];

    protected $casts = [
        'status' => 'boolean'
    ];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'slug' => ['required', 'alpha_dash', 'max:191', Rule::unique($this->table)->ignore($this->id)],
        ];
    }

    /**
     * Получаем список
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList($limit, $status = null)
    {
        $professions = self::orderBy('name');

        if (request()->input('name', false) !== false) {
            $professions->where('name', 'like', '%' . request()->input('name') . '%');
        }

        if ($status !== null){
            $professions->whereStatus($status);
        }
        return $professions->paginate($limit);
    }
}
