<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class Department extends Model
{

    protected $table = 'departments';

    protected $fillable = ['name', 'code', 'status'];

    protected $casts = [
        'status' => 'boolean'
    ];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'code' => ['required', 'max:191', Rule::unique($this->getTable())->ignore($this->id)],
        ];
    }

    /**
     * Получаем список
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList($limit, $status = null)
    {
        $departments = self::orderBy('code');

        if (request()->input('name', false) !== false) {
            $departments->where('name', 'like', '%' . request()->input('name') . '%');
        }

        if ($status !== null){
            $departments->whereStatus($status);
        }
        return $departments->paginate($limit);
    }
}
