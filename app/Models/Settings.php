<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Settings
 *
 * @package App\Models
 * @property integer $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property boolean $status
 * @property string $name
 * @property string $key
 * @property string $value
 * @property string $type
 * @property string $group
 * @property array $data
 * @property array $validation
 * @property array $position
 * @property boolean $api
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereApi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereValue($value)
 * @mixin \Eloquent
 * @property-read \App\Models\SettingsType $_group
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereValidation($value)
 */
class Settings extends Model
{
    protected $table = 'settings';

    protected $casts = [
        'status' => 'boolean',
        'api' => 'boolean',
        'data' => 'array',
        'validation' => 'array',
    ];

    protected $fillable = ['value'];

    protected $guarded = ['id', 'created_at', 'updated_at', 'name', 'position', 'status', 'data', 'key'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(SettingsType::class, 'alias', 'type');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function _group()
    {
        return $this->belongsTo(SettingsType::class, 'alias', 'group');
    }

    /**
     * @param null $status
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getItems($status = null)
    {
        $settings = static::select(['group', 'key', 'value'])->whereApi(true);
        if ($status !== null) {
            $settings->whereStatus((bool)$status);
        }
        return $settings->get();
    }

    /**
     * @param $data
     * @param null $status
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getItemsByKey($data, $status = null)
    {
        $settings = static::select()->where('key', $data);
        if ($status !== null) {
            $settings->whereStatus((bool)$status);
        }
        return $settings->get();
    }
}
