<?php
namespace App\Models;

/**
 * App\Models\Admin
 *
 * @property int $id
 * @property string|null $first_name
 * @property string|null $second_name
 * @property string|null $middle_name
 * @property string|null $phone
 * @property string|null $address
 * @property string|null $avatar
 * @property string $email
 * @property bool $subscription
 * @property string $password
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $last_login
 * @property \Carbon\Carbon|null $deleted_at
 * @property string|null $role_alias
 * @property bool $status
 * @property-read mixed $clear_phone
 * @property-read mixed $full_name
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \App\Models\UsersRole $role
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User customer()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User manager()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereLastLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereRoleAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereSecondName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereSubscription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property bool $confirmed
 * @property string|null $token
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereToken($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\Product[] $products
 * @property string|null $last_ip
 * @property int|null $count_auth
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereCountAuth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereLastIp($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cart\Cart[] $carts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order\Order[] $orders
 * @property string|null $confirm_token
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereConfirmToken($value)
 */
class Admin extends User
{
    protected $table = 'users';
}