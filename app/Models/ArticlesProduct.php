<?php

namespace App\Models;

use App\Models\Catalog\Product;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ArticlesProduct
 *
 * @package App\Models
 * @property int $id
 * @property int $article_id
 * @property int $product_id
 * @property int $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Article[] $articles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesProduct whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesProduct whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesProduct whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesProduct whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\Product[] $products
 * @property-read \App\Models\Article $article
 * @property-read \App\Models\Catalog\Product $product
 */

class ArticlesProduct extends Model {

    protected $table = 'articles_products';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function article() {
        return $this->hasOne(Article::class, 'id', 'article_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product() {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}
