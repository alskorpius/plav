<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelMain;

/**
 * Class MailTemplate
 *
 * @package App\Models
 * @property integer $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property boolean $status
 * @property string $name
 * @property string $alias
 * @property array $variables
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MailTemplate whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MailTemplate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MailTemplate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MailTemplate whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MailTemplate whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MailTemplate whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\MailTemplateTranslates $current
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MailTemplate whereVariables($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MailTemplateTranslates[] $data
 */
class MailTemplate extends Model
{
    use ModelMain;

    const TYPE_REGISTRATION = 'registration';
    const TYPE_REGISTRATION_ADMIN = 'registration_admin';
    const TYPE_NEW_ORDER_ADMIN = 'new_order_admin';
    const TYPE_NEW_ORDER = 'resend_order';
    const TYPE_FORGOT_PASSWORD = 'forgot_password';
    const TYPE_FORGOT_PASSWORD_ADMIN = 'forgot_password_admin';
    const TYPE_CALLBACK_ADMIN = 'callback_admin';
    const TYPE_SUBSCRIBE = 'news_send';
    const USER_SUBSCRIBE = 'user_subscribe';
    const TYPE_ARTICLE_REVIEW = 'article_review';
    const TYPE_EVENT_REG = 'event_reg';
    const TYPE_EVENT_REG_ADMIN = 'event_reg_admin';
    const TYPE_PRODUCT_REVIEW = 'product_review';
    const TYPE_PRODUCT_WAIT = 'product_wait';
    const TYPE_PRODUCT_ISSET = 'product_is_avilable';
    const TYPE_PRODUCT_WAIT_ADMIN = 'product_wait_admin';

    protected $fillable = ['name', 'status'];

    protected $casts = [
        'status' => 'boolean',
        'data' => 'array',
    ];

    /**
     * @return array
     */
    public function _rules()
    {
        return [
            'status' => ['required', 'boolean'],
            'name' => ['required', 'max:191'],
        ];
    }

}
