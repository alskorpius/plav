<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use App\Traits\ModelMain;

/**
 * Class Articles_category
 *
 * @package App\Models
 * @property int $id
 * @property int $position
 * @property bool $status
 * @property string $slug
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\ArticlesCategoryTranslates $current
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Article[] $articles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesCategory wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesCategory whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesCategory whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read string $human_date
 * @property-read string $summary
 * @property int $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesCategory whereType($value)
 * @property int|null $menu
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesCategory whereMenu($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ArticlesCategoryTranslates[] $data
 */
class ArticlesCategory extends Model
{
    use ModelMain;

    protected $table = 'articles_categories';
    protected $casts = ['status' => 'boolean',];

    protected $fillable = ['slug', 'position', 'status'];


    /**
     * Правила валидации
     *
     * @return array
     */
    public function _rules() {
        return [
            'slug' => ['required', 'alpha_dash', 'max:191', Rule::unique($this->getTable())->ignore($this->id)],
            'status' => ['required', 'boolean'],
            'position' => ['required', 'numeric', 'min:1'],
        ];
    }

    /**
     * @param array $ids
     * @param null $status
     * @return array|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getListByIds(array $ids, $status = null) {
        if (!$ids) {
            return [];
        }
        $news = static::whereIn('id', $ids);
        if ($status !== null) {
            $news->whereStatus($status);
        }
        return $news->latest('created_at')->get();
    }

    /**
     * Краткое описание статьи
     *
     * @return string
     */
    public function getSummaryAttribute() {
        return Str::limit(strip_tags($this->content), 120, '...');
    }

    /**
     * @return string
     */
    public function getHumanDateAttribute() {
        return $this->created_at->format('j ' . $this->shortNameOfTheMonth($this->date->format('n')) . ' Y');
    }

    /**
     * @param $limit
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList($limit = null, $type = null, $status = null, $front = null) {
        $pages = self::with(['current'])->latest('articles_categories.created_at');

        if (request()->input('name', '') !== '' && request()->input('name', '') !== null) {
            $pages
                ->join('articles_categories_translates', 'articles_categories.id', '=', 'articles_categories_translates.row_id')
                ->where('articles_categories_translates.name', 'like', '%' . request()->input('name') . '%')
                ->where('articles_categories_translates.language', '=', \Lang::getLocale());
        }

        if (request()->input('created_at', false) !== false) {
            $pages->where('created_at', '=', request()->input('date'));
        }

        if ($status !== null) {
            $pages->whereStatus($status);
        }
        if ($type !== null) {
            $pages->whereType($type);
        }

        if ($front !== null) {
            $articles = Article::getList(10000, $type, 1);
            $categoriesArr = [];
            foreach ($articles as $article) {
                $categoriesArr[] = $article->articles_categories_id;
            }
            $pages->whereIn('id', $categoriesArr);
        }

        if ($limit == null) {
            return $pages->get();
        }
        return $pages->paginate($limit);
    }

    /**
     * Формируем данные для АПИ
     *
     * @return array
     */
    public function api() {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'date' => $this->human_date,
        ];
    }

    /**
     * Мета даныне
     *
     * @return array
     */
    public function meta() {
        // Мета теги по дефолту
        $meta = [
            'h1' => $this->current->h1 !== null ? $this->current->h1 : $this->current->name,
            'title' => $this->current->title ? $this->current->title : $this->current->name,
            'description' => $this->current->description ? $this->current->description : $this->current->name,
            'keywords' => $this->current->keywords ? $this->current->keywords : $this->current->name,
        ];
        // Используем шаблон мета тегов для статтей
        if ($template = SeoTemplate::getTemplate(SeoTemplate::ARTICLES)) {
            if (!$meta['h1'] && $template->h1) {
                $meta['h1'] = str_replace(['{name}'], [$this->current->name], $template->h1);
            }
            if (!$meta['title'] && $template->title) {
                $meta['title'] = str_replace(['{name}'], [$this->current->name], $template->title);
            }
            if (!$meta['description'] && $template->description) {
                $meta['description'] = str_replace(['{name}'], [$this->current->name], $template->description);
            }
            if (!$meta['keywords'] && $template->keywords) {
                $meta['keywords'] = str_replace(['{name}'], [$this->current->name], $template->keywords);
            }
        }
        return $meta;
    }

    /**
     * Список статтей для возврата по АПИ
     *
     * @param $limit
     * @param null|bool $status
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getItems($limit, $status = null) {
        $article = static::where('created_at', '<=', Carbon::now());
        if ($status !== null) {
            $article->whereStatus((bool)$status);
        }
        return $article->where('created_at', '<=', Carbon::now())
            ->latest('created_at')
            ->paginate($limit);
    }

    /**
     * Достаем одну статью по алиасу страницы
     *
     * @param $slug
     * @param null|bool $status
     * @return Model|static
     */
    public static function getItemBySlug($slug, $status = null) {
        $article = static::whereSlug($slug);
        if ($status !== null) {
            $article->whereStatus((bool)$status);
        }
        return $article->where('created_at', '<=', Carbon::now())->firstOrFail();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles() {
        return $this->hasMany(Article::class, 'articles_categories_id', 'id');
    }

}
