<?php

namespace App\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Article
 *
 * @package App\Models
 * @property integer $id
 * @property string $language
 * @property string $content
 * @property string $h1
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $name
 * @property integer $row_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTranslates whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTranslates whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTranslates whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTranslates whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTranslates whereTitle($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\Article $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticleTranslates whereRowId($value)
 */
class ArticleTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'articles_translates';

    protected $fillable = ['name', 'text', 'h1', 'title', 'keywords', 'description'];


    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:191'],
        ];
    }

}
