<?php

namespace App\Models;

use App\Models\Catalog\Product;
use Illuminate\Database\Eloquent\Model;
use App\Traits\HumanDate;
use App\Traits\ModelImages;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use App\Traits\ModelMain;

/**
 * Class Article
 *
 * @package App\Models
 * @property int $id
 * @property int|null $articles_categories_id
 * @property int $position
 * @property bool $status
 * @property string $slug
 * @property string $image
 * @property \Carbon\Carbon $published_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\ArticlesCategory $articlesCategory
 * @property-read \App\Models\ArticleTranslates $current
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ArticlesComment[] $articlesComments
 * @property-read \App\Models\ArticlesProduct $articlesProduct
 * @property-read \App\Models\EventsRegistration $eventsRegistration
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereArticlesCategoriesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $type 0 - news, 1 - articles, 2 - events
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereType($value)
 * @property-read string $human_date
 * @property-read string $summary
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\Product[] $products
 * @property-read mixed $category_name
 * @property-read mixed $url
 * @property int|null $view_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereViewCount($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ArticleTranslates[] $data
 * @property-read mixed $recomm
 */
class Article extends Model
{
	use ModelImages, HumanDate, ModelMain;

	const TYPE_ARTICLES = 0;
	const TYPE_NEWS = 1;
	const TYPE_EVENTS = 2;

	protected $table = 'articles';

	protected $fillable = ['position', 'articles_categories_id', 'slug', 'published_at', 'image', 'status', 'type'];

	protected $dates = ['published_at'];

	/**
	 * @inheritdoc
	 */
	protected function imagesFolders()
	{
		return [
			'articles' => [
				'small' => [
					'width' => 470,
					'height' => 330,
					'resize' => true,
					'crop' => true,
					'watermark' => false,
				],
				'big' => [
					'width' => 960,
					'height' => 650,
					'resize' => true,
					'crop' => false,
					'watermark' => false,
				],
				'original' => [],
			],
		];
	}

	/**
	 * Правила валидации
	 *
	 * @return array
	 */
	public function _rules()
	{
		return [
			'slug' => ['required', 'alpha_dash', 'max:191', Rule::unique($this->getTable())->ignore($this->id)],
			'status' => ['required'],
			'image' => ['sometimes', 'image', 'max:' . config('custom.image-max-size')],
		];
	}

	/**
	 * @param array $ids
	 * @param null $status
	 * @return array|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
	 */
	public static function getListByIds(array $ids, $status = null)
	{
		if (!$ids) {
			return [];
		}
		$news = static::whereIn('id', $ids);
		if ($status !== null) {
			$news->whereStatus($status);
		}
		return $news->latest('published_at')->get();
	}

	public static function getListByName($name)
	{
		$pages = self::latest('created_at')->where('name', 'like', '%' . $name . '%');
		return $pages->get();
	}

	/**
	 * Краткое описание статьи
	 *
	 * @return string
	 */
	public function getSummaryAttribute()
	{
		return Str::limit(strip_tags($this->content), 120, '...');
	}

	/**
	 * @return string
	 */
	public function getHumanDateAttribute()
	{
		return $this->published_at->format('d.m.y');
	}

	/**
	 * @param $limit
	 * @param null $type
	 * @param null $status
	 * @param null $frontView
	 * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
	 */
	public static function getList($limit, $type = null, $status = null, $frontView = null)
	{
		$pages = self::with(['current'])->orderBy('articles.updated_at', 'DESC');
		if (request()->input('name', false) !== false) {
            $pages
                ->join('articles_translates', 'articles.id', '=', 'articles_translates.row_id')
                ->where('articles_translates.name', 'like', '%' . request()->input('name') . '%')
                ->where('articles_translates.language', '=', \Lang::getLocale());
		}

		if (request()->input('published_at', false) !== false) {
			$pages->where('published_at', 'like', '%' . request()->input('published_at') . '%');
		}

		if (request()->input('category_id', false) !== false) {
			$pages->where('articles_categories_id', request()->input('category_id'));
		}

		if ($status !== null) {
			$pages->whereStatus($status);
		}

		if ($type !== null) {
			$pages->whereType($type);
		}

		if ($frontView !== null) {
			$categoriesArr = [];
			$categories = ArticlesCategory::whereType($type)->where('status', 1)->get();

			foreach ($categories as $category) {
				$categoriesArr[] = $category->id;
			}

			$pages->whereIn('articles_categories_id', $categoriesArr);
		}

		return $pages->paginate($limit);
	}

	public static function getListForClientSide($limit, $type = null, $status = null, $frontView = null, $category_id = null)
	{
		$pages = self::with('current')->orderBy('published_at', 'DESC');

		$pages->where('published_at', '<=', Carbon::now());
		if ($status !== null) {
			$pages->whereStatus($status);
		}

		if ($type !== null) {
			$pages->whereType($type);
		}

		if ($frontView !== null) {
			$categoriesArr = [];
			$categories = ArticlesCategory::whereType($type)->where('status', 1)->get();

			foreach ($categories as $category) {
				$categoriesArr[] = $category->id;
			}

			$pages->whereIn('articles_categories_id', $categoriesArr);
		}

		if ($category_id !== null) {
			$pages->whereArticlesCategoriesId($category_id);
		}

		return $pages->paginate($limit);
	}

	/**
	 * Мета даныне
	 *
	 * @return array
	 */
	public function meta()
	{
		// Мета теги по дефолту
		$meta = [
			'h1' => $this->current->h1 !== null ? $this->current->h1 : $this->current->name,
			'title' => $this->current->title !== null ? $this->current->title : $this->current->name,
			'description' => $this->current->description !== null ? $this->current->description : $this->current->name,
			'keywords' => $this->current->keywords,
			'content' => $this->current->content,
		];
		// Используем шаблон мета тегов для статтей
		if ($template = SeoTemplate::getTemplate(SeoTemplate::ARTICLES)) {
			if (!$meta['h1'] && $template->h1) {
				$meta['h1'] = str_replace(['{name}'], [$this->current->name], $template->h1);
			}
			if (!$meta['title'] && $template->title) {
				$meta['title'] = str_replace(['{name}'], [$this->current->name], $template->title);
			}
			if (!$meta['description'] && $template->description) {
				$meta['description'] = str_replace(['{name}'], [$this->current->name], $template->description);
			}
			if (!$meta['keywords'] && $template->keywords) {
				$meta['keywords'] = str_replace(['{name}'], [$this->current->name], $template->keywords);
			}
		}
		return $meta;
	}

	/**
	 * Список статтей для возврата по АПИ
	 *
	 * @param $limit
	 * @param null|bool $status
	 * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
	 */
	public static function getItems($limit, $status = null)
	{
		$article = static::where('created_at', '<=', Carbon::now());
		if ($status !== null) {
			$article->whereStatus((bool)$status);
		}
		return $article->where('published_at', '<=', Carbon::now())
			->latest('published_at')
			->paginate($limit);
	}

	/**
	 * Достаем одну статью по алиасу страницы
	 *
	 * @param $slug
	 * @param null|bool $status
	 * @return Model|static
	 */
	public static function getItemBySlug($slug, $status = null)
	{
		$article = static::whereSlug($slug);
		if ($status !== null) {
			$article->whereStatus((bool)$status);
		}
		return $article->where('published_at', '<=', Carbon::now())->firstOrFail();
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function articlesCategory()
	{
		return $this->hasOne(ArticlesCategory::class, 'id', 'articles_categories_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
	public function products()
	{
		return $this->hasMany(ArticlesProduct::class, 'article_id', 'id');
	}

	public function getRecommAttribute() {
	    $products = ArticlesProduct::whereArticleId($this->id)->get();
	    $ids = [];
	    $i = 0;
	    foreach ($products as $item) {
            $ids[$i] = $item->product_id;
	        $i++;
        }
        $products = Product::whereIn('id', $ids)->get();
	    return $products;
    }
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function articlesComments()
	{
		return $this->hasMany(Comment::class, 'article_id', 'id')->where('status', 1)->orderBy('created_at', 'DESC');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function eventsRegistration()
	{
		return $this->hasMany(EventsRegistration::class, 'article_id', 'id')->where('status', 1)->orderBy('created_at', 'DESC');
	}

	public function getUrlAttribute()
	{
		$slug = $this->slug;
		$category = $this->articlesCategory->slug;
		switch ($this->type) {
			case 0:
				return route('content.show', ['type' => 'news', 'category' => $category, 'slug' => $slug]);
			case 1:
				return route('content.show', ['type' => 'articles', 'category' => $category, 'slug' => $slug]);
			case 2:
				return route('content.show', ['type' => 'events', 'category' => $category, 'slug' => $slug]);
		}
		return null;
	}

	public function getCategoryNameAttribute()
	{
		if ($this->articlesCategory && $this->articlesCategory->exists) {
			return $this->articlesCategory->name;
		}
		return null;
	}
}
