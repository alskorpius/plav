<?php

namespace App\Models\Cart;

use App\Models\User;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Str;

/**
 * App\Models\Cart\Cart
 *
 * @property int $id
 * @property int $user_id
 * @property string $hash
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cart\Cart whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cart\Cart whereHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cart\Cart whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cart\Cart whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cart\Cart whereUserId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cart\CartsItem[] $items
 * @property-read \App\Models\User $user
 */
class Cart extends Model
{
	protected $table = 'carts';

	protected $fillable = ['user_id', 'hash'];

	private static $instance = null;


	/**
	 * Правила валидации
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'user_id' => ['numeric', 'max:191'],
			'hash' => ['required'],
		];
	}

	/**
	 * @param $limit
	 * @param null $user_id
	 * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
	 * @internal param null $type
	 * @internal param null $status
	 */
	public static function getList($limit, $user_id = null)
	{
		$carts = self::orderBy('created_at', 'DESC');

		if (request()->input('created_at', false) !== false) {
			$carts->where('created_at', 'like', '%' . request()->input('created_at') . '%');
		}

		if (request()->input('user_id', false) !== false) {
			$carts->where('user_id', request()->input('user_id'));
		}

		if ($user_id !== null) {
			$carts->whereUserId($user_id);
		}

		return $carts->paginate($limit);
	}


	public static function loaded()
	{
		return static::$instance !== null;
	}

	public static function instance()
	{
		return static::$instance;
	}

	public static function set($cart)
	{
		return static::$instance = $cart;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function user()
	{
		return $this->hasOne(User::class, 'id', 'user_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function items()
	{
		return $this->hasMany(CartsItem::class, 'cart_id', 'id')->with('price');
	}

	public function createRow()
	{
		$hash = $this->generateHash();
		$this->hash = $hash;
		if (Auth::check()) {
			$this->user_id = Auth::user()->id;
		}
		$this->save();

		Cookie::queue('cart_hash', $hash, 44640, '/');
		return $this;
	}

	public function generateHash()
	{
		return sha1(microtime() . Str::random());
	}

	public function api()
	{
		return [
			'items' => $this->items->map(function (CartsItem $cartsItem) {
				return $cartsItem->api();
			})->toArray(),
		];
	}

}
