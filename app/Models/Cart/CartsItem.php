<?php

namespace App\Models\Cart;

use App\Models\Catalog\Product;
use App\Models\Catalog\ProductsPrice;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Cart\CartsItem
 *
 * @property int $id
 * @property int $cart_id
 * @property int $product_id
 * @property int $count
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property mixed product
 * @property mixed cart
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cart\CartsItem whereCartId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cart\CartsItem whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cart\CartsItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cart\CartsItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cart\CartsItem whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cart\CartsItem whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $price_id
 * @property-read ProductsPrice $price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cart\CartsItem wherePriceId($value)
 */
class CartsItem extends Model
{
    protected $table = 'carts_items';

    protected $fillable = ['cart_id', 'product_id', 'count', 'price_id', 'weight'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => ['required', 'numeric'],
            'cart_id' => ['required', 'numeric'],
            'count' => ['numeric'],
            'price_id' => ['numeric'],
        ];
    }

    /**
     * @param $limit
     * @param null $type
     * @param null $status
     * @return \Illuminate\Support\Collection
     */
    public static function getList($cart_id = null)
    {
        $cartItems = self::orderBy('name', 'DESC');


        if ($cart_id !== null) {
            $cartItems->whereCartId($cart_id);
        }

        return $cartItems->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cart()
    {
        return $this->hasOne(Cart::class, 'id', 'cart_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function price()
    {
        return $this->hasOne(ProductsPrice::class, 'id', 'price_id');
    }

    /**
     * @return array
     */
    public function api()
    {
        return [
            'product' => $this->product->api(),
            'id' => (int)$this->id,
            'is_accessory' => (int)$this->product->is_accessory,
            'count' => (int)$this->count,
            'amountItem' => (int)$this->price->price * (int)$this->count,
            'priceOneItem' => (int)$this->price->price,
            'year' => (int)$this->price->year,
            'volume' => (int)$this->price->volume,
            'weight' => (float)$this->price->weight,
        ];
    }
}
