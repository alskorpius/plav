<?php

namespace App\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

/**
 * Class ArticlesCategoryTranslates
 *
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string|null $h1
 * @property string|null $title
 * @property string|null $keywords
 * @property string|null $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $language
 * @property integer $row_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesCategoryTranslates whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesCategoryTranslates whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesCategoryTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesCategoryTranslates whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesCategoryTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesCategoryTranslates whereTitle($value)
 * @mixin \Eloquent
 * @property-read string $human_date
 * @property-read string $summary
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\ArticlesCategory $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesCategoryTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesCategoryTranslates whereRowId($value)
 */
class ArticlesCategoryTranslates extends Model
{
    use ModelTranslates;
    protected $table = 'articles_categories_translates';

    protected $fillable = ['name', 'h1', 'title', 'keywords', 'description'];


    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:191'],
        ];
    }

}
