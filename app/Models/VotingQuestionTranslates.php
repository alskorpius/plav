<?php

namespace App\Models;

use App\Traits\ModelTranslates;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Voting_question_translates
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $language
 * @property integer $row_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingQuestion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingQuestion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingQuestion whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingQuestion whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\Map\VotingQuestion $row
 */
class VotingQuestionTranslates extends Model
{
    use ModelTranslates;
    protected $table = 'voting_questions_translates';

    protected $fillable = ['name', 'description'];


    /**
     * @return array
     */
    public function _rules()
    {
        return [
            'name' => ['required'],
        ];
    }
}
