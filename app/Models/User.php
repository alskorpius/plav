<?php namespace App\Models;

use App\Helpers\Notify;
use App\Models\Cart\Cart;
use App\Models\Catalog\Product;
use App\Models\Order\Order;
use App\Notifications\Registration;
use App\Traits\ModelImages;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Validation\Rule;

/**
 * App\Models\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 * @property int $id
 * @property string $full_name
 * @property string $email
 * @property string $password
 * @property string $avatar
 * @property \Carbon\Carbon|null $last_login
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @property UsersRole $role
 * @property string $first_name
 * @property string $second_name
 * @property string $middle_name
 * @property string $phone
 * @property string $address
 * @property boolean $subscription
 * @property boolean $status
 * @property string|null $role_alias
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRoleAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereSecondName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereSubscription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withoutTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User customer()
 * @property-read mixed $clear_phone
 * @property-read integer $spent
 * @property-read integer $discount_percent
 * @property-read integer $count_of_completed_orders
 * @property-read integer $count_of_orders
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User manager()
 * @property string $name
 * @property int $confirmed
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\Product[] $products
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereConfirmed($value)
 * @property string|null $last_ip
 * @property int|null $count_auth
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCountAuth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastIp($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cart\Cart[] $carts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order\Order[] $orders
 * @property string|null $confirm_token
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereConfirmToken($value)
 */
class User extends Authenticatable
{
	use Notifiable, SoftDeletes, ModelImages;

	protected $dates = ['deleted_at', 'last_login'];

	protected $casts = [
		'status' => 'boolean',
		'subscription' => 'boolean',
	];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'first_name', 'second_name', 'middle_name', 'phone', 'address', 'status', 'email',
		'subscription', 'avatar', 'role_alias',
	];

	protected $guarded = ['id', self::CREATED_AT, self::UPDATED_AT, 'deleted_at'];

	/**
	 * @return int
	 */
	public static function subscribersCount()
	{
		return static::customer()->whereStatus(true)->whereSubscription(true)->count('id');
	}

	/**
	 * @return mixed
	 */
	public static function subscribers()
	{
		return static::customer()->whereStatus(true)->whereSubscription(true)->get()->mapWithKeys(function (User $user) {
			return [$user->id => $user->email];
		})->toArray();
	}

	/**
	 * @param $q
	 * @param $limit
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
	 */
	public static function search($q, $limit)
	{
		$users = static::latest('id')->whereStatus(true);
		$users->where(function ($query) use ($q) {
			$query->whereId($q);
			$query->orWhere('email', 'like', $q . '%');
			$query->orWhere('first_name', 'like', $q . '%');
			$query->orWhere('second_name', 'like', $q . '%');
			$query->orWhere('phone', 'like', $q . '%');
		});
		return $users->limit($limit)->get();
	}

	/**
	 * @return array
	 */
	public function vue()
	{
		$user = $this->toArray();
		$user['full_name'] = $this->full_name;
		$user['count_of_orders'] = $this->count_of_orders;
		$user['count_of_completed_orders'] = $this->count_of_completed_orders;
		$user['spent'] = $this->spent;
		$user['trashed'] = $this->trashed();
		$user['urls'] = [
			'edit' => route('customers.edit', ['customer' => $this->id]),
		];
		return $user;
	}

	/**
	 * @return array
	 */
	public function customerRules()
	{
		$rules = [
			'first_name' => ['nullable', 'min:2', 'max:191'],
			'second_name' => ['nullable', 'min:2', 'max:191'],
			'middle_name' => ['nullable', 'min:2', 'max:191'],
			'phone' => ['nullable', 'numeric'],
			'address' => ['nullable'],
			'status' => ['required', 'boolean'],
			'subscription' => ['required', 'boolean'],
			'email' => ['required', 'email', Rule::unique('users')->ignore($this->id)],
			'password' => ['nullable', 'min:5'],
		];
		if ($this->exists === false) {
			$rules['password'] = ['required', 'min:5'];
		}
		return $rules;
	}

	/**
	 * @return array
	 */
	public function managerRules()
	{
		$rules = [
			'first_name' => ['required', 'min:2', 'max:191'],
			'second_name' => ['nullable', 'min:2', 'max:191'],
			'status' => ['required', 'boolean'],
			'email' => ['required', 'email', Rule::unique('users')->ignore($this->id)],
			'phone' => ['required', 'numeric'],
			'password' => ['nullable', 'min:5'],
			'avatar' => ['nullable', 'image', 'max:' . config('custom.image-max-size')],
		];
		if ($this->exists === false) {
			$rules['password'] = ['required', 'min:5'];
		}
		return $rules;
	}

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	/**
	 * @return string
	 */
	protected function imageField()
	{
		return 'avatar';
	}

	/**
	 * @return array
	 */
	public function imagesFolders()
	{
		return [
			'avatar' => [
				'icon' => [
					'width' => 160,
					'height' => 160,
					'crop' => true,
				],
				'original' => [],
			],
		];
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function role()
	{
		return $this->hasOne(UsersRole::class, 'alias', 'role_alias');
	}

	/**
	 * @return string
	 */
	public function getFullNameAttribute()
	{
		return trim($this->first_name . ' ' . $this->second_name);
	}

	public function lastLoginUpdate()
	{
		$this->last_login = Carbon::now();
		$this->save();
	}

	/**
	 * @return array
	 */
	public function api()
	{
		return [
			'id' => $this->id,
			'email' => $this->email,
			'first_name' => $this->first_name,
			'second_name' => $this->second_name,
			'middle_name' => $this->middle_name,
			'phone' => $this->phone,
			'address' => $this->address,
			'subscription' => $this->subscription,
			'created_at' => $this->created_at ? $this->created_at->format('d.m.Y, H:i:s') : null,
			'last_login' => $this->last_login ? $this->last_login->format('d.m.Y, H:i:s') : null,
			'discount' => $this->discount_percent,
		];
	}

	/**
	 * Scope a query to only include customers.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeCustomer($query)
	{
		return $query->where('role_alias', '=', UsersRole::USER);
	}

	/**
	 * Scope a query to only include customers.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeManager($query)
	{
		return $query->where('role_alias', '=', UsersRole::MANAGER);
	}

	/**
	 * @return bool
	 */
	public function isAdmin()
	{
		return $this->role_alias === UsersRole::ADMIN;
	}

	/**
	 * @return bool
	 */
	public function isManager()
	{
		return $this->role_alias !== UsersRole::ADMIN;
	}

	public function confirm()
	{
		$this->confirmed = 1;
		$saved = $this->save();

		if ($saved){
			return true;
		}
		return false;
	}

	/**
	 * @return bool
	 */
	public function isCustomer()
	{
		return $this->role_alias === UsersRole::USER;
	}

	/**
	 * @param $id
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 */
	public static function getTrashedUser($id)
	{
		return static::onlyTrashed()->findOrFail($id);
	}

	public static function findByToken($token)
	{
    	return User::whereConfirmToken($token)->first();
	}

	/**
	 * @param $limit
	 * @param bool $trashed
	 * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
	 */
	public function getCustomers($limit, $trashed = false)
	{
		$users = self::customer();
		if (request()->input('full_name') !== null) {
			$users->where('first_name', 'like', '%' . request()->input('full_name') . '%')
				->orWhere('second_name', 'like', '%' . request()->input('full_name') . '%');
		}
		if (request()->input('email') !== null) {
			$users->where('email', 'like', '%' . request()->input('email') . '%');
		}
		if (request()->input('phone') !== null) {
			$users->where('phone', 'like', '%' . request()->input('phone') . '%');
		}
		if (request()->input('created_at') !== null) {
			$users->where('created_at', 'like', '%' . request()->input('created_at') . '%');
		}
		if ($trashed === true) {
			$users->onlyTrashed();
		}
		return $users->latest('id')->paginate($limit);
	}

	/**
	 * @param $limit
	 * @param bool $trashed
	 * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
	 */
	public function getManagers($limit, $trashed = false)
	{
		$users = self::where('role_alias', '<>', 'user');
		if (request()->input('full_name') !== null) {
			$users->where('first_name', 'like', '%' . request()->input('full_name') . '%')
				->orWhere('second_name', 'like', '%' . request()->input('full_name') . '%');
		}
		if (request()->input('email') !== null) {
			$users->where('email', 'like', '%' . request()->input('email') . '%');
		}
		if (request()->input('phone') !== null) {
			$users->where('phone', 'like', '%' . request()->input('phone') . '%');
		}
		if (request()->input('created_at') !== null) {
			$users->where('created_at', 'like', '%' . request()->input('created_at') . '%');
		}
		if ($trashed === true) {
			$users->onlyTrashed();
		}
		return $users->latest('id')->paginate($limit);
	}

	/**
	 * @return mixed
	 */
	public function getClearPhoneAttribute()
	{
		return preg_replace('/[^0-9\+]*/', '', $this->phone);
	}

	/**
	 * Достаем пользователя из БД.
	 * Восстанавливаем в правах, если нужно.
	 *
	 * @param $email
	 * @return \Illuminate\Database\Eloquent\Model|null|static
	 */
	public static function userForOrderByEmail($email)
	{
		$user = static::whereEmail($email)->withTrashed()->first();
		if (!$user) {
			return null;
		}
		if ($user->status === true && $user->trashed() === false) {
			return $user;
		}
		if ($user->status === false) {
			$user->status = true;
		}
		if ($user->trashed() === true) {
			$user->deleted_at = null;
		}
		$user->save();
		return $user;
	}

	/**
	 * Регистрируем нового пользователя
	 *
	 * @param null $name
	 * @param null $password
	 * @return $this|\Illuminate\Database\Eloquent\Model
	 */
	public static function registration($name = null, $password = null)
	{
		$name = request()->input('first_name', $name);
		$nameArr = explode(' ', $name);
		$password = request()->input('password', $password);
		$confirmToken = str_random(30);
		$user = (new User)->forceCreate([
			'first_name' => isset($nameArr[1]) ? $nameArr[1] : null,
			'second_name' => isset($nameArr[0]) ? $nameArr[0] : null,
			'middle_name' => isset($nameArr[2]) ? $nameArr[2] : null,
			'email' => request()->input('email'),
			'password' => \Hash::make($password),
			'confirm_token' => $confirmToken,
			'status' => true,
			'subscription' => true,
			'role_alias' => UsersRole::USER,
			'confirmed' => 1,
		]);
		Notify::send(new Registration($user, $password));


		return $user;
	}

}
