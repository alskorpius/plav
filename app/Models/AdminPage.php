<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AdminPage
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminPage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminPage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminPage whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminPage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AdminPage extends Model
{
    protected $table = 'admin_pages';

    protected $fillable = ['name'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
        ];
    }
}
