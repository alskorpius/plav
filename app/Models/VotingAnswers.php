<?php

namespace App\Models;

use App\Traits\ModelMain;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Voting_answers
 *
 * @property int $id
 * @property int $question_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\VotingAnswersTranslates $current
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingAnswers whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingAnswers whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingAnswers whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingAnswers whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingAnswers whereStatus($value)
 * @property-read \App\Models\VotingQuestion $votingQuestion
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VotingResult[] $votingResult
 * @property int $position
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingAnswers wherePosition($value)
 * @property int|null $own_percent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingAnswers whereOwnPercent($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VotingAnswersTranslates[] $data
 */
class VotingAnswers extends Model
{
    use ModelMain;

    protected $table = 'voting_answers';

    protected $fillable = ['question_id', 'status',];

    protected $casts = ['status' => 'boolean'];

    public function _rules()
    {
        return [
            'question_id' => ['required'],
        ];
    }

    /**
     * @param null $question_id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getList($question_id = null)
    {
        $answers = VotingAnswers::with('current')->oldest('position');

        if ($question_id !== null) {
            $answers->where('question_id', '=', $question_id);
        }

        if (request()->input('name', false) !== false) {
            $answers->where('name', 'like', '%' . request()->input('name') . '%');
        }

        return $answers->get();
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Query\Builder|static
     */
    public static function getItemById($id)
    {
        $answer = VotingAnswers::with('current')->oldest('position');

            $answer->where('id', '=', $id);

        return $answer;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function votingQuestion()
    {
        return $this->hasOne(VotingQuestion::class, 'id', 'question_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function votingResult()
    {
        return $this->hasMany(VotingResult::class, 'answer_id', 'id');
    }
}
