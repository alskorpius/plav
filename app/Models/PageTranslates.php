<?php

namespace App\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PageTranslates
 *
 * @package App\Models
 * @property integer $id
 * @property string $language
 * @property string $name
 * @property string $h1
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $content
 * @property integer $row_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageTranslates whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageTranslates whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageTranslates whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageTranslates whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageTranslates whereTitle($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Page $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PageTranslates whereRowId($value)
 * @property-read \App\Models\Language $lang
 */
class PageTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'pages_translates';

    public $timestamps = false;

    protected $fillable = ['name', 'content', 'h1', 'title', 'keywords', 'description'];

    public function rules()
    {
        return [
            'name' => ['required', 'max:191'],
        ];
    }

}
