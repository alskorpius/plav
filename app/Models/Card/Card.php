<?php

namespace App\Models\Card;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $table = 'cards';

    protected $fillable = ['first_name', 'last_name', 'middle_name', 'birthday', 'gender', 'married', 'fisherman', 'payment', 'department_id',
        'profession_id', 'factor_id', 'is_registration_address', 'last_visit'];

    protected $casts = [
    ];

    protected $dates = ['birthday', 'last_visit'];

    protected $guarded = ['id', self::CREATED_AT, self::UPDATED_AT, 'deleted_at'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required'],
            'last_name' => ['required'],
            'middle_name' => ['required'],
            'birthday' => ['required'],
            'gender' => ['required'],
            'married' => ['required'],
            'fisherman' => ['required'],
            'payment' => ['required'],
            'department_id' => ['required', 'not_in:0'],
            'profession_id' => ['required', 'not_in:0'],
            'factor_id' => ['required', 'not_in:0'],
            'registration_address.city_id' => ['required', 'not_in:0'],
            'registration_address.street_id' => ['required', 'not_in:0'],
            'registration_address.house_number' => ['required'],
            'fact_address.city_id' => ['required', 'not_in:0'],
            'fact_address.street_id' => ['required', 'not_in:0'],
            'fact_address.house_number' => ['required'],
        ];
    }

    public function addresses()
    {
        return $this->hasMany(CardsAddress::class, 'card_id', 'id')->orderBy('is_registration_address');
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return trim($this->last_name . ' ' . $this->first_name . ' ' . $this->middle_name);
    }

    /**
     * @return string
     */
    public function getHumanDateAttribute()
    {
        return $this->birthday->format('d.m.y');
    }

    public static function getArrayToSelect( Model $model, $sort = 'name' )
    {
        $collections = $model::all()->sortBy($sort);
        $arr = [];
        $arr[0] = 'Не выбрано';
        foreach ($collections as $c) {
            $arr[$c->id] = $c->name;
        }
        return $arr;
    }

    /**
     * Получаем список
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList($limit, $status = null)
    {
        $cards = self::orderBy('last_name');

        if (request()->input('last_name', false) !== false) {
            $cards->where('last_name', 'like', '%' . request()->input('last_name') . '%');
        }

        return $cards->paginate($limit);
    }
}
