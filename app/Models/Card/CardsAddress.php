<?php

namespace App\Models\Card;

use App\Models\City;
use App\Models\Street;
use Illuminate\Database\Eloquent\Model;

class CardsAddress extends Model
{
    protected $table = 'cards_addresses';

    protected $fillable = ['card_id', 'cities_id', 'streets_id', 'house_number', 'apartment_number', 'housing_number', 'is_registration_address'];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'card_id' => ['required', 'not_in:0'],
            'city_id' => ['required', 'not_in:0'],
            'street_id' => ['required', 'not_in:0'],
            'house_number' => ['required','numeric'],
            'apartment_number' => [],
            'housing_number' => [],
        ];
    }

    /**
     * Получаем список
     * @param integer $card_id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function getList($card_id = null)
    {
        $values =  static::sortBy('is_registration_address');


        if ($card_id !== null) {
            $values->where('card_id', '=', $card_id);
        }

        return $values->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function card()
    {
        return $this->hasOne(Card::class, 'id', 'card_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function street()
    {
        return $this->hasOne(Street::class, 'id', 'street_id');
    }

}
