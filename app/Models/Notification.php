<?php namespace App\Models;

use Html;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Notification
 *
 * @package App\Models
 * @property integer $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $read_at
 * @property string $type
 * @property array $data
 * @property-read string $message
 * @property-read string $link
 * @property-read string $icon
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereReadAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Notification extends Model
{

    const TYPE_REGISTRATION = 'registration';
    const TYPE_NEW_ORDER = 'new_order';
    const TYPE_CALLBACK = 'callback';
    const TYPE_SUBSCRIBE = 'news_send';

    protected $dates = ['read_at'];

    protected $casts = [
        'data' => 'array',
    ];

    /**
     * @return string
     */
    public function getMessageAttribute()
    {
        switch ($this->type) {
            case static::TYPE_REGISTRATION:
                return 'Новый пользователь';

            case static::TYPE_NEW_ORDER:
                return "Новый заказ #{$this->data['id']} на сумму {$this->data['amount']}";

            case static::TYPE_CALLBACK:
                return 'Запрос звонка';

        }
        return 'Неизвестное уведомление';
    }

    /**
     * @return string
     */
    public function getLinkAttribute()
    {
        switch ($this->type) {
            case static::TYPE_REGISTRATION:
                return route('admin.customers.edit', ['customer' => $this->data['id']]);

            case static::TYPE_NEW_ORDER:
                return route('admin.orders.edit', ['order' => $this->data['id']]);
        }
        return '#';
    }

    /**
     * @return null|string
     */
    public function getIconAttribute()
    {
        switch ($this->type) {
            case static::TYPE_REGISTRATION:
                return (string)Html::tag('i', '', ['class' => 'fa fa-user-plus text-aqua'])->toHtml();

            case static::TYPE_NEW_ORDER:
                return (string)Html::tag('i', '', ['class' => 'fa fa-shopping-cart text-green'])->toHtml();
        }
        return null;
    }

    /**
     * @param $type
     * @param array $data
     * @return Notification
     */
    public static function send($type, array $data = [])
    {
        $notification = new self();
        $notification->type = $type;
        $notification->data = $data;
        $notification->save();
        return $notification;
    }

    /**
     * @return $this
     */
    public function read()
    {
        $this->read_at = Carbon::now();
        $this->save();
        return $this;
    }

    /**
     * @param array $ids
     * @return bool
     */
    public static function readByIds(array $ids)
    {
        if (!$ids) {
            return false;
        }
        static::whereIn('id', $ids)->update([
            'read_at' => Carbon::now(),
        ]);
    }

    /**
     * @param $limit
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function last($limit)
    {
        return static::where('read_at', '=', null)
            ->latest('id')
            ->limit($limit)
            ->get();
    }

    /**
     * @return int
     */
    public static function countUnread()
    {
        return static::where('read_at', '=', null)->count();
    }

    /**
     * @param $limit
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList($limit)
    {
        $notifications = static::latest('id');
        if (request()->input('date_from', false) !== false) {
            $notifications->where('created_at', '>=', request()->input('date_from'));
        }
        if (request()->input('date_to', false) !== false) {
            $date = new Carbon(request()->input('date_to'));
            if ($date) {
                $notifications->where('created_at', '<', $date->addDay(1));
            }
        }
        return $notifications->paginate($limit);
    }

}
