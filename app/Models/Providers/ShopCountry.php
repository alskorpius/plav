<?php

namespace App\Models\Providers;

use App\Models\Catalog\Product;
use App\Traits\ModelImages;
use App\Traits\ModelMain;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Providers\ShopCountry
 *
 * @property int $id
 * @property string $name
 * @property string|null $image
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Providers\ShopProvider[] $shopProviders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Providers\ShopRegion[] $shopRegions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopCountry whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopCountry whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopCountry whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopCountry whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopCountry whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $slug
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\Product[] $products
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopCountry whereSlug($value)
 * @property int $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopCountry whereStatus($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Providers\ShopCountryTranslates[] $data
 */
class ShopCountry extends Model
{
    use ModelImages;
    use ModelMain;

    protected $table = 'shop_countries';

    protected $fillable = ['status'];

    /**
     * @inheritdoc
     */
    protected function imagesFolders() {
        return [
            'countries' => [
                'small' => [
                    'width' => 50,
                    'height' => 50,
                    'resize' => true,
                    'crop' => true,
                    'watermark' => false,
                ],
                'icon' => [
                    'width' => 20,
                    'height' => 20,
                    'resize' => true,
                    'crop' => true,
                    'watermark' => false,
                ],
                'original' => [],
            ],
        ];
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function _rules()
    {
        return [
        ];
    }

    /**
     * @param null $name
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList($limit, $name = null)
    {
        $pages = static::with(['current'])->oldest('shop_countries.created_at');

        if (request()->input('name', false) !== false) {
            $pages
                ->join('shop_countries_translates', 'shop_countries.id', '=', 'shop_countries_translates.row_id')
                ->where('shop_countries_translates.name', 'like', '%' . request()->input('name') . '%')
                ->where('shop_countries_translates.language', '=', \Lang::getLocale());
//                ->where('name', 'like', '%' . request()->input('name') . '%');
        }

        if ($name !== null) {
            $pages->whereName($name);
        }

        return $pages->paginate($limit);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shopRegions()
    {
        return $this->hasMany(ShopRegion::class, 'country_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shopProviders()
    {
        return $this->hasMany(ShopProvider::class, 'country_id', 'id');
    }

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function products()
	{
		return $this->hasMany(Product::class, 'country_id', 'id');
	}

    public function countriesFilterCount($color_id = [], $sweetness_id = [], $type_id = [], $kind_id = [],  $alcohol_id = [], $sugar_id = [], $country_id = [], $provider_id = [], $sort_id = [], $year = [], $volume = [], $region = [])
    {
        $count = 0;
        $products = Product::getListForFilter(1000, $color_id, $sweetness_id, $type_id, $kind_id, $alcohol_id, $sugar_id, $country_id, $provider_id, $sort_id, $year, $volume, $region);
        foreach ($products as $product) {
            if($this->id == $product->country_id){
                $count++;
            }
        }

        return $count;
    }
}
