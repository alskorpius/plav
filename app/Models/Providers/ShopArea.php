<?php

namespace App\Models\Providers;

use App\Traits\ModelMain;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Providers\ShopArea
 *
 * @property int $id
 * @property string $name
 * @property int $region_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Providers\ShopProvider[] $shopProviders
 * @property-read \App\Models\Providers\ShopRegion $shopRegion
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopArea whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopArea whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopArea whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopArea whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopArea whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Providers\ShopAreaTranslates[] $data
 */
class ShopArea extends Model
{
    use ModelMain;

    protected $table = 'shop_areas';

    protected $fillable = ['region_id'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules() {
        return [
            'region_id' => ['required', 'numeric'],
        ];
    }

    /**
     * @param null $name
     * @param null $region_id
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList($limit, $name = null, $region_id = null) {
        $pages = self::oldest('name');

        if (request()->input('name', false) !== false) {
            $pages->where('name', 'like', '%' . request()->input('name') . '%');
        }

        if ($name !== null) {
            $pages->whereName($name);
        }

        if ($region_id !== null) {
            $pages->where('region_id', '=', $region_id);
        }

        return $pages->paginate($limit);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shopProviders() {
        return $this->hasMany(ShopProvider::class, 'region_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function shopRegion() {
        return $this->hasOne(ShopRegion::class, 'id', 'region_id');
    }
}
