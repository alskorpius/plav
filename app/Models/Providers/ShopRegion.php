<?php

namespace App\Models\Providers;

use App\Models\Catalog\Product;
use App\Traits\ModelMain;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

/**
 * App\Models\Providers\ShopRegion
 *
 * @property int $id
 * @property string $name
 * @property int $country_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Providers\ShopRegionTranslates $current
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Providers\ShopArea[] $shopAreas
 * @property-read \App\Models\Providers\ShopCountry $shopCountry
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Providers\ShopProvider[] $shopProviders
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopRegion whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopRegion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopRegion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopRegion whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed $products_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Providers\ProvidersRegion[] $providersRegions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Providers\ShopRegionTranslates[] $data
 *
 */
class ShopRegion extends Model
{
    use ModelMain;

    protected $table = 'shop_regions';

    protected $fillable = ['country_id', 'status','slug'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules() {
        return [
            'slug' => ['required', 'max:191', Rule::unique($this->getTable())->ignore($this->id)],
            'country_id' => ['required', 'numeric'],
        ];
    }

    /**
     * @param null $name
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList($limit, $name = null, $country_id = null, $status = null) {
        $pages = static::with(['current'])->oldest('shop_regions.created_at');

        // if (request()->input('name', false) !== false) {
            // $pages
            //     ->join('shop_regions_translates', 'shop_regions.id', '=', 'shop_regions_translates.row_id')
            //     ->where('shop_regions_translates.name', 'like', '%' . request()->input('name') . '%')
            //     ->where('shop_regions_translates.language', '=', \Lang::getLocale());
//            ->where('name', 'like', '%' . request()->input('name') . '%');
        // }

        // if ($name !== null) {
        //     $pages->whereName($name);
        // }

        if ($country_id !== null) {
            $pages->where('country_id', '=', (int)$country_id);
        }

        if ($status !== null) {
            $pages->where('status', 1);
        }

        return $pages->paginate($limit);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shopAreas() {
        return $this->hasMany(ShopArea::class, 'region_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shopProviders() {
        return $this->hasMany(ProvidersRegion::class, 'region_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function shopCountry() {
        return $this->hasOne(ShopCountry::class, 'id', 'country_id');
    }

    public function providersRegions() {
        return $this->hasMany(ProvidersRegion::class, 'region_id', 'id');
    }

    public function getProductsCountAttribute()
    {
        $providersRegions = ProvidersRegion::whereRegionId($this->id)->get();
        $providersIds = [];
        foreach ($providersRegions as $providersRegion) {
            $providersIds[] = $providersRegion->provider_id;
        }
        $products = Product::whereIn('provider_id', $providersIds)->get();
        return sizeof($products);
    }

    public function regionsFilterCount($color_id = [], $sweetness_id = [], $type_id = [], $kind_id = [],  $alcohol_id = [], $sugar_id = [], $country_id = [], $provider_id = [], $sort_id = [], $year = [], $volume = [], $region = [])
    {
        $count = 0;
        $providersRegions = ProvidersRegion::whereRegionId($this->id)->get();
        $providersIds = [];
        foreach ($providersRegions as $providersRegion) {
            $providersIds[] = $providersRegion->provider_id;
        }
        $products = Product::getListForFilter(1000, $color_id, $sweetness_id, $type_id, $kind_id, $alcohol_id, $sugar_id, $country_id, $provider_id, $sort_id, $year, $volume, $region);

        foreach ($products as $product) {
            if (in_array($product->provider_id, $providersIds)) {
                $count++;
            }
        }
        return $count;
    }
}
