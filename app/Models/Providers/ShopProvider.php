<?php

namespace App\Models\Providers;

use App\Models\Catalog\Product;
use App\Models\Catalog\ShopBrand;
use App\Traits\ModelMain;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

/**
 * App\Models\Providers\ShopProvider
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int|null $country_id
 * @property int|null $region_id
 * @property int|null $area_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Providers\ShopArea $shopArea
 * @property-read \App\Models\Providers\ShopCountry $shopCountry
 * @property-read \App\Models\Providers\ShopRegion $shopRegion
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopProvider whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopProvider whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopProvider whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopProvider whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopProvider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopProvider whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopProvider whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopProvider whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\ShopBrand[] $shopBrands
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\Product[] $product
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalog\Product[] $products
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Providers\ProvidersRegion[] $regions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Providers\ShopProviderTranslates[] $data
 */
class ShopProvider extends Model
{
    use ModelMain;

    protected $table = 'shop_providers';

    protected $fillable = ['country_id', 'area_id', 'slug'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function _rules() {
        return [
            'slug' => ['required', 'max:191', Rule::unique($this->getTable())->ignore($this->id)],
            'country_id' => ['required', 'numeric'],
            'region_id' => ['required', 'array'],
        ];
    }

    /**
     * @param null $name
     * @param null $country_id
     * @param null $region_id
     * @param null $area_id
     * @return \Illuminate\Support\Collection
     */
    public static function getList($limit, $name = null, $country_id = null, $region_id = null, $area_id = null) {
        $pages = static::oldest('shop_providers.created_at');

        if (request()->input('name', false) !== false) {
            $pages
                ->join('shop_providers_translates', 'shop_providers.id', '=', 'shop_providers_translates.row_id')
                ->where('shop_providers_translates.name', 'like', '%' . request()->input('name') . '%')
                ->where('shop_providers_translates.language', '=', \Lang::getLocale());
//                ->where('name', 'like', '%' . request()->input('name') . '%');
        }

            if ($name !== null) {
                $pages->whereName($name);
            }

        if ($country_id !== null) {
            $pages->where('country_id', '=', $country_id);
        }

        if ($region_id !== null) {
            $pages->where('region_id', '=', $region_id);
        }

        if ($area_id !== null) {
            $pages->where('area_id', '=', $area_id);
        }

        return $pages->paginate($limit);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function shopCountry() {
        return $this->hasOne(ShopCountry::class, 'id', 'country_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function regions() {
        return $this->hasMany(ProvidersRegion::class, 'provider_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function shopArea() {
        return $this->hasOne(ShopArea::class, 'id', 'area_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shopBrands() {
        return $this->hasMany(ShopBrand::class, 'shop_provider_id', 'id');
    }

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function products() {
		return $this->hasMany(Product::class, 'provider_id', 'id');
	}

    public function providersFilterCount($color_id = [], $sweetness_id = [], $type_id = [], $kind_id = [],  $alcohol_id = [], $sugar_id = [], $country_id = [], $provider_id = [], $sort_id = [], $year = [], $volume = [], $region = [])
    {
        $count = 0;
        $products = Product::getListForFilter(1000, $color_id, $sweetness_id, $type_id, $kind_id, $alcohol_id, $sugar_id, $country_id, $provider_id, $sort_id, $year, $volume, $region);
        foreach ($products as $product) {
            if($this->id == $product->provider_id){
                $count++;
            }
        }

        return $count;
    }

}
