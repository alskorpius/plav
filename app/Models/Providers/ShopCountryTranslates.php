<?php

namespace App\Models\Providers;


use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Providers\ShopCountryTranslates
 *
 * @property int $id
 * @property string $name
 * @property string|null $image
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Providers\ShopProvider[] $shopProviders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Providers\ShopRegion[] $shopRegions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopCountryTranslates whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopCountryTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopCountryTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopCountryTranslates whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\Providers\ShopCountry $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopCountryTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopCountryTranslates whereRowId($value)
 */
class ShopCountryTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'shop_countries_translates';

    protected $fillable = ['name'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:191'],
        ];
    }
}
