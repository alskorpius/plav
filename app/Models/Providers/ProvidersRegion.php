<?php

namespace App\Models\Providers;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Providers\ProvidersRegion
 *
 * @property int $id
 * @property int $provider_id
 * @property int $region_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Providers\ShopProvider $provider
 * @property-read \App\Models\Providers\ShopRegion $region
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ProvidersRegion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ProvidersRegion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ProvidersRegion whereProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ProvidersRegion whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ProvidersRegion whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ProvidersRegion extends Model
{
    protected $table = 'providers_regions';

    protected $fillable = ['provider_id', 'region_id'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provider_id' => ['required'],
            'region_id' => ['required'],
        ];
    }

    public function provider()
    {
        return $this->hasOne(ShopProvider::class, 'id', 'provider_id');
    }

    public function region()
    {
        return $this->hasOne(ShopRegion::class, 'id', 'region_id');
    }
}
