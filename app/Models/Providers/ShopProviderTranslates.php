<?php

namespace App\Models\Providers;

use App\Models\Catalog\Product;
use App\Models\Catalog\ShopBrand;
use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

/**
 * App\Models\Providers\ShopProviderTranslates
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopProviderTranslates whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopProviderTranslates whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopProviderTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopProviderTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopProviderTranslates whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\Providers\ShopProvider $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopProviderTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopProviderTranslates whereRowId($value)
 */
class ShopProviderTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'shop_providers_translates';

    protected $fillable = ['name', 'description'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => ['required', 'max:191'],
        ];
    }

}
