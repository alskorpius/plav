<?php

namespace App\Models\Providers;

use App\Models\Catalog\Product;
use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

/**
 * App\Models\Providers\ShopRegionTranslates
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopRegionTranslates whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopRegionTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopRegionTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopRegionTranslates whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\Providers\ShopRegion $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopRegionTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopRegionTranslates whereRowId($value)
 */
class ShopRegionTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'shop_regions_translates';

    protected $fillable = ['name'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => ['required', 'max:191'],
        ];
    }
}
