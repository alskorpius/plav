<?php

namespace App\Models\Providers;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Providers\ShopAreaTranslates
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property integer $row_id
 * @property string $language
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopAreaTranslates whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopAreaTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopAreaTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopAreaTranslates whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\Providers\ShopArea $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopAreaTranslates whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Providers\ShopAreaTranslates whereRowId($value)
 */
class ShopAreaTranslates extends Model
{
    use ModelTranslates;

    protected $table = 'shop_areas_translates';

    protected $fillable = ['name'];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => ['required', 'max:191'],
        ];
    }
}
