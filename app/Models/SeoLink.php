<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SeoLink
 *
 * @package App\Models
 * @property integer $id
 * @property integer $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $url
 * @property string $h1
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $content
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SeoLink whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SeoLink whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SeoLink whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SeoLink whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SeoLink whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SeoLink whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SeoLink whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SeoLink whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SeoLink whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SeoLink whereUrl($value)
 * @mixin \Eloquent
 */
class SeoLink extends Model
{
    protected $casts = [
        'status' => 'boolean',
    ];

    protected $fillable = ['status', 'url', 'h1', 'title', 'keywords', 'description', 'content'];

    public function rules()
    {
        return [
            'url' => ['required', 'max:191'],
            'status' => ['required', 'boolean'],
            'title' => ['required', 'max:191'],
        ];
    }

    /**
     * @param $limit
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getList($limit)
    {
        $result = self::oldest('id');
        if (request()->input('url', false) !== false) {
            $result->where('url', 'like', '%' . request()->input('url') . '%');
        }
        return $result->paginate($limit);
    }

    /**
     * @param $url
     * @return array|null
     */
    public function isRequiredForUrl($url)
    {
        $meta = self::whereUrl($url)->whereStatus(true)->first();
        if (!$meta) {
            return null;
        }
        return [
            'h1' => $meta->h1,
            'title' => $meta->title,
            'keywords' => $meta->keywords,
            'description' => $meta->description,
            'content' => $meta->content,
        ];
    }
}
