<?php

namespace App\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Voting_answers_translates
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $language
 * @property integer $row_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingAnswers whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingAnswers whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingAnswers whereName($value))
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingAnswers whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Language $lang
 * @property-read \App\Models\VotingAnswers $row
 */
class VotingAnswersTranslates extends Model
{
    use ModelTranslates;
    protected $table = 'voting_answers_translates';

    protected $fillable = ['name'];


    public function rules()
    {
        return [
            'name' => ['required'],
        ];
    }
}
