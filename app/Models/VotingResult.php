<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VotingResult
 *
 * @property int $id
 * @property int $question_id
 * @property int $answer_id
 * @property int|null $user_id
 * @property string $ip
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingResult whereAnswerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingResult whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingResult whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingResult whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingResult whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingResult whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingResult whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\VotingResult $votingAnswer
 * @property-read \App\Models\VotingResult $votingQuestion
 */
class VotingResult extends Model
{
    protected $table = 'voting_results';

    protected $fillable = ['question_id', 'answer_id', 'user_id', 'ip'];

    public function rules()
    {
        return [
            'question_id' => ['required'],
            'answer_id' => ['required'],
            'ip' => ['required'],
        ];
    }

    /**
     * @param null $question_id
     * @param null $answer_id
     * @return \Illuminate\Support\Collection|VotingResult[]
     */
    public static function getList($question_id = null, $answer_id = null)
    {
        $results = VotingResult::latest('created_at');

        if (request()->input('question_id', false) !== false) {
            $results->where('question_id', 'like', '%' . request()->input('question_id') . '%');
        }

        if (request()->input('user_id', false) !== false) {
            $results->where('user_id', 'like', '%' . request()->input('user_id') . '%');
        }

        if ($question_id !== null) {
            $results->where('question_id', '=', $question_id);
        }

        if ($answer_id !== null) {
            $results->where('answer_id', '=', $answer_id);
        }

        return $results->get();
    }

    /**
     * @param null $ip
     * @param null $user_id
     * @return array
     */
    public static function getUserQuestions($ip = null, $user_id = null)
    {

        $results = VotingResult::select('question_id', 'id')->latest('created_at');

        if ($user_id !== null) {
            $results->orWhere('user_id', '=', $user_id);
        }

        if ($ip !== null) {
            $results->orWhere('ip', '=', $ip);
        }

        return $results->get()->map(function (VotingResult $result) {
            return $result->question_id;
        })->toArray();


    }

    /**
     * @param $question_id
     * @param $answer_id
     * @return float
     */
    public static function getAnswersResult($question_id, $answer_id)
    {
        $answersAllCount = VotingResult::getList($question_id)->count();
        $currentAnswerCount = VotingResult::getList($question_id = null, $answer_id)->count();

        if ($currentAnswerCount !== 0 and $answersAllCount !== 0) {
            $percent = ($currentAnswerCount / $answersAllCount) * 100;
        } else {
            $percent = 0;
        }

        return round($percent);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function votingQuestion()
    {
        return $this->hasOne(VotingResult::class, 'id', 'question_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function votingAnswer()
    {
        return $this->hasOne(VotingResult::class, 'id', 'answer_id');
    }


}
