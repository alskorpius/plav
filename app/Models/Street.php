<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class Street extends Model
{

    protected $table = 'streets';

    protected $fillable = ['name', 'status'];

    protected $casts = [
        'status' => 'boolean'
    ];

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
        ];
    }

    /**
     * Получаем список
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList($limit, $status = null)
    {
        $users = self::orderBy('name');

        if (request()->input('name', false) !== false) {
            $users->where('name', 'like', '%' . request()->input('name') . '%');
        }

        if ($status !== null){
            $users->whereStatus($status);
        }
        return $users->paginate($limit);
    }
}
