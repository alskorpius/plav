<?php

namespace App\Models;

use App\Traits\ModelTranslates;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemPageTranslates
 *
 * @package App\Models
 * @property integer $id
 * @property string $language
 * @property string $content
 * @property string $h1
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $name
 * @property integer $row_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemPageTranslates whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemPageTranslates whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemPageTranslates whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemPageTranslates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemPageTranslates whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemPageTranslates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemPageTranslates whereTitle($value)
 * @mixin \Eloquent
 * @property-read \App\Models\SystemPage $row
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemPageTranslates whereRowId($value)
 * @property-read \App\Models\Language $lang
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemPageTranslates whereLanguage($value)
 */
class SystemPageTranslates extends Model
{

    use ModelTranslates;

    protected $table = 'system_pages_translates';

    public $timestamps = false;

    protected $fillable = ['name', 'content', 'h1', 'title', 'keywords', 'description'];

    public function rules()
    {
        return [
            'name' => ['required', 'max:191'],
        ];
    }
}
