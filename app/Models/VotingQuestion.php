<?php

namespace App\Models;

use App\Traits\ModelMain;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Voting_question
 *
 * @property int $id
 * @property int $position
 * @property int $status
 * @property int $voting_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\VotingQuestionTranslates $current
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingQuestion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingQuestion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingQuestion wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingQuestion whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingQuestion whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingQuestion whereVotingId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VotingAnswers[] $votingAnswers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VotingResult[] $votingResult
 * @property \Carbon\Carbon|null $from_at
 * @property \Carbon\Carbon|null $to_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingQuestion whereFromAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingQuestion whereToAt($value)
 * @property string $description
 * @property-read string $human_date
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingQuestion whereDescription($value)
 * @property string $date_from
 * @property string $date_to
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingQuestion whereDateFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VotingQuestion whereDateTo($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VotingQuestionTranslates[] $data
 */
class VotingQuestion extends Model
{
    use ModelMain;
    protected $table = 'voting_questions';

    protected $fillable = [ 'status', 'description', 'date_from', 'date_to'];

    protected $dates = ['date_from', 'date_to'];

    /**
     * @return array
     */
    public function _rules()
    {
        return [
        ];
    }

    /**
     * @return string
     */
    public function getHumanDateAttribute()
    {
        return $this->created_at->format('d.m.Y');
    }


    /**
     * @param null $frontView
     * @param null $status
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getList($limit, $frontView = null, $status = null)
    {
        $questions = self::with(['current', 'votingAnswers'])->oldest('date_from');

        if (request()->input('name', false) !== false) {
            $questions
                ->join('voting_questions_translates', 'voting_questions.id', '=', 'voting_questions_translates.row_id')
                ->where('voting_questions_translates.name', 'like', '%' . request()->input('name') . '%')
                ->where('voting_questions_translates.language', '=', \Lang::getLocale());
        }

        if (request()->input('created_at', false) !== false) {
            $questions->where('created_at', 'like', '%' . request()->input('created_at') . '%');
        }

        if (request()->input('date_from', false) !== false) {
            $questions->where('date_from','>', request()->input('date_from'));
        }

        if (request()->input('date_to', false) !== false) {
            $questions->where('date_to','<', request()->input('date_to'));
        }

        if (request()->input('date_from', false) !== false && request()->input('date_to', false) !== false) {
            $questions->where('date_to', '<=', Carbon::createFromFormat('Y-m-d', request()->input('date_to')))
            ->Where('date_from', '>=', Carbon::createFromFormat('Y-m-d', request()->input('date_from')));
        }

        if ($status !== null) {
            $questions->where('status', '=', $status);
        }

        if ($frontView !== null) {
            $questions->where('status', '=', 1);
            $questions->where('date_from', '<', Carbon::now()->format('Y-m-d H:i:s'));
            $questions->where('date_to', '>', Carbon::now()->format('Y-m-d H:i:s'));
        }

        return $questions->paginate($limit);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function votingAnswers()
    {
        return $this->hasMany(VotingAnswers::class, 'question_id', 'id')->where('status', 1)->orderBy('position');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function votingResult()
    {
        return $this->hasMany(VotingResult::class, 'question_id', 'id');
    }

}
