<?php namespace App\Services\jQuery;

class Insertion
{

    /**
     * Generate true answer for wAjax.js for insertion block
     * @param $method
     * @param $selector
     * @param $content
     * @return array
     */
    private function generate($method, $selector, $content)
    {
        return [
            'action' => 'insertion',
            'method' => $method,
            'selector' => $selector,
            'content' => $content,
        ];
    }

    /**
     * Method prependTo
     * @param $selector
     * @param $content
     * @return array
     */
    public function prependTo($selector, $content)
    {
        return $this->generate(__FUNCTION__, $selector, $content);
    }

    /**
     * Method insertAfter
     * @param $selector
     * @param $content
     * @return array
     */
    public function insertAfter($selector, $content)
    {
        return $this->generate(__FUNCTION__, $selector, $content);
    }

    /**
     * Method insertBefore
     * @param $selector
     * @param $content
     * @return array
     */
    public function insertBefore($selector, $content)
    {
        return $this->generate(__FUNCTION__, $selector, $content);
    }

    /**
     * Method append
     * @param $selector
     * @param $content
     * @return array
     */
    public function append($selector, $content)
    {
        return $this->generate(__FUNCTION__, $selector, $content);
    }

    /**
     * Method prepend
     * @param $selector
     * @param $content
     * @return array
     */
    public function prepend($selector, $content)
    {
        return $this->generate(__FUNCTION__, $selector, $content);
    }

    /**
     * Method after
     * @param $selector
     * @param $content
     * @return array
     */
    public function after($selector, $content)
    {
        return $this->generate(__FUNCTION__, $selector, $content);
    }

    /**
     * Method before
     * @param $selector
     * @param $content
     * @return array
     */
    public function before($selector, $content)
    {
        return $this->generate(__FUNCTION__, $selector, $content);
    }

    /**
     * Method appendTo
     * @param $selector
     * @param $content
     * @return array
     */
    public function appendTo($selector, $content)
    {
        return $this->generate(__FUNCTION__, $selector, $content);
    }

    /**
     * Method text
     * @param $selector
     * @param $content
     * @return array
     */
    public function text($selector, $content)
    {
        return $this->generate(__FUNCTION__, $selector, $content);
    }

    /**
     * Method html
     * @param $selector
     * @param $content
     * @return array
     */
    public function html($selector, $content)
    {
        return $this->generate(__FUNCTION__, $selector, $content);
    }

    /**
     * Method html
     * @param $counter_name
     * @param $content
     * @return array
     */
    public function counter($counter_name, $content)
    {
        return $this->generate(__FUNCTION__, $counter_name, $content);
    }

    /**
     * Method replaceWith
     * @param $selector
     * @param $content
     * @return array
     */
    public function replaceWith($selector, $content)
    {
        return $this->generate(__FUNCTION__, $selector, $content);
    }

}