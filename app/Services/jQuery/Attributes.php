<?php namespace App\Services\jQuery;

class Attributes
{

    /**
     * Generate true answer for wAjax.js for attributes block
     * @param $method
     * @param $selector
     * @param $value
     * @return array
     */
    private function generate($method, $selector, $value, $attribute = '')
    {
        return [
            'action' => 'attributes',
            'method' => $method,
            'selector' => $selector,
            'value' => $value,
            'attribute' => $attribute,
        ];
    }

    /**
     * Method addClass
     * @param $selector
     * @param $value
     * @return array
     */
    public function addClass($selector, $value)
    {
        return $this->generate(__FUNCTION__, $selector, $value);
    }

    /**
     * Method removeAttr
     * @param $selector
     * @param $value
     * @return array
     */
    public function removeAttr($selector, $value)
    {
        return $this->generate(__FUNCTION__, $selector, $value);
    }

    /**
     * Method removeClass
     * @param $selector
     * @param $value
     * @return array
     */
    public function removeClass($selector, $value)
    {
        return $this->generate(__FUNCTION__, $selector, $value);
    }

    /**
     * Method toggleClass
     * @param $selector
     * @param $value
     * @return array
     */
    public function toggleClass($selector, $value)
    {
        return $this->generate(__FUNCTION__, $selector, $value);
    }

    /**
     * Method height
     * @param $selector
     * @param $value
     * @return array
     */
    public function height($selector, $value)
    {
        return $this->generate(__FUNCTION__, $selector, $value);
    }

    /**
     * Method width
     * @param $selector
     * @param $value
     * @return array
     */
    public function width($selector, $value)
    {
        return $this->generate(__FUNCTION__, $selector, $value);
    }

    /**
     * Method attr
     * @param $selector
     * @param $attribute
     * @param $value
     * @return array
     */
    public function attr($selector, $attribute, $value)
    {
        return $this->generate(__FUNCTION__, $selector, $value, $attribute);
    }

    public function attrCustom($selector, $value)
    {
        return $this->generate(__FUNCTION__, $selector, $value);
    }

    /**
     * Method prop
     * @param $selector
     * @param $value
     * @return array
     */
    public function prop($selector, $value)
    {
        return $this->generate(__FUNCTION__, $selector, $value);
    }

    /**
     * Method data
     * @param $selector
     * @param $attribute
     * @param $value
     * @return array
     */
    public function data($selector, $attribute, $value)
    {
        return $this->generate(__FUNCTION__, $selector, $value, $attribute);
    }

    /**
     * Method val
     * @param $selector
     * @param $value
     * @return array
     */
    public function val($selector, $value)
    {
        return $this->generate(__FUNCTION__, $selector, $value);
    }

}