<?php namespace App\Services\jQuery;

class Remove
{

    /**
     * Generate true answer for wAjax.js for remove block
     * @param $method
     * @param $selector
     * @return array
     */
    private function generate($method, $selector)
    {
        return [
            'action' => 'remove',
            'method' => $method,
            'selector' => $selector,
        ];
    }

    /**
     * Method empty
     * @param $selector
     * @return array
     */
    public function _empty($selector)
    {
        return $this->generate('empty', $selector);
    }

    /**
     * Method detach
     * @param $selector
     * @return array
     */
    public function detach($selector)
    {
        return $this->generate(__FUNCTION__, $selector);
    }

    /**
     * Method unwrap
     * @param $selector
     * @return array
     */
    public function unwrap($selector)
    {
        return $this->generate(__FUNCTION__, $selector);
    }

    /**
     * Method remove
     * @param $selector
     * @return array
     */
    public function remove($selector)
    {
        return $this->generate(__FUNCTION__, $selector);
    }

    /**
     * Method remove
     * @param $selector
     * @return array
     */
    public function removeWithFade($selector)
    {
        return $this->generate(__FUNCTION__, $selector);
    }

}