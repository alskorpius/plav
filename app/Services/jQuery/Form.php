<?php namespace App\Services\jQuery;

class Form
{

    /**
     * Generate true answer for wAjax.js for attributes block
     * @param $method
     * @param $selector
     * @param $value
     * @return array
     */
    private function generate($method, $selector, $value)
    {
        return [
            'action' => 'form',
            'method' => $method,
            'selector' => $selector,
            'value' => $value,
        ];
    }

    /**
     * Method val
     * @param $selector
     * @param $value
     * @return array
     */
    public function val($selector, $value)
    {
        return $this->generate(__FUNCTION__, $selector, $value);
    }

}