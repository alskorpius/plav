<?php namespace App\Services\jQuery;

class DataTables
{
    /**
     * Method val
     * @return array
     */
    public function update()
    {
        return [
            'action' => 'dataTables',
            'method' => __FUNCTION__,
        ];
    }

}