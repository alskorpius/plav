<?php namespace App\Services\jQuery;

class Visibility
{

    /**
     * Generate true answer for wAjax.js for visibility block
     * @param $method
     * @param $selector
     * @param $time
     * @return array
     */
    private function generate($method, $selector, $time)
    {
        return [
            'action' => 'visibility',
            'method' => $method,
            'selector' => $selector,
            'time' => $time,
        ];
    }

    /**
     * Method hide
     * @param $selector
     * @param $time
     * @return array
     */
    public function hide($selector, $time)
    {
        return $this->generate(__FUNCTION__, $selector, $time);
    }

    /**
     * Method show
     * @param $selector
     * @param $time
     * @return array
     */
    public function show($selector, $time)
    {
        return $this->generate(__FUNCTION__, $selector, $time);
    }

    /**
     * Method toggle
     * @param $selector
     * @param $time
     * @return array
     */
    public function toggle($selector, $time)
    {
        return $this->generate(__FUNCTION__, $selector, $time);
    }

    /**
     * Method fadeIn
     * @param $selector
     * @param $time
     * @return array
     */
    public function fadeIn($selector, $time)
    {
        return $this->generate(__FUNCTION__, $selector, $time);
    }

    /**
     * Method fadeOut
     * @param $selector
     * @param $time
     * @return array
     */
    public function fadeOut($selector, $time)
    {
        return $this->generate(__FUNCTION__, $selector, $time);
    }

    /**
     * Method fadeToggle
     * @param $selector
     * @param $time
     * @return array
     */
    public function fadeToggle($selector, $time)
    {
        return $this->generate(__FUNCTION__, $selector, $time);
    }

    /**
     * Method slideUp
     * @param $selector
     * @param $time
     * @return array
     */
    public function slideUp($selector, $time)
    {
        return $this->generate(__FUNCTION__, $selector, $time);
    }

    /**
     * Method slideDown
     * @param $selector
     * @param $time
     * @return array
     */
    public function slideDown($selector, $time)
    {
        return $this->generate(__FUNCTION__, $selector, $time);
    }

    /**
     * Method slideToggle
     * @param $selector
     * @param $time
     * @return array
     */
    public function slideToggle($selector, $time)
    {
        return $this->generate(__FUNCTION__, $selector, $time);
    }

}