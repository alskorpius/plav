<?php namespace App\Services\jQuery;

class Trigger
{

    /**
     * Generate true answer for wAjax.js for attributes block
     * @param $trigger
     * @param $selector
     * @return array
     */
    private function generate($trigger, $selector)
    {
        return [
            'action' => 'trigger',
            'trigger' => $trigger,
            'selector' => $selector,
        ];
    }

    /**
     * Method val
     * @param $selector
     * @return array
     */
    public function change($selector)
    {
        return $this->generate(__FUNCTION__, $selector);
    }


    /**
     * Method val
     * @param $selector
     * @return array
     */
    public function click($selector)
    {
        return $this->generate(__FUNCTION__, $selector);
    }


    /**
     * Method val
     * @param $selector
     * @return array
     */
    public function magnificClick($selector)
    {
        return $this->generate(__FUNCTION__, $selector);
    }

}