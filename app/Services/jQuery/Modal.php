<?php namespace App\Services\jQuery;

class Modal
{

    /**
     * Open modal popup
     * @param string $content
     * @param string $title
     * @return array
     */
    public function open($content, $title = '')
    {
        return [
            'action' => 'modal',
            'method' => 'open',
            'content' => $content,
            'title' => $title,
        ];
    }

    /**
     * Open modal popup
     * @return array
     */
    public function close()
    {
        return [
            'action' => 'modal',
            'method' => 'close',
        ];
    }

}