<?php namespace App\Services\jQuery;

class Magnific
{

    /**
     * Open magnific popup
     * @param string $content
     * @return array
     */
    public function open($content)
    {
        return [
            'action' => 'magnific',
            'method' => 'open',
            'content' => '<div class="pointer-events-none">' . $content . '</div>',
        ];
    }

    /**
     * Open magnific popup
     * @return array
     */
    public function close()
    {
        return [
            'action' => 'magnific',
            'method' => 'close',
        ];
    }

}