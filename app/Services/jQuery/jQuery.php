<?php namespace App\Services\jQuery;

/**
 * Class jQuery
 * @package system
 */
class jQuery
{

    /**
     * @var Insertion
     */
    public $insertion;

    /**
     * @var Remove
     */
    public $remove;

    /**
     * @var Attributes
     */
    public $attributes;

    /**
     * @var Visibility
     */
    public $visibility;

    /**
     * @var Magnific
     */
    public $magnific;

    /**
     * @var Modal
     */
    public $modal;

    /**
     * @var Form
     */
    public $form;

    /**
     * @var DataTables
     */
    public $dataTables;

    /**
     * @var Trigger
     */
    public $trigger;

    /**
     * jQuery constructor.
     */
    public function __construct()
    {
        $this->insertion = new Insertion();
        $this->remove = new Remove();
        $this->attributes = new Attributes();
        $this->visibility = new Visibility();
        $this->magnific = new Magnific();
        $this->modal = new Modal();
        $this->form = new Form();
        $this->dataTables = new DataTables();
        $this->trigger = new Trigger();
    }

    /**
     * Init plugins
     * @param $plugin
     * @param $timeout
     * @return array
     */
    public function init($plugin, $timeout = null)
    {
        return [
            'action' => 'init',
            'plugin' => $plugin,
            'timeout' => $timeout,
        ];
    }

    /**
     * console.log($content)
     * @param $content
     * @return array
     */
    public function log($content)
    {
        return [
            'action' => 'log',
            'content' => $content,
        ];
    }

    /**
     * Pop up message
     * @param $content
     * @param string $type - 'success', 'info', 'warning', 'danger'
     * @return array
     */
    public function message($content, $type = 'default')
    {
        return [
            'action' => 'message',
            'type' => $type,
            'content' => $content,
        ];
    }

    /**
     * Reset current form or by selector
     * @param null $selector
     * @return array
     */
    public function resetForm($selector = null)
    {
        return [
            'action' => 'reset_form',
            'selector' => $selector,
        ];
    }

    /**
     * Redirect user to another page
     * @param $link
     * @return array
     */
    public function redirect($link)
    {
        return [
            'action' => 'redirect',
            'link' => $link,
        ];
    }

    /**
     * Set css $styles to $selector
     * @param $selector
     * @param array $styles
     * @return array
     */
    public function css($selector, array $styles)
    {
        return [
            'action' => 'css',
            'selector' => $selector,
            'styles' => $styles,
        ];
    }

    /**
     * Animate something
     * @param $selector
     * @param $time
     * @param array $config
     * @return array
     */
    public function animate($selector, $time, $config = [])
    {
        return [
            'action' => 'animate',
            'selector' => $selector,
            'time' => $time,
            'config' => $config,
        ];
    }

    /**
     * Reinitialize validation on forms
     * @return array
     */
    public function validation()
    {
        return [
            'action' => 'validation',
        ];
    }

    /**
     * Answer is validation ok
     * @param string $fieldName - name="$fieldName"
     * @param string $content - message to show
     * @param bool $success - answer type
     * @return array
     */
    public function validation_answer($fieldName, $content = null, $success = true)
    {
        return [
            'action' => 'validation_answer',
            'name' => $fieldName,
            'content' => $content,
            'method' => $success ? 'success' : 'error',
        ];
    }

}