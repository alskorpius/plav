<?php namespace App\Services;

use Illuminate\Support\Str;

/**
 * Class Object
 * @package App\Services
 *
 * @method array getOther()
 */
class Object
{

    /** @var array Свойства, не попавшие в основные */
    protected $other = [];

    /**
     * Достаем элемент из "других" элементов
     *
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public function getFromOther($key, $default = null)
    {
        if (array_key_exists($key, $this->other)) {
            return $this->other[$key];
        }
        return $default;
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed|null
     */
    public function __call($name, $arguments)
    {
        $property = lcfirst(Str::replaceFirst('get', '', $name));
        if (property_exists($this, $property)) {
            return $this->{$property};
        }
        if (array_key_exists($property, $this->other)) {
            return $this->other[$property];
        }
        return null;
    }

    /**
     * Преобразовываем в булевое значение
     *
     * @param string $string
     * @return bool
     */
    protected function toBoolean($string)
    {
        return $string === 'true';
    }

    /**
     * Устанавливаем значение для какого-то свойства из свойства внешнего источника
     *
     * @param \SimpleXMLElement|\SimpleXMLElement[] $element
     * @param $key
     * @param $property
     * @param string $type
     * @param null $default
     * @return array|float|int|null|string
     */
    protected function setElementFromProperty(\SimpleXMLElement $element, $key, $property = null, $type = 'string', $default = null)
    {
        $property = $property ?: $key;
        if (isset($element->{$key}) === false) {
            $value = $default;
        } else {
            $value = $this->getValue($element->{$key}, $type);
        }
        if (property_exists($this, $property) === false) {
            return $this->other[$property] = $value;
        }
        return $this->{$property} = $value;
    }

    /**
     * Устанавливаем значение для какого-то свойства из аттрибутов внешнего источника
     *
     * @param \SimpleXMLElement $element
     * @param $key
     * @param $property
     * @param string $type
     * @param null $default
     * @return array|float|int|null|string
     */
    protected function setElementFromAttribute(\SimpleXMLElement $element, $key, $property = null, $type = 'string', $default = null)
    {
        $property = $property ?: $key;
        if (isset($element[$key]) === false) {
            $value = $default;
        } else {
            $value = $this->getValue($element[$key], $type);
        }
        $value = is_string($value) ? trim($value) : $value;
        if (property_exists($this, $property) === false) {
            return $this->other[$property] = $value;
        }
        return $this->{$property} = $value;
    }

    /**
     * Приводим значение к определенному типу
     *
     * @param $value
     * @param $type
     * @return array|float|int|string
     */
    private function getValue($value, $type)
    {
        switch ($type) {
            case 'int':
            case 'integer':
                return (int)$value;
            case 'array':
                return (array)$value;
            case 'float':
                return (float)$value;
            case 'bool':
            case 'boolean':
                return $this->toBoolean($value);
        }
        return trim((string)$value) ?: null;
    }

}