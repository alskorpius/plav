<?php

namespace App\Http\Controllers\Admin\Statistic;

use App\Models\StatisticMy;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;
use Klisl\Statistics\Models\KslStatistic;

class HitsController extends Controller
{
    protected $pageName = 'Переходы по сайту';

    public function index()
    {
        $hits = StatisticMy::getHitsList($this->rowsCount);
        return view('admin.statistic.hits', ['hits' => $hits]);
    }
}
