<?php

namespace App\Http\Controllers\Admin\Statistic;

use App\Models\StatisticMy;
use Request;
use App\Http\Controllers\Admin\Controller;
use Klisl\Statistics\Models\KslStatistic;

class RefersController extends Controller
{
    protected $pageName = 'Реферы';

    public function index()
    {
        $refers = StatisticMy::getRefersList($this->rowsCount);
        return view('admin.statistic.refers', ['refers' => $refers]);
    }
}
