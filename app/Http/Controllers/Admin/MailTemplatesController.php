<?php namespace App\Http\Controllers\Admin;

use App\Helpers\Alert;
use App\Models\MailTemplate;
use Illuminate\Http\Request;

class MailTemplatesController extends Controller
{

    protected $pageName = 'Шаблоны писем';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $templates = MailTemplate::all()->sortBy('name');
        return view('admin.mail-templates.index', [
            'templates' => $templates,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MailTemplate  $mailTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit(MailTemplate $mailTemplate)
    {
        view()->share('jsValidator', \JsValidator::make($mailTemplate->rules()));
        return view('admin.mail-templates.update', ['template' => $mailTemplate]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MailTemplate  $mailTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MailTemplate $mailTemplate)
    {
        $this->validate($request, $mailTemplate->rules());
        $mailTemplate->fill($request->input());
        $saved = $mailTemplate->save();
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('mail-template', $mailTemplate->id);
        }
        return redirect(route('admin.mail-templates.edit', ['mail-template' => $mailTemplate->id]))->withInput();
    }
}
