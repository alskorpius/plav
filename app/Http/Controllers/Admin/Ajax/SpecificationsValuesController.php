<?php

namespace App\Http\Controllers\Admin\Ajax;

use App\Helpers\Alert;
use App\Models\Catalog\ShopProductsDictionariesValueTranslates;
use App\Models\Catalog\ShopProductsDictionariesValue;
use App\Models\Catalog\ShopProductsDictionary;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class SpecificationsValuesController extends Controller
{
    public function addValue(Request $request)
    {
        $parent_id = $request->input('id');
        $newSlug = $request->input('newSlug');
        $option = $request->input('option');
        $newName = \GuzzleHttp\json_decode($request->input('newNames'));
        if ($newName == null || $newSlug == null) {
            return response()->json([
                'fail' => true,
            ]);
        } else {
            $value = new ShopProductsDictionariesValue();

            $this->validate($request, $value->rules());
            $value->slug = $newSlug;
            $value->dictionary_id = $parent_id;
            $value->option = $option;
            $value->save();

            foreach ($newName as $lang=>$name) {
                $val = new ShopProductsDictionariesValueTranslates();
                $val->row_id = $value->id;
                $val->name = $name;
                $val->language = $lang;
                $val->save();
            }
            $specification = ShopProductsDictionary::find($parent_id);
            $html = view('admin.catalog.specifications.valuesTable', ['specification' => $specification])->render();
            return response()->json([
                'success' => true,
                'html' => $html,
            ]);
        }
    }
}
