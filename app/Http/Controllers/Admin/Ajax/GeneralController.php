<?php namespace App\Http\Controllers\Admin\Ajax;

use App\Helpers\Cart;
use App\Helpers\NovaPoshta;
use App\Models\Article;
use App\Models\ArticlesCategory;
use App\Models\Catalog\Product;
use App\Models\Catalog\ProductRelReward;
use App\Models\Catalog\ProductsPrice;
use App\Models\Catalog\ProductsRewards;
use App\Models\Catalog\ProductsWait;
use App\Models\Catalog\ShopBrand;
use App\Models\Catalog\ShopProductsDictionariesValue;
use App\Models\Catalog\ShopProductsDictionary;
use App\Models\Catalog\UsersWishlist;
use App\Models\News;
use App\Models\Notification;
use App\Models\Order\Order;
use App\Models\Order\OrdersItem;
use App\Models\Providers\ShopArea;
use App\Models\Providers\ShopRegion;
use App\Models\Slogan;
use App\Models\User;
use App\Models\VotingAnswers;
use App\Models\VotingAnswersTranslates;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;
use DB;
use Schema;
use Illuminate\Support\Str;
use App\Helpers\Alert;

/**
 * Class GeneralController
 * @package App\Http\Controllers\Admin\Ajax
 */
class GeneralController extends Controller
{

	/**
	 * Change status in the table $_POST['table'] by id = $_POST['id']
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function changeStatus(Request $request)
	{
		$id = $request->input('id');
		$table = $request->input('table');
		if (Schema::hasTable($table) === false) {
			return response()->json([
				'success' => false,
				'message' => 'Нет такой таблицы в текущей БД',
			]);
		}
		$row = DB::table($table)->find($id);
		if (!$row) {
			return response()->json([
				'success' => false,
				'message' => 'Запись с указанным ID отсутствует в таблице ' . $table,
			]);
		}
		if (DB::table($table)->where('id', '=', $id)->update(['status' => $row->status ? 0 : 1])) {
			return response()->json([
				'success' => true,
			]);
		}
		return response()->json([
			'success' => false,
			'message' => 'Ошибка БД'
		]);
	}

	/**
	 * Group delete in table $_POST['table'] by id = $_POST['id']
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function groupDelete(Request $request)
	{
		$ids = $request->input('id');
		$table = $request->input('table');

		if (Schema::hasTable($table) === false) {
			return response()->json([
				'success' => false,
				'message' => 'Нет такой таблицы в текущей БД',
			]);
		}

		foreach ($ids as $id) {
			DB::table($table)->delete($id);
		}
		return response()->json([
			'success' => true,
		]);
	}

	/**
	 * Group status change in table $_POST['table'] by id = $_POST['id']
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function groupStatus(Request $request)
	{
		$ids = $request->input('id');
		$table = $request->input('table');

		if (Schema::hasTable($table) === false) {
			return response()->json([
				'success' => false,
				'message' => 'Нет такой таблицы в текущей БД',
			]);
		}

		foreach ($ids as $id) {
			DB::table($table)->where('id', $id)->update(['status' => 0]);
		}
		return response()->json([
			'success' => true,
		]);
	}

	/**
	 * Group status change in table $_POST['table'] by id = $_POST['id']
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function groupStatusEnable(Request $request)
	{
		$ids = $request->input('id');
		$table = $request->input('table');

		if (Schema::hasTable($table) === false) {
			return response()->json([
				'success' => false,
				'message' => 'Нет такой таблицы в текущей БД',
			]);
		}

		foreach ($ids as $id) {
			DB::table($table)->where('id', $id)->update(['status' => 1]);
		}
		return response()->json([
			'success' => true,
		]);
	}

	/**
	 * Group status change in table $_POST['table'] by id = $_POST['id']
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function groupStatus2(Request $request)
	{
		$ids = $request->input('id');
		$table = $request->input('table');

		if (Schema::hasTable($table) === false) {
			return response()->json([
				'success' => false,
				'message' => 'Нет такой таблицы в текущей БД',
			]);
		}

		foreach ($ids as $id) {
			DB::table($table)->where('id', $id)->update(['status' => 2]);
		}
		return response()->json([
			'success' => true,
		]);
	}

	/**
	 * Group status change in table $_POST['table'] by id = $_POST['id']
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function groupStatus3(Request $request)
	{
		$ids = $request->input('id');
		$table = $request->input('table');

		if (Schema::hasTable($table) === false) {
			return response()->json([
				'success' => false,
				'message' => 'Нет такой таблицы в текущей БД',
			]);
		}

		foreach ($ids as $id) {
			DB::table($table)->where('id', $id)->update(['status' => 3]);
		}
		return response()->json([
			'success' => true,
		]);
	}

	/**
	 * Group status change in table $_POST['table'] by id = $_POST['id']
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function groupStatus4(Request $request)
	{
		$ids = $request->input('id');
		$table = $request->input('table');

		if (Schema::hasTable($table) === false) {
			return response()->json([
				'success' => false,
				'message' => 'Нет такой таблицы в текущей БД',
			]);
		}

		foreach ($ids as $id) {
			DB::table($table)->where('id', $id)->update(['status' => 4]);
		}
		return response()->json([
			'success' => true,
		]);
	}

	/**
	 * Group status change in table $_POST['table'] by id = $_POST['id']
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function groupStatus5(Request $request)
	{
		$ids = $request->input('id');
		$table = $request->input('table');

		if (Schema::hasTable($table) === false) {
			return response()->json([
				'success' => false,
				'message' => 'Нет такой таблицы в текущей БД',
			]);
		}

		foreach ($ids as $id) {
			DB::table($table)->where('id', $id)->update(['status' => 5]);
		}
		return response()->json([
			'success' => true,
		]);
	}

	/**
	 * Сортируем списки, задаем родителя
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function sortable(Request $request)
	{
		$json = $request->input('json');
		$arr = json_decode($json, true);
		$table = $request->input('table');
		if (Schema::hasTable($table) === false) {
			return response()->json([
				'success' => false,
				'message' => 'Нет такой таблицы в текущей БД',
			]);
		}

		$tree = false;
		if (Schema::hasColumn($table, 'parent_id')) {
			$tree = true;
		}

		function saveSort($arr, $table, $parentID, $tree, $i = 0)
		{
			foreach ($arr AS $a) {
				if ($tree === true) {
					$data = ['position' => $i, 'parent_id' => $parentID ?: 0];
				} else {
					$data = ['position' => $i];
				}
				$id = array_get($a, 'id');
				DB::table($table)->where('id', '=', $id)->update($data);
				$i++;
				$children = array_get($a, 'children', []);
				if (count($children)) {
					if ($tree === false) {
						$i = saveSort($children, $table, $id, $tree, $i);
					} else {
						saveSort($children, $table, $id, $tree);
					}
				}
			}
			return $i;
		}

		saveSort($arr, $table, 0, $tree);
		return response()->json([
			'success' => true,
		]);
	}

	/**
	 * Делаем алиас из текста
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function makeSlug(Request $request)
	{
		return response()->json([
			'success' => true,
			'slug' => Str::slug($request->input('name')),
		]);
	}

	public function readNotifications(Request $request)
	{
		Notification::readByIds($request->input('notificationsIds', []));
		return response()->json([
			'success' => true,
		]);
	}

	public function searchUsers(Request $request)
	{
		$users = User::search($request->input('q'), 20);
		return [
			'items' => $users->map(function (User $user) {
				return [
					'id' => $user->id,
					'text' => $user->full_name ? sprintf('%s, %s [%d]', $user->full_name, $user->email, $user->id) : $user->email . '[' . $user->id . ']',
				];
			}),
		];
	}

	public function allNews()
	{
		$news = (new News)->getList(10, true);
		return [
			'items' => $news->map(function (News $news) {
				return [
					'id' => $news->id,
					'checked' => false,
					'name' => $news->name,
					'summary' => $news->summary,
					'date' => $news->human_date,
					'image' => $news->showImage('small'),
				];
			}),
			'pagination' => [
				'currentPage' => (int)$news->currentPage(),
				'perPage' => (int)$news->perPage(),
				'lastPage' => (int)$news->lastPage(),
				'total' => (int)$news->total(),
				'hasMorePages' => $news->hasMorePages(),
				'hasPages' => $news->hasPages(),
				'isEmpty' => $news->isEmpty(),
			],
		];
	}

	/**
	 * Удаляем ответ в вопросе
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function deleteAnswer(Request $request)
	{

		$answer = VotingAnswers::getItemById($request->input('id'));
		$answer->delete();

		$answers = VotingAnswers::getList($request->input('question'));
		$countAnswers = count($answers);

		$html = view('admin.voting.answerItems', ['answers' => $answers, 'votingId' => $request->input('question')])->render();
		return response()->json([
			'success' => true,
			'html' => $html,
			'count' => $countAnswers,
		]);
	}

	/**
	 * Добавляем ответ в вопросе
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function addAnswer(Request $request)
	{
		if ($request->input('answer') == '' || $request->input('answer') == null || empty($request->input('answer'))) {
			return response()->json([
				'fail' => true,
			]);
		} else {
		    $answers =  \GuzzleHttp\json_decode($request->input('answer'));
			$answer = new VotingAnswers();
			$answer->status = 1;
			$answer->position = 15;
			$answer->question_id = $request->input('question');
			$answer->save();

			$id = $answer->id;
            foreach ($answers as $lang=>$name){
                $answer = new VotingAnswersTranslates();
                $answer->row_id = $id;
                $answer->name = $name;
                $answer->language = $lang;
                $answer->save();
            }

			$answers = VotingAnswers::getList($request->input('question'));
			$countAnswers = count($answers);

			$html = view('admin.voting.answerItems', ['answers' => $answers, 'votingId' => $request->input('question')])->render();
			return response()->json([
				'success' => true,
				'html' => $html,
				'count' => $countAnswers,
			]);
		}

	}

	/**
	 * Выбор статьи по типу в создании комментария
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function selectArticle(Request $request)
	{
		$type = $request->input('type');
		$articles = Article::getList(100, $type);
		$articlesArr = [];

		foreach ($articles as $article) {
			$articlesArr[$article->id] = $article->name;
		}

		$html = view('admin.widgets.form.select', ['additionalClass' => 'articlesSelected', 'name' => 'article_id', 'label' => 'Статья', 'data' => $articlesArr, 'options' => ['required']])->render();
		return response()->json([
			'success' => true,
			'html' => $html,
		]);
	}

	/**
	 * Выбор региона по стране
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function selectRegions(Request $request)
	{
		$country_id = $request->input('country_id');
		$regions = ShopRegion::getList(100, null, $country_id, 1)->all();

		$regionsArr = [];
		foreach ($regions as $region) {
			$regionsArr[$region->id] = $region->current->name;
		}




		$html = view('admin.widgets.form.select', ['name' => 'region_id[]', 'label' => 'Регион', 'data' => isset($country_id) ? $regionsArr : [], 'options' => ['required', 'class' => 'form-control regionProvide select2', 'multiple' => 'multiple']])->render();
		return response()->json([
			'success' => true,
			'html' => $html,
		]);
	}

	/**
	 * Транслит
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function translit(Request $request)
	{
		$translit = str_slug($request->input('source'));
		$translitNew = str_slug($request->input('sourceNew'));
		return response()->json([
			'success' => true,
			'translit' => $translit,
			'translitNew' => $translitNew,
		]);
	}

	/**
	 * Добавление нового значение в характеристики
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function addValue(Request $request)
	{
	    $parent_id = $request->input('id');
        $newSlug = $request->input('newSlug');
        $option = $request->input('option');
        $newName = \GuzzleHttp\json_decode($request->input('newNames'));
        if ($newName == null || $newSlug == null) {
			return response()->json([
				'fail' => true,
			]);
        } else {
            $value = new ShopProductsDictionariesValue();
            $this->validate($request, $value->rules());
		    foreach ($newName as $lang=>$name) {
                $value->name = $name;
            }
			$value->slug = $newSlug;
			$value->dictionary_id = $parent_id;
			$value->option = $option;
			$value->save();

			$specification = ShopProductsDictionary::find($parent_id);
			$html = view('admin.catalog.specifications.valuesTable', ['specification' => $specification])->render();
			return response()->json([
				'success' => true,
				'html' => $html,
			]);
		}
	}

	/**
	 * Удаление значение с характеристик
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function removeValue(Request $request)
	{
		$parent_id = $request->input('specification');
		$id = $request->input('id');

		$value = ShopProductsDictionariesValue::find($id);
		$value->delete();

		$specification = ShopProductsDictionary::find($parent_id);

		$html = view('admin.catalog.specifications.valuesTable', ['specification' => $specification])->render();
		return response()->json([
			'success' => true,
			'html' => $html,
		]);

	}

	/**
	 * Редактирование значение с характеристик
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function editValue(Request $request)
	{
		$parent_id = $request->input('specification');
		$id = $request->input('id');
        $names = \GuzzleHttp\json_decode($request->input('newName'));
		$slug = $request->input('newSlug');
		$option = $request->input('option');


		$value = ShopProductsDictionariesValue::find($id);
		$this->validate($request, $value->rules());
		$i = 0;
        foreach ($names as $name ){
            $value->data[$i]->name = $name;
            $value->data[$i]->save();
            $i++;
        }
		$value->slug = $slug;
		$value->option = $option;
		$value->dictionary_id = $parent_id;
		$value->save();


		$specification = ShopProductsDictionary::find($parent_id);

		$html = view('admin.catalog.specifications.valuesTable', ['specification' => $specification])->render();
		return response()->json([
			'success' => true,
			'html' => $html,
		]);

	}

	/**
	 * Добавление новой цены
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function addPrice(Request $request)
	{
		$product_id = $request->input('product_id');
		$volume = $request->input('volume');
		$year = $request->input('year');
		$price = $request->input('price');
		$weight = $request->input('weight');
		$sale = $request->input('sale');
		$artikul = $request->input('artikul');

		if ($volume == null || $year == null || $price == null || $weight == null || $artikul == null) {
			return response()->json([
				'fail' => true,
			]);
		} else {
			$prices = ProductsPrice::whereProductId($product_id)->get();

			$check_flag = 0;

			if (isset($prices)) {
				foreach ($prices as $itemPrice) {
					if ($itemPrice->is_checked == 1) {
						$check_flag = 1;
					}
				}
			}

			$price = new ProductsPrice();
			$this->validate($request, $price->rules());
			$price->fill($request->input());
			$price->is_checked = $check_flag == 1 ? 0 : 1;
			$price->save();

			$product = Product::find($product_id);
			$html = view('admin.catalog.products.pricesTable', ['product' => $product])->render();
			return response()->json([
				'success' => true,
				'html' => $html,
			]);
		}
	}

	/**
	 * Удаление цены
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function removePrice(Request $request)
	{
		$product_id = $request->input('product_id');
		$id = $request->input('id');

		$value = ProductsPrice::find($id);
		$value->delete();

		$product = Product::find($product_id);
		$html = view('admin.catalog.products.pricesTable', ['product' => $product])->render();
		return response()->json([
			'success' => true,
			'html' => $html,
		]);
	}

	/**
	 * Редактирование цены
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function editPrice(Request $request)
	{
		$product_id = $request->input('product_id');
		$id = $request->input('id');
		$volume = $request->input('volume');
		$year = $request->input('year');
		$price = $request->input('price');
		$weight = $request->input('weight');
		$sale = $request->input('sale');
		$artikul = $request->input('artikul');
		$checked = $request->input('is_checked');

		if ($checked == 1) {
			$productPrices = ProductsPrice::whereProductId($product_id)->get();

			foreach ($productPrices as $item) {
				$item->is_checked = 0;
				$item->save();
			}
		}

		$value = ProductsPrice::find($id);
		$this->validate($request, $value->rules());
		$value->volume = $volume;
		$value->year = $year;
		$value->price = $price;
		$value->weight = $weight;
		$value->product_id = $product_id;
		$value->sale = $sale;
		$value->artikul = $artikul;
		$value->is_checked = $checked;
		$value->save();


		$product = Product::find($product_id);

		$html = view('admin.catalog.products.pricesTable', ['product' => $product])->render();
		return response()->json([
			'success' => true,
			'html' => $html,
		]);

	}


    /**
     * Добавление новой награды
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addReward(Request $request)
    {
        $product_id = $request->input('product_id');
        $reward_id = $request->input('reward_id');
        $points = $request->input('points');

        if ($reward_id == null || $points == null ) {
            return response()->json([
                'fail' => true,
            ]);
        } else {

            $rel = new ProductRelReward();
            $this->validate($request, $rel->rules());
            $rel->fill($request->input());
            $rel->save();

            $product_rewards = ProductRelReward::whereProductId($product_id)->get();
            $rewards = ProductsRewards::getList();
            $html = view('admin.catalog.products.rewardTable', ['product_reward' => $product_rewards, 'rewards' => $rewards])->render();
            return response()->json([
                'success' => true,
                'html' => $html,
            ]);
        }
    }

    /**
     * Удаление награды
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeReward(Request $request)
    {
        $product_id = $request->input('product_id');
        $id = $request->input('id');

        $value = ProductRelReward::whereId($id);
        $value->delete();

        $product_rewards = ProductRelReward::whereProductId($product_id)->get();
        $rewards = ProductsRewards::getList();
        $html = view('admin.catalog.products.rewardTable', ['product_reward' => $product_rewards, 'rewards' => $rewards])->render();
        return response()->json([
            'success' => true,
            'html' => $html,
        ]);
    }

    /**
     * Редактирование награды
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editReward(Request $request)
    {
        $product_id = $request->input('product_id');
        $reward_id = $request->input('reward_id');
        $points = $request->input('points');
        $id = $request->input('id');


//        $value = ProductRelReward::whereId($id)->get();
//        $value = (new ProductRelReward)->find($id);
        $value = ProductRelReward::find($id);
        $this->validate($request, $value->rules());
        $this->validate($request, $value->rules());
        $value->reward_id = $reward_id;
        $value->points = $points;
        $value->save();


        $product_rewards = ProductRelReward::whereProductId($product_id)->get();
        $rewards = ProductsRewards::getList();
        $html = view('admin.catalog.products.rewardTable', ['product_reward' => $product_rewards, 'rewards' => $rewards])->render();
        return response()->json([
            'success' => true,
            'html' => $html,
        ]);

    }


	/**
	 * Установка процента голосования
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function newPercent(Request $request)
	{
		$nAnCounter = 0;
		foreach ($request->input() as $key => $value) {
			if ($value === 'NaN') {
				$nAnCounter++;
			}
		}
		foreach ($request->input() as $key => $value) {
			$answer = VotingAnswers::find($key);
			if ($nAnCounter < count($request->input())) {
				$answer->own_percent = ($value === 'NaN') ? 0 : $value;
			} else {
				$answer->own_percent = ($value === 'NaN') ? null : $value;
			}
			$answer->save();
		}

		return response()->json([
			'success' => true,
		]);

	}

	/**
	 * Смена позиции категории
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function setCategoryPosition(Request $request)
	{
		$id = $request->input('question');
		$value = $request->input('value');

		$category = ArticlesCategory::find($id);

		$category->position = $value;
		$category->save();


		return response()->json([
			'success' => true,
		]);

	}

    /**
     * Смена позиции товара
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setProductPosition(Request $request)
    {
        $id = $request->input('question');
        $value = $request->input('value');
        $product = Product::find($id);

        $product->position = $value;
        $product->save();


        return response()->json([
            'success' => true,
        ]);

    }

    /**
     * Смена позиции товара
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setFrontProductPosition(Request $request)
    {
        $id = $request->input('question');
        $value = $request->input('value');
        $product = Product::find($id);

        $product->position_front = $value;
        $product->save();


        return response()->json([
            'success' => true,
        ]);

    }

    /**
     * Смена позиции бренда
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setBrandPosition(Request $request)
    {
        $id = $request->input('brand');
        $value = $request->input('value');

        $brand = ShopBrand::find($id);

        $brand->position = $value;
        $brand->save();


        return response()->json([
            'success' => true,
        ]);

    }

	/**
	 * Редактирование слогана
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function saveSlogan(Request $request)
	{
		$id = $request->input('id');
		$slogan = $request->input('slogan');
		$text = $request->input('text');
		if ($text == null || $text = '') {
            return response()->json([
                'fail' => true,
            ]);
        }

		$sloganEdt = Slogan::find($id);

		$sloganEdt->text = $text;
		$sloganEdt->save();


		return response()->json([
			'success' => true,
		]);

	}

	/**
	 * Удаление слогана
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function delSlogan(Request $request)
	{
		$id = $request->input('id');

		$sloganEdt = Slogan::find($id);

		$sloganEdt->delete();

		return response()->json([
			'success' => true,
		]);

	}

	/**
	 * Добавление слогана
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function addSlogan(Request $request)
	{
		$slogan = $request->input('slogan');
		$text = $request->input('text');

		$sloganNew = new Slogan();

		$sloganNew->text = $text;
		$sloganNew->position = 0;
		$sloganNew->parent_slogan = $slogan;
		$sloganNew->save();

		return response()->json([
			'success' => true,
		]);

	}

	/**
	 * Добавление слогана
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function delImgWithoutRoute(Request $request)
	{
		$id = $request->input('id');
		$obj = ShopProductsDictionariesValue::find($id);
		$obj->image = null;
		$obj->save();
		return response()->json([
			'success' => true,
		]);

	}

	/**
	 * Заполнения данных пользователя по выбору
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ordersUserSelect(Request $request)
	{
		$id = $request->input('id');
		$user = User::find($id);
		$json = [];
		$json['name'] = $user->second_name . ' ' . $user->first_name . ' ' . $user->middle_name;
		$json['phone'] = $user->phone;
		$json['email'] = $user->email;
		$json['address'] = $user->address;
		return response()->json([
			'success' => true,
			'user' => $json,
		]);
	}

	/**
	 * Заполнения данных пользователя по выбору
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ordersWarehousesSelect(Request $request)
	{
		$ref = $request->input('ref');
		$enable = $request->input('enable');
		$warehouses = NovaPoshta::getWarehousesByCityRef($ref);
		$html = view('admin.widgets.form.select', ['name' => 'delivery_warehouse', 'label' => 'Отделение новой почты', 'data' => $warehouses, 'options' => ['class' => 'form-control select2', 'id' => 'user-warehouse', $enable == 1 ? '' : 'disabled']])->render();

		return response()->json([
			'success' => true,
			'html' => $html,
		]);
	}

	/**
	 * Измение количества товара в заказе
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ordersItemsCount(Request $request)
	{
		$id = (int)$request->input('id');
		$value = (int)$request->input('value');

		$orderItem = OrdersItem::find($id);
		$orderItem->count = $value;
		$saved = $orderItem->save();

		if ($saved) {
			$order = Order::find($orderItem->order_id);
			$orderItems = OrdersItem::whereOrderId($order->id)->get();
			$ref = $order->city;
			$deliveryType = $order->delivery_type;
			$paymentType = $order->payment_type;
			$totalSumOrder = 0;
			$totalWeightOrder = 0;
			$totalCountOrder = 0;
			foreach ($orderItems as $item) {
				$totalSumOrder += $item->totalPrice * $item->count;
				$totalWeightOrder += $item->price->weight * $item->count;
                $totalCountOrder += $item->count;
			}
            $additionalPaymentAddress = (int) config('db.pack_amount.address-payment');
            $deliveryCost = $order->delivery_type == 0 ? 0 :(int) config('db.basic.door-delivery') + $additionalPaymentAddress;;
            $order->total_price = $totalSumOrder + $deliveryCost;
            $order->delivery_cost = $deliveryCost;
			$order->save();
			$html = view('admin.orders.orderItems', ['order' => $order])->render();
			return response()->json([
				'success' => true,
				'totalSumOrder' => $totalSumOrder + $deliveryCost,
				'deliveryCost' => $deliveryCost,
				'message' => 'Количество успешно изменено',
				'html' => $html,
			]);
		}

		return response()->json([
			'success' => false,
			'message' => 'Ошибка сервера'
		]);
	}

	/**
	 * Измение количества товара в заказе
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ordersItemsDelete(Request $request)
	{
		$id = (int)$request->input('id');

		$orderItem = OrdersItem::find($id);
		$deleted = $orderItem->delete();

		if ($deleted) {
			$order = Order::find($orderItem->order_id);
			$orderItems = OrdersItem::whereOrderId($order->id)->get();
			$totalSumOrder = 0;
			$totalWeightOrder = 0;
			$deliveryCost = 0;
			$totalCountOrder = 0;
			if (sizeof($orderItems)){
				$ref = $order->city;
				$deliveryType = $order->delivery_type;
				$paymentType = $order->payment_type;
				foreach ($orderItems as $item) {
					$totalSumOrder += $item->totalPrice * $item->count;
					$totalWeightOrder += $item->price->weight * $item->count;
					$totalCountOrder += $item->count;
				}
                $additionalPaymentAddress = (int) config('db.pack_amount.address-payment');
                $deliveryCost = $order->delivery_type == 0 ? 0 :(int) config('db.basic.door-delivery') + $additionalPaymentAddress;;
                $order->total_price = $totalSumOrder + $deliveryCost;
                $order->delivery_cost = $deliveryCost;
				$order->save();
			}
			$html = view('admin.orders.orderItems', ['order' => $order])->render();
			return response()->json([
				'success' => true,
				'totalSumOrder' => $totalSumOrder + $deliveryCost,
				'deliveryCost' => $deliveryCost,
				'message' => 'Успешно удалено',
				'html' => $html,
			]);
		}

		return response()->json([
			'success' => false,
			'message' => 'Ошибка сервера'
		]);
	}


	public function ordersProductSelectPrices(Request $request)
	{
		$id = (int)$request->input('id');

		$prices = ProductsPrice::whereProductId($id)->get();

		if (sizeof($prices) > 0) {

			$pricesArr = [];

			foreach ($prices as $price) {
				$sale = ($price->sale == null) ? 0 : (int)$price->sale;
				$priceTotal = ($sale !== 0) ? ceil($price->price - ($price->price * ($sale / 100))) : $price->price;
				$pricesArr[$price->id] = "Обьем: $price->volume мл | Год: $price->year  год | Вес: $price->weight кг | Скидка: $sale % | Цена: $price->price грн | Цена со скидкой: $priceTotal грн ";
			}
			$html = view('admin.widgets.form.select', ['name' => 'order-product-price', 'label' => 'Цена', 'data' => $pricesArr, 'options' => ['id' => 'order-products-price']])->render();
			$html .= view('admin.widgets.form.input', ['name' => 'order-product-count', 'label' => 'Количество', 'options' => ['id' => 'order-products-count', 'style' => 'width: 60px;']])->render();
			return response()->json([
				'success' => true,
				'message' => 'Выберите цену',
				'html' => $html
			]);
		}

		return response()->json([
			'success' => false,
			'message' => 'У данного товара нету цен'
		]);
	}

	public function ordersProductCount(Request $request)
	{
		$val = (int)$request->input('value');

		if ($val !== null && $val !== 0) {

			$html = '<button type="button" id="orders-add-new-product" class="btn btn-m btn-success">Добавить товар</button>';

			return response()->json([
				'success' => true,
				'html' => $html,
				'message' => 'Подтвердите добавление товара',
			]);
		}

		return response()->json([
			'success' => false,
			'message' => 'Количество не может быть пустым и равняться нулю',
		]);
	}

	public function ordersSaveProduct(Request $request)
	{
		$orderID = $request->input('order_id');
		$productID = $request->input('product_id');
		$priceID = $request->input('price_id');
		$count = $request->input('count');

		$findedOrderItem = OrdersItem::whereOrderId($orderID)->whereProductId($productID)->wherePriceId($priceID)->first();

		if ($findedOrderItem !== null) {
			$findedOrderItem->count += $count;
			$saved = $findedOrderItem->save();
			if ($saved) {
				$order = Order::find($orderID);
				$ref = $order->city;
				$deliveryType = $order->delivery_type;
				$paymentType = $order->payment_type;
				$html = view('admin.orders.orderItems', ['order' => $order])->render();
				$orderItems = OrdersItem::whereOrderId($orderID)->get();
				$totalSumOrder = 0;
				$totalWeightOrder = 0;
				$totalCountOrder = 0;
				foreach ($orderItems as $orderItem) {
					$totalSumOrder += $orderItem->totalPrice * $orderItem->count;
					$totalWeightOrder += $orderItem->price->weight * $orderItem->count;
					$totalCountOrder += $orderItem->count;
				}
                $additionalPaymentAddress = (int) config('db.pack_amount.address-payment');
				$deliveryCost = $order->delivery_type == 0 ? 0 :(int) config('db.basic.door-delivery') + $additionalPaymentAddress;;
				$order->total_price = $totalSumOrder + $deliveryCost;
				$order->delivery_cost = $deliveryCost;
				$order->save();
				return response()->json([
					'success' => true,
					'html' => $html,
					'totalSumOrder' => $totalSumOrder,
					'deliveryCost' => $deliveryCost,
					'message' => 'Товар уже есть в заказе, количество обновлено',
				]);
			}
			return response()->json([
				'success' => false,
				'message' => 'Ошибка сервера, перезагрузите страницу и попробуйте еще раз',
			]);
		}

		$newOrder = new OrdersItem();
		$this->validate($request, $newOrder->rules());
		$newOrder->order_id = $orderID;
		$newOrder->product_id = $productID;
		$newOrder->price_id = $priceID;
		$newOrder->count = $count;
		$saved = $newOrder->save();

		if ($saved) {
			$order = Order::find($orderID);
			$ref = $order->city;
			$deliveryType = $order->delivery_type;
			$paymentType = $order->payment_type;
			$html = view('admin.orders.orderItems', ['order' => $order])->render();
			$orderItems = OrdersItem::whereOrderId($orderID)->get();
			$totalSumOrder = 0;
			$totalWeightOrder = 0;
			$totalCountOrder = 0;
			foreach ($orderItems as $orderItem) {
				$totalSumOrder += $orderItem->totalPrice * $orderItem->count;
				$totalWeightOrder += $orderItem->price->weight * $orderItem->count;
				$totalCountOrder += $orderItem->count;
			}
            $additionalPaymentAddress = (int) config('db.pack_amount.address-payment');
            $deliveryCost = $order->delivery_type == 0 ? 0 :(int) config('db.basic.door-delivery') + $additionalPaymentAddress;;
            $order->total_price = $totalSumOrder + $deliveryCost;
            $order->delivery_cost = $deliveryCost;
			$order->save();
			return response()->json([
				'success' => true,
				'html' => $html,
				'totalSumOrder' => $totalSumOrder + $deliveryCost,
				'deliveryCost' => $deliveryCost,
				'message' => 'Товар успешно добавлен в заказ',
			]);
		}
		return response()->json([
			'success' => false,
			'message' => 'Ошибка сервера, перезагрузите страницу и попробуйте еще раз',
		]);
	}

    public function ordersWaitsStatus(Request $request)
    {
        $user_id = $request->input('user_id');
        $user = User::find($user_id);
        $status = $request->input('status');
        $waits = ProductsWait::whereUserId($user_id)->get();
        $saved = false;
        foreach ($waits as $wait) {
            $wait->status = $status;
            $saved = $wait->save();
        }

        if ($saved) {
            return response()->json([
                'success' => true,
                'message' => 'Успешно сохранено',
                'html' => view('admin.widgets.form.select', ['name' => 'status', 'label' => false, 'data' => [1 => 'Выполнен', 2 => 'Отменен', 0 => 'Новый'], 'value' => $status, 'options' => ['id' => 'order-wait-status', 'data-user-id' => $user->id]])->render(),
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Ошибка сервера, перезагрузите страницу и попробуйте еще раз',
        ]);
    }

}
