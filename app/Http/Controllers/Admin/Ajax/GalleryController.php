<?php namespace App\Http\Controllers\Admin\Ajax;

use App\Http\Controllers\Admin\Controller;
use App\Models\Catalog\Product;
use App\Models\Catalog\ProductsImage;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Class GeneralController
 * @package App\Http\Controllers\Admin\Ajax
 */
class GalleryController extends Controller
{

    public function getPhotos()
    {
        $id = (int)(request()->input('id'));


        $product = Product::find($id);
        $images = [];
        if($product && $product->id){
            $images = $product->images;
        }

        $show_images = view('admin.catalog.products.uploaded_images', [
            'images' => $images
        ])->render();

        return response()->json([
            'images' => $show_images,
        ]);
    }

    public function uploadPhotos()
    {
        $gallery_id = (int)(request()->input('id'));

        $image = new ProductsImage();
        $image->fill(request()->input());
        $image->product_id = $gallery_id;
        $image->uploadImage();
        $image->save();

        return response()->json([
            'confirm' => true,
        ]);
    }

    public function deletePhoto()
    {
        $photo_id = (int)(request()->input('id'));
        /** @var ProductsImage $photo */
        $photo = ProductsImage::find($photo_id);
        if ($photo) {
            $photo->delete();
        }
    }

    public function sortPhotos()
    {
        $order = request()->input('order', []);
        if (count($order)) {
            $i = 0;
            foreach ($order as $photo_id) {
                /** @var ProductsImage $photo */
                $photo = ProductsImage::find($photo_id);
                if ($photo) {
                    $photo->position = $i++;
                    $photo->save();
                }
            }
        }
        return response()->json([
            'success' => true,
        ]);
    }

}