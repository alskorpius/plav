<?php

namespace App\Http\Controllers\Admin\Callback;

use App\Models\Article;
use App\Models\EventsRegistration;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;
use App\Helpers\Alert;

class EventsSubController extends Controller
{
    protected $pageName = 'Регистрация на мероприятие';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventSubs = EventsRegistration::getList($this->rowsCount, null);

        $categories = Article::whereType(2)->where('status', 1)->get();
        $categoriesArr = [];
        foreach ($categories as $category) {
            $categoriesArr[$category->id] = $category->current->name;
        }
        $events = $categoriesArr;

        return view('admin..callback.events.index', [
            'eventSubs' => $eventSubs,
            'events' => $events,
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new EventsRegistration())->rules()));
        $events = Article::getList(100, 2);
        $eventsArr = [];
        foreach ($events as $event){
            $eventsArr[$event->id] = $event->current->name;
        }
        $events = $eventsArr;
        return view('admin.callback.events.create', ['events' => $events]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $eventsSub = new EventsRegistration();
        $this->validate($request, $eventsSub->rules());
        $eventsSub->fill($request->input());
        $saved = $eventsSub->save();
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('events-sub', $eventsSub->id);
        }
        return redirect(route('admin.callback.events.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EventsRegistration $eventsSub
     * @return \Illuminate\Http\Response
     */
    public function edit(EventsRegistration $eventsSub)
    {
        view()->share('jsValidator', \JsValidator::make($eventsSub->rules()));
        $events = Article::getList(100, 2);
        $eventsArr = [];
        foreach ($events as $event){
            $eventsArr[$event->id] = $event->current->name;
        }
        $events = $eventsArr;
        return view('admin.callback.events.update', ['eventsSub' => $eventsSub, 'events' => $events]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\EventsRegistration $eventsSub
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventsRegistration $eventsSub)
    {
        $this->validate($request, $eventsSub->rules());
        $eventsSub->fill($request->input());
        $saved = $eventsSub->save();
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('events-sub', $eventsSub->id);
        }
        return redirect(route('admin.events-sub.edit', ['eventsSub' => $eventsSub->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EventsRegistration $eventsSub
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventsRegistration $eventsSub)
    {
        Alert::afterDeleting($eventsSub->delete());
        return redirect(route('admin.events-sub.index'));
    }
}
