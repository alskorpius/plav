<?php

namespace App\Http\Controllers\Admin\Callback;

use App\Helpers\Alert;
use App\Models\Callback;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class CallBacksController extends Controller
{
    protected $pageName = 'Сообщения из контактной формы';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $callbacks = Callback::getList($this->rowsCount);

        return view('admin.callback.callbacks.index', [
            'callbacks' => $callbacks,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        view()->share('jsValidator', \JsValidator::make((new Callback())->rules()));
        return view('admin.callback.callbacks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $callback = new Callback();
        $this->validate($request, $callback->rules());
        $callback->fill($request->input());
        $callback->status = 0;
        $saved = $callback->save();
        Alert::afterCreating($saved);

        if ($saved) {
            return $this->redirectUserTo('callback', $callback->id);
        }
        return redirect(route('admin.callback.callbacks.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param callable $callback
     * @return \Illuminate\Http\Response
     * @internal param \App\Models\ArticlesCategory $category
     */
    public function edit(Callback $callback) {
        view()->share('jsValidator', \JsValidator::make($callback->rules()));
        return view('admin.callback.callbacks.update', ['callback' => $callback]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Callback $callback
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Callback $callback) {
        $this->validate($request, $callback->rules());
        $callback->fill($request->input());
        $saved = $callback->save();
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('callback', $callback->id);
        }
        return redirect(route('admin.callback.callbacks.edit', ['callback' => $callback->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArticlesCategory $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Callback $callback) {
        Alert::afterDeleting($callback->delete());
        return redirect(route('admin.callbacks.index'));
    }
}
