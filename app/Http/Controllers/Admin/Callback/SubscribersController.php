<?php

namespace App\Http\Controllers\Admin\Callback;

use App\Helpers\Alert;
use App\Helpers\Notify;
use App\Mail\News;
use App\Models\Article;
use App\Notifications\Subscribe;
use Carbon\Carbon;
use Mail;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class SubscribersController extends Controller
{

    protected $pageName = 'Подписчики';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscribers = Subscriber::getList($this->rowsCount);
        return view('admin.callback.subscribers.index', [
            'subscribers' => $subscribers,
        ]);
    }

    /**
     * Show the form for creating a letter for sending.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new Subscriber())->rules()));
        return view('admin.callback.subscribers.create');
    }

    /**
     * Show the form for creating a letter for sending.
     *
     * @return \Illuminate\Http\Response
     */
    public function createNew()
    {
        view()->share('jsValidator', \JsValidator::make((new Subscriber())->rules()));
        return view('admin.callback.subscribers.createNew');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subscribers = Subscriber::getList(10000, 1);
        foreach ($subscribers as $subscriber) {
            Notify::send(new Subscribe($subscriber, ['name' => $request->input('subject'), 'content' => $request->input('letter')]));
        }
        return redirect(route('admin.subscribers.index'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeNew(Request $request)
    {
        $subscriber = new Subscriber();
        $this->validate($request, $subscriber->rules());
        $subscriber->fill($request->input());
        $subscriber->status = $request->input('status');
        $saved = $subscriber->save();
        Alert::afterCreating($saved);
        if ($saved) {
            return redirect(route('admin.subscribers.index'));
        }
        return redirect(route('admin.subscribers.createNew'))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Subscriber $subscriber
     * @return \Illuminate\Http\Response
     * @internal param \App\Models\EventsRegistration $eventsSub
     */
    public function destroy(Subscriber $subscriber)
    {
        Alert::afterDeleting($subscriber->delete());
        return redirect(route('admin.subscribers.index'));
    }

    public function sendNews(Request $request)
    {
        $content = '';
        $articles = Article::where('published_at', '>=', Carbon::now()->subDay(7))->whereStatus(1)->get();
        $subscribers = Subscriber::getList(10000, 1);
        foreach ($subscribers as $subscriber) {
            Notify::send(new Subscribe($subscriber, ['name' => 'События за последнюю неделю', 'content' => $content], $articles));
        }
        return redirect(route('admin.subscribers.index'))->with('message', 'Рассылка прошла успешно');
    }

}
