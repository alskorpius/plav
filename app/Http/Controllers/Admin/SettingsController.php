<?php namespace App\Http\Controllers\Admin;

use App\Helpers\Alert;
use App\Models\Settings;
use App\Models\SettingsGroup;
use Illuminate\Http\Request;

class SettingsController extends Controller
{

    protected $pageName = 'Настройки сайта';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Settings::whereStatus(1)->get();
        $rules = [];
        foreach ($settings AS $setting) {
            if ($setting->validation) {
                $rules[$setting->key] = $setting->validation;
            }
        }
        view()->share('jsValidator', \JsValidator::make($rules));
        $groups = SettingsGroup::with('settings')->oldest('position')->get();
        return view('admin.settings.index', ['groups' => $groups]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $settings = Settings::whereStatus(1)->get();
        $rules = [];
        foreach ($settings AS $setting) {
            if ($setting->validation) {
                $rules[$setting->key] = $setting->validation;
            }
        }
        $this->validate($request, $rules);
        foreach ($settings AS $setting) {
            if ($request->input($setting->key, false) !== false) {
                $setting->value = $request->input($setting->key);
                $setting->save();
            }
        }
        Alert::success('Данные успешно сохранены', 'Изменение настроек сайта');
        return redirect()->back();
    }
}
