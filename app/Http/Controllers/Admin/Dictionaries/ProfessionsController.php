<?php

namespace App\Http\Controllers\Admin\Dictionaries;

use App\Helpers\Alert;
use App\Models\Profession;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class ProfessionsController extends Controller
{
    protected $pageName = 'Профессии';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professions = (new Profession())->getList($this->rowsCount);
        return view('admin.dictionaries.professions.index', [
            'professions' => $professions,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new Profession())->rules()));
        return view('admin.dictionaries.professions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $profession = new Profession();
        $this->validate($request, $profession->rules());

        $profession->slug = str_slug($profession->slug);

        $profession->fill($request->input());
        $saved = $profession->save();

        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('professions', $profession->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Profession  $profession
     * @return \Illuminate\Http\Response
     */
    public function edit(Profession $profession)
    {
        view()->share('jsValidator', \JsValidator::make($profession->rules()));
        return view('admin.dictionaries.professions.update', ['profession' => $profession]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Profession  $profession
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profession $profession)
    {
        $this->validate($request, $profession->rules());
        $profession->fill($request->input());
        $saved = $profession->save();
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('profession', $profession->id);
        }
        return redirect(route('admin.dictionaries.professions.edit', ['profession' => $profession->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Profession  $profession
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profession $profession)
    {
        Alert::afterDeleting($profession->delete());
        return redirect(route('admin.professions.index'));
    }
}
