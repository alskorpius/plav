<?php namespace App\Http\Controllers\Admin;

use Lang;
use Config;
use Route;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $rowsCount;

    protected $pageName;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        Config::set('app.locale', 'ru');
        Lang::setLocale('ru');
        Config::set('laravel-widgets.default_namespace', 'App\Admin\Widgets');
        $this->rowsCount = config('db.basic.rows-count');
        $this->customBreadcrumbs();
    }

    protected function breadcrumb($name, $route = null, $params = [], $icon = null)
    {
        $breadcrumbs = config('breadcrumbs', []);
        $breadcrumbs[] = [
            'name' => $name,
            'icon' => $icon,
            'route' => $route,
            'params' => $params,
        ];
        Config::set('breadcrumbs', $breadcrumbs);
    }

    protected function customBreadcrumbs()
    {
        $this->breadcrumb('Dashboard', 'admin.dashboard', [], 'fa fa-dashboard');
        $routeElements = explode('.', Route::currentRouteName());
        $currentAction = array_pop($routeElements);
        $routeBase = implode('.', $routeElements);
        if (count($routeElements) > 0) {

            if ($this->pageName) {

                if ($currentAction === 'index') {
                    $this->breadcrumb($this->pageName);

                } else {
                    if (Route::has($routeBase . '.index')) {
                        $this->breadcrumb($this->pageName, $routeBase . '.index');
                    }
                    switch ($currentAction) {
                        case 'deleted':
                            $this->breadcrumb('Удаленные');
                            break;
                        case 'create':
                            $this->breadcrumb('Добавление');
                            break;
                        case 'edit':
                            $this->breadcrumb('Редактирование');
                            break;
                        case 'show':
                            $this->breadcrumb('Просмотр');
                            break;
                    }
                }
            }
        }
    }

    /**
     * Перенаправляем пользвоателя на другую страницу после успешного сохранения данных
     * @param $key
     * @param $value
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectUserTo($key, $value)
    {
        $currentRouteElements = explode('.', Route::currentRouteName());
        array_pop($currentRouteElements);
        if (array_key_exists('submit_only', request()->input())) {
            $currentRouteElements[] = 'edit';
            return redirect(route(implode('.', $currentRouteElements), [$key => $value]));
        } elseif (array_key_exists('submit_close', request()->input())) {
            $currentRouteElements[] = 'index';
            return redirect(route(implode('.', $currentRouteElements)));
        } elseif (array_key_exists('submit_add', request()->input())) {
            $currentRouteElements[] = 'create';
            return redirect(route(implode('.', $currentRouteElements)));
        }
        return redirect(request()->url());
    }
}
