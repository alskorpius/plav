<?php namespace App\Http\Controllers\Admin;

use App\Models\Crop;
use Validator;
use App\Helpers\Alert;
use Illuminate\Http\Request;

/**
 * Class CropController
 * @package App\Http\Controllers\Admin
 */
class CropController extends Controller
{

    /** @var object */
    private $model;

    /** @var string */
    private $size;

    /** @var string */
    private $folder;

    /** @var string */
    private $field;

    /** @var array */
    private $settings;

    /** @var Crop */
    private $crop;

    /**
     * Подготавливаем почву для работы кропа
     *
     * @param Request $request
     */
    private function prepare(Request $request)
    {
        if (!$request->input('id') || !$request->input('back') || !$request->input('model')) {
            debug(':' . __LINE__);
            abort(404);
        }
        $modelName = $request->input('model');
        $this->model = $modelName::findOrFail($request->input('id'));
        if (!method_exists($this->model, 'getImagesFolders') || !$this->model->getImagesFolders()) {
            debug(':' . __LINE__);
            abort(404);
        }
        $this->folder = $request->input('folder') ?: $this->model->getFolder();
        $this->field = $request->input('field') ?: $this->model->getImageField();
        if ($this->model->imageExists('original', $this->folder, $this->field) === false) {
            debug(':' . __LINE__);
            debug($this->model->getImagesFolders());
            abort(404);
        }
        $this->settings = array_get($this->model->getImagesFolders(), $this->folder, []);
        unset($this->settings['original']);
        foreach ($this->settings AS $folder => $localSettings) {
            if (array_get($localSettings, 'crop', false) !== true || !array_get($localSettings, 'width') || !array_get($localSettings, 'height')) {
                unset($this->settings[$folder]);
            }
        }
        if (empty($this->settings)) {
            debug(':' . __LINE__);
            abort(404);
        }
        $sizesList = array_keys($this->settings);
        $defaultSize = array_shift($sizesList);
        $this->size = $request->input('size', $defaultSize);
        if (array_key_exists($this->size, $this->settings) === false) {
            debug(':' . __LINE__);
            abort(404);
        }
        $this->crop = Crop::whereModelId($request->input('id'))
            ->whereModelName($request->input('model'))
            ->whereSize($this->size)
            ->whereFolder($this->folder)
            ->first();
        if (!$this->crop) {
            $this->crop = new Crop();
            $this->crop->model_id = $request->input('id');
            $this->crop->model_name = $request->input('model');
            $this->crop->size = $this->size;
            $this->crop->folder = $this->folder;
        }
    }

    /**
     * Как кроп видит администратор
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $this->breadcrumb('Обрезание изображения');
        $this->prepare($request);
        return view('admin.crop.index', [
            'settings' => $this->settings,
            'model' => $this->model,
            'localSettings' => $this->settings[$this->size],
            'currentSize' => $this->size,
            'crop' => $this->crop,
            'default' => $this->crop->exists ? $this->crop->toArray() : false,
        ]);
    }

    /**
     * Как происходит процесс обработки фото
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $this->prepare($request);
        $info = json_decode($request->input('data', '[]'), true);
        $validator = Validator::make($info, $this->crop->rules());
        if ($validator->failed()) {
            abort(404);
        }
        $setting = $this->settings[$this->size];
        $originalImage = $this->model->getImageSettings('original', $this->folder, $this->field);
        $file = \Image::make($originalImage['dirname'] . '/' . $originalImage['basename']);
        $newWidth = array_get($setting, 'width');
        $newHeight = array_get($setting, 'height');
        $file->crop((int)$info['width'], (int)$info['height'], (int)$info['x'], (int)$info['y']);
        $file->resize($newWidth, $newHeight);
        if (array_get($setting, 'watermark') && is_file(public_path(config('watermark.path')))) {
            $watermark = \Image::make(public_path(config('watermark.path')));
            $watermark->widen(config('watermark.width') * $file->width());
            $watermark->opacity(config('watermark.opacity'));
            $file->insert($watermark, config('watermark.position'), config('watermark.x'), config('watermark.y'));
        }
        $file->save(public_path('uploads/images/' . $this->folder . '/' . $this->size . '/' . $originalImage['basename']), 100);
        $this->crop->fill($info);
        $this->crop->save();
        Alert::success('Фото изменено!');
        return redirect()->back();
    }
}
