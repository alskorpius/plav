<?php

namespace App\Http\Controllers\Admin\Providers;

use App\Helpers\Alert;
use App\Models\Providers\ProvidersRegion;
use App\Models\Providers\ShopArea;
use App\Models\Providers\ShopCountry;
use App\Models\Providers\ShopProvider;
use App\Models\Providers\ShopRegion;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class ProvidersController extends Controller
{
    protected $pageName = 'Производители';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $providers = ShopProvider::getList($this->rowsCount);
        return view('admin.providers.providers.index', [
            'providers' => $providers,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new ShopProvider())->rules()));

        $countries = ShopCountry::all()->sortBy('name')->where('status', 1);
        $countriesArr = [];
        $countriesArr[''] = '';
        foreach ($countries as $country) {
            $countriesArr[$country->id] = $country->current->name;
        }
        $countries = $countriesArr;

        $regions = ShopRegion::all()->sortBy('name')->where('status', 1);
        $regionsArr = [];
        $regionsArr[''] = '';
        foreach ($regions as $region) {
            $regionsArr[$region->id] = $region->current->name;
        }
        $regions = $regionsArr;

        $areas = ShopArea::all()->sortBy('name');
        $areasArr = [];
        $areasArr[''] = '';
        foreach ($areas as $area) {
            $areasArr[$area->id] = $area->current->name;
        }
        $areas = $areasArr;

        return view('admin.providers.providers.create', ['countries' => $countries, 'regions' => $regions, 'areas' => $areas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $provider = new ShopProvider();
        $this->validate($request, $provider->rules());
        $saved = $provider->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            $regions = $request->input('region_id');
            foreach ($regions as $region) {
                $providerRegion = new ProvidersRegion();
                $providerRegion->region_id = $region;
                $providerRegion->provider_id = $provider->id;
                $providerRegion->save();
            }
            return $this->redirectUserTo('providers', $provider->id);
        }
        return redirect(route('admin.providers.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ShopProvider $provider
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(ShopProvider $provider)
    {
        view()->share('jsValidator', \JsValidator::make($provider->rules()));

        $countries = ShopCountry::all()->sortBy('name')->where('status', 1);
        $countriesArr = [];
        $countriesArr[''] = '';
        foreach ($countries as $country) {
            $countriesArr[$country->id] = $country->current->name;
        }
        $countries = $countriesArr;

        $regions = ShopRegion::getList(100, null, $provider->country_id, 1);
        $regionsArr = [];
        $regionsArr[''] = '';
        foreach ($regions as $region) {
            if ($region->current) {
                $regionsArr[$region->id] = $region->current->name;
            }
        }
        $regions = $regionsArr;

        $selectedRegions = ProvidersRegion::whereProviderId($provider->id)->get();
        $selectedRegionsArr = [];
        $i = 0;
        foreach ($selectedRegions as $selectedRegion) {
            $selectedRegionsArr[$i] = $selectedRegion->region_id;
                $i++;
        }
        $selectedRegions = $selectedRegionsArr;

        return view('admin.providers.providers.update', ['provider' => $provider, 'countries' => $countries, 'regions' => $regions, 'selectedRegions' => $selectedRegions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ShopProvider $provider
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, ShopProvider $provider)
    {
        $this->validate($request, $provider->rules());
        $saved = $provider->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            $selectedProviderRegions = ProvidersRegion::whereProviderId($provider->id)->get();
            foreach ($selectedProviderRegions as $item) {
                $item->delete();
            }
            $regions = $request->input('region_id');
            foreach ($regions as $region) {
                $providerRegion = new ProvidersRegion();
                $providerRegion->region_id = $region;
                $providerRegion->provider_id = $provider->id;
                $providerRegion->save();
            }
            return $this->redirectUserTo('providers', $provider->id);
        }
        return redirect(route('admin.providers.edit', ['provider' => $provider->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ShopProvider $provider
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShopProvider $provider)
    {
        Alert::afterDeleting($provider->delete());
        return redirect(route('admin.providers.index'));
    }
}
