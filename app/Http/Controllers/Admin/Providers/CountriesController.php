<?php

namespace App\Http\Controllers\Admin\Providers;

use App\Helpers\Alert;
use App\Models\Providers\ShopCountry;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class CountriesController extends Controller
{
    protected $pageName = 'Страны';


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = ShopCountry::getList($this->rowsCount);
        return view('admin.providers.countries.index', [
            'countries' => $countries,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new ShopCountry())->rules()));
        return view('admin.providers.countries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $providers_country = new ShopCountry();
        $this->validate($request, $providers_country->rules());
        $providers_country->uploadImage();
        $saved = $providers_country->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('providers-countries', $providers_country->id);
        }
        return redirect(route('admin.providers-countries.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ShopCountry $providers_country
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(ShopCountry $providers_country)
    {
        view()->share('jsValidator', \JsValidator::make($providers_country->rules()));
        return view('admin.providers.countries.update', ['country' => $providers_country]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ShopCountry $providers_country
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @internal param ShopCountry $country
     * @internal param int $id
     */
    public function update(Request $request, ShopCountry $providers_country)
    {
        $this->validate($request, $providers_country->rules());
        $providers_country->uploadImage();
        $saved = $providers_country->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('providers-countries', $providers_country->id);
        }
        return redirect(route('admin.providers-countries.edit', ['country' => $providers_country->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ShopCountry $providers_country
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(ShopCountry $providers_country)
    {
        Alert::afterDeleting($providers_country->delete());
        return redirect(route('admin.providers-countries.index'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteImage(Request $request, $id) {
        $providers_country = ShopCountry::findOrFail($id);
        Alert::afterImageDeleting($providers_country->deleteImage()->save());
        return redirect(route('admin.providers-countries.edit', ['country' => $providers_country->id]));
    }
}
