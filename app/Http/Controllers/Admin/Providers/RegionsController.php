<?php

namespace App\Http\Controllers\Admin\Providers;

use App\Helpers\Alert;
use App\Models\Providers\ShopCountry;
use App\Models\Providers\ShopRegion;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class RegionsController extends Controller
{
    protected $pageName = 'Регионы';


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regions = ShopRegion::getList($this->rowsCount);
        // dd($regions);
        return view('admin.providers.regions.index', [
            'regions' => $regions,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new ShopRegion())->rules()));

        $countries = ShopCountry::all()->sortBy('name')->where('status', 1);
        $countriesArr = [];
        foreach ($countries as $country){
            $countriesArr[$country->id] = $country->current->name;
        }
        $countries = $countriesArr;

        return view('admin.providers.regions.create', ['countries' => $countries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $providers_region = new ShopRegion();
        $this->validate($request, $providers_region->rules());
        $saved = $providers_region->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('providers-regions', $providers_region->id);
        }
        return redirect(route('admin.providers-regions.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ShopRegion $providers_region
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(ShopRegion $providers_region)
    {
        view()->share('jsValidator', \JsValidator::make($providers_region->rules()));

        $countries = ShopCountry::all()->sortBy('name')->where('status', 1);
        $countriesArr = [];
        foreach ($countries as $country){
            $countriesArr[$country->id] = $country->current->name;
        }
        $countries = $countriesArr;

        return view('admin.providers.regions.update', ['region' => $providers_region, 'countries' => $countries]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ShopRegion $providers_region
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @internal param int $id
     */
    public function update(Request $request, ShopRegion $providers_region)
    {
        $this->validate($request, $providers_region->rules());
        $providers_region->fill($request->input());
        $saved = $providers_region->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('providers-regions', $providers_region->id);
        }
        return redirect(route('admin.providers-regions.edit', ['region' => $providers_region->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ShopRegion $providers_region
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(ShopRegion $providers_region)
    {
        Alert::afterDeleting($providers_region->delete());
        return redirect(route('admin.providers-regions.index'));
    }
}
