<?php

namespace App\Http\Controllers\Admin\Providers;

use App\Helpers\Alert;
use App\Models\Providers\ShopArea;
use App\Models\Providers\ShopCountry;
use App\Models\Providers\ShopRegion;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class AreasController extends Controller
{
    protected $pageName = 'Подрегионы';


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $areas = ShopArea::getList($this->rowsCount);
        return view('admin.providers.areas.index', [
            'areas' => $areas,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new ShopArea())->rules()));
        $countries = ShopCountry::all()->sortBy('name');
        $regionsArr = [];
        foreach ($countries as $country) {
            foreach ($country->shopRegions as $region) {
                $regionsArr[$country->name][$region->id] = $region->name;
            }
        }
        $regions = $regionsArr;
        return view('admin.providers.areas.create', ['regions' => $regions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $providers_area = new ShopArea();
        $this->validate($request, $providers_area->rules());
        $providers_area->fill($request->input());
        $saved = $providers_area->save();
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('providers-areas', $providers_area->id);
        }
        return redirect(route('admin.providers-areas.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ShopArea $providers_area
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(ShopArea $providers_area)
    {
        view()->share('jsValidator', \JsValidator::make($providers_area->rules()));
        $countries = ShopCountry::all()->sortBy('name');
        $regionsArr = [];
        foreach ($countries as $country) {
            foreach ($country->shopRegions as $region) {
                $regionsArr[$country->name][$region->id] = $region->name;
            }
        }
        $regions = $regionsArr;
        return view('admin.providers.areas.update', ['area' => $providers_area, 'regions' => $regions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ShopArea $providers_area
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @internal param int $id
     */
    public function update(Request $request, ShopArea $providers_area)
    {
        $this->validate($request, $providers_area->rules());
        $providers_area->fill($request->input());
        $saved = $providers_area->save();
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('providers-areas', $providers_area->id);
        }
        return redirect(route('admin.providers-areas.edit', ['area' => $providers_area->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ShopArea $providers_area
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(ShopArea $providers_area)
    {
        Alert::afterDeleting($providers_area->delete());
        return redirect(route('admin.providers-areas.index'));
    }
}
