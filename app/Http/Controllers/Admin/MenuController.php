<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Alert;
use App\Models\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{

    protected $pageName = 'Меню сайта';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu_items = (new Menu())->getList();
        $menu_arr = [];
        foreach ($menu_items as $menu) {
            $menu_arr[(int)$menu->parent_id][] = $menu;
        }
        return view('admin.menu.index', [
            'menu_items' => $menu_arr,
            'cur' => 0,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new Menu())->rules()));
        return view('admin.menu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $menu = new Menu();
        $this->validate($request, $menu->rules());
        $menu->fill($request->input());
        $saved = $menu->save();
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('menu', $menu->id);
        }
        return redirect(route('admin.menu.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        view()->share('jsValidator', \JsValidator::make($menu->rules()));
        return view('admin.menu.update', ['menu' => $menu]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        $this->validate($request, $menu->rules());
        $menu->fill($request->input());
        $saved = $menu->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('menu', $menu->id);
        }
        return redirect(route('admin.menu.edit', ['menu' => $menu->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        Alert::afterDeleting($menu->delete());
        return redirect(route('admin.menu.index'));
    }
}
