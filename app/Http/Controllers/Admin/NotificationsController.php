<?php namespace App\Http\Controllers\Admin;

use App\Models\Notification;

class NotificationsController extends Controller
{

    protected $pageName = 'Уведомления';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = Notification::getList($this->rowsCount);
        return view('admin.notifications.index', [
            'notifications' => $notifications,
        ]);
    }

    public function read(Notification $notification)
    {
        $notification->read();
        return redirect($notification->link);
    }

}
