<?php

namespace App\Http\Controllers\Admin\Orders;

use App\Helpers\Alert;
use App\Helpers\NovaPoshta;
use App\Models\Catalog\Product;
use App\Models\Order\Order;
use App\Models\Order\OrdersItem;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class OrdersController extends Controller
{
	protected $pageName = 'Заказы';

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$orders = Order::getList($this->rowsCount);
		return view('admin.orders.index', [
			'orders' => $orders,
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$users = User::whereRoleAlias('user')->get();
		$usersArr = [];
		$usersArr[0] = '';
		foreach ($users as $user) {
			$usersArr[$user->id] = $user->second_name . ' ' . $user->first_name;
		}
		$cities = NovaPoshta::getCities();
		view()->share('jsValidator', \JsValidator::make((new Order())->rules()));
		return view('admin.orders.create', ['users' => $usersArr, 'cities' => $cities]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function store(Request $request)
	{
		$order = new Order();
		$this->validate($request, $order->rules());
		$order->fill($request->input());

		if ($request->input('delivery_type') == 0) {
			$order->delivery_address = null;
		} else {
			$order->delivery_warehouse = null;
		}
		if ($request->input('user_id') == 0) {
            $order->user_id = null;
        }
        $order->city_name = NovaPoshta::getCity($request->input('city'));
		$order->delivery_warehouse_name = NovaPoshta::getWarehouse($request->input('delivery_warehouse'));
		$order->comment = $request->input('comment');
		$saved = $order->save();
		Alert::afterCreating($saved);
		if ($saved) {
			return $this->redirectUserTo('order', $order->id);
		}
		return redirect(route('admin.orders.create'))->withInput();
	}

	/**
	 * Show the form for editing the specified resource.
	 * @param Order $order
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Order $order)
	{
		$users = User::whereRoleAlias('user')->get();

		$usersArr = [];
		$usersArr[0] = '';
		foreach ($users as $user) {
			$usersArr[$user->id] = $user->second_name . ' ' . $user->first_name;
		}

		$products = (new Product)->getList(100000, 1 ,1);
		$productsArr = [];
		$productsArr[0] = '';
		foreach ($products as $product) {
			$productsArr[$product->id] = $product->artikuls . $product->current->name;
		}

		$cities = NovaPoshta::getCities();
		$warehouses = NovaPoshta::getWarehousesByCityRef($order->city);

		$orderItems = OrdersItem::whereOrderId($order->id)->get();
        $weightItems = 0;
		if (sizeof($orderItems) == 0) {
			$order->delivery_cost = 0;
			$order->total_price = 0;
			$order->save();
		} else {
			$sumItems = 0;
			$weightItems = 0;
			$countItems = 0;
			foreach ($orderItems as $item) {
				$sumItems += $item->total_price * $item->count;
				$weightItems += $item->price->weight * $item->count;
                $countItems += $item->count;
			}
            $additionalPaymentAddress = (int) config('db.pack_amount.address-payment');
            $order->delivery_cost = $order->delivery_type == 0 ? 0 :(int) config('db.basic.door-delivery') + $additionalPaymentAddress;
			$order->total_price = $order->delivery_cost + $sumItems;
			$order->save();
		}
		view()->share('jsValidator', \JsValidator::make($order->rules()));
		return view('admin.orders.update', [
			'order' => $order,
			'users' => $usersArr,
			'cities' => $cities,
			'products' => $productsArr,
			'warehouses' => $warehouses,
            'weightItems' => $weightItems,
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param Order $order
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function update(Request $request, Order $order)
	{
		$this->validate($request, $order->rules());
		$order->fill($request->input());
		if ($request->input('delivery_type') == 0) {
			$order->delivery_address = null;
		} else {
			$order->delivery_warehouse = null;
		}
        if ($request->input('user_id') == 0) {
            $order->user_id = null;
        }
        $order->city_name = NovaPoshta::getCity($request->input('city'));
        $order->delivery_warehouse_name = NovaPoshta::getWarehouse($request->input('delivery_warehouse'));
		$order->comment = $request->input('comment');
		$saved = $order->save();
		Alert::afterUpdating($saved);
		if ($saved) {
			return $this->redirectUserTo('order', $order->id);
		}
		return redirect(route('admin.orders.edit', ['order' => $order->id]))->withInput();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Order $order
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Order $order)
	{
		Alert::afterDeleting($order->delete());
		return redirect(route('admin.orders.index'));
	}

	public function printOrder(Request $request, $id)
	{
		$order = Order::find($id);
		return view('admin.orders.print', ['order' => $order]);
	}
}
