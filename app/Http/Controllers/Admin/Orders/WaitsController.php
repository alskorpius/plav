<?php

namespace App\Http\Controllers\Admin\Orders;

use App\Helpers\Alert;
use App\Models\Catalog\Product;
use App\Models\Catalog\ProductsWait;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class WaitsController extends Controller
{
    protected $pageName = 'Сообщить о наличии';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $waits = ProductsWait::getList(null);
        $waitArr = [];
        foreach ($waits as $wait) {
            $waitArr[$wait->user_id] = $wait;
        }
        $waits = $waitArr;
        return view('admin.waits.index', [
            'waits' => $waits,
        ]);
    }
    public function show(Request $request, $id)
    {
        $user = User::find($id);
        $waits = ProductsWait::whereUserId($id)->get();
        $idsArr = [];
        $date = null;
        $status = 0;
        foreach ($waits as $wait) {
            $idsArr[] = $wait->product_id;
            $date = $wait->created_at;
            $status = $wait->status;
        }
        $products = Product::whereIn('id', $idsArr)->get();
        return view('admin.waits._form', [
            'products' => $products,
            'user' => $user,
            'date' => $date,
            'status' => $status,
        ]);
    }
    public function destroy(Request $request, $id)
    {
        $waits = ProductsWait::whereUserId($id)->get();
        foreach ($waits as $wait) {
            $wait->delete();
        }
        Alert::afterDeleting(true);
        return redirect(route('admin.waits.index'));
}
}
