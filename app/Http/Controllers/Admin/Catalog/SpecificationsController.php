<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Helpers\Alert;
use App\Models\Catalog\ShopProductsDictionary;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class SpecificationsController extends Controller
{
    protected $pageName = 'Характеристики';


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $specifications = ShopProductsDictionary::getList();
        return view('admin.catalog.specifications.index', [
            'specifications' => $specifications,
        ]);
    }

    /**
     * @param ShopProductsDictionary $specification
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(ShopProductsDictionary $specification)
    {

        view()->share('jsValidator', \JsValidator::make($specification->rules()));
        return view('admin.catalog.specifications.update', ['specification' => $specification]);
    }

    /**
     * @param Request $request
     * @param ShopProductsDictionary $specification
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, ShopProductsDictionary $specification)
    {
        $arrRequest = [];
        foreach ($request->input() as $key => $value) {
            if ($value == '0') {
                $arrRequest[$key] = null;
            } else {
                $arrRequest[$key] = $value;
            }
        }
        $this->validate($request, $specification->rules());
        $specification->fill($arrRequest);
        $saved = $specification->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('specifications', $specification->id);
        }
        return redirect()->route('admin.catalog.specifications.edit', ['specification' => $specification->id])->withInput();
    }

}
