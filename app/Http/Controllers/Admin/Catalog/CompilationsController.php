<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Helpers\Alert;
use App\Models\Catalog\ProductsCompilation;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class CompilationsController extends Controller
{
    protected $pageName = 'Подборки';


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $compilations = ProductsCompilation::getList($this->rowsCount);
        return view('admin.catalog.compilations.index', [
            'compilations' => $compilations,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new ProductsCompilation())->rules()));
        return view('admin.catalog.compilations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $compilation = new ProductsCompilation();
        $this->validate($request, $compilation->rules());
        $compilation->uploadImage();
        $compilation->slug = str_slug($compilation->slug);
        $saved = $compilation->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('compilation', $compilation->id);
        }
        return redirect(route('admin.compilations.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ProductsCompilation $compilation
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductsCompilation $compilation)
    {
        view()->share('jsValidator', \JsValidator::make($compilation->rules()));
        return view('admin.catalog.compilations.update', ['compilation' => $compilation]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ProductsCompilation $compilation
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, ProductsCompilation $compilation)
    {
        $this->validate($request, $compilation->rules());
        $compilation->fill($request->input());
        $compilation->uploadImage();
        $compilation->slug = str_slug($compilation->slug);
        $saved = $compilation->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('compilation', $compilation->id);
        }
        return redirect(route('admin.compilations.edit', ['compilation' => $compilation->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ProductsCompilation $compilation
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductsCompilation $compilation)
    {
        $compilation->deleteImage();
        Alert::afterDeleting($compilation->delete());
        return redirect(route('admin.compilations.index'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteImage(Request $request, $id)
    {
        $compilation = ProductsCompilation::findOrFail($id);
        Alert::afterImageDeleting($compilation->deleteImage()->save());
        return redirect(route('admin.compilations.edit', ['compilation' => $compilation->id]));
    }
}
