<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Helpers\Alert;
use App\Models\Catalog\ProductsLine;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class LinesController extends Controller
{
    protected $pageName = 'Линейки';


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lines = ProductsLine::getList($this->rowsCount);
        return view('admin.catalog.lines.index', [
            'lines' => $lines,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new ProductsLine())->rules()));
        return view('admin.catalog.lines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $line = new ProductsLine();
        $this->validate($request, $line->rules());
        $line->uploadImage();
        $line->slug = str_slug($line->slug);
        $saved = $line->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('line', $line->id);
        }
        return redirect(route('admin.lines.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ProductsLine $line
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductsLine $line)
    {
        view()->share('jsValidator', \JsValidator::make($line->rules()));
        return view('admin.catalog.lines.update', ['line' => $line]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ProductsLine $line
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, ProductsLine $line)
    {
        $this->validate($request, $line->rules());
        $line->uploadImage();
        $line->slug = str_slug($line->slug);
        $saved = $line->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('line', $line->id);
        }
        return redirect(route('admin.lines.edit', ['line' => $line->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ProductsLine $line
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductsLine $line)
    {
        $line->deleteImage();
        Alert::afterDeleting($line->delete());
        return redirect(route('admin.lines.index'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteImage(Request $request, $id)
    {
        $line = ProductsLine::findOrFail($id);
        Alert::afterImageDeleting($line->deleteImage()->save());
        return redirect(route('admin.lines.edit', ['line' => $line->id]));
    }
}
