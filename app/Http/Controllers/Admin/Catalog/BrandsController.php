<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Helpers\Alert;
use App\Models\Catalog\Product;
use App\Models\Catalog\ShopBrand;
use App\Models\Providers\ShopProvider;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class BrandsController extends Controller
{
    protected $pageName = 'Бренды';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        ShopBrand::workWithBD();
        $brands = ShopBrand::getList($this->rowsCount);
        return view('admin.catalog.brands.index', [
            'brands' => $brands,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        view()->share('jsValidator', \JsValidator::make((new ShopBrand())->rules()));

        $providers = ShopProvider::all();
        $providersArr = [];
        foreach ($providers as $provider){
            $providersArr[$provider->id] = $provider->current->name;
        }
        $providers = $providersArr;

        return view('admin.catalog.brands.create', ['providers' => $providers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $brand = new ShopBrand();
        $this->validate($request, $brand->rules());
        $brand->uploadImage();
		if ($request->input('h1') == null){
//			$brand->current->h1 = $request->input('h1');
		}
        $brand->slug = str_slug($brand->slug);
        $saved = $brand->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('brand', $brand->id);
        }
        return redirect(route('admin.brands.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ShopBrand $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(ShopBrand $brand) {
        view()->share('jsValidator', \JsValidator::make($brand->rules()));

        $providers = ShopProvider::all();
        $providersArr = [];
        foreach ($providers as $provider){
            $providersArr[$provider->id] = $provider->current->name;
        }
        $providers = $providersArr;

        return view('admin.catalog.brands.update', ['brand' => $brand, 'providers' => $providers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ShopBrand $brand
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, ShopBrand $brand) {
        $this->validate($request, $brand->rules());
        $brand->fill($request->input());
        $brand->uploadImage();
		if ($request->input('h1') == null){
			$brand->current->h1 = $request->input('h1');
		}
        $brand->slug = str_slug($brand->slug);
        $saved = $brand->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            $products = Product::whereBrandId($brand->id)->get();
            foreach ($products as $product) {
                $product->provider_id = $request->input('shop_provider_id');
                $product->save();
            }
            return $this->redirectUserTo('brand', $brand->id);
        }
        return redirect(route('admin.brands.edit', ['brand' => $brand->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ShopBrand $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShopBrand $brand) {
        $brand->deleteImage();
        Alert::afterDeleting($brand->delete());
        return redirect(route('admin.brands.index'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteImage(Request $request, $id) {
        /** @var ShopBrand $brand */
        $brand = ShopBrand::findOrFail($id);
        Alert::afterImageDeleting($brand->deleteImage()->save());
        return redirect(route('admin.brands.edit', ['brand' => $brand->id]));
    }
}
