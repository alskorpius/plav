<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Helpers\Alert;
use App\Models\Catalog\Product;
use App\Models\Catalog\ProductsReviews;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class ProductsReviewsController extends Controller
{
	protected $pageName = 'Отзывы о товарах';

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$reviews = ProductsReviews::getList($this->rowsCount);
		return view('admin.catalog.reviews.index', [
			'reviews' => $reviews,
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$products = (new Product)->getList(100, 1);
		$users = User::all();
		$productsArr = [];
		$usersArr = [];
		$usersArr[0] = '';
		foreach ($products as $product) {
			$productsArr[$product->id] = $product->current->name;
		}
		foreach ($users as $user) {
			$usersArr[$user->id] = $user->second_name . ' ' . $user->first_name;
		}
		view()->share('jsValidator', \JsValidator::make((new ProductsReviews())->rules()));
		return view('admin.catalog.reviews.create', ['products' => $productsArr, 'users' => $usersArr]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function store(Request $request)
	{
		$review = new ProductsReviews();
		$this->validate($request, $review->rules());
        $mark = ((int) $request->input('mark') > 5) ? 5 : (int) $request->input('mark');
        $review->fill($request->input());
		$review->created_at = $request->input('created_at');
		if ($request->input('user_id') == 0) {
			$review->user_id = null;
		}
        $review->mark = ($mark < 0) ? 0 : $mark;
        $saved = $review->save();
		Alert::afterCreating($saved);
		if ($saved) {
			return $this->redirectUserTo('products-reviews', $review->id);
		}
		return redirect(route('admin.products-reviews.create'))->withInput();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param ProductsReviews $products_review
	 * @return \Illuminate\Http\Response
	 * @internal param \App\Models\Comment $comment
	 */
	public function edit(ProductsReviews $products_review)
	{
		$product = Product::find($products_review->product_id);
		$user = User::find($products_review->user_id);
		view()->share('jsValidator', \JsValidator::make($products_review->rules()));
		return view('admin.catalog.reviews.update', ['review' => $products_review, 'product' => $product, 'user' => $user]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param ProductsReviews $products_review
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 * @internal param \App\Models\Comment $comment
	 */
	public function update(Request $request, ProductsReviews $products_review)
	{
        $this->validate($request, $products_review->rules());
        $mark = ((int) $request->input('mark') > 5) ? 5 : (int) $request->input('mark');
        $products_review->fill($request->input());
		$products_review->created_at = $request->input('created_at');
		$products_review->status = $request->input('status');
		$products_review->mark = ($mark < 0) ? 0 : $mark;
		$saved = $products_review->save();
		Alert::afterUpdating($saved);
		if ($saved) {
			return $this->redirectUserTo('products-reviews', $products_review->id);
		}
		return redirect(route('admin.products-reviews.edit', ['review' => $products_review->id]))->withInput();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param ProductsReviews $products_review
	 * @return \Illuminate\Http\Response
	 * @internal param \App\Models\Comment $comment
	 */
	public function destroy(ProductsReviews $products_review)
	{
		Alert::afterDeleting($products_review->delete());
		return redirect(route('admin.products-reviews.index'));
	}
}
