<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Helpers\Alert;
use App\Helpers\Notify;
use App\Models\Catalog\Product;
use App\Models\Catalog\ProductsRewards;
use Illuminate\Http\Request;

use App\Http\Controllers\Admin\Controller;

class ProductsRewardsController extends Controller
{

    protected $pageName = 'Награды';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $rewards = ProductsRewards::getList();
        return view('admin.catalog.rewards.index', [
            'rewards' => $rewards,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new ProductsRewards())->rules()));
        return view('admin.catalog.rewards.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $arrRequest = [];
        foreach ($request->input() as $key => $value) {
            if ($value == '0') {
                $arrRequest[$key] = null;
            } else {
                $arrRequest[$key] = $value;
            }
        }
        $reward = new ProductsRewards();
        $this->validate($request, $reward->rules());
        $reward->uploadImage();

        $saved = $reward->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('rewards', $reward->id);
        }
        return redirect(route('admin.rewards.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ProductsRewards $reward
     * @return \Illuminate\Http\Response
     */

    public function edit(ProductsRewards $reward)
    {
        view()->share('jsValidator', \JsValidator::make($reward->rules()));
        return view('admin.catalog.rewards.update', [
            'reward' => $reward,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, ProductsRewards $reward)
    {

        $arrRequest = [];
        foreach ($request->input() as $key => $value) {
            if ($value == '0') {
                $arrRequest[$key] = null;
            } else {
                $arrRequest[$key] = $value;
            }
        }


        $this->validate($request, $reward->rules());
        $reward->fill($arrRequest);
        $reward->uploadImage();
        $saved = $reward->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('rewards', $reward->id);
        }
        return view('admin.catalog.rewards.update', [
            'reward' => $reward,
        ])->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductsRewards $reward)
    {
        Alert::afterDeleting($reward->delete());
        return redirect(route('admin.rewards.index'));
    }
    public function deleteImage(Request $request, $id)
    {

        $product = ProductsRewards::findOrFail($id);
        Alert::afterImageDeleting($product->deleteImage()->save());
        return redirect(route('admin.rewards.edit', ['product' => $product->id]));
    }
}
