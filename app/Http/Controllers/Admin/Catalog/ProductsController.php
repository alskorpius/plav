<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Helpers\Alert;
use App\Helpers\Notify;
use App\Models\Catalog\Product;
use App\Models\Catalog\ProductRelCompilation;
use App\Models\Catalog\ProductRelReward;
use App\Models\Catalog\ProductsCompilation;
use App\Models\Catalog\ProductsImage;
use App\Models\Catalog\ProductsLine;
use App\Models\Catalog\ProductSort;
use App\Models\Catalog\ProductsPresentation;
use App\Models\Catalog\ProductsPrice;
use App\Models\Catalog\ProductsRecommendation;
use App\Models\Catalog\ProductsRewards;
use App\Models\Catalog\ProductsWait;
use App\Models\Catalog\ShopBrand;
use App\Models\Catalog\ShopProductsDictionariesValue;
use App\Notifications\ProductIsset;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class ProductsController extends Controller
{
    protected $pageName = 'Товары';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Product::workWithBD();
        $brandsArr = [];
        $linesArr = [];
        $compilationsArr = [];

        $brands = ShopBrand::getList(1000, 1);
        $lines = ProductsLine::getList(1000, 1);
        $compilations = ProductsCompilation::getList(1000, 1);

        foreach ($brands as $brand) {
            $brandsArr[$brand->id] = $brand->current->name;
        }
        foreach ($lines as $line) {
            $linesArr[$line->id] = $line->current->name;
        }
        foreach ($compilations as $compilation) {
            $compilationsArr[$compilation->id] = $compilation->current->name;
        }
        $products = (new Product)->getList($this->rowsCount);
        return view('admin.catalog.products.index', [
            'products' => $products,
            'brands' => $brandsArr,
            'lines' => $linesArr,
            'compilations' => $compilationsArr,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $colors = ShopProductsDictionariesValue::getList(1, 1);
        $colorsArr = [];
        $colorsArr[0] = 'Не выбрано';
        foreach ($colors as $color) {
            $colorsArr[$color->id] = $color->current->name;
        }

        $sweetness = ShopProductsDictionariesValue::getList(2, 1);
        $sweetnessArr = [];
        $sweetnessArr[0] = 'Не выбрано';
        foreach ($sweetness as $item) {
            $sweetnessArr[$item->id] = $item->current->name;
        }

        $type = ShopProductsDictionariesValue::getList(3, 1);
        $typeArr = [];
        $typeArr[0] = 'Не выбрано';
        foreach ($type as $item) {
            $typeArr[$item->id] = $item->current->name;
        }

        $kind = ShopProductsDictionariesValue::getList(4, 1);
        $kindArr = [];
        $kindArr[0] = 'Не выбрано';
        foreach ($kind as $item) {
            $kindArr[$item->id] = $item->current->name;
        }

        $alcohol = ShopProductsDictionariesValue::getList(5, 1);
        $alcoholArr = [];
        $alcoholArr[0] = 'Не выбрано';
        foreach ($alcohol as $item) {
            $alcoholArr[$item->id] = $item->current->name;
        }

        $sort = ShopProductsDictionariesValue::getList(6, 1);
        $sortArr = [];
        $sortArr[0] = 'Не выбрано';
        foreach ($sort as $item) {
            $sortArr[$item->id] = $item->current->name;
        }

        $sugar = ShopProductsDictionariesValue::getList(7, 1);
        $sugarArr = [];
        $sugarArr[0] = 'Не выбрано';
        foreach ($sugar as $item) {
            $sugarArr[$item->id] = $item->current->name;
        }

        $brands = ShopBrand::getList(100, 1);
        $brandsArr = [];
        $brandsArr[0] = 'Не выбрано';
        foreach ($brands as $item) {
            $brandsArr[$item->id] = $item->current->name;
        }

        $lines = ProductsLine::getList(100, 1);
        $linesArr = [];
        $linesArr[0] = 'Не выбрано';
        foreach ($lines as $item) {
            $linesArr[$item->id] = $item->current->name;
        }

        $compilation = ProductsCompilation::getList(100, 1);
        $compilationArr = [];
        $compilationArr[0] = 'Не выбрано';
        foreach ($compilation as $item) {
            $compilationArr[$item->id] = $item->current->name;
        }

        $products = (new Product)->getList(10000, 1, 1);
        $productsArr = [];
        foreach ($products as $product) {
            $productsArr[$product->id] = $product->current->name;
        }
        $products = $productsArr;

        view()->share('jsValidator', \JsValidator::make((new Product())->rules()));
        return view('admin.catalog.products.create', [
            'colors' => $colorsArr,
            'sweetness' => $sweetnessArr,
            'types' => $typeArr,
            'kind' => $kindArr,
            'alcohol' => $alcoholArr,
            'sort' => $sortArr,
            'sugar' => $sugarArr,
            'brands' => $brandsArr,
            'lines' => $linesArr,
            'compilation' => $compilationArr,
            'products' => $products,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $brandId = $request->input('brand_id');
        $countryId = null;
        $providerId = null;
        if ($brandId !== 0) {
            $brand = ShopBrand::find($brandId);
            $countryId = $brand->shopProvider->shopCountry->id;
            $providerId = $brand->shopProvider->id;
        }
        $arrRequest = [];
        foreach ($request->input() as $key => $value) {
            $arrRequest[$key] = $value;
        }
        $request->merge($arrRequest);

        $product = new Product();
        $this->validate($request, $product->rules());
        $product->uploadImage();
        $product->slug = str_slug($product->slug);
        $product->is_avilable = $request->input('is_avilable');
        $product->country_id = $countryId;
        $product->provider_id = $providerId;
        if ($request->input('is_accessory') == '1') {
            $request->merge([
                'color_id' => null,
                'sweetness_id' => null,
                'type_id' => null,
                'kind_id' => null,
                'alcohol_id' => null,
                'sugar_id' => null,
            ]);
        }
        $saved = $product->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            $productImage = new ProductsImage();
            $productImage->uploadImage();
            $productImage->product_id = $product->id;
            $productImage->status = 1;
            $productImage->position = 0;
            $productImage->save();
            $savedProduct = Product::all()->last();
            $sort = $request->input('sort_id');
            if (sizeof($sort) > 0){
                foreach ($request->input('sort_id') as $sort) {
                    $sortValueNew = new ProductSort();
                    $sortValueNew->product_id = $savedProduct->id;
                    $sortValueNew->sort_id = $sort;
                    $sortValueNew->save();
                }
            }
            $compilation = $request->input('compilation_id');
            if (sizeof($compilation) > 0){
                foreach ($request->input('compilation_id') as $compilation) {
                    $compilationValueNew = new ProductRelCompilation();
                    $compilationValueNew->product_id = $product->id;
                    $compilationValueNew->compilation_id = $compilation;
                    $compilationValueNew->save();
                }
            }
            $contentPresent = $request->input('contentPresent');
            $combinationPresent = $request->input('combinationPresent');
            $tastePresent = $request->input('tastePresent');
            $aromaPresent = $request->input('aromaPresent');
            $colorPresent =$request->input('colorPresent');

            if (isset($contentPresent) || isset($combinationPresent) || isset($tastePresent) || isset($aromaPresent) || isset($colorPresent)){
                $presentation = new ProductsPresentation();
                $presentation->fill($request->input());
                $presentation->product_id = $savedProduct->id;
                $presentation->save();
            }

            $productsArr = $request->input('products_recomm');
            if (sizeof($productsArr) > 0) {
                $prodRecomms = ProductsRecommendation::whereParentId($product->id)->get();
                if (sizeof($prodRecomms) > 0) {
                    foreach ($prodRecomms as $item) {
                        $item->delete();
                    }
                }
                foreach ($productsArr as $value) {
                    $newArticleProduct = new ProductsRecommendation();
                    $newArticleProduct->parent_id = $product->id;
                    $newArticleProduct->product_id = $value;
                    $newArticleProduct->save();
                }
            }

            return $this->redirectUserTo('product', $product->id);
        }
        return redirect(route('admin.products.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {

        $colors = ShopProductsDictionariesValue::getList(1, 1);
        $colorsArr = [];
        $colorsArr[0] = 'Не выбрано';
        foreach ($colors as $color) {
            $colorsArr[$color->id] = $color->current->name;
        }

        $sweetness = ShopProductsDictionariesValue::getList(2, 1);
        $sweetnessArr = [];
        $sweetnessArr[0] = 'Не выбрано';
        foreach ($sweetness as $item) {
            $sweetnessArr[$item->id] = $item->current->name;
        }

        $type = ShopProductsDictionariesValue::getList(3, 1);
        $typeArr = [];
        $typeArr[0] = 'Не выбрано';
        foreach ($type as $item) {
            $typeArr[$item->id] = $item->current->name;
        }

        $kind = ShopProductsDictionariesValue::getList(4, 1);
        $kindArr = [];
        $kindArr[0] = 'Не выбрано';
        foreach ($kind as $item) {
            $kindArr[$item->id] = $item->current->name;
        }

        $alcohol = ShopProductsDictionariesValue::getList(5, 1);
        $alcoholArr = [];
        $alcoholArr[0] = 'Не выбрано';
        foreach ($alcohol as $item) {
            $alcoholArr[$item->id] = $item->current->name;
        }

        $sort = ShopProductsDictionariesValue::getList(6, 1);
        $sortArr = [];
        $sortArr[0] = 'Не выбрано';
        foreach ($sort as $item) {
            $sortArr[$item->id] = $item->current->name;
        }

        $sugar = ShopProductsDictionariesValue::getList(7, 1);
        $sugarArr = [];
        $sugarArr[0] = 'Не выбрано';
        foreach ($sugar as $item) {
            $sugarArr[$item->id] = $item->current->name;
        }

        $brands = ShopBrand::getList(100, 1);
        $brandsArr = [];
        $brandsArr[0] = 'Не выбрано';
        foreach ($brands as $item) {
            $brandsArr[$item->id] = $item->current->name;
        }

        $lines = ProductsLine::getList(100, 1);
        $linesArr = [];
        $linesArr[0] = 'Не выбрано';
        foreach ($lines as $item) {
            $linesArr[$item->id] = $item->current->name;
        }

        $compilation = ProductsCompilation::getList(100, 1);
        $compilationArr = [];
        $compilationArr[0] = 'Не выбрано';
        foreach ($compilation as $item) {
            $compilationArr[$item->id] = $item->current->name;
        }

        $prices = ProductsPrice::whereProductId($product->id)->get();

        if (sizeof($prices) == 1) {
            foreach ($prices as $price) {
                $price->is_checked = 1;
                $price->save();
            }
        }

        $check_flag = 0;
        if (sizeof($prices) !== 0) {
            foreach ($prices as $itemPrice) {
                if ($itemPrice->is_checked == 1) {
                    $check_flag = 1;
                }
            }

            if ($check_flag == 0) {
                $forCheckPrice = ProductsPrice::whereProductId($product->id)->orderBy('price')->first();
                $forCheckPrice->is_checked = 1;
                $forCheckPrice->save();
            }
        }

        $selectedSorts = [];
        if (sizeof($product->sorts) > 0) {
            $i = 0;
            foreach ($product->sorts as $sort) {
                $selectedSorts[$i] = $sort->value->id;
                $i++;
            }
        } else {
            $selectedSorts = null;
        }

        $selectedCompilations = [];
        if (sizeof($product->compilations) > 0) {
            $i = 0;
            foreach ($product->compilations as $compilation) {
                $selectedCompilations[$i] = $compilation->value->id;
                $i++;
            }
        } else {
            $selectedCompilations = null;
        }

        $productsRecomm = (new Product)->getList(10000, 1, 1);
        $productsArr = [];
        foreach ($productsRecomm as $productsRecommItem) {
            $productsArr[$productsRecommItem->id] = $productsRecommItem->current->name;
        }
        $products = $productsArr;

        $selectedProducts = ProductsRecommendation::whereParentId($product->id)->get();
        $selectedArr = [];
        foreach ($selectedProducts as $item) {
            $selectedArr[] = $item->product_id;
        }
//        dd($selectedArr);

        $rewards = ProductsRewards::getList();
        $product_rewards = ProductRelReward::whereProductId($product->id)->get();

        view()->share('jsValidator', \JsValidator::make($product->rules()));
//        dd($product);
        return view('admin.catalog.products.update', [
            'product' => $product,
            'rewards' => $rewards,
            'colors' => $colorsArr,
            'product_reward' => $product_rewards,
            'sweetness' => $sweetnessArr,
            'types' => $typeArr,
            'kind' => $kindArr,
            'alcohol' => $alcoholArr,
            'sort' => $sortArr,
            'sugar' => $sugarArr,
            'brands' => $brandsArr,
            'lines' => $linesArr,
            'compilation' => $compilationArr,
            'selectedSorts' => $selectedSorts,
            'selectedCompilations' => $selectedCompilations,
            'products' => $products,
            'selected' => $selectedArr,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Product $product)
    {
        if ($product->is_avilable == 0 && $request->input('is_avilable') == 1) {
            $waits = ProductsWait::getList(0, $product->id);
            foreach ($waits as $wait) {
                Notify::send(new ProductIsset($product, $wait->user->email));
            }
        }

        $brandId = $request->input('brand_id');
        $countryId = null;
        $providerId = null;

        if (isset($brandId)) {
            $brand = ShopBrand::find($brandId);
            if ($brand->shopProvider) {
                if ($brand->shopProvider->shopCountry) {
                    $countryId = $brand->shopProvider->shopCountry->id;
                }
                $providerId = $brand->shopProvider->id;
            }
        }
        foreach ($request->input() as $key => $value) {
            if ($value == '0' and $key != 'is_avilable' and $key != 'status') {
                $request->merge([$key=> null]);
            }
        }

        $this->validate($request, $product->rules());
        $product->uploadImage();
        $product->slug = str_slug($product->slug);
        $product->is_avilable = $request->input('is_avilable');
        $product->country_id = $countryId;
        $product->provider_id = $providerId;
        
        if ($request->input('is_accessory') == '1') {
            $request->merge([
                'color_id' => null,
                'sweetness_id' => null,
                'type_id' => null,
                'kind_id' => null,
                'alcohol_id' => null,
                'sugar_id' => null,
            ]);
        }
        
        $saved = $product->updateRow($request);
        
        Alert::afterUpdating($saved);
        if ($saved) {
            $existedImgs = ProductsImage::whereProductId($product->id)->get();

            if (sizeof($existedImgs) == 0){
                $productImage = new ProductsImage();
                $productImage->uploadImage();
                $productImage->product_id = $product->id;
                $productImage->status = 1;
                $productImage->position = 0;
                $productImage->save();
            }


            if ($request->input('sort_id') && sizeof($request->input('sort_id')) > 0) {
                $productSorts = ProductSort::whereProductId($product->id)->get();
                foreach ($productSorts as $productSort) {
                    $productSort->delete();
                }

                foreach ($request->input('sort_id') as $sort) {
                    $sortValueNew = new ProductSort();
                    $sortValueNew->product_id = $product->id;
                    $sortValueNew->sort_id = $sort;
                    $sortValueNew->save();
                }
            }
            $compilation = $request->input('compilation_id');
            if (!empty($compilation)){
                $productRelCompilation = ProductRelCompilation::whereProductId($product->id)->get();
                foreach ($productRelCompilation as $compilation) {
                    $compilation->delete();
                }

                foreach ($request->input('compilation_id') as $compilation) {
                    $compilationValueNew = new ProductRelCompilation();
                    $compilationValueNew->product_id = $product->id;
                    $compilationValueNew->compilation_id = $compilation;
                    $compilationValueNew->save();
                }
            }
            $presentation = ProductsPresentation::whereProductId($product->id)->first();
            if (!isset($presentation)){
                $presentation = new ProductsPresentation();
            }
//            $presentation->current->color = $request->input('colorPresent');
//            $presentation->current->aroma = $request->input('aromaPresent');
//            $presentation->current->taste = $request->input('tastePresent');
//            $presentation->current->combination = $request->input('combinationPresent');
//            $presentation->current->content = $request->input('contentPresent');

            $presentation->product_id = $product->id;
            $presentation->updateRow($request);

            $productsArr = $request->input('products_recomm', []);
            if ( $productsArr && sizeof($productsArr) > 0) {
                $prodRecomms = ProductsRecommendation::whereParentId($product->id)->get();
                if (sizeof($prodRecomms) > 0) {
                    foreach ($prodRecomms as $item) {
                        $item->delete();
                    }
                }
                foreach ($productsArr as $value) {
                    $newArticleProduct = new ProductsRecommendation();
                    $newArticleProduct->parent_id = $product->id;
                    $newArticleProduct->product_id = $value;
                    $newArticleProduct->save();
                }
            }

            return $this->redirectUserTo('product', $product->id);
        }
        return redirect(route('admin.products.edit', ['product' => $product->id]))->withInput();
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        Alert::afterDeleting($product->delete());
        return redirect(route('admin.products.index'));
    }

    public function excel()
    {
        $products = Product::get();
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Артикул')
        ->setCellValue('B1', 'Название')
        ->setCellValue('C1', 'Год')
        ->setCellValue('D1', 'Цена');
        $i = 1;
        foreach ($products as $product) {
            if(count($product->prices))
            {
                foreach ($product->prices as $price) {
                    $i++;
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValueExplicit('A' . $i, $price->artikul, \PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValue('B' . $i, $product->current->name)
                        ->setCellValue('C' . $i, $price->year)
                        ->setCellValue('D' . $i, $price->price);
                }
            }
        }
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'. date('d.m.Y') . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }
    
    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteImage(Request $request, $id)
    {
        /** @var Product $news */
        $product = Product::findOrFail($id);
        Alert::afterImageDeleting($product->deleteImage()->save());
        return redirect(route('admin.products.edit', ['product' => $product->id]));
    }
}
