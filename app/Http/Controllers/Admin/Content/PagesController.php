<?php

namespace App\Http\Controllers\Admin\Content;

use App\Helpers\Alert;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class PagesController extends Controller
{

    protected $pageName = 'Текстовые страницы';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::getList();
        return view('admin.content.pages.index', [
            'pages' => $pages,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new Page())->rules()));
        return view('admin.content.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $page = new Page();
        $this->validate($request, $page->rules());
        $page->slug = str_slug($page->slug);
        $saved = $page->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('pages', $page->id);
        }
        return redirect(route('admin.pages.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        view()->share('jsValidator', \JsValidator::make($page->rules()));
        return view('admin.content.pages.update', ['page' => $page]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Page $page)
    {
        $this->validate($request, $page->rules());
        $page->slug = str_slug($page->slug);
        $saved = $page->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('pages', $page->id);
        }
        return redirect(route('admin.pages.edit', ['page' => $page->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        Alert::afterDeleting($page->delete());
        return redirect(route('admin.pages.index'));
    }
}
