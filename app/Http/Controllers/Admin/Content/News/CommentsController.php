<?php

namespace App\Http\Controllers\Admin\Content\News;

use App\Helpers\Alert;
use App\Models\Article;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class CommentsController extends Controller
{

    protected $pageName = 'Отзывы о новостях';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $id) {
        $comments = Comment::getList($this->rowsCount,0,$idArticle = $id->input('idArticle'), $idUser = $id->input('idUser'));
        return view('admin.content.news.comments.index', [
            'comments' => $comments,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $articles = Article::getList(100, 0);
        $users = User::all();
        $articlesArr = [];
        $usersArr = [];
        $usersArr[0] = '';
        foreach ($articles as $article){
            $articlesArr[$article->id] = $article->name;
        }
        foreach ($users as $user){
            $usersArr[$user->id] = $user->second_name . ' ' . $user->first_name;
        }
        view()->share('jsValidator', \JsValidator::make((new Comment())->rules()));
        return view('admin.content.news.comments.create', ['articles' => $articlesArr, 'users' => $usersArr]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $comments = new Comment();
        $this->validate($request, $comments->rules());
        $comments->fill($request->input());
        $comments->position = 0;
        $comments->type = 0;
        $comments->created_at = $request->input('created_at');
        if ($request->input('user_id') == 0) {
            $comments->user_id = null;
        }
        $saved = $comments->save();
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('news-comments', $comments->id);
        }
        return redirect(route('admin.news-comments.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $newsComment) {
        $article = Article::find($newsComment->article_id);
        $user = User::find($newsComment->user_id);
        view()->share('jsValidator', \JsValidator::make($newsComment->rules()));
        return view('admin.content.news.comments.update', ['comments' => $newsComment, 'article' => $article, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $newsComment) {
        $this->validate($request, $newsComment->rules());
        $newsComment->fill($request->input());
        $newsComment->created_at = $request->input('created_at');
        $newsComment->type = 0;
        $saved = $newsComment->save();
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('news-comments', $newsComment->id);
        }
        return redirect(route('admin.news-comments.edit', ['comments' => $newsComment->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $newsComment) {
        Alert::afterDeleting($newsComment->delete());
        return redirect(route('admin.news-comments.index'));
    }
}
