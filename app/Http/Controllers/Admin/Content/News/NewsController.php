<?php namespace App\Http\Controllers\Admin\Content\News;

use App\Helpers\Alert;
use App\Models\Article;
use App\Models\ArticlesCategory;
use App\Models\ArticlesProduct;
use App\Models\Catalog\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class NewsController extends Controller
{

    protected $pageName = 'Новости';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = Article::getList($this->rowsCount, 0);

        $categories = ArticlesCategory::whereType(0)->where('status', 1)->get();
        $categoriesArr = [];
        foreach ($categories as $category) {
            $categoriesArr[$category->id] = $category->current->name;
        }
        $categories = $categoriesArr;

        return view('admin.content.news.item.index', [
            'news' => $news,
            'categories' => $categories,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = ArticlesCategory::whereType(0)->where('status', 1)->get();
        $categoriesArr = [];
        foreach ($categories as $category) {
            $categoriesArr[$category->id] = $category->current->name;
        }
        $categories = $categoriesArr;
        $products = (new Product)->getList(10000, 1, 1);
        $productsArr = [];
        foreach ($products as $product) {
            $productsArr[$product->id] = $product->current->name;
        }
        $products = $productsArr;
        view()->share('jsValidator', \JsValidator::make((new Article())->rules()));
        return view('admin.content.news.item.create', [
            'categories' => $categories,
            'products' => $products,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
    public function store(Request $request)
    {
        $news = new Article();
        $this->validate($request, $news->rules());
        $news->uploadImage();

		if ($request->input('published_at') > Carbon::now()->format('Y-m-d H:i:s') && (int) $request->input('status') !== 0) {
			$news->status = 2;
		} elseif((int) $request->input('status') !== 0) {
			$news->status = 1;
		} else {
			$news->status = 0;
		}
        $news->slug = str_slug($news->slug);
        $saved = $news->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            $productsArr = $request->input('products');
            if (sizeof($productsArr) > 0) {
                $articleProducts = ArticlesProduct::whereArticleId($news->id)->get();
                if (sizeof($articleProducts) > 0) {
                    foreach ($articleProducts as $item) {
                        $item->delete();
                    }
                }
                foreach ($productsArr as $value) {
                    $newArticleProduct = new ArticlesProduct();
                    $newArticleProduct->article_id = $news->id;
                    $newArticleProduct->product_id = $value;
                    $newArticleProduct->save();
                }
            }
            return $this->redirectUserTo('news', $news->id);
        }
        return redirect(route('admin.news.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article $news
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $news)
    {
        //dd($news);
        view()->share('jsValidator', \JsValidator::make($news->rules()));
        $categories = ArticlesCategory::whereType(0)->where('status', 1)->get();
        $categoriesArr = [];
        foreach ($categories as $category) {
            $categoriesArr[$category->id] = $category->current->name;
        }
        $categories = $categoriesArr;
        $products = (new Product)->getList(10000, 1, 1);
        $productsArr = [];
        foreach ($products as $product) {
            $productsArr[$product->id] = $product->current->name;
        }
        $products = $productsArr;
        $selectedProducts = ArticlesProduct::whereArticleId($news->id)->get();
        $selectedArr = [];
        $i = 0;
        foreach ($selectedProducts as $item) {
            $selectedArr[$i] = $item->product_id;
            $i++;
        }
        return view('admin.content.news.item.update', [
            'news' => $news,
            'categories' => $categories,
            'products' => $products,
            'selected' => $selectedArr
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\News $news
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Article $news)
    {
        $this->validate($request, $news->rules());
		if ($request->input('published_at') > Carbon::now()->format('Y-m-d H:i:s') && (int) $request->input('status') !== 0) {
			$news->status = 2;
		} elseif((int) $request->input('status') !== 0) {
			$news->status = 1;
		} else {
			$news->status = 0;
		}
        $news->slug = str_slug($news->slug);
        $saved = $news->updateRow($request);
        $news->uploadImage();
        Alert::afterUpdating($saved);
        if ($saved) {
            $productsArr = $request->input('products');
            if (sizeof($productsArr) > 0) {
                $articleProducts = ArticlesProduct::whereArticleId($news->id)->get();
                if (sizeof($articleProducts) > 0) {
                    foreach ($articleProducts as $item) {
                        $item->delete();
                    }
                }
                foreach ($productsArr as $value) {
                    $newArticleProduct = new ArticlesProduct();
                    $newArticleProduct->article_id = $news->id;
                    $newArticleProduct->product_id = $value;
                    $newArticleProduct->save();
                }
            }
            return $this->redirectUserTo('news', $news->id);
        }
        return redirect(route('admin.news.edit', ['news' => $news->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $news)
    {
        $news->deleteImage();
        Alert::afterDeleting($news->delete());
        return redirect(route('admin.news.index'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteImage(Request $request, $id)
    {
        /** @var News $news */
        $news = Article::findOrFail($id);
        Alert::afterImageDeleting($news->deleteImage()->save());
        return redirect(route('admin.news.edit', ['news' => $news->id]));
    }
}
