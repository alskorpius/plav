<?php namespace App\Http\Controllers\Admin\Content\News;

use App\Helpers\Alert;
use App\Models\Article;
use App\Models\ArticlesCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class CategoriesController extends Controller
{

    protected $pageName = 'Категории';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $categories = ArticlesCategory::getList($this->rowsCount, 0);
        return view('admin.content.news.categories.index', [
            'categories' => $categories,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        view()->share('jsValidator', \JsValidator::make((new ArticlesCategory())->rules()));
        return view('admin.content.news.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $categories = new ArticlesCategory();
        $this->validate($request, $categories->rules());
        $categories->slug = str_slug($categories->slug);
        $categories->type = 0;
        $categories->menu = $request->input('menu');
        $saved = $categories->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('news-categories', $categories->id);
        }
        return redirect(route('admin.news-categories.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ArticlesCategory $category
     * @return \Illuminate\Http\Response
     */
    public function edit(ArticlesCategory $newsCategory) {
        view()->share('jsValidator', \JsValidator::make($newsCategory->rules()));
        return view('admin.content.news.categories.update', ['categories' => $newsCategory]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\ArticlesCategory $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArticlesCategory $newsCategory) {
        $this->validate($request, $newsCategory->rules());
        $newsCategory->slug = str_slug($newsCategory->slug);
        $newsCategory->type = 0;
        $newsCategory->menu = $request->input('menu');
        $saved = $newsCategory->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('news-categories', $newsCategory->id);
        }
        return redirect(route('admin.news-categories.edit', ['categories' => $newsCategory->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArticlesCategory $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArticlesCategory $newsCategory) {
        Alert::afterDeleting($newsCategory->delete());
        return redirect(route('admin.news-categories.index'));
    }
}
