<?php

namespace App\Http\Controllers\Admin\Content\Articles;

use App\Helpers\Alert;
use App\Models\Article;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class CommentsController extends Controller
{

    protected $pageName = 'Отзывы о статьи';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $id)
    {
        $comments = Comment::getList($this->rowsCount,1, $idArticle = $id->input('idArticle'), $idUser = $id->input('idUser'));
        return view('admin.content.articles.comments.index', [
            'comments' => $comments,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $articles = Article::getList(100, 1);
        $users = User::all();
        $articlesArr = [];
        $usersArr = [];
        $usersArr[0] = '';
        foreach ($articles as $article) {
            $articlesArr[$article->id] = $article->current->name;
        }
        foreach ($users as $user) {
            $usersArr[$user->id] = $user->second_name . ' ' . $user->first_name;
        }
        view()->share('jsValidator', \JsValidator::make((new Comment())->rules()));
        return view('admin.content.articles.comments.create', ['articles' => $articlesArr, 'users' => $usersArr]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comments = new Comment();
        $this->validate($request, $comments->rules());
        $comments->fill($request->input());
        $comments->position = 0;
        $comments->type = 1;
        $comments->created_at = $request->input('created_at');
        if ($request->input('user_id') == 0) {
            $comments->user_id = null;
        }
        $saved = $comments->save();
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('articles-comments', $comments->id);
        }
        return redirect(route('admin.articles-comments.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $articlesComment)
    {
        $article = Article::find($articlesComment->article_id);
        $user = User::find($articlesComment->user_id);
        view()->share('jsValidator', \JsValidator::make($articlesComment->rules()));
        return view('admin.content.articles.comments.update', ['comments' => $articlesComment, 'article' => $article, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $articlesComment)
    {
        $this->validate($request, $articlesComment->rules());
        $articlesComment->fill($request->input());
        $articlesComment->created_at = $request->input('created_at');
        $articlesComment->type = 1;
        $articlesComment->status = $request->input('status');
        $saved = $articlesComment->save();
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('articles-comments', $articlesComment->id);
        }
        return redirect(route('admin.articles-comments.edit', ['comments' => $articlesComment->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $articlesComment)
    {
        Alert::afterDeleting($articlesComment->delete());
        return redirect(route('admin.articles-comments.index'));
    }
}
