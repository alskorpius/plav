<?php namespace App\Http\Controllers\Admin\Content\Articles;

use App\Helpers\Alert;
use App\Models\Article;
use App\Models\ArticlesCategory;
use App\Models\ArticlesProduct;
use App\Models\Catalog\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class ArticlesController extends Controller
{

    protected $pageName = 'Статьи';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $news = Article::getList($this->rowsCount, 1);

        $categories = ArticlesCategory::whereType(1)->where('status', 1)->get();
        $categoriesArr = [];
        foreach ($categories as $category) {
            $categoriesArr[$category->id] = $category->current->name;
        }
        $categories = $categoriesArr;

        return view('admin.content.articles.item.index', [
            'news' => $news,
            'categories' => $categories,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = ArticlesCategory::whereType(1)->where('status', 1)->get();
        $categoriesArr = [];
        foreach ($categories as $category) {
            $categoriesArr[$category->id] = $category->current->name;
        }
        $categories = $categoriesArr;

        $products = (new Product)->getList(10000, 1, 1);
        $productsArr = [];
        foreach ($products as $product) {
            $productsArr[$product->id] = $product->current->name;
        }
        $products = $productsArr;
        view()->share('jsValidator', \JsValidator::make((new Article())->rules()));
        return view('admin.content.articles.item.create', [
            'categories' => $categories,
            'products' => $products,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $news = new Article();
        $this->validate($request, $news->rules());
        $news->uploadImage();

        if ($request->input('published_at') > Carbon::now()->format('Y-m-d H:i:s') && (int)$request->input('status') !== 0) {
            $news->status = 2;
        } elseif ((int)$request->input('status') !== 0) {
            $news->status = 1;
        } else {
            $news->status = 0;
        }

        $news->slug = str_slug($news->slug);
        $news->type = 1;
        $saved = $news->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            $productsArr = $request->input('products');
            if (sizeof($productsArr) > 0) {
                $articleProducts = ArticlesProduct::whereArticleId($news->id)->get();
                if (sizeof($articleProducts) > 0) {
                    foreach ($articleProducts as $item) {
                        $item->delete();
                    }
                }
                foreach ($productsArr as $value) {
                    $newArticleProduct = new ArticlesProduct();
                    $newArticleProduct->article_id = $news->id;
                    $newArticleProduct->product_id = $value;
                    $newArticleProduct->save();
                }
            }
            return $this->redirectUserTo('articles', $news->id);
        }
        return redirect(route('admin.articles.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article $news
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        view()->share('jsValidator', \JsValidator::make($article->rules()));
        $categories = ArticlesCategory::whereType(1)->where('status', 1)->get();
        $categoriesArr = [];
        foreach ($categories as $category) {
            $categoriesArr[$category->id] = $category->current->name;
        }
        $categories = $categoriesArr;
        $products = (new Product)->getList(10000, 1, 1);
        $productsArr = [];
        foreach ($products as $product) {
            $productsArr[$product->id] = $product->current->name;
        }
        $products = $productsArr;
        $selectedProducts = ArticlesProduct::whereArticleId($article->id)->get();
        $selectedArr = [];
        $i = 0;
        foreach ($selectedProducts as $item) {
            $selectedArr[$i] = $item->product_id;
            $i++;
        }
        return view('admin.content.articles.item.update', [
            'news' => $article,
            'categories' => $categories,
            'products' => $products,
            'selected' => $selectedArr
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\News $news
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Article $article)
    {

        $this->validate($request, $article->rules());
        $article->uploadImage();

        if ($request->input('published_at') > Carbon::now()->format('Y-m-d H:i:s') && (int)$request->input('status') !== 0) {
            $article->status = 2;
        } elseif ((int)$request->input('status') !== 0) {
            $article->status = 1;
        } else {
            $article->status = 0;
        }
        $article->slug = str_slug($article->slug);
        $article->type = 1;
        $saved = $article->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            $productsArr = $request->input('products');
            if (sizeof($productsArr) > 0) {
                $articleProducts = ArticlesProduct::whereArticleId($article->id)->get();
                if (sizeof($articleProducts) > 0) {
                    foreach ($articleProducts as $item) {
                        $item->delete();
                    }
                }
                foreach ($productsArr as $value) {
                    $newArticleProduct = new ArticlesProduct();
                    $newArticleProduct->article_id = $article->id;
                    $newArticleProduct->product_id = $value;
                    $newArticleProduct->save();
                }
            }
            return $this->redirectUserTo('articles', $article->id);
        }
        return redirect(route('admin.articles.edit', ['news' => $article->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->deleteImage();
        Alert::afterDeleting($article->delete());
        return redirect(route('admin.articles.index'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteImage(Request $request, $id)
    {
        /** @var News $news */
        $news = Article::findOrFail($id);
        Alert::afterImageDeleting($news->deleteImage()->save());
        return redirect(route('admin.articles.edit', ['news' => $news->id]));
    }
}
