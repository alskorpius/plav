<?php namespace App\Http\Controllers\Admin\Content\Articles;

use App\Helpers\Alert;
use App\Models\ArticlesCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class CategoriesController extends Controller
{

    protected $pageName = 'Категории';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $categories = ArticlesCategory::getList($this->rowsCount, 1);
        return view('admin.content.articles.categories.index', [
            'categories' => $categories,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        view()->share('jsValidator', \JsValidator::make((new ArticlesCategory())->rules()));
        return view('admin.content.articles.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $categories = new ArticlesCategory();
        $this->validate($request, $categories->rules());
        $categories->slug = str_slug($categories->slug);
        $categories->type = 1;
        $categories->menu = $request->input('menu');
        $saved = $categories->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('articles-categories', $categories->id);
        }
        return redirect(route('admin.articles-categories.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ArticlesCategory $category
     * @return \Illuminate\Http\Response
     */
    public function edit(ArticlesCategory $articlesCategory) {
        view()->share('jsValidator', \JsValidator::make($articlesCategory->rules()));
        return view('admin.content.articles.categories.update', ['categories' => $articlesCategory]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\ArticlesCategory $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArticlesCategory $articlesCategory) {
        $this->validate($request, $articlesCategory->rules());
        $articlesCategory->slug = str_slug($articlesCategory->slug);
        $articlesCategory->type = 1;
        $articlesCategory->menu = $request->input('menu');
        $saved = $articlesCategory->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('articles-categories', $articlesCategory->id);
        }
        return redirect(route('admin.articles-categories.edit', ['categories' => $articlesCategory->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArticlesCategory $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArticlesCategory $articlesCategory) {
        Alert::afterDeleting($articlesCategory->delete());
        return redirect(route('admin.articles-categories.index'));
    }
}
