<?php

namespace App\Http\Controllers\Admin\Content;

use App\Helpers\Alert;
use App\Http\Controllers\Admin\Controller;
use App\Models\SystemPage;
use Illuminate\Http\Request;

class SystemPagesController extends Controller
{

    protected $pageName = 'Системные страницы';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = (new SystemPage)->getList($this->rowsCount);
        return view('admin.content.system-pages.index', ['pages' => $pages]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SystemPage  $systemPage
     * @return \Illuminate\Http\Response
     */
    public function edit(SystemPage $systemPage)
    {
        view()->share('jsValidator', \JsValidator::make($systemPage->rules()));
        return view('admin.content.system-pages.update', ['page' => $systemPage]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SystemPage  $systemPage
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, SystemPage $systemPage)
    {
        $systemPage->uploadImage();
        $systemPage->settings = $request->input('SETTINGS', []);
        $saved = $systemPage->updateRow($request);

        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('system-pages', $systemPage->id);
        }
        return redirect(route('admin.system-pages.edit', ['news' => $systemPage->id]))->withInput();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteImage(Request $request, $id) {
        /** @var SystemPage $systemPage */
        $systemPage = SystemPage::findOrFail($id);
        Alert::afterImageDeleting($systemPage->deleteImage()->save());
        return redirect(route('admin.system-pages.edit', ['news' => $systemPage->id]));
    }
}
