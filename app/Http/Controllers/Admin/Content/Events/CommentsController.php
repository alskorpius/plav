<?php

namespace App\Http\Controllers\Admin\Content\Events;

use App\Helpers\Alert;
use App\Models\Article;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class CommentsController extends Controller
{

    protected $pageName = 'Комментарии';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $id) {
        $comments = Comment::getList(2,$idArticle = $id->input('idArticle'), $idUser = $id->input('idUser'));
        return view('admin.content.events.comments.index', [
            'comments' => $comments,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $articles = Article::getList(100, 2);
        $users = User::all();
        $articlesArr = [];
        $usersArr = [];
        $usersArr[0] = '';
        foreach ($articles as $article){
            $articlesArr[$article->id] = $article->name;
        }
        foreach ($users as $user){
            $usersArr[$user->id] = $user->second_name . ' ' . $user->first_name;
        }
        view()->share('jsValidator', \JsValidator::make((new Comment())->rules()));
        return view('admin.content.events.comments.create', ['articles' => $articlesArr, 'users' => $usersArr]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $comments = new Comment();
        $this->validate($request, $comments->rules());
        $comments->fill($request->input());
        $comments->position = 0;
        $comments->type = 2;
		if ($request->input('h1') == null){
			$comments->h1 = $request->input('name');
		}
        $comments->created_at = $request->input('created_at');
        if ($request->input('user_id') == 0) {
            $comments->user_id = null;
        }
        $saved = $comments->save();
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('events-comments', $comments->id);
        }
        return redirect(route('admin.events-comments.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $eventsComment) {
        $article = Article::find($eventsComment->article_id);
        $user = User::find($eventsComment->user_id);
        view()->share('jsValidator', \JsValidator::make($eventsComment->rules()));
        return view('admin.content.events.comments.update', ['comments' => $eventsComment, 'article' => $article, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $eventsComment) {
        $this->validate($request, $eventsComment->rules());
        $eventsComment->fill($request->input());
        $eventsComment->created_at = $request->input('created_at');
        $eventsComment->type = 2;
		if ($request->input('h1') == null){
			$eventsComment->h1 = $request->input('name');
		}
        $saved = $eventsComment->save();
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('events-comments', $eventsComment->id);
        }
        return redirect(route('admin.events-comments.edit', ['comments' => $eventsComment->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $eventsComment) {
        Alert::afterDeleting($eventsComment->delete());
        return redirect(route('admin.events-comments.index'));
    }
}
