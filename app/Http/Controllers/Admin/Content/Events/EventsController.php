<?php namespace App\Http\Controllers\Admin\Content\Events;

use App\Helpers\Alert;
use App\Models\Article;
use App\Models\ArticlesCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class EventsController extends Controller
{

    protected $pageName = 'Мероприятия';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = Article::getList($this->rowsCount, 2);

        $categories = ArticlesCategory::whereType(2)->where('status', 1)->get();
        $categoriesArr = [];
        foreach ($categories as $category) {
            $categoriesArr[$category->id] = $category->name;
        }
        $categories = $categoriesArr;

        return view('admin.content.events.item.index', [
            'news' => $news,
            'categories' => $categories,

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = ArticlesCategory::whereType(2)->where('status', 1)->get();
        $categoriesArr = [];
        foreach ($categories as $category) {
            $categoriesArr[$category->id] = $category->current->name;
        }
        $categories = $categoriesArr;
        view()->share('jsValidator', \JsValidator::make((new Article())->rules()));
        return view('admin.content.events.item.create', [
            'categories' => $categories,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $news = new Article();
        $this->validate($request, $news->rules());
        $news->uploadImage();

		if ($request->input('published_at') > Carbon::now()->format('Y-m-d H:i:s') && (int) $request->input('status') !== 0) {
			$news->status = 2;
		} elseif((int) $request->input('status') !== 0) {
			$news->status = 1;
		} else {
			$news->status = 0;
		}
        $news->slug = str_slug($news->slug);
        $news->type = 2;
        $saved = $news->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('events', $news->id);
        }
        return redirect(route('admin.events.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article $news
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $event)
    {
        //dd($news);
        view()->share('jsValidator', \JsValidator::make($event->rules()));
        $categories = ArticlesCategory::whereType(2)->where('status', 1)->get();
        $categoriesArr = [];
        foreach ($categories as $category) {
            $categoriesArr[$category->id] = $category->current->name;
        }
        $categories = $categoriesArr;
        return view('admin.content.events.item.update', ['news' => $event, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
    public function update(Request $request, Article $event)
    {
        $this->validate($request, $event->rules());
        $event->uploadImage();

		if ($request->input('published_at') > Carbon::now()->format('Y-m-d H:i:s') && (int) $request->input('status') !== 0) {
			$event->status = 2;
		} elseif((int) $request->input('status') !== 0) {
			$event->status = 1;
		} else {
			$event->status = 0;
		}
        $event->slug = str_slug($event->slug);
        $event->type = 2;
        $event->published_at = $request->input('published_at');
        $saved = $event->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('events', $event->id);
        }
        return redirect(route('admin.events.edit', ['news' => $event->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $event)
    {
        $event->deleteImage();
        Alert::afterDeleting($event->delete());
        return redirect(route('admin.events.index'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteImage(Request $request, $id)
    {
        /** @var Article $event */
        $event = Article::findOrFail($id);
        Alert::afterImageDeleting($event->deleteImage()->save());
        return redirect(route('admin.events.edit', ['news' => $event->id]));
    }
}
