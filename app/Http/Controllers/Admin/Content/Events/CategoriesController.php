<?php namespace App\Http\Controllers\Admin\Content\Events;

use App\Helpers\Alert;
use App\Models\Article;
use App\Models\ArticlesCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class CategoriesController extends Controller
{

    protected $pageName = 'Категории';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $categories = ArticlesCategory::getList($this->rowsCount, 2);
        return view('admin.content.events.categories.index', [
            'categories' => $categories,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        view()->share('jsValidator', \JsValidator::make((new ArticlesCategory())->rules()));
        return view('admin.content.events.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $categories = new ArticlesCategory();
        $this->validate($request, $categories->rules());
        $categories->slug = str_slug($categories->slug);
        $categories->type = 2;
        $categories->menu = $request->input('menu');
        $saved = $categories->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('events-categories', $categories->id);
        }
        return redirect(route('admin.events-categories.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ArticlesCategory $category
     * @return \Illuminate\Http\Response
     */
    public function edit(ArticlesCategory $eventsCategory) {
        view()->share('jsValidator', \JsValidator::make($eventsCategory->rules()));
        return view('admin.content.events.categories.update', ['categories' => $eventsCategory]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\ArticlesCategory $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArticlesCategory $eventsCategory) {
        $this->validate($request, $eventsCategory->rules());
        $eventsCategory->slug = str_slug($eventsCategory->slug);
        $eventsCategory->type = 2;
        $eventsCategory->menu = $request->input('menu');
        $saved = $eventsCategory->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('events-categories', $eventsCategory->id);
        }
        return redirect(route('admin.events-categories.edit', ['categories' => $eventsCategory->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArticlesCategory $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArticlesCategory $eventsCategory) {
        $articles = Article::whereArticlesCategoriesId($eventsCategory->id)->get();
        foreach ($articles as $article) {
            $article->articles_categories_id = null;
            $article->save();
        }
        Alert::afterDeleting($eventsCategory->delete());
        return redirect(route('admin.events-categories.index'));
    }
}
