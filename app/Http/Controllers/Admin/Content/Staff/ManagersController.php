<?php

namespace App\Http\Controllers\Admin\Content\Staff;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;
use App\Models\Staff;
use App\Helpers\Alert;


class ManagersController extends Controller
{

    protected $pageName = 'Менеджеры';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staff = (new Staff())->getList(1);
        return view('admin.staff.managers.index', [
            'staff' => $staff,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new Staff())->rules()));
        return view('admin.staff.managers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $staff = new Staff();
        $this->validate($request, $staff->rules());
        $staff->uploadImage();
        $staff->type = 1;
        $saved = $staff->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('staff-managers', $staff->id);
        }
        return redirect(route('admin.staff-managers.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Staff $staff
     * @return \Illuminate\Http\Response
     */
    public function edit(Staff $staffManager)
    {
        view()->share('jsValidator', \JsValidator::make($staffManager->rules()));
        return view('admin.staff.managers.update', ['staff' => $staffManager]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Staff $staff
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Staff $staffManager)
    {
        $this->validate($request, $staffManager->rules());
        $staffManager->status = $request->input('status');
        $staffManager->uploadImage();
        $staffManager->type = 1;
        $saved = $staffManager->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('staff-managers', $staffManager->id);
        }
        return redirect(route('admin.staff-managers.edit', ['staff' => $staffManager->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Staff $staff
     * @return \Illuminate\Http\Response
     */
    public function destroy(Staff $staffManager)
    {
        $staffManager->deleteImage();
        Alert::afterDeleting($staffManager->delete());
        return redirect(route('admin.staff-managers.index'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteImage(Request $request, $id) {
        /** @var News $news */
        $staff = Staff::findOrFail($id);
        Alert::afterImageDeleting($staff->deleteImage()->save());
        return redirect(route('admin.staff-managers.edit', ['staff' => $staff->id]));
    }
}
