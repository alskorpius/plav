<?php

namespace App\Http\Controllers\Admin\Content\Staff;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;
use App\Models\Staff;
use App\Helpers\Alert;


class ExpertsController extends Controller
{

    protected $pageName = 'Експерты';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staff = (new Staff())->getList(0);
        return view('admin.staff.experts.index', [
            'staff' => $staff,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new Staff())->rules()));
        return view('admin.staff.experts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $staff = new Staff();
        $this->validate($request, $staff->rules());
        $staff->uploadImage();
        $staff->type = 0;
        $saved = $staff->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('staff-experts', $staff->id);
        }
        return redirect(route('admin.staff-experts.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Staff $staffExpert
     * @return \Illuminate\Http\Response
     */
    public function edit(Staff $staffExpert)
    {
        view()->share('jsValidator', \JsValidator::make($staffExpert->rules()));
        return view('admin.staff.experts.update', ['staff' => $staffExpert]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Staff $staffExpert
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Staff $staffExpert)
    {
        $this->validate($request, $staffExpert->rules());
        $staffExpert->status = $request->input('status');
        $staffExpert->uploadImage();
        $staffExpert->type = 0;
        $saved = $staffExpert->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('staff-experts', $staffExpert->id);
        }
        return redirect(route('admin.staff-experts.edit', ['staff' => $staffExpert->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Staff $staffExpert
     * @return \Illuminate\Http\Response
     */
    public function destroy(Staff $staffExpert)
    {
        $staffExpert->deleteImage();
        Alert::afterDeleting($staffExpert->delete());
        return redirect(route('admin.staff-experts.index'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteImage(Request $request, $id) {
        /** @var News $news */
        $staff = Staff::findOrFail($id);
        Alert::afterImageDeleting($staff->deleteImage()->save());
        return redirect(route('admin.staff-experts.edit', ['staff' => $staff->id]));
    }
}
