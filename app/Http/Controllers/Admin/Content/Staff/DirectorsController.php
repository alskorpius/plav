<?php

namespace App\Http\Controllers\Admin\Content\Staff;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;
use App\Models\Staff;
use App\Helpers\Alert;


class DirectorsController extends Controller
{

    protected $pageName = 'Соучередители';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staff = (new Staff())->getList(2);
        return view('admin.staff.directors.index', [
            'staff' => $staff,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new Staff())->rules()));
        return view('admin.staff.directors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $staff = new Staff();
        $this->validate($request, $staff->rules());
        $staff->uploadImage();
        $staff->type = 2;
        $saved = $staff->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('staff-directors', $staff->id);
        }
        return redirect(route('admin.staff-directors.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Staff $staffDirector
     * @return \Illuminate\Http\Response
     */
    public function edit(Staff $staffDirector)
    {
        view()->share('jsValidator', \JsValidator::make($staffDirector->rules()));
        return view('admin.staff.directors.update', ['staff' => $staffDirector]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Staff $staffDirector
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Staff $staffDirector)
    {
        $this->validate($request, $staffDirector->rules());
        $staffDirector->status = $request->input('status');
        $staffDirector->uploadImage();
        $staffDirector->type = 2;
        $saved = $staffDirector->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('staff-directors', $staffDirector->id);
        }
        return redirect(route('admin.staff-directors.edit', ['staff' => $staffDirector->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Staff $staffDirector
     * @return \Illuminate\Http\Response
     */
    public function destroy(Staff $staffDirector)
    {
        $staffDirector->deleteImage();
        Alert::afterDeleting($staffDirector->delete());
        return redirect(route('admin.staff-directors.index'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteImage(Request $request, $id) {
        /** @var News $news */
        $staff = Staff::findOrFail($id);
        Alert::afterImageDeleting($staff->deleteImage()->save());
        return redirect(route('admin.staff-directors.edit', ['staff' => $staff->id]));
    }
}
