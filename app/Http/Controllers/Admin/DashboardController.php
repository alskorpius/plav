<?php namespace App\Http\Controllers\Admin;

use App\Models\Catalog\Product;
use App\Models\Catalog\ProductsReviews;
use App\Models\Catalog\ProductsWait;
use App\Models\Order\Order;
use App\Models\StatisticMy;
use App\Models\User;

class DashboardController extends Controller
{

    public function index()
    {

        return view('admin.dashboard.index', [

        ]);
    }

}
