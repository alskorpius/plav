<?php

namespace App\Http\Controllers\Admin\Users;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class FakeAuthController extends Controller
{
    public function index(Request $request)
    {
        $id = $request->input('id');
        if (Auth::guard('admin')->check()) {
            Auth::guard('admin')->loginUsingId($id);
            return redirect(route('admin.dashboard'));
        }
    }
}
