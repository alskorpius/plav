<?php namespace App\Http\Controllers\Admin\Users;

use App\Notifications\ForgotPassword;
use App\Notifications\Registration;
use Hash;
use App\Helpers\Alert;
use App\Models\User;
use App\Models\UsersRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;
use App\Helpers\Notify;

class ManagersController extends Controller
{

    protected $pageName = 'Список администраторов';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $managers = (new User)->getManagers($this->rowsCount);
        return view('admin.users.managers.index', [
            'managers' => $managers,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleted()
    {
        $managers = (new User)->getManagers($this->rowsCount, true);
        return view('admin.users.managers.index', [
            'managers' => $managers,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = UsersRole::where('alias', '<>', 'user')->get();
        $rolesArr = [];
        foreach ($roles as $role) {
            $rolesArr[$role->alias] = $role->name;
        }
        view()->share('jsValidator', \JsValidator::make((new User())->managerRules()));
        return view('admin.users.managers.create', ['roles' => $rolesArr]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $manager = new User();
        $this->validate($request, $manager->managerRules());
        $manager->fill($request->input());
        $manager->uploadImage();
        $manager->role_alias = $request->input('role_alias');
        $manager->password = Hash::make($request->input('password'));
        $saved = $manager->save();
        Alert::afterCreating($saved);
        if ($saved) {
            //Notify::send(new Registration($manager, $request->input('password')));
            return $this->redirectUserTo('manager', $manager->id);
        }
        return redirect(route('admin.managers.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  integer  $manager
     * @return \Illuminate\Http\Response
     */
    public function edit($manager)
    {
        $roles = UsersRole::where('alias', '<>', 'user')->get();
        $rolesArr = [];
        foreach ($roles as $role) {
            $rolesArr[$role->alias] = $role->name;
        }
        $manager = User::withTrashed()->where('id', $manager)->first();
        view()->share('jsValidator', \JsValidator::make($manager->managerRules()));
        return view('admin.users.managers.update', ['manager' => $manager, 'roles' => $rolesArr]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $manager
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $manager)
    {
        $manager = User::withTrashed()->where('id', $manager)->first();
        $this->validate($request, $manager->managerRules());
        $manager->fill($request->input());
        $manager->uploadImage();
        if ($request->input('password') !== null) {
            if (Hash::check($request->input('password'), $manager->password) === false) {
                //Notify::send(new ForgotPassword($manager, $request->input('password')));
            }
            if ($request->input('password')) {
                $manager->password = Hash::make($request->input('password'));
            }
        }
        $saved = $manager->save();
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('manager', $manager->id);
        }
        return redirect()->route('admin.managers.edit', ['manager' => $manager->id])->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $manager
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $manager)
    {
        if ($manager->isManager() === false) {
            abort(404);
        }
        Alert::afterDeleting($manager->delete());
        return redirect()->route('admin.managers.index');
    }

    /**
     * @param Request $request
     * @param integer $manager
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteAvatar(Request $request, $manager)
    {
        $manager = User::manager()->whereId($manager)->withTrashed()->firstOrFail();
        $manager->deleteImage();
        Alert::afterImageDeleting($manager->save());
        return redirect()->back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        /** @var User $manager */
        $manager = User::getTrashedUser($id);
        if ($manager->isManager() === false) {
            abort(404);
        }
        Alert::afterRestoring($manager->restore());
        return redirect()->route('admin.managers.deleted');
    }

    /**
     * Fully delete the specified resource from storage.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function forceDelete($id)
    {
        /** @var User $manager */
        $manager = User::getTrashedUser($id);
        if ($manager->isManager() === false) {
            abort(404);
        }
        Alert::afterDeleting($manager->forceDelete());
        return redirect()->route('admin.managers.deleted');
    }
}
