<?php namespace App\Http\Controllers\Admin\Users;

use App\Models\Blacklist;
use App\Models\UsersRole;
use App\Notifications\ForgotPassword;
use App\Notifications\Registration;
use Hash;
use App\Helpers\Alert;
use App\Http\Controllers\Admin\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Helpers\Notify;

class CustomersController extends Controller
{

    protected $pageName = 'Список пользователей';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = (new User)->getCustomers($this->rowsCount);
        return view('admin.users.customers.index', [
            'customers' => $customers,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleted()
    {
        $customers = (new User)->getCustomers($this->rowsCount, true);

        return view('admin.users.customers.index', [
            'customers' => $customers,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new User())->customerRules()));
        return view('admin.users.customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $customer = new User();
        $this->validate($request, $customer->customerRules());
        $customer->fill($request->input());
        $customer->role_alias = UsersRole::USER;
        $customer->confirmed = 1;
        $customer->password = Hash::make($request->input('password'));
        $saved = $customer->save();
        Alert::afterCreating($saved);
        if ($saved) {
            //Notify::send(new Registration($customer, $request->input('password')));
            return $this->redirectUserTo('customer', $customer->id);
        }
        return redirect(route('admin.customers.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  integer $customer
     * @return \Illuminate\Http\Response
     */
    public function edit($customer)
    {
        $customer = User::customer()->whereId($customer)->withTrashed()->firstOrFail();
        view()->share('jsValidator', \JsValidator::make($customer->customerRules()));
        return view('admin.users.customers.update', ['customer' => $customer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  integer $customer
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $customer)
    {
        $customer = User::customer()->whereId($customer)->withTrashed()->firstOrFail();
        $this->validate($request, $customer->customerRules());
        $customer->fill($request->input());
		$customer->confirmed = 1;
		if ($request->input('password') !== null) {
            if (Hash::check($request->input('password'), $customer->password) === false) {
                //Notify::send(new ForgotPassword($customer, $request->input('password')));
            }
            if ($request->input('password')) {
                $customer->password = Hash::make($request->input('password'));
            }
        }
        $saved = $customer->save();
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('customer', $customer->id);
        }
        return redirect()->route('admin.customers.edit', ['customer' => $customer->id])->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $customer)
    {
        if ($customer->isCustomer() === false) {
            abort(404);
        }
        Alert::afterDeleting($customer->delete());
        return redirect()->route('admin.customers.index');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        /** @var User $customer */
        $customer = User::getTrashedUser($id);
        if ($customer->isCustomer() === false) {
            abort(404);
        }
        Alert::afterRestoring($customer->restore());
        return redirect()->route('admin.customers.deleted');
    }

    /**
     * Fully delete the specified resource from storage.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function forceDelete($id)
    {
        /** @var User $customer */
        $customer = User::getTrashedUser($id);
        if ($customer->isCustomer() === false) {
            abort(404);
        }
        Alert::afterDeleting($customer->forceDelete());
        return redirect()->route('admin.customers.deleted');
    }

}
