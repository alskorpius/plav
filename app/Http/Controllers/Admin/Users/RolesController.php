<?php

namespace App\Http\Controllers\Admin\Users;

use App\Helpers\Alert;
use App\Models\PagesRole;
use App\Models\UsersRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class RolesController extends Controller
{
    protected $pageName = 'Список ролей';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = UsersRole::where('alias', '<>', UsersRole::ADMIN)->where('alias', '<>', UsersRole::USER)->get();
        return view('admin.users.roles.index', [
            'roles' => $roles,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new UsersRole())->rules()));
        return view('admin.users.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $newRole = new UsersRole();
        $this->validate($request, $newRole->rules());
        $newRole->fill($request->input());
        $saved = $newRole->save();
        Alert::afterCreating($saved);
        if ($saved) {
            for ($i = 1; $i <= 18; $i++) {
                $newPageRole = new PagesRole();
                $newPageRole->page_id = $i;
                $newPageRole->role_id = $newRole->id;
                $newPageRole->status = 1;
                $newPageRole->save();
            }
            return $this->redirectUserTo('role', $newRole->id);
        }
        return redirect(route('admin.roles.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param UsersRole $role
     * @return \Illuminate\Http\Response
     */
    public function edit(UsersRole $role)
    {
        $statusPages = PagesRole::whereRoleId($role->id)->get();
        $statusPagesArr = [];
        foreach ($statusPages as $statusPage) {
            $statusPagesArr[$statusPage->page_id] = $statusPage->status;
        }
        view()->share('jsValidator', \JsValidator::make($role->rules()));
        return view('admin.users.roles.update', ['role' => $role, 'statusPages' => $statusPagesArr]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param UsersRole $role
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, UsersRole $role)
    {
//        dd($request->input());
        $this->validate($request, $role->rules());
        $role->fill($request->input());
        $saved = $role->save();
        Alert::afterUpdating($saved);
        if ($saved) {
            $pagesRoles = PagesRole::whereRoleId($role->id)->get();
            $i = 1;
            foreach ($pagesRoles as $pagesRole) {
                $pagesRole->status = $request->input('status' . $i);
                $pagesRole->save();
                $i++;
            }
            return $this->redirectUserTo('role', $role->id);
        }
        return redirect(route('admin.roles.edit', ['role' => $role->id]))->withInput();
    }

    /**
     * @param UsersRole $role
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(UsersRole $role)
    {
        Alert::afterDeleting($role->delete());
        return redirect(route('admin.roles.index'));
    }
}
