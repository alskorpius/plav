<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently deliver its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    protected $rules = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->redirectTo = route('admin.dashboard');
        $this->middleware('guest')->except('logout');
        $this->rules = [
            $this->username() => 'required|string|email',
            'password' => 'required|string',
        ];
    }

    protected function validateLogin(Request $request)
    {

        $this->validate($request, $this->rules);
    }

    public function showLoginForm(Request $request)
    {
        view()->share('jsValidator', \JsValidator::make($this->rules, [], [], '.login-box-body form'));
        return view('admin.auth.login');
    }

    protected function sendLoginResponse(Request $request)
    {
        if (Auth::user()->isCustomer() || Auth::user()->status === false) {
            Auth::logout();
            return $this->sendFailedLoginResponse($request);
        }
        Auth::user()->lastLoginUpdate();
        $request->session()->regenerate();
        $this->clearLoginAttempts($request);
        return $this->authenticated($request, Auth::user())
            ?: redirect()->intended($this->redirectPath());
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        return redirect($this->redirectTo);
    }

}
