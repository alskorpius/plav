<?php

namespace App\Http\Controllers\Admin\Cards;

use App\Helpers\Alert;
use App\Models\Card\Card;
use App\Models\Card\CardsAddress;
use App\Models\Card\Factor;
use App\Models\City;
use App\Models\Department;
use App\Models\Profession;
use App\Models\Street;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class CardsController extends Controller
{
    protected $pageName = 'Карточки';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cards = (new Card())->getList($this->rowsCount);
        return view('admin.cards.index', [
            'cards' => $cards,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new Card())->rules()));
        $cities = Card::getArrayToSelect((new City), 'name');
        $streets = Card::getArrayToSelect((new Street), 'name');
        $departments = Card::getArrayToSelect((new Department));
        $professions = Card::getArrayToSelect((new Profession));
        $factors = Card::getArrayToSelect((new Factor()));
        return view('admin.cards.create', [
            'cities' => $cities,
            'streets' => $streets,
            'departments' => $departments,
            'professions' => $professions,
            'factors' => $factors,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $card = new Card();
        $isRegistrationAddress = $request->input('is_registration_address');
        $this->validate($request, $card->rules());

        $card->fill($request->input());
        $saved = $card->save();

        Alert::afterCreating($saved);
        if ($saved) {
            $cardsAddress = new CardsAddress();
            $cardsAddress->card_id = $card->id;
            $cardsAddress->city_id = $request->input('registration_address.city_id');
            $cardsAddress->street_id = $request->input('registration_address.street_id');
            $cardsAddress->house_number = $request->input('registration_address.house_number');
            $cardsAddress->apartment_number = $request->input('registration_address.apartment_number');
            $cardsAddress->housing_number = $request->input('registration_address.housing_number');
            $cardsAddress->is_registration_address = 1;
            $cardsAddress->save();
            if(!$isRegistrationAddress) {
                $cardsAddress = new CardsAddress();
                $cardsAddress->card_id = $card->id;
                $cardsAddress->city_id = $request->input('fact_address.city_id');
                $cardsAddress->street_id = $request->input('fact_address.street_id');
                $cardsAddress->house_number = $request->input('fact_address.house_number');
                $cardsAddress->apartment_number = $request->input('fact_address.apartment_number');
                $cardsAddress->housing_number = $request->input('fact_address.housing_number');
                $cardsAddress->is_registration_address = 0;
                $cardsAddress->save();
            }
            return $this->redirectUserTo('cards', $card->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Card  $card
     * @return \Illuminate\Http\Response
     */
    public function edit(Card $card)
    {
        view()->share('jsValidator', \JsValidator::make($card->rules()));
        $cities = Card::getArrayToSelect((new City), 'name');
        $streets = Card::getArrayToSelect((new Street), 'name');
        $departments = Card::getArrayToSelect((new Department));
        $professions = Card::getArrayToSelect((new Profession));
        $factors = Card::getArrayToSelect((new Factor()));

        $selectedAddresses = [];
        if (sizeof($card->addresses) > 0) {
            foreach ($card->addresses as $address) {
                if($address->is_registration_address) {
                    $selectedAddresses['registration'] = $address;
                } else {
                    $selectedAddresses['fact'] = $address;
                }
            }
        } else {
            $selectedAddresses['registration'] = null;
            $selectedAddresses['fact'] = null;
        }

        return view('admin.cards.update', [
            'card' => $card,
            'cities' => $cities,
            'streets' => $streets,
            'departments' => $departments,
            'professions' => $professions,
            'factors' => $factors,
            'selectedAddresses' => $selectedAddresses,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Card  $card
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Card $card)
    {
        $isRegistrationAddress = $request->input('is_registration_address');
        $this->validate($request, $card->rules());
        $card->fill($request->input());
        $saved = $card->save();
        Alert::afterUpdating($saved);
        if ($saved) {
            $cardsAddresses = CardsAddress::whereCardId($card->id)->get();
            foreach ($cardsAddresses as $cardsAddress) {
                $cardsAddress->delete();
            }

            $cardsAddress = new CardsAddress();
            $cardsAddress->card_id = $card->id;
            $cardsAddress->city_id = $request->input('registration_address.city_id');
            $cardsAddress->street_id = $request->input('registration_address.street_id');
            $cardsAddress->house_number = $request->input('registration_address.house_number');
            $cardsAddress->apartment_number = $request->input('registration_address.apartment_number');
            $cardsAddress->housing_number = $request->input('registration_address.housing_number');
            $cardsAddress->is_registration_address = 1;
            $cardsAddress->save();
            if(!$isRegistrationAddress) {
                $cardsAddress = new CardsAddress();
                $cardsAddress->card_id = $card->id;
                $cardsAddress->city_id = $request->input('fact_address.city_id');
                $cardsAddress->street_id = $request->input('fact_address.street_id');
                $cardsAddress->house_number = $request->input('fact_address.house_number');
                $cardsAddress->apartment_number = $request->input('fact_address.apartment_number');
                $cardsAddress->housing_number = $request->input('fact_address.housing_number');
                $cardsAddress->is_registration_address = 0;
                $cardsAddress->save();
            }
            return $this->redirectUserTo('card', $card->id);
        }
        return redirect(route('admin.cards.edit', ['card' => $card->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Card  $card
     * @return \Illuminate\Http\Response
     */
    public function destroy(Card $card)
    {
        Alert::afterDeleting($card->delete());
        return redirect(route('admin.cards.index'));
    }
}
