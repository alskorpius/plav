<?php namespace App\Http\Controllers\Admin;

use App\Helpers\Alert;
use App\Helpers\Files;
use App\Models\User;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProfileController extends Controller
{
    public function index()
    {
        view()->share('jsValidator', \JsValidator::make([
            'first_name' => ['required', 'min:2', 'max:32'],
            'second_name' => ['present', 'max:32'],
            'email' => ['required', 'email', Rule::unique('users')->ignore(Auth::user()->id)],
        ]));
        return view('admin.profile.index');
    }

    public function savePersonalData(Request $request)
    {
        $user = Auth::user();
        $this->validate($request, [
            'first_name' => ['required', 'min:2', 'max:32'],
            'second_name' => ['present', 'max:32'],
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->id)],
        ]);
        $user->first_name = $request->input('first_name');
        $user->second_name = $request->input('second_name');
        $user->email = $request->input('email');
        $user->save();
        return redirect()->route('admin.profile');
    }

    public function changePassword()
    {
        view()->share('jsValidator', \JsValidator::make([
            'current_password' => ['required', 'current_password', 'min:5', 'max:32'],
            'new_password' => ['required', 'different:current_password', 'min:5', 'max:32', 'confirmed'],
        ]));
        return view('admin.profile.change-password');
    }

    public function saveNewPassword(Request $request)
    {
        $user = Auth::user();
        $this->validate($request, [
            'current_password' => ['required', 'current_password', 'min:5', 'max:32'],
            'new_password' => ['required', 'different:current_password', 'min:5', 'max:32', 'confirmed'],
        ]);
        $user->password = Hash::make($request->input('new_password'));
        $user->saveOrFail();
        Alert::afterUpdating();
        return redirect()->back();
    }

    public function changeAvatar()
    {
        view()->share('jsValidator', \JsValidator::make([
            'image' => ['required', 'image', 'max:' . config('custom.image-max-size')],
        ]));
        if (Auth::user()->imageExists('icon')) {
            return view('admin.profile.change-avatar');
        }
        return view('admin.profile.change-avatar-form');
    }

    public function saveNewAvatar(Request $request)
    {
        $user = Auth::user();
        $this->validate($request, [
            'image' => ['required', 'image', 'max:3072'],
        ]);
        $user->uploadImage();
        $user->saveOrFail();
        Alert::afterUpdating();
        return redirect()->back();
    }

    public function deleteAvatar(Request $request)
    {
        $user = Auth::user();
        $user->deleteImage();
        Alert::afterImageDeleting($user->save());
        return redirect()->back();
    }
}
