<?php

namespace App\Http\Controllers\Admin\Map;

use App\Helpers\Alert;
use App\Models\Map\MapRegion;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class RegionsController extends Controller
{
    protected $pageName = 'Регионы';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regions = MapRegion::getList($this->rowsCount);
        return view('admin.map.region.index', [
            'regions' => $regions,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('jsValidator', \JsValidator::make((new MapRegion())->rules()));
        return view('admin.map.region.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $region = new MapRegion();
        $this->validate($request, $region->rules());
        $region->slug = str_slug($region->slug);
        $saved = $region->createRow($request);
        Alert::afterCreating($saved);
        if ($saved) {
            return $this->redirectUserTo('region', $region->id);
        }
        return redirect(route('admin.where-buy-regions.create'))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param MapRegion $where_buy_region
     * @return \Illuminate\Http\Response
     * @internal param \App\Models\Blacklist $blacklist
     */
    public function edit(MapRegion $where_buy_region)
    {
        view()->share('jsValidator', \JsValidator::make($where_buy_region->rules()));
        return view('admin.map.region.update', ['region' => $where_buy_region]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param MapRegion $where_buy_region
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @internal param \App\Models\Blacklist $blacklist
     */
    public function update(Request $request, MapRegion $where_buy_region)
    {
        $where_buy_region->slug = str_slug($where_buy_region->slug);
        $this->validate($request, $where_buy_region->rules());
        $saved = $where_buy_region->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('region', $where_buy_region->id);
        }
        return redirect(route('admin.where-buy-regions.edit', ['region' => $where_buy_region->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param MapRegion $where_buy_region
     * @return \Illuminate\Http\Response
     * @internal param \App\Models\Blacklist $blacklist
     */
    public function destroy(MapRegion $where_buy_region)
    {
        Alert::afterDeleting($where_buy_region->delete());
        return redirect(route('admin.where-buy-regions.index'));
    }
}
