<?php

namespace App\Http\Controllers\Admin\Map;

use App\Helpers\Alert;
use App\Models\Catalog\ProductsLine;
use App\Models\Map\MapProductLine;
use App\Models\Map\MapRegion;
use App\Models\Map\MapSpot;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Controller;

class SpotsController extends Controller
{
    protected $pageName = 'Заведения';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $spots = MapSpot::getList($this->rowsCount);

        $regions = MapRegion::getList(1000);
        $regionsArr = [];
        foreach ($regions as $region) {
            $regionsArr[$region->row_id] = $region->name;
        }
        foreach ($spots as $spot){
            $region = MapRegion::whereId($spot->region_id)->first();
            $spot->region = $region->current->name;
        }
        return view('admin.map.spots.index', [
            'spots' => $spots,
            'regions' => $regionsArr,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $regions = MapRegion::getList(1000);
        $regionsArr = [];
        foreach ($regions as $region) {
            $regionsArr[$region->row_id] = $region->name;
        }
        $lines = ProductsLine::getList(1000, 1);
        $linesArr = [];
        foreach ($lines as $line) {
            $linesArr[$line->id] = $line->current->name;
        }
        view()->share('jsValidator', \JsValidator::make((new MapSpot())->rules()));
        return view('admin.map.spots.create', ['regions' => $regionsArr, 'lines' => $linesArr]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $spot = new MapSpot();
        $this->validate($request, $spot->rules());
        $spot->settings = $request->input('SETTINGS', []);
        $saved = $spot->createRow($request);
        if ($saved) {
            foreach ($request->input('line_id') as $line_id) {
                $mapProductLine = new MapProductLine();
                $mapProductLine->spot_id = $spot->id;
                $mapProductLine->line_id = $line_id;
                $mapProductLine->save();
            }
        }
        Alert::afterUpdating($saved);
        if ($saved) {
            return $this->redirectUserTo('spot', $spot->id);
        }
        return redirect(route('admin.where-buy-spots.edit', ['spot' => $spot->id]))->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param MapSpot $where_buy_spot
     * @return \Illuminate\Http\Response
     */
    public function edit(MapSpot $where_buy_spot)
    {
        $regions = MapRegion::getList(1000);
        $regionsArr = [];
        foreach ($regions as $region) {
            $regionsArr[$region->row_id] = $region->name;
        }
        $lines = ProductsLine::getList(1000, 1);
        $linesArr = [];
        foreach ($lines as $line) {
            $linesArr[$line->id] = $line->current->name;
        }
        $selectedLines = MapProductLine::whereSpotId($where_buy_spot->id)->get();
        $selectedLinesArr = [];
        $i = 0;
        foreach ($selectedLines as $selectedLine) {
            $selectedLinesArr[$i] = $selectedLine->line_id;
            $i++;
        }
        view()->share('jsValidator', \JsValidator::make($where_buy_spot->rules()));
        return view('admin.map.spots.update', ['spot' => $where_buy_spot, 'regions' => $regionsArr, 'lines' => $linesArr, 'selectedLines' => $selectedLinesArr]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param MapSpot $where_buy_spot
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, MapSpot $where_buy_spot)
    {
        $this->validate($request, $where_buy_spot->rules());
        $where_buy_spot->settings = $request->input('SETTINGS', []);
        $where_buy_spot->current->address = $request->input('addressBD');
        $saved = $where_buy_spot->updateRow($request);
        Alert::afterUpdating($saved);
        if ($saved) {

            if (sizeof($request->input('line_id')) > 0) {
                $selectedLines = MapProductLine::whereSpotId($where_buy_spot->id)->get();
                foreach ($selectedLines as $selectedLine) {
                    $selectedLine->delete();
                }
                foreach ($request->input('line_id') as $line_id) {
                    $mapProductLine = new MapProductLine();
                    $mapProductLine->spot_id = $where_buy_spot->id;
                    $mapProductLine->line_id = $line_id;
                    $mapProductLine->save();
                }
            }
            return $this->redirectUserTo('spot', $where_buy_spot->id);
        }
        return redirect(route('admin.where-buy-spots.edit', ['spot' => $where_buy_spot->id]))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param MapSpot $where_buy_spot
     * @return \Illuminate\Http\Response
     */
    public function destroy(MapSpot $where_buy_spot)
    {
        Alert::afterDeleting($where_buy_spot->delete());
        return redirect(route('admin.where-buy-spots.index'));
    }
}
