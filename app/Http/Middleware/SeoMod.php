<?php

namespace App\Http\Middleware;

use App\Helpers\Meta;
use App\Models\SeoRedirect;
use App\Models\SeoScript;
use Closure;

class SeoMod
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Метрики
        if (\Schema::hasTable('seo_scripts')) {
            view()->share('scripts', (new SeoScript)->getListForClientSide());
        }
        // Мета теги наивысшего уровня
        Meta::highLevel();
        // Выдаем результат на экран
        return $next($request);
    }
}
