<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;


class AgeCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (stristr(\Request::server('HTTP_REFERER'), 'facebook')) {
            return $next($request);
        }
        if (Cookie::get('check_age') === null && \Request::route()->getName() !== 'age-check'
            && \Request::route()->getName() !== 'age-check-access'
            && \Request::route()->getName() !== 'age-check-denied'
        ) {
            return redirect(route('age-check'));
        }

        if (Cookie::get('check_age') === 0 && \Request::route()->getName() !== 'age-check-denied') {
            return redirect(route('age-check-denied'));
        }
        Session::put('protectionPassed', true);
        return $next($request);
    }
}
