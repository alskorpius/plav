<?php

namespace App\Http\Middleware;

use Session;
use Closure;

class BotProtection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (request()->method() !== 'PATCH') {
            if (session()->get('protectionPassed')) {
                // protection passed
            } elseif (config('db.security.bot-protection') && \Request::route()->getName() !== 'age-check' && \Request::route()->getName() !== 'age-check-access' && \Request::route()->getName() !== 'age-check-denied') {
                return abort(401);
            }
        }

        return $next($request);
    }
}
