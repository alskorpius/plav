<?php

namespace App\Http\Middleware;

use Closure, Auth, Route;

class Access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $except = ['admin.logout', 'crop'];
        $currentRoute = Route::currentRouteName();
        if (
            Auth::user()->isManager() &&
            $request->route()->getPrefix() !== 'admin/account' &&
            in_array($currentRoute, $except) === false &&
            $this->getAccess(config('aside'), $currentRoute) === false
        ) {
            abort(404);
        }
        return $next($request);
    }

    private function getAccess(array $items, $currentRoute = null)
    {
        $currentRoute = $currentRoute ?: Route::currentRouteName();
        foreach ($items AS $item) {
            if (is_array($item) === false) {
                continue;
            }
            if (array_get($item, 'route') === $currentRoute && array_get($item, 'manager', false) === true) {
                return true;
            }
            if (in_array($currentRoute, array_get($item, 'routes', [])) && array_get($item, 'manager', false) === true) {
                return true;
            }
            if (array_get($item, 'menu', []) && $this->getAccess($item['menu'], $currentRoute) === true || Route::currentRouteName() == 'admin.print-order.index') {
                return true;
            }
        }
        return false;
    }
}
