<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Illuminate\Session\TokenMismatchException;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/admin/ajax/gallery-upload-photos',
        '/admin/ajax/gallery-get-photos',
        '/ulogin/callback',
        '/ulogin/connect',
        '/checkout/check-payment/portmone',
        '/forgot-password',
        '/checkout/check-payment',
        '/checkout/check-payment/portmone',
        '/',
    ];

    public function handle($request, \Closure $next)
    {
        if (
            $this->isReading($request)
            || $this->tokensMatch($request)
            || $this->inExceptArray($request)
            || (request()->server('REQUEST_URI') === '/' && request()->method() === 'PATCH')
        )
        {
            return $this->addCookieToResponse($request, $next($request));
        }

        throw new TokenMismatchException;
    }

}
