<?php

namespace App\Http\Middleware;

use Closure;

class OptimizeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $buffer = $response->getContent();
       /* $buffer = preg_replace('/[\r\n\t]+/', ' ', $buffer);
        $buffer = preg_replace('/[\s]+/', ' ', $buffer);
        $buffer = preg_replace("/\> \</", "><", $buffer);
        $buffer = preg_replace("/\<!--[^\[*?\]].*?--\>/", "", $buffer);*/
        $response->setContent($buffer);
        return $response;
    }
}