<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class News extends Mailable
{
    use Queueable, SerializesModels;

    public  $subject;
    public  $letter;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $letter)
    {
        $this->subject = $subject;
        $this->letter = $letter;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('admin.mail.news')
            ->with(['letter' => $this->letter])
            ->subject($this->subject);
    }
}
