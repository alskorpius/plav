<?php

namespace App\Exceptions;

use App\Models\StatisticMy;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Schema;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Arr;
use Jenssegers\Agent\Agent;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if (Schema::hasTable('kslStatistics')) {
            $agent = new Agent();
            $device = $agent->device() === 'WebKit' ? 'Computer' : $agent->device();
            $platform = $agent->platform() . ' ' . $agent->version($agent->platform());
            $browser = $agent->browser() . ' ' . $agent->version($agent->browser());
            $ip = \Request::ip(); //получаем IP текущего посетителя
//        if($ip == '127.0.0.1') return;

            $count_model = new StatisticMy(); //модель

            $str_url = \URL::full(); //URL текущей страницы c параметрами

            //Проверка на бота
            $bot_name = self::isBot();
            //$bot_name = 'rambler'; //для тестирования

            if(!$bot_name){
                //Проверка в черном списке
                $black = $count_model->inspection_black_list($ip);

                if(!$black && stristr($str_url, 'filemanager') === false && stristr($str_url, 'uploads/images') === false){
                    $count_model->setCount($ip, $str_url, 0, 404, $browser,$platform,$device);
                }
            }
            $records = StatisticMy::all()->where('created_at',  '<', Carbon::now()->subDays(30));

            foreach ($records as $record) {
                $record->delete();
            }
        }

        parent::report($exception);
    }

    //Проверяет, является ли посетитель роботом поисковой системы.
    public static function isBot(&$botname = ''){
        $bots = array(
            'rambler','googlebot','aport','yahoo','msnbot','turtle','mail.ru','omsktele',
            'yetibot','picsearch','sape.bot','sape_context','gigabot','snapbot','alexa.com',
            'megadownload.net','askpeter.info','igde.ru','ask.com','qwartabot','yanga.co.uk',
            'scoutjet','similarpages','oozbot','shrinktheweb.com','aboutusbot','followsite.com',
            'dataparksearch','google-sitemaps','appEngine-google','feedfetcher-google',
            'liveinternet.ru','xml-sitemaps.com','agama','metadatalabs.com','h1.hrn.ru',
            'googlealert.com','seo-rus.com','yaDirectBot','yandeG','yandex',
            'yandexSomething','Copyscape.com','AdsBot-Google','domaintools.com',
            'Nigma.ru','bing.com','dotnetdotcom'
        );
        foreach($bots as $bot)
            if(stripos(Arr::get($_SERVER, 'HTTP_USER_AGENT', ''), $bot) !== false){
                $botname = $bot;
                return $botname;
            }
        return false;
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Exception $exception)
    {
        if (method_exists($exception, 'getStatusCode') && $exception->getStatusCode() > 401 && env('APP_DEBUG') === false) {
            return response()->view('errors.404-custom');
        }
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        if (request()->is('admin')) {
            return redirect()->guest(route('admin.login'));
        }
        return redirect()->guest(route('site.index'));
    }
}
