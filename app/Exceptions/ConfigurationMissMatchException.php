<?php namespace App\Exceptions;

use Throwable, Exception;

/**
 * Class ConfigurationMissMatchException
 * @package App\Exceptions
 */
class ConfigurationMissMatchException extends Exception
{
    public function __construct($message = "Неверная конфигурация", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
