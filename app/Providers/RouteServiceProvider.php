<?php

namespace App\Providers;

use PaginateRoute;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    protected $adminNamespace = 'App\Http\Controllers\Admin';
    protected $webNamespace = 'App\Http\Controllers\Site';
    protected $ajaxNamespace = 'App\Http\Controllers\Admin\Ajax';
    protected $ajaxSiteNamespace = 'App\Http\Controllers\Site\Ajax';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        PaginateRoute::registerMacros();

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
		$this->mapAjaxRoutes();
		$this->mapSiteAjaxRoutes();
		$this->mapAdminRoutes();
		$this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return \Illuminate\Http\RedirectResponse|void
     */
    protected function mapWebRoutes()
    {
        Route::middleware(['web', 'botProtection', 'seoMod'])
            ->namespace($this->webNamespace)
            ->group(base_path('routes/web.php'));
    }

    protected function mapAdminRoutes()
    {
        Route::prefix('admin')
            ->middleware('web')
            ->namespace($this->adminNamespace)
            ->group(base_path('routes/admin.php'));
    }

    protected function mapAjaxRoutes()
    {
        Route::prefix('admin/ajax')
            ->middleware(['web', 'auth'])
            ->namespace($this->ajaxNamespace)
            ->group(base_path('routes/ajax.php'));
    }

    protected function mapSiteAjaxRoutes()
    {
        Route::prefix('/ajax')
            ->middleware(['web'])
            ->namespace($this->ajaxSiteNamespace)
            ->group(base_path('routes/ajaxSite.php'));
    }

}
