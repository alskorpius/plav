<?php

namespace App\Providers;

use App\Models\SeoRedirect;
use App\Models\Settings;
use Auth;
use Form;
use Schema;
use Illuminate\Support\Facades\Config;
use Route;
use Hash;
use Validator;
use Illuminate\Support\ServiceProvider;
use Lang;
use App\Models\Language;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        // /index.php => /
        if (request()->getRequestUri() == '/index.php') {
            redirect(env('APP_URL', 'http://plavsostav.loc'), 301)->send();
        }
        // www to non www
        if (stristr(request()->url(), 'http://www.plav.loc')) {
            redirect(env('APP_URL', 'http://plavsostav.loc'), 301)->send();
        }

        if (strpos(request()->getRequestUri(), '/admin') === 0) {
            Config::set('auth.defaults.guard', 'admin');
        } else {
            Config::set('jsvalidation.form_selector', 'form.form');
        }

        // Включаем связи в ИнноДБ
        Schema::enableForeignKeyConstraints();
        // Устанавливаем максимальную длину varchar во избежание ошибок
        Schema::defaultStringLength(191);
        // Глобальные паттерны для роутинга
        Route::pattern('id', '[0-9]+');
        // Кастомные правила валидации
        Validator::extend('current_password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, Auth::user()->password);
        });
        Validator::extend('phone_number', function ($attribute, $value, $parameters) {
            return (substr($value, 0, 3) == '380' && strlen($value) == 12) || (strlen($value) == 10 && substr($value, 0, 1) == '0') ;
        });

        if (Config::get('app.fallback_locale') == Lang::getLocale()) {
            Config::set('prefix', '');
        } else {
            Config::set('prefix', Lang::getLocale());
        }
        if (Schema::hasTable('settings')) {
            // Делаем список настроек сайта доступными везде
            $settings = Settings::whereStatus(true)->get();
            Config::set('db', []);
            foreach ($settings AS $setting) {
                Config::set('db.' . $setting->group . '.' . $setting->key, $setting->value);
            }
        }


        Form::component('wText', 'site.ui.form.text', ['name', 'value' => null, 'attributes' => []]);
        Form::component('wPassword', 'site.ui.form.password', ['name', 'value' => null, 'attributes' => []]);
        Form::component('wEmail', 'site.ui.form.email', ['name', 'value' => null, 'attributes' => []]);
        Form::component('wTel', 'site.ui.form.tel', ['name', 'value' => null, 'attributes' => []]);
        Form::component('wCheckbox', 'site.ui.form.checkbox', ['name', 'value' => null, 'text' => null, 'checked' => null, 'attributes' => [], 'title' => null]);
        Form::component('wRadio', 'site.ui.form.radio', ['name', 'value' => null, 'text' => null, 'checked' => null, 'attributes' => []]);
        Form::component('wSelect', 'site.ui.form.select', ['name', 'value' => null, 'options' => null, 'default' => null, 'attributes' => []]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Ide Helper integration
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
