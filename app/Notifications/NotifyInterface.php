<?php namespace App\Notifications;

interface NotifyInterface
{

    public function run();

}