<?php namespace App\Notifications;

use App\Helpers\Email;
use App\Models\MailTemplate;
use App\Models\Notification;
use App\Models\User;

class Registration implements NotifyInterface
{

    /**
     * @var User
     */
    public $user;

    /**
     * @var string
     */
    public $newPassword;

	/**
	 * Create a new notification instance.
	 *
	 * @param User $user
	 * @param string $password
	 * @param $confirmToken
	 */
    public function __construct($user, $password)
    {
        $this->user = $user;
        $this->newPassword = $password;
    }

    public function run()
    {
        $this->toMail();
        $this->toDatabase();
    }

    protected function toMail()
    {
        Email::templateInView(MailTemplate::TYPE_REGISTRATION, $this->user->email, [
            'user_name' => $this->user->full_name,
            'email' => $this->user->email,
            'password' => $this->newPassword,
        ]);

        Email::templateInView(MailTemplate::TYPE_REGISTRATION_ADMIN, config('db.basic.admin_email'), [
            'user_name' => $this->user->full_name,
            'phone' => $this->user->phone,
            'email' => $this->user->email,
            'date' => $this->user->created_at,
        ]);
    }

    protected function toDatabase()
    {
        Notification::send(Notification::TYPE_REGISTRATION, [
            'email' => $this->user->email,
            'name' => $this->user->full_name,
            'id' => $this->user->id,
        ]);
    }

}
