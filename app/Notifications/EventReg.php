<?php namespace App\Notifications;

use Auth;
use App\Helpers\Email;
use App\Models\MailTemplate;
use App\Models\Notification;
use App\Models\Subscriber;

class EventReg implements NotifyInterface
{

    public $article;
    public $review;

    /**
     * Create a new notification instance.
     * @param $article
     * @param $review
     */
    public function __construct($article, $review)
    {
        $this->article = $article;
        $this->review = $review;
    }

    public function run()
    {
        $this->toMail();
    }

    protected function toMail()
    {
        Email::templateInView(MailTemplate::TYPE_EVENT_REG,  $this->review->email, [
            'name' => ' '.$this->article->name,
            'user_name' => $this->review->name,
            'email' => $this->review->email,
            'phone' => $this->review->phone,
            'date' => $this->review->created_at,
        ]);

        Email::templateInView(MailTemplate::TYPE_EVENT_REG_ADMIN,  config('db.basic.admin_email'), [
            'name' => ' '.$this->article->name,
            'user_name' => $this->review->name,
            'email' => $this->review->email,
            'phone' => $this->review->phone,
            'date' => $this->review->created_at,
        ]);
    }

}
