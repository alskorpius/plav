<?php namespace App\Notifications;

use Auth;
use App\Helpers\Email;
use App\Models\MailTemplate;
use App\Models\Notification;
use App\Models\Subscriber;

class Subscribe implements NotifyInterface
{

    /**
     * @var Subscriber
     */
    public $subscriber;
    public $data;
    public $news;

    /**
     * Create a new notification instance.
     *
     * @param Subscriber $subscriber
     * @param array $data
     * @param null $news
     */
    public function __construct($subscriber, $data = [], $news = null)
    {
        $this->subscriber = $subscriber;
        $this->data = $data;
        $this->news = $news;
    }

    public function run()
    {
        $this->toMail();
    }

    protected function toMail()
    {
        Email::templateInView(MailTemplate::TYPE_SUBSCRIBE, $this->subscriber->email, [
            'name' => $this->data['name'],
            'content' => $this->data['content'],
            'news' => $this->news,
        ], 'emails.news');
    }

}
