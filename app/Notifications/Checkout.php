<?php namespace App\Notifications;

use App\Models\Order\Order;
use App\Models\User;
use Auth;
use App\Helpers\Email;
use App\Models\MailTemplate;
use App\Models\Notification;

class Checkout implements NotifyInterface
{

    /**
     * @var Order
     */
    public $order;

    /**
     * @var User
     */
    public $user;

    /**
     * Create a new notification instance.
     *
     * @param Order $order
     * @param User $user
     */
    public function __construct($order, $user)
    {
        $this->order = $order;
        $this->user = $user;
    }

    public function run()
    {
        $this->toMail();
        $this->toDatabase();
    }

    protected function toMail()
    {
        // To customer
        Email::templateInView(MailTemplate::TYPE_NEW_ORDER,  $this->order->email, [
            'user_name' => isset($this->order->user_id) ? $this->order->user->full_name : $this->order->user_name,
            'id' => $this->order->id,
            'amount' => $this->order->total_price,
            'delivery_cost' => $this->order->delivery_cost,
            'items_table' => view('emails.particials.order-items', ['order' => $this->order])->render(),
            'orders_info' => view('emails.particials.order-information', ['order' => $this->order])->render(),
        ], 'emails.order', [
            'order' => $this->order,
        ]);
        // To administrator
        Email::templateInView(MailTemplate::TYPE_NEW_ORDER_ADMIN, config('db.basic.admin_email'), [
            'user_name' => isset($this->order->user_id) ? $this->order->user->full_name : $this->order->user_name,
            'id' => $this->order->id,
            'amount' => $this->order->total_price,
            'delivery_cost' => $this->order->delivery_cost,
            'items_table' => view('emails.particials.order-items', ['order' => $this->order])->render(),
            'orders_info' => view('emails.particials.order-information', ['order' => $this->order])->render(),
        ], 'emails.order', [
            'order' => $this->order,
        ]);
    }

    protected function toDatabase()
    {
        Notification::send(Notification::TYPE_NEW_ORDER, [
            'id' => $this->order->id,
            'amount' => $this->order->total_price,
            'user' => [
                'id' => isset($this->user->id) ? $this->user->id : null,
                'name' => $this->order->user_name,
                'email' => $this->order->email,
            ],
        ]);
    }

}
