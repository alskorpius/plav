<?php namespace App\Notifications;

use Auth;
use App\Helpers\Email;
use App\Models\MailTemplate;
use App\Models\Notification;
use App\Models\Subscriber;

class ArticleReview implements NotifyInterface
{

    public $article;
    public $review;

    /**
     * Create a new notification instance.
     * @param $article
     * @param $review
     */
    public function __construct($article, $review)
    {
        $this->article = $article;
        $this->review = $review;
    }

    public function run()
    {
        $this->toMail();
    }

    protected function toMail()
    {
        Email::templateInView(MailTemplate::TYPE_ARTICLE_REVIEW,  config('db.basic.admin_email'), [
            'name' => ' '.$this->article->name,
            'review' => $this->review->text,
            'user_name' => $this->review->user_name,
            'email' => $this->review->email,
            'date' => $this->review->created_at,
        ]);
    }

}
