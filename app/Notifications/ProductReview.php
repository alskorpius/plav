<?php namespace App\Notifications;

use Auth;
use App\Helpers\Email;
use App\Models\MailTemplate;
use App\Models\Notification;
use App\Models\Subscriber;

class ProductReview implements NotifyInterface
{

    public $product;
    public $review;

    /**
     * Create a new notification instance.
     * @param $product
     * @param $review
     */
    public function __construct($product, $review)
    {
        $this->product = $product;
        $this->review = $review;
    }

    public function run()
    {
        $this->toMail();
    }

    protected function toMail()
    {
        Email::templateInView(MailTemplate::TYPE_PRODUCT_REVIEW,  config('db.basic.admin_email'), [
            'name' => $this->product->name,
            'review' => isset($this->review->text) ? $this->review->text : $this->review->phone,
            'user_name' => $this->review->user_name,
            'email' => $this->review->email,
            'rating' => $this->review->mark,
            'date' => $this->review->created_at,
        ]);
    }

}
