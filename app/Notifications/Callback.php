<?php namespace App\Notifications;

use App\Helpers\Email;
use App\Models\MailTemplate;
use App\Models\Notification;
use App\Models\Callback AS Model;

class Callback implements NotifyInterface
{

    /**
     * @var Model
     */
    public $callback;

    /**
     * Create a new notification instance.
     *
     * @param Model $callback
     */
    public function __construct($callback)
    {
        $this->callback = $callback;
    }

    public function run()
    {
        $this->toMail();
        $this->toDatabase();
    }

    protected function toMail()
    {
        Email::templateInView(MailTemplate::TYPE_CALLBACK_ADMIN, null, [
            'name' => $this->callback->name,
            'phone' => $this->callback->phone,
        ]);
    }

    protected function toDatabase()
    {
        Notification::send(Notification::TYPE_CALLBACK, [
            'id' => $this->callback->id,
            'name' => $this->callback->name,
            'phone' => $this->callback->phone,
        ]);
    }

}
