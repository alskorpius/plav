<?php namespace App\Notifications;

use App\Helpers\Email;
use App\Models\MailTemplate;
use App\Models\User;

class ForgotPassword implements NotifyInterface
{

    /**
     * @var User
     */
    public $user;

    /**
     * @var string
     */
    public $newPassword;

    /**
     * Create a new notification instance.
     *
     * @param User $user
     * @param string $password
     */
    public function __construct($user, $password)
    {
        $this->user = $user;
        $this->newPassword = $password;
    }

    public function run()
    {
        $this->toMail();
    }

    protected function toMail()
    {
        Email::templateInView(MailTemplate::TYPE_FORGOT_PASSWORD, $this->user->email, [
            'user_name' => $this->user->full_name,
            'email' => $this->user->email,
            'password' => $this->newPassword,
        ]);

        Email::templateInView(MailTemplate::TYPE_FORGOT_PASSWORD_ADMIN, config('db.basic.admin_email'), [
            'email' => $this->user->email,
            'password' => $this->newPassword,
        ]);
    }

}
