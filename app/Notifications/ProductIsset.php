<?php namespace App\Notifications;

use Auth;
use App\Helpers\Email;
use App\Models\MailTemplate;
use App\Models\Notification;
use App\Models\Subscriber;

class ProductIsset implements NotifyInterface
{

    public $product;
    public $user;

    /**
     * Create a new notification instance.
     * @param $product
     * @param $user
     */
    public function __construct($product, $user)
    {
        $this->product = $product;
        $this->user = $user;
    }

    public function run()
    {
        $this->toMail();
    }

    protected function toMail()
    {
        Email::templateInView(MailTemplate::TYPE_PRODUCT_ISSET,  $this->user, [
            'name' => $this->product->name,
            'imgSmall' => '<img src="' . $_SERVER['HTTP_HOST'] . '/uploads/images/products/small/' . $this->product->image . '" title="' . $this->product->name . '" alt="' .$this->product->name . '">',
            'imgBig' => '<img src="' . $_SERVER['HTTP_HOST'] . '/uploads/images/products/big/' . $this->product->image . '" title="' . $this->product->name . '" alt="' .$this->product->name . '">',
            'imgIcon' => '<img src="' . $_SERVER['HTTP_HOST'] . '/uploads/images/products/icon/' . $this->product->image . '" title="' . $this->product->name . '" alt="' .$this->product->name . '">',
            'price' => $this->product->checked_price->price,
        ]);
    }

}
