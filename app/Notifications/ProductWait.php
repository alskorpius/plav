<?php namespace App\Notifications;

use Auth;
use App\Helpers\Email;
use App\Models\MailTemplate;
use App\Models\Notification;
use App\Models\Subscriber;

class ProductWait implements NotifyInterface
{

    public $product;
    public $user;

    /**
     * Create a new notification instance.
     * @param $product
     * @param $user
     */
    public function __construct($product, $user)
    {
        $this->product = $product;
        $this->user = $user;
    }

    public function run()
    {
        $this->toMail();
    }

    protected function toMail()
    {
        Email::templateInView(MailTemplate::TYPE_PRODUCT_WAIT, $this->user->email, [
            'name' => $this->product->name,
            'img' => '<img src="' . $_SERVER['HTTP_HOST'] . '/uploads/images/products/icon/' . $this->product->image . '" alt="' . $this->product->name .'">',
            'price' => $this->product->checked_price->price,
        ]);

        Email::templateInView(MailTemplate::TYPE_PRODUCT_WAIT_ADMIN, config('db.basic.admin_email'), [
            'name' => $this->product->name,
            'img' => '<img src="' . $_SERVER['HTTP_HOST'] . '/uploads/images/products/icon/' . $this->product->image . '" alt="' . $this->product->name .'">',
            'price' => $this->product->checked_price->price,
        ]);
    }

}
