<?php namespace App\Notifications;

use Auth;
use App\Helpers\Email;
use App\Models\MailTemplate;
use App\Models\Notification;
use App\Models\Subscriber;

class UserSubscribe implements NotifyInterface
{

    /**
     * @var Subscriber
     */
    public $email;

    /**
     * Create a new notification instance.
     * @param $email
     */
    public function __construct($email)
    {
        $this->email = $email;
    }

    public function run()
    {
        $this->toMail();
    }

    protected function toMail()
    {
        Email::templateInView(MailTemplate::USER_SUBSCRIBE, $this->email, []);
    }

}
