<?php namespace App\Traits;

use App\Exceptions\ConfigurationMissMatchException;
use App\Helpers\Files;
use Carbon\Carbon;
use File;
use Illuminate\Support\Str;
use Request;
use Illuminate\Http\UploadedFile;
use Image;

/**
 * Class ModelImages
 * @package App\Traits
 */
trait ModelImages
{

    /**
     * Хранилище данных в публичной папке
     *
     * @var string
     */
    private $folder = 'uploads/images';

    /**
     * Заглушка
     *
     * @return string
     */
    public function noImage()
    {
        return '/no-avatar.png';
    }

    /**
     * Достаем поле для работы с БД
     *
     * @return string
     */
    protected function imageField()
    {
        return 'image';
    }

    /**
     * Достаем поле для работы с БД
     *
     * @return string
     */
    public function getImageField()
    {
        return $this->imageField();
    }

    /**
     * Имя поля из формы
     *
     * @return string
     */
    protected function imageInputName()
    {
        return 'image';
    }

    /**
     * Основная папку для сохранения изображения
     *
     * @return string
     */
    protected function folder()
    {
        return $this->getFolderNameToUpload(null);
    }

    /**
     * Основная папку для сохранения изображения
     *
     * @return string
     */
    public function getFolder()
    {
        return $this->folder();
    }

    /**
     * Настройки для загрузки изображений
     *
     * @return array
     */
    protected function imagesFolders()
    {
        return [];
    }

    /**
     * Настройки для загрузки изображений
     *
     * @return array
     */
    public function getImagesFolders()
    {
        return $this->imagesFolders();
    }

    /**
     * Удаляем загруженное изображение из всех папок
     *
     * @param null $field
     * @param null $folder
     * @return $this
     */
    public function deleteImage($field = null, $folder = null)
    {
        $field = $field ?: $this->imageField();
        if ($this->{$field} === null) {
            return $this;
        }
        $folder = $folder ?: $this->folder();
        if (array_key_exists($folder, $this->imagesFolders())) {
            Files::deleteImage($folder, array_get($this->imagesFolders(), $folder), $this->{$field});
        }
        $this->{$field} = null;
        return $this;
    }

    /**
     * Загружаем изображение
     *
     * @param null|string $mainFolder
     * @param null|string $imageInputName
     * @param null|string $imageField
     * @return $this
     * @throws ConfigurationMissMatchException
     */
    public function uploadImage($mainFolder = null, $imageInputName = null, $imageField = null)
    {
        if (empty($this->imagesFolders())) {
            throw new ConfigurationMissMatchException('Пожалуйста укажите настройки изображений в методе ' . self::class . '::imagesFolders');
        }
        $imageInputName = $imageInputName ?: $this->imageInputName();
        $imageField = $imageField ?: $this->imageField();
        $mainFolder = $this->getFolderNameToUpload($mainFolder);
        $imageName = null;
        if (request()->hasFile($imageInputName)) {
            $imageName = Files::uploadImage(request(), $this->imagesFolders(), $mainFolder, $imageInputName);
            if ($imageName !== null) {
                $this->{$imageField} = $imageName;
            }
        }
        return $this;
    }

    /**
     * Загружаем изображение по объекту файла
     *
     * @param UploadedFile $file
     * @param null $mainFolder
     * @param null $imageInputName
     * @param null $imageField
     * @return $this
     * @throws ConfigurationMissMatchException
     */
    public function uploadImageWithObject(UploadedFile $file, $mainFolder = null, $imageInputName = null, $imageField = null, $filename = null)
    {
        if (empty($this->imagesFolders())) {
            throw new ConfigurationMissMatchException('Пожалуйста укажите настройки изображений в методе ' . self::class . '::imagesFolders');
        }
        if ($file->isFile() === false) {
            return $this;
        }
        $imageInputName = $imageInputName ?: $this->imageInputName();
        $imageField = $imageField ?: $this->imageField();
        $mainFolder = $this->getFolderNameToUpload($mainFolder);
        $imageName = Files::uploadImageWithObject($file, $this->imagesFolders(), $mainFolder, $imageInputName, $filename);
        if ($imageName !== null) {
            $this->{$imageField} = $imageName;
        }
        return $this;
    }

    /**
     * Достаем папку для загрузки
     *
     * @param string $folder Папка в которую хотим загрузить изображение
     * @return mixed
     * @throws ConfigurationMissMatchException
     */
    private function getFolderNameToUpload($folder)
    {
        if ($folder !== null) {
            if (is_string($folder) === false || array_key_exists($folder, $this->imagesFolders()) === false) {
                throw new ConfigurationMissMatchException('Пожалуйста укажите корректную папку для загрузки изображения в методе ' . self::class . '::uploadImage');
            }
            return $folder;
        }
        $acceptedFolders = array_keys($this->imagesFolders());
        return array_shift($acceptedFolders);
    }

    /**
     * Путь к изображению со слеша
     *
     * @param $folder [big, small, original, etc.]
     * @param null|string $mainFolder [news, users, etc.]
     * @param null|string $imageField
     * @return string
     * @throws ConfigurationMissMatchException
     */
    public function imageUrl($folder, $mainFolder = null, $imageField = null)
    {
        $mainFolder = $mainFolder ?: $this->folder();
        if (is_array($folder) == false) {
            $folder = explode('.', $folder);
        }
        if (!$folder) {
            throw new ConfigurationMissMatchException('Укажите папку ' . self::class . '::url!');
        }
        array_unshift($folder, $mainFolder);
        if (array_get($this->imagesFolders(), implode('.', $folder), false) === false) {
            throw new ConfigurationMissMatchException('Папки ' . implode('.', $folder) . ' не существует в настройках ' . self::class . '::url!');
        }
        $imageField = $imageField ?: $this->imageField();
        array_unshift($folder, trim($this->folder, '/'));
        return '/' . implode('/', $folder) . '/' . $this->{$imageField};
    }

    /**
     * Версия файла
     *
     * @param $folder [big, small, original, etc.]
     * @param null|string $mainFolder [news, users, etc.]
     * @param null|string $imageField
     * @return bool|int
     */
    public function getVersion($folder, $mainFolder = null, $imageField = null)
    {
        return filemtime($this->imagePath($folder, $mainFolder, $imageField));
    }

    /**
     * Путь к файлу
     *
     * @param $folder [big, small, original, etc.]
     * @param null|string $mainFolder [news, users, etc.]
     * @param null|string $imageField
     * @return string
     */
    public function imagePath($folder, $mainFolder = null, $imageField = null)
    {
        return public_path($this->imageUrl($folder, $mainFolder, $imageField));
    }

    /**
     * Существует ли изображение конкретного размера
     *
     * @param $folder [big, small, original, etc.]
     * @param null|string $mainFolder [news, users, etc.]
     * @param null|string $imageField
     * @return bool
     */
    public function imageExists($folder, $mainFolder = null, $imageField = null)
    {
        $imageField = $imageField ?: $this->imageField();
        return $this->{$imageField} && File::isFile(public_path($this->imageUrl($folder, $mainFolder, $imageField)));
    }

    /**
     * Возвращаем путь к изображению для отображения
     *
     * @param $folder
     * @param null $mainFolder
     * @param boolean $version
     * @param boolean $imageField
     * @return null|string
     */
    public function showImage($folder, $mainFolder = null, $version = false, $imageField = null)
    {
        $imageField = $imageField ?: $this->imageField();
        return $this->getImage($folder, $mainFolder, $version, $imageField) ?: $this->noImage();
    }
    #todo Кропает :=)

    /**
     * Кропаем изображение в нужную папку с original
     * @param $folder
     * @param array $settings
     * @param string $source
     * @return string
     */
    public function cropImageIfNotExist($folder, array $settings, $source = 'original')
    {
        $fileSourceRealPath = public_path('uploads/images/' . $this->folder() . '/' . $source . '/' . $this->image);
        try {
            $file = new UploadedFile($fileSourceRealPath, $this->image, null, getimagesize($fileSourceRealPath), 0, true);
            $image = $this->uploadImageWithObject($file, null, null, null, $this->image);
            return $image->getImage($folder);
        } catch (\Exception $e) {
            @unlink($fileSourceRealPath);
        }
        return null;
    }

    /**
     * Кроп placeholder'a
     * @param $folder
     * @param array $settings
     * @return string
     */

    public function cropPlaceholder($folder, array $settings)
    {
        $checkFile = public_path('uploads/images/' . $this->folder() . '/' . $folder . '/' . 'placeholder.jpg');
        if (file_exists($checkFile)) {
            return '/public/uploads/images/' . $this->folder() . '/' . $folder . '/' . 'placeholder.jpg';
        }
        $fileSourceRealPath = public_path('images/placeholder.jpg');
        $this->image = 'placeholder.jpg';
        try {
            $file = new UploadedFile($fileSourceRealPath, $this->image, null, getimagesize($fileSourceRealPath), 0, true);
            $image = $this->uploadImageWithObject($file, null, null, null, $this->image);
            return $image->getImage($folder);
        } catch (\Exception $e) {
            @unlink($fileSourceRealPath);
        }
        return public_path('images/placeholder.jpg');
    }

    /**
     * Возвращаем путь к изображению или пустую строку, если его нет
     *
     * @param $folder
     * @param null $mainFolder
     * @param boolean $version
     * @param null|string $imageField
     * @return string
     */
    public function getImage($folder, $mainFolder = null, $version = false, $imageField = null, $source = 'original')
    {
        $imageField = $imageField ?: $this->imageField();
        if ($this->imageExists($folder, $mainFolder, $imageField)) {
            return $this->imageUrl($folder, $mainFolder, $imageField) . ($version ? '?v=' . $this->getVersion($folder, $mainFolder, $imageField) : null);
        } elseif (!$this->imageExists($folder, $mainFolder, $imageField) && $this->imageExists('original', $mainFolder, $imageField)) {
            return $this->cropImageIfNotExist($folder, $this->imagesFolders(), $source);
        } elseif (!$this->imageExists('original', $mainFolder, $imageField) || $this->image == null) {
            return $this->cropPlaceholder($folder, $this->imagesFolders());
        }
//        } elseif (!$this->imageExists('original', $mainFolder, $imageField && isset($this->image))){
//            return $this->cropPlaceholder($folder, $this->imagesFolders());
//        }
        return null;
    }

    /**
     * Достаем вписок ссылок на все возможные изображения
     *
     * @param null $mainFolder
     * @return array
     */
    public function getAllImagesUrls($mainFolder = null)
    {
        $images = [];
        foreach (array_get($this->imagesFolders(), $mainFolder ?: $this->folder(), []) AS $imageFolder => $customSettings) {
            $currentImage = $this->imageUrl($imageFolder, $mainFolder);
            $images[$imageFolder] = [
                'url' => $currentImage,
                'info' => $this->getImageSettings($imageFolder, $mainFolder),
            ];
        }
        return $images;
    }

    /**
     * Достаем подробную информацию о файле
     *
     * @param $folder
     * @param null|string $mainFolder
     * @param null|string $imageField
     * @return array
     */
    public function getImageSettings($folder, $mainFolder = null, $imageField = null)
    {
        if ($this->imageExists($folder, $mainFolder, $imageField) === false) {
            return [];
        }
        $pathToImage = $this->imagePath($folder, $mainFolder, $imageField);
        $size = File::size($pathToImage);
        $prettySize = (float)number_format($size / (1024 * 1024), 2, '.', '');
        return [
            'size' => $size,
            'pretty_size' => $prettySize . 'Mb',
            'last_modified' => date('Y-m-d H:i:s', File::lastModified($pathToImage)),
            'name' => File::name($pathToImage),
            'type' => File::type($pathToImage),
            'mime' => File::mimeType($pathToImage),
            'ext' => File::extension($pathToImage),
            'basename' => File::basename($pathToImage),
            'dirname' => File::dirname($pathToImage),
        ];
    }

}
