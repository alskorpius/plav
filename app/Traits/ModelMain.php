<?php namespace App\Traits;

use App\Models\Log;
use App\Models\Catalog\Product;
use Request;

/**
 * Trait ModelMain
 *
 * Методы для работы с основной таблицей, имеющей таблицу в БД с переводами
 *
 * @package App\Traits
 * @property \Illuminate\Database\Eloquent\Collection $data
 * @property $this $current
 * @property object|mixed|null $dataFor
 */
trait ModelMain
{
    public function logAlias()
    {
        return null;
    }

    public function relatedModelName()
    {
        $currentClass = static::class;
        $translatesClass = $currentClass . 'Translates';
        return $translatesClass;
    }

    public function getRelatedTableName()
    {
        $relatedModelName = $this->relatedModelName();
        $relatedModel = new $relatedModelName;
        return $relatedModel->getTable();
    }

    protected function _rules()
    {
        return [];
    }

    public function rules()
    {
        $rules = $this->_rules();
        $modelName = $this->relatedModelName();
        $model = new $modelName;
        $translatesRules = method_exists($model, 'rules') ? $model->rules() : [];
        foreach ($translatesRules AS $key => $rule) {
            foreach (config('languages') AS $language) {
                $rules[$language['slug'] . '.' . $key] = $rule;
            }
        }
        return $rules;
    }

    public function data()
    {
        return $this->hasMany($this->relatedModelName(), 'row_id', 'id');
    }

    public function current()
    {
        return $this
            ->hasOne($this->relatedModelName(), 'row_id', 'id')
            ->where('language', '=', app()->getLocale());
    }

    public function dataFor($lang, $default = null)
    {
        $data = $this->data;
        foreach ($data as $element) {
            if ($element->language == $lang) {
                return $element;
            }
        }
        return $default;
    }

    /**
     * @param Request|\Illuminate\Http\Request|array $request
     * @return bool
     */
    public function createRow($request)
    {
        if (is_array($request)) {
            $this->fill($request);
        } else {
            $this->fill($request->input() ?: []);
        }
        if (method_exists($this, 'uploadImage')) {
            $this->uploadImage();
        }
//        dump($request);
//        dd($this);
        if ($this->save() !== true) {
            return false;
        }
        $modelName = $this->relatedModelName();
        foreach (config('languages') AS $language) {
            $translate = new $modelName();
            $translate->fill(is_array($request) ? array_get($request, $language['slug'], []) : $request->input($language['slug']) ?: []);
            $translate->row_id = $this->id;
            $translate->language = $language['slug'];
            $translate->save();
        }
        if ($this->logAlias() !== null) {
            Log::add($this->logAlias(), $this->id);
        }
        return true;
    }

    /**
     * @param Request|\Illuminate\Http\Request|array $request
     * @return Product
     */
    public function createRowImport($request)
    {
        if (is_array($request)) {
            $this->fill($request);
        } else {
            $this->fill($request->input() ?: []);
        }
        if (method_exists($this, 'uploadImage')) {
            $this->uploadImage();
        }
        if ($this->save() !== true) {
            return false;
        }
        $modelName = $this->relatedModelName();
        foreach (config('languages') AS $language) {
            $translate = new $modelName();
            $translate->fill(is_array($request) ? array_get($request, $language['slug'], []) : $request->input($language['slug']) ?: []);
            $translate->row_id = $this->id;
            $translate->language = $language['slug'];
            $translate->save();
        }
        if ($this->logAlias() !== null) {
            Log::add($this->logAlias(), $this->id);
        }
        return $this;
    }

    /**
     * @param Request|\Illuminate\Http\Request $request
     * @return bool
     */
    public function updateRow($request)
    {
        $this->fill($request->input());
        if (method_exists($this, 'uploadImage')) {
            $this->uploadImage();
        }
        $changes = [];
        if ($this->isDirty()) {
            $changes['main'] = array_keys($this->getDirty());
        }
        if ($this->save() !== true) {
            return false;
        }
        $modelName = $this->relatedModelName();
        foreach (config('languages') AS $language) {
            $translate = $this->dataFor($language['slug']);
            if (!$translate) {
                $translate = new $modelName();
            }
            $translate->fill($request->input($language['slug'], []));
            $translate->row_id = $this->id;
            $translate->language = $language['slug'];
            if ($translate->isDirty()) {
                $changes[$translate->language] = array_keys($translate->getDirty());
            }
            $translate->save();
        }
        if ($this->logAlias() !== null && count($changes) > 0) {
            Log::add($this->logAlias(), $this->id, Log::UPDATED, $changes);
        }
        return true;
    }

    public function deleteRow()
    {
        if (method_exists($this, 'deleteImage')) {
            $this->deleteImage();
        }
        if ($this->delete()) {
            Log::add($this->logAlias(), $this->id, Log::DELETED);
            return true;
        }
        return false;
    }

    public function meta()
    {
        return [
            'name' => ($this->current && $this->current->exists && isset($this->current->name)) ? $this->current->name : null,
            'h1' => ($this->current && $this->current->exists && isset($this->current->h1)) ? $this->current->h1 : null,
            'title' => ($this->current && $this->current->exists && isset($this->current->title)) ? $this->current->title : null,
            'keywords' => ($this->current && $this->current->exists && isset($this->current->keywords)) ? $this->current->keywords : null,
            'description' => ($this->current && $this->current->exists && isset($this->current->description)) ? $this->current->description : null,
            'content' => ($this->current && $this->current->exists && isset($this->current->content)) ? $this->current->content : null,
        ];
    }

}
